## Preface
This is a pre-release of EOS (Environment for Optimization and Simulation) under GPL v3. It is functional, however the documentation and some fine tuning are incomplete. This repository shall serve as the development hub until a formal release.

## Authors
- Luiz da Rocha-Schmidt (darocha@tum.de)
- Markus Schatz (markus.schatz@tum.de)

## Contributors
- Enzo Maier
- Sebastian Flach
- Marc Köhler
- Sebastian Rödl
- Paul Müller
- Johannes Achleitner
- Pranav Nagarajan
- Andreas Hermanutz

## Description
This Python framework can be used to perform any sort of simulation, from a single FE simulation to a paramteric design study up to full design optimization. Eos handles the management of input files, creation of temporary work folders, gathering of results and creation of reports in Word and/or Excel files. Models can be defined arbitrarily, from simple analytical models to complex jobs in various Finite Element Analysis Software (currently Ansys, Abaqus, Nastran and LS-Dyna). Adding interfaces to other software is easy. EOS is intended for Windows and Linux workstations or cluster environments (currently: [SLRUM](http://slurm.schedmd.com/)).

## Requirements:
Currently development runs on WinPython 3.6 32bit. It should still be compatible with 2.X versions of Python, but this is not validated anymore.

Required Python Modules (many of these are optional, you can install them when needed):
- python-docx (For Word reporting)
- XlsxWriter (For Excel reporting)
- ets 4.4.3
- traits + traitsui 4.5.0
- mayavi 4.3.1
- ToPy 0.1.1
- pyNastran 0.6.1
- dill 0.2.1.zip
- pyOpt 1.2.0
- apptools 4.3.0
- pyface 4.5.0
- PyCrypto
- Paramiko

## How to run

- copy the eos.cfg.sample in the root directory to eos.cfg and modify it as needed.
- look at the included samples in the projects-folder. "SimpleAnsysModel" is a good starting point. Models go in the "models" folder, the study definition goes to the "studies" folder.
- execute the run.py in the project's subfolder

## Implemented features

- Simulation classes to launch the following Tools:
    - ANSYS
    - LS-Dyna
    - Matlab
    - Abaqus
    - Nastran
- Simple evaluation:
    - Description: Performs a simple evaluation of [EOS-Model-Object].ComputeResp(x)
	- Code: [EOS-Simulation-Object].Eval(x)
        1. Param: x ... Parameter vector
    - Contact person: Luiz da Rocha and Markus Schatz
- Sensitivity study:
    - Description: Performs a sensitivity study at x. If no sensitivities via [EOS-Model-Object].ComputeSens(x) given finite differences will be used.
	- Code: [EOS-Simulation-Object].SensitivityStudy(x)
        1. Param: x ... Parameter vector
    - Contact person: Luiz da Rocha and Markus Schatz
- Design optimization:
    - Description: Can be used to perform optimizations based on zeroth, first and second order algorithms taken from pyOpt
	- Code: [EOS-Simulation-Object].Optimize(x0,xL,xU,Alg='SLSQP',Norm=True,Options=dict(),**kwargs)
        1. Param: x0 ... Start vector
        2. Param: xL ... Lower bound
        3. Param: xU ... Upper bound
        4. Param: Options ... Dictionary with options
            - Options['ng'] ... Number of constraints g in optimization problem
        5. Param: Alg ... Algorithm choice: ['SLSQP'], 'NSGA2', 'ALPSO', ... see pyOpt.org
        6. Param: Norm ... Normalization: [True], False
        7. Param: **kwargs ... Passes options to PyOpt algorithms, see pyOpt.org
    - Contact person: Markus Schatz
- Global search for starting point:
    - Description: Performs a global search for nGS optimal starting points analogously to a Design of Experiments
	- Code: [EOS-Simulation-Object].GlobalSearchForDesOpt( xL, xU, nDoE = 30, nGS = 3)
        1. Param: xL ... Lower bound
        2. Param: xU ... Upper bound
        3. Param: nDoE ... Number of sample points for global search
        4. Param: nGS ... Number of starting points taken from nDoE sample points, thus nGS < nDoE
    - Contact person: Markus Schatz
- Perform a design of experiments:
    - Description: Performs a design of experiments, which is for instance needed for surrogate modelling
	- Code: [EOS-Simulation-Object].PerformDoE(NumberSamplePoints,RSAlower,RSAupper,SamplingMethod='LHS',**kwargs)
        1. Param: NumberSamplePoints ... Number of sample points in total
        2. Param: RSAlower ... Lower bound for DoE
        3. Param: RSAupper ... Upper bound for DoE
        4. Param: SamplingMethod ... DoE method: ['LHS'], 'LHS+Corner', 
        5. Param: **kwargs ... Passes options DoE method
    - Contact person: Markus Schatz
- Get a surrogate model (RSA is Response Surface Approximation):
    - Description: Computes a surrogate model based on sample points and information from a prior Design of Experiments
	- Code: [EOS-Simulation-Object].GetRSAmodel(xDoE,fDoE=None,RSAMethod='PolyReg',**kwargs)
        1. Param: xDoE ... DoE sample points
        2. Param: fDoE ... DoE sample information, thus either responses or optimization responses such as objective
        4. Param: RSAMethod ... RSA method: ['PolyReg'], 'Kriging', 
        5. Param: **kwargs ... Passes options RSA method
    - Contact person: Markus Schatz
- Make a 2-D plot:
    - Description: Generates a 2-D plot based on passed data points
	- Code: [EOS-Simulation-Object].LinePlot(x,y,xLabel='x',yLabel='y',fLegend=None,FigTitle=None,ShowFig=False,FigFileName='Figure.png',Plotstyle = 'b-')
        1. Param: 
    - Contact person: Markus Schatz
- Plot results on a shell mesh:
    - Description: Plot results on a shell mesh by taking advantage of the mayavi module's capabilities
	- Code: [EOS-Simulation-Object].PlotShellMeshResult(NodeArray, ElementArray, Results=None, Camera = dict(), FigName='ShellMeshPlot.png')
        1. Param: NodeArray ... Array of nodes, i.e. np.array([ [NodeID | x | y | z], [NodeID | x | y | z], ...])
        2. Param: ElementArray ... Array of elements, i.e. np.array([ [ElID | N1ID | N2ID etc.], [ElID | N1ID | N2ID etc.], ...])
        3. Param: Camera ... Defines the camera position in plot [IsoparametricView] <- Is default
        4. Param: Figname ... Defines the name of the saved picture ['ShellMeshPlot.png']
    - Contact person: Markus Schatz

## Planned features:

- [ ] Submit sub-jobs to a queuing system like SLURM or SGE, as used on the LLB cluster (partially implemented)
- [ ] Implement further Simulation Software
- [ ] Unify handling of models in classes
- [ ] simplify starting, independent of where it is run (locally (windows/linux) or on the cluster)