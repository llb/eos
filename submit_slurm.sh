# this file can be used to submit EOS jobs on clusters with the SLURM queuing system.
# usage: ./submit_slurm.sh ProjectName 
# the ProjectName is the name of the folder below /projects/

sbatch --export=project=$1 rum_EOS.sh