## Contribution
You are very welcome to contribute to the development of EOS in several ways:
### Raising issues
Whenever you find a problem with eos that you think should be fixed, please feel free to add an [Issue](https://gitlab.com/llb/eos/issues).
### Code Contribution
If you want to add a new feature or improve existing code, please first create a [Branch](https://gitlab.com/llb/eos/branches/new) and check it out on your computer. Within this branch, implement and test your improvements. Once they are completed, please create a [Merge Request](https://gitlab.com/llb/eos/merge_requests). We will review your code and merge it to the master branch.

[This video](https://www.youtube.com/watch?v=raXvuwet78M) illustrates the process quite nicely. Note that EOS does not (yet) have CI implemented, so you can ignore that part of the video.