#!/bin/bash

# This script can be used to run EOS jobs on Linux Systems.
# It is intended for use with the SLRUM queuing system via the following lines.
# Modify these as required.

#SBATCH --get-user-env
#SBATCH --output=../../results/eos.%j.out
#SBATCH --error=../../results/eos.%j.err
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4
#SBATCH --mincpus=4
###SBATCH --nodelist=node35

# Parameters:
# project="SimpleAnsysModel"      # this defines the project name, i.e. the folder name below /projects/ .This parameter can be set here or be passed externally from submit_slurm.sh
jobid=$SLURM_JOB_ID               # use the SLRUM job id
jobname=$project                  # define the job name
jobdir=/home/$USER/jobs/$jobname  # the source directory of this job
tmpdir=/tmp/$USER.$jobid         # the working directory. This will be deleted at the end
resdir=/home/$USER/results/$jobname.$jobid  # the result folder. Simulation results will be moved there.

# make sure $project is set
if [ "$project" = "" ]; then
    echo "please set the \$project variable"
	exit
fi

# create directories and copy files
mkdir $tmpdir
mkdir $resdir
cp -r $jobdir/* $tmpdir
cd $tmpdir/projects/$project

# prepare the environment. comment out the "module" command if you don't use the module system
. /etc/profile
module load pyopt

# execute EOS
python run.py > EOS.out

# move all files to the result directory and delete the temp files
cd $tmpdir
mv * $resdir
cd /tmp
rm -r $tmpdir
