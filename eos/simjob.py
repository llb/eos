# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import getpass
import os
import binascii
from datetime import datetime
import pickle
import shutil
import logging
import platform
import sys
import subprocess
import signal
import copy as cp
import glob
import time
import numpy as np
from eos.NastranTools import NastranClass
import math
import re
import eos
import threading
import collections
from subprocess import DEVNULL, STDOUT, check_call

# for Python 2/3 compatibility
from six import string_types
# ConfigParser has beed renamed to configparser in Python 3 -> make it compatible
if sys.version_info[0] < 3:
    import ConfigParser as configparser
else:
    import configparser


class SimJob:
    def __init__(self, sourcefolder, jobname, inputfile, projectfolder=None, workfolderType='Std', modulesystem=True, keepresults=True, configfile='eos.cfg'):
        """
        Defines a Simulation Job
        keepresults=True           (bool) keep the temporary results? This can be used to trigger the method DeleteWorkFolder
                                    This is not done automatically!
        """
        logging.debug("initializing SimJob Class")

        self.keepresults = keepresults # set, whether simulation results (in the work folder) are to be kept or not
        self.localorcluster = 'local'  # preparation for future implementation of sbatch job spawning
        self.modulesystem = modulesystem  # if using linux: is the module system installed? True/False

        if projectfolder:
            self.projectfolder = projectfolder
        else:
            #self.projectfolder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
            self.projectfolder = os.getcwd()

        # get the path of the eos folder
        self.EosPath = os.path.dirname(os.path.dirname(eos.__file__))

        # read configuration file
        self.config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
        config_path = os.path.join(self.EosPath, configfile)
        if os.path.isfile(config_path):
            logging.info('Reading config file "%s"' % config_path)
            self.config.read(config_path)
        else:
            msg = 'EOS config file "%s" could not be found. Please copy "eos.cfg.sample" and modify as needed' % config_path
            logging.fatal(msg)
            sys.exit(msg)

        # read the windows ansys path from the config file and check if it contains any old formatting (BasicInterpolation instead of new ExtendedInterpolation)
        ansyspath = self.config.get('Ansys', 'ansys_command_win')
        test = re.compile(r'%\(.*\)s')
        if test.search(ansyspath):
            text = 'Your eos.cfg file still uses the old style of String Interpolation. Please update it according to eos.cfg.sample.'
            logging.warning(text)
            print(text)
            self.config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
            self.config.read(config_path)

        try:
            config_user = self.config.get('General', 'username')
        except:
            logging.info("username not defined in config file, using default")

        if not config_user or config_user == 'None':
            # if the username is not defined in the config file, set it to the system user name
            self.config.set('General', 'username', getpass.getuser())

        if platform.system() == 'Windows':
            # self.newLine = '\r\n'
            self.newLine = '\n'
            self.platform = 'windows'
            self.tempfolder = self.config.get('General', 'temp_win')
            logging.debug('Running on Windows, setting tempfolder to: %s' % self.tempfolder)
        elif platform.system() == 'Linux':
            self.platform = 'linux'
            self.newLine = '\n'
            if self.localorcluster == 'local':
                # get the base temp folder from the config file
                self.temp_base = self.config.get('General', 'temp_linux')
                # we want to run on the local computer, now we determine where to put the tmp-folder
                if os.environ.get('SLURM_JOBID') != '' and os.environ.get('SLURM_JOBID') is not None:
                    # if we are on a cluster node within a slurm job, we use the slurm job folder
                    self.tempfolder = os.path.join(self.temp_base+'.'+os.environ.get('SLURM_JOBID'), 'tmp')
                    logging.info('running within SLURM, setting tempfolder to %s' % self.tempfolder)
                else:
                    # if we are running outside of slurm, we use /tmp/$username/
                    self.tempfolder = self.temp_base
                    logging.info('running on linux outside of SLRUM , setting tempfolder to %s' % self.tempfolder)
            elif self.localorcluster == 'cluster':
                # we want to spawn a job using the queuing system - not yet implemented.
                self.tempfolder = os.path.join(self.projectfolder, "clusterjobs")
                logging.info('preparing job spawning on the cluster in SLURM, setting tempfolder to %s' % self.tempfolder)
            else:
                logging.fatal('local or cluster not set, could not determine tempfolder')
                sys.exit()
        else:
            logging.fatal('Platform could not be determined')
            sys.exit()

        # self.jobid = binascii.hexlify(os.urandom(32))
        self.timestring = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        self.jobname = jobname
        self.inputfile = inputfile

        try:
            self.orgName = self.config.get('General', 'org_name')
        except:
            self.orgName = 'parameter "org_name" not set in "eos.cfg" config file'

        try:
            self.orgShortName = self.config.get('General', 'org_short_name')
        except:
            self.orgShortName = 'parameter "org_short_name" not set in "eos.cfg" config file'

        self.sourcefolder = os.path.join(self.projectfolder, sourcefolder)

        if workfolderType == 'Std':
            # we can use a workfolder of the type "jobname_timestring"
            self.workfolder = os.path.join(self.tempfolder, jobname + '_' + self.timestring)
        elif workfolderType == 'Short':
            # we want to use a short workfolder (e.g. for Nastran)
            def getNextWorkFolder(folder):
                # a small function that returns the lowest available integer folder name (1, 2, 3, ...) for a given folder.
                highest_num = 0
                TMPFolder = None
                Iter = 0
                while TMPFolder==None and Iter < 1e5:
                    Iter += 1
                    if not os.path.isdir(os.path.join(folder, str(highest_num))):
                        TMPFolder = os.path.join(folder, str(highest_num))
                    else:
                        highest_num += 1
                return TMPFolder
            if not os.path.isdir(self.tempfolder):
                self.workfolder = os.path.join(self.tempfolder, '0')
            else:
                self.workfolder = getNextWorkFolder(self.tempfolder)

        if 'projects' in self.sourcefolder:
            # if the sourcefolder contains "projects" we are in the main Eos folder, so we put the results to the separate "results" folder.
            self.resultfolder = os.path.join(self.EosPath, 'results', self.jobname + '.' + self.timestring)
        else:
            # in this case, we are in a separate eos working folder, so we can fetch the simulation results to a direct subfolder.
            self.resultfolder = os.path.join(self.projectfolder, 'results', self.jobname + '.' + self.timestring)

        self.PrepareRun()  # copy files to working folder

        return

    def __str__(self):
        return self.GetInfo()

    def PrepareRun(self):
        """
        Prepare the simulation run: Copy files to temp-folder
        """
        # delete work dir if it exists
        logging.debug('checking if workfolder exists')
        if os.path.isdir(self.workfolder):
            logging.info('workfolder %s already exists, trying to delete' % self.workfolder)
            try:
                shutil.rmtree(self.workfolder)
            except:
                logging.fatal('could not delete work folder %s' % self.workfolder)
                sys.exit('Work folder "%s" already exists, could not be deleted. exiting.' % self.workfolder)
            else:
                logging.info('workfolder %s deleted' % self.workfolder)
        # copy source files to work folder (also creates work folder)
        shutil.copytree(self.sourcefolder, self.workfolder)

    def FetchResults(self, targetfolder='', deletework=True):
        """
        Moves Simulation Results from temp folder to the project results folder
        """

        if not targetfolder:
            targetfolder = self.resultfolder

        # copy back the results from the working folder to the result folder, delete the work
        # folder only if copying back was successful
        try:
            shutil.copytree(self.workfolder, self.resultfolder)
        except:
            logging.warn('error while copying results from workfolder "%s" to resultfolder "%s"' % (self.workfolder, self.tempfolder))
        else:
            logging.info('copy back to "%s" successful - trying to delete temporary work folder' % (self.resultfolder))

            if deletework:
                self.DeleteWorkFolder()
            else:
                logging.info('Not deleting work folder "%s"' % self.workfolder)
        return

    def GetClusterModules(self, program=''):
        """
            A function that returns the available versions of a given module on the LLB Cluster as found via the "module avail" command

            parameters:
                program - a string of the desired program, e.g. "ansys"
            returns:
                a list of the available module versions, e.g. ["v145", "v150"]

            If no program is given or no according module is found, an empty list is returned
        """
        import re

        modules = []
        if not program:
            logging.warn('"program" not set for method GetClusterModules()')
            return modules

        if self.platform != 'linux':
            logging.warn("Method GetClusterModules() only implemented for linux")
            return modules

        # call the "module avail" command to get the available modules
        call = '. /etc/profile; module avail'
        process = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        moduletext = process.communicate()[1]

        # build the regular expression that checks for the module versions. Helpful tool: https://regex101.com
        regex = program + "\/(\S[^\n$^\ $^\($]*)"

        # each regex match in the moduletext string is a version of the module. They are put in the list "modules"
        for m in re.finditer(regex, moduletext):
            modules.append(m.group(1))

        # sort the list from lowest to highest
        modules = sorted(modules)

        logging.info('Found the following modules for "%s": %s' % (program, modules))

        return modules

    def ReplaceInFile(self, filename, oldtext, newtext):
        """
        Replace text in a file in the working folder
        :param filename:   (string) filename, e.g. 'lsdyna.k'
        :param oldtext:    (string) the old text that should be replaced
        :param newtext:    (string) the new text
        :return:
        """
        file = os.path.join(self.workfolder, filename)

        f = open(file, 'r')
        filedata = f.read()
        f.close()

        newdata = filedata.replace(oldtext, newtext)

        f = open(file, 'w')
        f.write(newdata)
        f.close()

        return

    def GetInfo(self):
        return self.__class__.__name__ + ' simulation in ' + self.workfolder + ' of ' + self.inputfile + ' as ' + self.jobname + ' job'

    def DeleteWorkFolder(self):
        """
        clean up the temp folder
        :return:

        """
        try:
            shutil.rmtree(self.workfolder)
            logging.info("temporary work folder '%s' deleted" % self.workfolder)
        except:
            logging.warn("temporary work folder '%s' could not be deleted, please delete manually" % self.workfolder)
            print("temporary work folder '%s' could not be deleted, please delete manually" % self.workfolder)


class AnsysJob(SimJob):
    def __init__(self, sourcefolder, jobname, inputfile, ansysversion=None, ansysproduct=None, numcpu=2, keepresults=True):
        """
        Defines an Ansys Job, derived from Class Ansysjob
        sourcefolder:              (string) the folder where the input files are to be found
        jobname:                   (string) the job name
        inputfile                  (string) the Ansys Input file name, e.g. 'ansys.mac'
        ansysversion='160'         (string) the desired Ansys version in the given format
        ansysproduct='aa_t_a'      (string) the Ansys product code
        numcpu=2                   (integer) Number of CPUs to use for simulation
        keepresults=True           (bool) keep the temporary results? This can be used to trigger the method DeleteWorkFolder
                                   This is not done automatically!
        """

        logging.debug("initializing AnsysJob Class")
        SimJob.__init__(self, sourcefolder, jobname, inputfile, keepresults=keepresults)

        if ansysversion:
            # a custom ansys version was requested in the class call, we use that
            # update the config so that the path is correctly generated
            self.config.set('Ansys', 'ansys_version', ansysversion)
        else:
            # no version was requested, we use the one from the config file
            pass

        if ansysproduct:
            # a custom ansys product was requested in the class call, we use that
            # update the config so that the path is correctly generated
            self.config.set('Ansys', 'ansys_product', ansysproduct)
        else:
            # no version was requested, we leave it at default with one exception:
            # if this Job is a "AnsysLSDynaJob", we use the corresponding value from the config file
            if self.__class__.__name__ == 'AnsysLSDynaJob':
                self.config.set('Ansys', 'ansys_product', self.config.get('LSDyna', 'lsdyna_ansysproduct'))

        self.numcpu = numcpu

        # Identify Ansys Executable
        if self.platform == 'windows':
            self.executable = self.config.get('Ansys', 'ansys_command_win')
        elif self.platform == 'linux':
            if self.modulesystem:
                # We are running on the Cluster with a module system. Get the available list of Ansys cluster modules
                ansysmodules = self.GetClusterModules("ansys")
                if not ansysmodules:
                    logging.fatal('no cluster modules found for Ansys. If you are running locally on linux, set modulesystem=False in your SimJob call')
                    sys.exit()
                if ('v' + self.config.get('Ansys', 'ansys_version')) not in ansysmodules:
                    highestversion = ansysmodules[-1]  # the highest found ansys version is the last one
                    highestversion = highestversion[1:]  # cut off the first letter, we need "160" instead of "v160"
                    logging.warn('Requested Ansys cluster module version "%s" not found, switching to "%s"' % (self.config.get('Ansys', 'ansys_version'), highestversion))
                    self.config.set('Ansys', 'ansys_version', highestversion)
                else:
                    logging.info('Using Ansys cluster module version "%s" ' % self.config.get('Ansys', 'ansys_version'))

                self.executable = self.config.get('Ansys', 'ansys_command_linux_module')
            else:
                # We are running locally on linux without a module system
                self.executable = self.config.get('Ansys', 'ansys_command_linux')

        else:
            logging.fatal('Could not determine Ansys executable, "self.platform" not set')
            sys.exit()

    def AddParameterFile(self, params, parfile, includeComments=True, appendCommands=''):

        filepath = os.path.join(self.workfolder, parfile)
        if os.path.isfile(filepath):
            logging.info('parameter file %s already exists, deleting and re-creating' % filepath)
            try:
                os.remove(filepath)
            except:
                logging.warning('failed to delete %s' % filepath)

        else:
            logging.debug('writing parameter file %s' % filepath)

        with open(filepath, 'w') as f:
            for param, item in sorted(params.items()):
                if isinstance(item, eos.Parameter):
                    # if the parameter is a named tuple, use its "value" field for the rest of the code
                    value = item.value
                    unit = item.unit
                    description = item.description
                    if includeComments:
                        added_text = '          ! [' + unit + '], ' + description
                    else:
                        added_text = ''
                else:
                    value = item
                    added_text = ''

                if isinstance(value, string_types) or isinstance(value, bool):
                    f.write(param + '=\'' + str(value) + '\'' + added_text + self.newLine)
                else:
                    f.write(param + '=' + str(value) + added_text + self.newLine)

            if appendCommands:
                f.write(appendCommands)

    def Run(self, custom_inputfile=False, writePostprocessingLaunchFile=True, maxTries=30, waitTime=60, maxCPUCores=4):
        """Run the Simulation
        params:
        writePostprocessingLaunchFile (bool, default True): write a .cmd file to launch ANSYS with the correct jobname  for postprocessing
        maxTries (int, default: 30): maximal number of tries in case of Licensing Errors
        waitTime (int, default: 60): number of seconds to wait before retry in case of Licensing Errors
        maxCPUCores (int, default: 4): defines the maximum number of CPU cores to use for a solution process, in case the possible 4 cores with Ansys 19.0 are not desired
        """

        if custom_inputfile:
            inputfile = custom_inputfile
        else:
            inputfile = self.inputfile

        if not os.path.isfile(os.path.join(self.sourcefolder, inputfile)):
            logging.warning('Inputfile "%s" does not exist - Exiting' % os.path.join(self.sourcefolder, inputfile))
            sys.exit()

        runSuccessful = False
        numTries = 1

        FourCoreLcenses = ['meba']

        # limit to two CPU cores if a license is selected, that does not support more than 2 cores
        if self.numcpu > 2:
            if self.config.get('Ansys', 'ansys_product') not in FourCoreLcenses:
                self.numcpu = 2
                logging.debug('more than 2 CPU cores requested for a license that does not support it (%s). Reducing CPU cores to 2.' % self.config.get('Ansys', 'ansys_product'))

        # use 4 cpu cores if the system has them, ansys version is >= 19.0 and an enterprise license is selected
        if int(self.config.get('Ansys', 'ansys_version')) >= 190:

            if self.config.get('Ansys', 'ansys_product') in FourCoreLcenses:
                self.numcpu = maxCPUCores
                logging.debug('Ansys > 19.0 is used with a 4-Core License in a solution task -> using %i CPU cores for the solution' % maxCPUCores)

        while not runSuccessful and numTries <= maxTries:

            logging.debug('Running simulation "%s" with inputfile "%s"' % (self.jobname, inputfile))
            # runs the simulation and moves back the result files (if not specified otherwise with FetchResults='false')
            outFile = 'ansys_' + self.jobname + '.out'

            call = self.executable + ' -b -j ansys_' + self.jobname + ' -p ' + self.config.get('Ansys', 'ansys_product') + ' -i ' + \
                   inputfile + ' -o ' + outFile + ' -np ' + str(self.numcpu)

            # args = shlex.split(call)
            os.chdir(self.workfolder)
            logging.debug('Changed current dir to workfolder: "%s"' % self.workfolder)
            # result = os.system(call)
            # proc = subprocess.Popen(args, stdout=subprocess.PIPE, shell=True)
            # proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            # proc = subprocess.Popen(args)
            # (out, err) = proc.communicate()

            try:
                # time.sleep(60)
                logging.info('Starting Ansys with command: "%s"' % call)
                # out = subprocess.check_output(call)
                os.system(call)
                out = ""
            except:
                print("warnings in ansys")
                out = "warnings"

            print("Ansys Output: " + out)

            # check the jobname.out file for license errors
            with open(outFile, 'r') as f:
                text = f.read()
            regex = "ANSYS\ LICENSE\ MANAGER\ ERROR"
            if re.findall(regex, text):
                runSuccessful = False
                logging.warning("Licensing error, waiting %i seconds to retry (%i/%i)" % (waitTime, numTries, maxTries))
                numTries += 1
                time.sleep(waitTime)
            else:
                runSuccessful = True

        if not runSuccessful:
            logging.critical("Ansys run failed due to Licensing Error, retried %i times with %i seconds delay" % (maxTries, 60))
            sys.exit("Ansys run failed due to Licensing Error, retried %i times with %i seconds delay" % (maxTries, 60))
        else:
            logging.debug("Ansys run successful")

        # write a batch file to quickly start postprocessing in ANSYS
        if writePostprocessingLaunchFile and self.platform == 'windows':
            filename = '_view_ansys_' + self.jobname + '.cmd'
            with open(filename, 'w') as f:
                try:
                    prepostLicense = self.config.get('Ansys', 'ansys_product_prepost')
                except:
                    logging.warning('ansys_product_prepost not set in config.cfg. Using ansys_product instead')
                    prepostLicense = self.config.get('Ansys', 'ansys_product')

                command = self.executable + ' -g -j ansys_' + self.jobname + ' -d 3d -p ' + prepostLicense
                f.write(command)

        os.chdir(self.projectfolder)
        logging.debug('Changed current dir to projectfolder: "%s"' % self.projectfolder)

    def ReadTextResults(self, filename):

        resultfile = os.path.join(self.workfolder, filename)
        if os.path.isfile(resultfile):
            logging.debug('Reading result text file "%s"' % resultfile)
            f = open(resultfile, 'r')
            # TextResults = f.readlines()
            results = np.loadtxt(f)
            f.close()
            logging.info('Results read: %s' % results)
        else:
            logging.warning('Result file does not exist: "%s"' % resultfile)
            results = ''
        return results

    def GetModalResults(self, resultFolder=False, resultFile=False):
        """
        Read modal analysis results directly from the ANSYS output file

        :param resultFile: (optional) file name of the ANSYS .out file
        :param resultFolder: (optional) folder name of the ANSYS .out file
        :return: list of all found eigenfrequencies
        """

        if not resultFolder:
            resultFolder = self.workfolder
        if not resultFile:
            resultFile = 'ansys_' + self.jobname + '.out'

        outFile = os.path.join(resultFolder, resultFile)
        try:
            with open(outFile, 'r') as f:
                resultText = f.readlines()

            eigenFrequencies = list()
            counter = 1
            for i, line in enumerate(resultText):
                if "FREQUENCY RANGE REQUESTED=" in line:
                    while len(resultText[i + counter]) > 3:
                        freq = np.fromstring(resultText[i + counter], sep=' ')[1]
                        print(freq)
                        eigenFrequencies.append(freq)
                        counter += 1
                    break
            if len(eigenFrequencies) > 0:
                logging.debug('successfully read eigenfrequencies from file "%s"' % outFile)
            else:
                logging.warning('no eigenfrequencies found in file "%s"' % outFile)
            return eigenFrequencies
        except:
            logging.warning('could not read eigenfrequencies from result file "%s"' % resultFile)
            return

    def CleanupAnsysFiles(self, AdditionalFilesToDelete=[], FilesToKeep=[]):
        import glob
        FilesToDelete = ['*.log', '*.err', '*.out', '*.esav', '*.emat', '*.full', '*.mlv', '*.mode', '*.rst', '*.stat',
                         '*.bcs', '*.dbb', 'fort.14', '*.lock', '*.mntr', '*.page', '*.LN*', '*.osav', '*.rdb',
                         '*.ldhi', '*.r0*', '*.DSP', '*.nr*'] + AdditionalFilesToDelete
        try:
            for fileToKeep in FilesToKeep:
                FilesToDelete.remove(fileToKeep)
        except:
            logging.warning('requested FilesToKeep could not be parsed in CleanupAnsysFiles()')
            pass

        logging.debug('Deleting ANSYS-files: %s' % str(FilesToDelete))
        for f in FilesToDelete:
            filepath = os.path.join(self.workfolder, f)
            for g in glob.glob(filepath):
                # the glob method turns wildcard filenames (e.g. 'd3plot*') to a list of paths, e.g. ['d3plot', 'd3plot01']
                if os.path.isfile(g):
                    try:
                        os.remove(g)
                        logging.debug('deleted file "%s"' % g)
                    except:
                        logging.warning('Could not delete "%s"' % g)


class AnsysLSDynaJob(AnsysJob):

    def __init__(self, sourcefolder, jobname, inputfile, ansysversion=None, ansysproduct=None, lsdynaversion=None, lsdynaprecision=None, numcpu=2, lsdynaoutput='both', QuasiStaticConvergenceCriterion=0., lsdynamemory=0, keepresults=True, energyCSVFile = ''):
        """
        Defines an Ansys-LS-Dyna Job, derived from Class Ansysjob
        Most defaults defined in EOS Config file!
        sourcefolder:              (string) the folder where the input files are to be found
        jobname:                   (string) the job name
        inputfile                  (string) the Ansys Input file name, e.g. 'ansys.mac'
        ansysversion='160'         (string) the desired Ansys version in the given format
        ansysproduct='aa_t_a'      (string) the Ansys product code
        lsdynaversion='971_R51'    (string) the LS-Dyna version (only relevant for the cluster currently)
        lsdynaprecision='double'   (string) 'double' or 'single', determine the precision of the LS-Dyna solution
        numcpu=2                   (integer) Number of CPUs to use for simulation
        lsdynaoutput='both'        (string) 'ansys', 'lsdyna', 'both' define the output format of the LS-Dyna solver
        lsdynamemory=0             (integer) mememory in MB
        keepresults=True           (bool) keep the temporary results? This can be used to trigger the method DeleteWorkFolder
                                    This is not done automatically!
        """
        logging.debug("initializing class " + self.__class__.__name__)
        AnsysJob.__init__(self, sourcefolder, jobname, inputfile, ansysversion=ansysversion, ansysproduct=ansysproduct, numcpu=numcpu, keepresults=keepresults)

        if lsdynaprecision == 'double': self.config.set('LSDyna', 'lsdyna_precision', 'd')
        if lsdynaprecision == 'single': self.config.set('LSDyna', 'lsdyna_precision', 's')
        if ansysproduct: self.config.set('LSDyna', 'lsdyna_ansysproduct', ansysproduct)
        if ansysversion: self.config.set('LSDyna', 'lsdyna_ansysversion', ansysversion)

        self.QuasiStaticConvergenceCriterion = QuasiStaticConvergenceCriterion
        self.lsdynamemory = 524288*lsdynamemory  # convert to MB
        self.lsdynamemorystring = ''

        self.energyCSVFile = os.path.join(self.workfolder, energyCSVFile)

        # set the environment variable "LSTC_FORMAT" so that LS-Dyna writes the desired output files
        if lsdynaoutput == 'both':
            # output = ansys + lsdyna
            os.environ['LSTC_FORMAT'] = 'taurus+ansys'
        elif lsdynaoutput == 'ansys':
            # output = ansys
            os.environ['LSTC_FORMAT'] = 'ansys'
        else:
            # output = lsdyna, this is default, no env necessary
            pass

        # Identify lsdyna Executable
        if self.platform == 'windows':
            if lsdynaversion: self.config.set('LSDyna', 'lsdyna_version_win', lsdynaversion)
            self.lsdynaexecutable = self.config.get('LSDyna', 'lsdyna_command_win')

        elif self.platform == 'linux':
            # if a LSDyna version was specifically called in the SimJob, overwrite the config
            if lsdynaversion: self.config.set('LSDyna', 'lsdyna_version_linux', lsdynaversion)
            # otherwise, get the LSDyna Version from the config file
            self.lsdynaversion = self.config.get('LSDyna', 'lsdyna_version_linux')

            if self.modulesystem:
                # We are running on the Cluster with a module system. Get the available list of lsdyna cluster modules
                lsdynamodules = self.GetClusterModules("lsdyna")
                if not lsdynamodules:
                    logging.fatal('no cluster modules found for lsdyna. If you are running locally on linux, set modulesystem=False in your SimJob call')
                    sys.exit()
                if self.lsdynaversion not in lsdynamodules:
                    highestversion = lsdynamodules[-1]  # the highest found lsdyna version is the last one
                    logging.warn('Requested lsdyna cluster module version "%s" not found, switching to "%s"' % (self.lsdynaversion, highestversion))
                    self.lsdynaversion = highestversion
                    self.config.set('LSDyna', 'lsdyna_version_linux', self.lsdynaversion) # update the config as well so that the path will be correctly built
                else:
                    logging.info('Using lsdyna cluster module version "%s"' % self.lsdynaversion)

                self.lsdynaexecutable = self.config.get('LSDyna', 'lsdyna_command_linux_module')
            else:
                # We are running locally on linux without a module system
                self.lsdynaexecutable = self.config.get('LSDyna', 'lsdyna_command_linux')
                if not os.path.isfile(self.lsdynaexecutable):
                    logging.critical('LS-Dyna Executable not found under the following path: "%s"' % self.lsdynaexecutable)
                    sys.exit()
        else:
            logging.fatal('Could not determine lsdyna executable, "self.platform" not set')
            sys.exit()

        return

    def SetupQuasiStaticConvergenceCriterion(self, interval=5., energyCSVFile='', **kwargs):
        """
        Setup the Convergence Criterion for Quasi-Static Simulations in LS-Dyna.
        During the LS-Dyna simulation, the "glstat" file is continually read. Kinetic energy and internal energy are
        determined, and the following ration is evaluated:
        if convergence is found, LS-Dyna is stopped.
        You can define several convergence criteria via the keywords below.

        :param interval=5.: (float) The check inverval in seconds

        :keyword   crit_conv_e_damp (float):
            set this value to enable check for damping energy convergence. This is calculated as follows:
            conv_e_damp = 1 - abs(E_damp_n-1 / E_damp_n)   (n is the output-step number)
            convergence is found if conv_e_damp <= abs(crit_conv_e_damp)

        :keyword   crit_conv_e_tot (float):
            set this value to enable check for total energy convergence. This is calculated as follows:
            conv_e_tot = 1 - abs(E_tot_n-1 / E_tot_n)   (n is the output-step number)
            convergence is found if conv_e_tot <= abs(crit_conv_e_tot)

        :keyword   crit_ratio_e_kin_e_int (float)
            set this value to enable check for the ratio of kinetic energy to internal energy. This is calculated as follows:
             ratio_e_kin_e_int = e_kin / e_int
             convergence is found if ratio_e_kin_e_int <= crit_ratio_e_kin_e_int

        :return: Killer (Class): LSDynaKiller Thread Class Object

        To stop the Thread externally use:
        Killer.stop = True
        """
        class LSDynaKiller():
            def __init__(self, LSDynaObject, interval=1., energyCSVFile = '', **kwargs):
                self.interval = interval
                self.LSDynaObject = LSDynaObject
                self.stop = False
                self.prev_resultcount = 0
                self.prev_e_damp = 0.
                self.prev_e_tot = 0.
                self.prev_e_slid = 0.
                self.conv_e_damp = 1.
                self.conv_e_tot = 1.
                self.conv_e_slid = 1.
                self.convergencefound = False
                self.time = 0.

                self.crit_conv_e_damp = kwargs['crit_conv_e_damp']
                self.crit_conv_e_tot = kwargs['crit_conv_e_tot']
                self.crit_ratio_e_kin_e_int = kwargs['crit_ratio_e_kin_e_int']
                self.crit_conv_e_slid = kwargs['crit_conv_e_slid']

                self.energyCSVFile = energyCSVFile

                thread = threading.Thread(target=self.run, args=())
                thread.demon = True
                thread.start()

            def run(self):
                logstr = 'Started LSDynaKiller with'
                if self.crit_conv_e_damp:
                    logstr += ', crit_conv_e_damp="%s"' % str(self.crit_conv_e_damp)
                if self.crit_conv_e_tot:
                    logstr += ', crit_conv_e_tot="%s"' % str(self.crit_conv_e_tot)
                if self.crit_conv_e_damp:
                    logstr += ', crit_ratio_e_kin_e_int="%s"' % str(self.crit_ratio_e_kin_e_int)
                if self.crit_conv_e_damp:
                    logstr += ', crit_conv_e_slid="%s"' % str(self.crit_conv_e_slid)

                logging.debug(logstr)

                # waiting for the "glstat" file to be written by LS-Dyna
                glstatfile = os.path.join(self.LSDynaObject.workfolder, 'glstat')
                while not os.path.isfile(glstatfile):
                    time.sleep(self.interval)

                # now the "glstat" file has been found, we will read it until the variable self.stop is set
                while not self.stop:
                    time.sleep(self.interval)
                    results = self.ReadEnergies()

                    # prepare a list of convergence criteria. If all are fulfulled, the LS-Dyna simulation will be stopped
                    conv_list = []

                    # if requested, check if the damping energy has converged
                    if self.crit_conv_e_damp:
                        conv_e_damp = results['conv_e_damp']
                        if abs(conv_e_damp) <= abs(self.crit_conv_e_damp):
                            conv_list.append(True)
                        else:
                            conv_list.append(False)

                    # if requested, check if the total energy has converged
                    if self.crit_conv_e_tot:
                        conv_e_tot = results['conv_e_tot']
                        if abs(conv_e_tot) <= abs(self.crit_conv_e_tot):
                            conv_list.append(True)
                        else:
                            conv_list.append(False)

                    # if requested, check if the ratio of kinetic over internal energy has converged
                    if self.crit_ratio_e_kin_e_int:
                        ratio_e_kin_e_int = results['ratio_e_kin_e_int']
                        if abs(ratio_e_kin_e_int) <= self.crit_ratio_e_kin_e_int:
                            conv_list.append(True)
                        else:
                            conv_list.append(False)

                    # if requested, check if the sliding interface energy has converged
                    if self.crit_conv_e_slid:
                        conv_e_slid = results['conv_e_slid']
                        if abs(conv_e_slid) <= abs(self.crit_conv_e_slid):
                            conv_list.append(True)
                        else:
                            conv_list.append(False)

                    # now we check the entire list of convergence criteria (conv_list). If all entries are True, global
                    # convergence has been found and we can stop the LS-Dyna simulation.
                    if all(conv is True for conv in conv_list):
                        logging.info('LS-Dyna convergence found, stopping LSDynaKiller')
                        self.LSDynaObject.KillLSDyna()
                        self.stop = True
                        self.convergencefound = True

                if self.convergencefound:
                    logging.debug("LSDynaKiller Loop stopped, found convergence")
                else:
                    logging.warn("LSDynaKiller Loop stopped without finding convergence")

            def ReadEnergies(self):
                """
                Read the LS-Dyna "glstat" file for total and kinetic energy.
                returns a dict with several results:
                results['time']:                : the current time simulated time
                results['e_damp']               : Damping Energy (sum over entire simulation)
                results['e_kin']                : Kinetic Energy (current)
                results['e_slid']               : Interface Sliding Energy (current)
                results['e_int']                : Internal Energy (current)
                results['e_tot']                : Total Energy (current)
                results['ratio_e_kin_e_int']    : E_kin / E_int (ratio of kinetic over internal energy)
                results['conv_e_slid']          : 1 - (E_slid_n-1 / E_slid_n) (convergence value for the interface sliding energy)
                results['conv_e_damp']          : 1 - (E_damp_n-1 / E_damp_n) (convergence value for the damping energy)
                results['conv_e_tot']           : 1 - (E_tot_n-1 / E_tot_n) (convergence value for the damping energy)
                """
                import re

                glstatfile = os.path.join(self.LSDynaObject.workfolder, 'glstat')

                try:
                    with open(glstatfile, mode='r') as f:
                        text = f.read()
                except IOError:
                    logging.warning('glastat file not found, LS-Dyna run probably finished')
                    return

                # build the regular expression that checks for the module versions. Helpful tool: https://regex101.com
                regex1 = "\ kinetic\ energy\.\.\.\.\.\.\.\.\.\.\.\.*\ *(\S*)"
                regex2 = "\ internal\ energy\.\.\.\.\.\.\.\.\.\.\.*\ *(\S*)"
                regex3 = "\ system\ damping\ energy\.*\ *(\S*)"
                regex4 = "\ total\ energy\.\.*\ *(\S*)"
                regex5 = "\ sliding\ interface\ energy\.*\ *(\S*)"
                regexTime = "\ time\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.*\ *(\S*)"

                e_kin = 1.
                e_int = 1.
                e_damp = 1.
                e_tot = 1.
                e_slid = 1.

                resultcount = 0
                # each regex match in the moduletext string is a version of the module. They are put in the list "modules"
                for m in re.finditer(regex1, text):
                    e_kin = float(m.group(1))
                    resultcount += 1  # count the number of results printed until now

                for m in re.finditer(regex2, text):
                    e_int = float(m.group(1))

                for m in re.finditer(regex3, text):
                    e_damp = float(m.group(1))

                for m in re.finditer(regex4, text):
                    e_tot = float(m.group(1))

                for m in re.finditer(regex5, text):
                    e_slid = float(m.group(1))

                for m in re.finditer(regexTime, text):
                    self.time = float(m.group(1))

                if e_int and e_kin:
                    ratio_e_kin_e_int = e_kin/e_int
                else:
                    ratio_e_kin_e_int = 1.

                if not resultcount == self.prev_resultcount:  # only log, if a new set of results has been printed out

                    if self.crit_conv_e_damp:
                        if e_damp:
                            self.conv_e_damp = 1.-(self.prev_e_damp / e_damp)
                        else:
                            self.conv_e_damp = 1.
                            # damping energy not available, deactivating convergence check
                            logging.warn('LS-Dyna convergence check for damping energy not possible, no damping energy in "glstat" file.')
                            self.crit_conv_e_damp = 0
                    if self.crit_conv_e_tot:
                        if e_tot:
                            self.conv_e_tot = 1.-(self.prev_e_tot / e_tot)
                        else:
                            self.conv_e_tot = 1.
                            # total energy not available, deactivating convergence check
                            logging.warn('LS-Dyna convergence check for total energy not possible, no total energy in "glstat" file.')
                            self.crit_conv_e_tot = 0
                    if self.crit_conv_e_slid:
                        if e_slid:
                            self.conv_e_slid = 1.-(self.prev_e_slid / e_slid)
                        else:
                            self.conv_e_slid = 1.
                            # sliding interface energy not available, deactivating convergence check
                            logging.warn('LS-Dyna convergence check for sliding interface energy not possible, no total energy in "glstat" file.')
                            self.crit_conv_e_slid = 0


                    self.prev_e_damp = e_damp
                    self.prev_e_tot = e_tot
                    self.prev_e_slid = e_slid
                    self.prev_resultcount = resultcount
                    logging.debug('%i: time: %f E_kin: %E, E_int: %E, E_kin/E_int: %E, E_slid: %E, conv_E_slid: %E E_damp: %E, conv_E_damp: %E, E_tot: %E, conv_E_tot: %E' % (resultcount, self.time, e_kin, e_int, ratio_e_kin_e_int, e_slid, self.conv_e_slid, e_damp, self.conv_e_damp, e_tot, self.conv_e_tot))



                    if self.energyCSVFile:
                        if not os.path.exists(self.energyCSVFile):
                            logging.debug('Energy logging to file "%s" requested' % self.energyCSVFile)
                            newFile = True
                        else:
                            newFile = False
                        with open(self.energyCSVFile, 'a') as f:
                            if newFile:
                                # new file, write heading line
                                logging.debug('writing header line to energy csv file "%s"' % self.energyCSVFile)
                                f.write('step;time;E_kin;E_int;E_kin/E_int;E_slid;conv_E_slid;E_damp;conv_E_damp;E_tot;conv_E_tot\n')
                            f.write('%i;%f;%E;%E;%E;%E;%E;%E;%E;%E;%E\n' % (resultcount, self.time, e_kin, e_int, ratio_e_kin_e_int, e_slid, self.conv_e_slid, e_damp, self.conv_e_damp, e_tot, self.conv_e_tot))

                results = dict()
                results['e_damp'] = e_damp
                results['e_kin'] = e_kin
                results['e_slid'] = e_slid
                results['e_int'] = e_int
                results['e_tot'] = e_tot
                results['ratio_e_kin_e_int'] = ratio_e_kin_e_int
                results['conv_e_slid'] = self.conv_e_slid
                results['conv_e_tot'] = self.conv_e_tot
                results['conv_e_damp'] = self.conv_e_damp

                return results

        # check which convergence criteria were requested in the kwargs (keyword arguments)
        if kwargs['crit_conv_e_damp']:
            crit_conv_e_damp = kwargs['crit_conv_e_damp']
        else:
            crit_conv_e_damp = 0

        if kwargs['crit_conv_e_tot']:
            crit_conv_e_tot = kwargs['crit_conv_e_tot']
        else:
            crit_conv_e_tot = 0

        if kwargs['crit_ratio_e_kin_e_int']:
            crit_ratio_e_kin_e_int = kwargs['crit_ratio_e_kin_e_int']
        else:
            crit_ratio_e_kin_e_int = 0

        if kwargs['crit_conv_e_slid']:
            crit_conv_e_slid = kwargs['crit_conv_e_slid']
        else:
            crit_conv_e_slid = 0

        # initiate the LSDynaKiller class and return it
        Killer = LSDynaKiller(self, energyCSVFile=energyCSVFile, interval=interval, crit_ratio_e_kin_e_int=crit_ratio_e_kin_e_int, crit_conv_e_damp=crit_conv_e_damp, crit_conv_e_tot=crit_conv_e_tot, crit_conv_e_slid=crit_conv_e_slid)
        return Killer

    def CleanupLSDynaFiles(self):
        import glob
        FilesToDelete=['adptmp', 'bndout', 'd3hsp', 'd3plot*', 'd3thdt', 'disk8', 'glstat', 'ldcrvv', 'matsum', 'messag', 'part_des', 'spcforc', 'status.out', 'lsdyna_*.out', '*.his', '*.rst']
        logging.debug('Deleting old LSDyna-files: %s' % str(FilesToDelete))
        for f in FilesToDelete:
            filepath = os.path.join(self.workfolder, f)
            for g in glob.glob(filepath):
                # the glob method turns wildcard filenames (e.g. 'd3plot*') to a list of paths, e.g. ['d3plot', 'd3plot01']
                if os.path.isfile(g):
                    try:
                        os.remove(g)
                        logging.debug('deleted file "%s"' % g)
                    except:
                        logging.warn('Could not delete "%s"' % g)

    def RunLSDyna(self, inputfile):
        """
        Execute LS-dyna with the given input file in the job directory
        inputfile:   (string) the LS-Dyna input file, e.g. 'lsdyna.k'

        :return: SimulationSuccessful (bool) - set to false if the QuasiStatic Convergence Criterion was requested, but not found.
        """
        # initialize a flag for failed simulations
        SimulationSuccessful = True

        # clean up any old ls-dyna files in the work directory
        self.CleanupLSDynaFiles()

        if not os.path.isfile(os.path.join(self.workfolder, inputfile)):
            logging.warning('Inputfile "%s" does not exist - Exiting' % os.path.join(self.sourcefolder, inputfile))
            sys.exit()

        logging.debug('Running simulation "%s" with inputfile "%s"' % (self.jobname, inputfile))

        if self.lsdynamemory:
            self.lsdynamemorystring = ' memory=' + str(self.lsdynamemory)

        call = self.lsdynaexecutable + ' i=' + inputfile + ' ncpu=-' + str(self.numcpu) + self.lsdynamemorystring + ' > lsdyna_' + self.jobname + '.out 2>&1'

        # args = shlex.split(call)
        os.chdir(self.workfolder)
        logging.debug('Changed current dir to workfolder: "%s"' % self.workfolder)
        # result = os.system(call)
        # proc = subprocess.Popen(args, stdout=subprocess.PIPE, shell=True)
        # proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # proc = subprocess.Popen(args)
        # (out, err) = proc.communicate()

        # If requested, Start the Thread to monitor the kinetic to internal energy ratio for quasi-static simulations.
        if self.QuasiStaticConvergenceCriterion:
            qscc = self.QuasiStaticConvergenceCriterion
            Killer = self.SetupQuasiStaticConvergenceCriterion(energyCSVFile=self.energyCSVFile, crit_conv_e_damp=qscc, crit_conv_e_tot=qscc, crit_ratio_e_kin_e_int=2.0*qscc, crit_conv_e_slid=qscc)


        try:
            #time.sleep(60)
            logging.debug('Starting LS-Dyna with command: "%s"' % call)
            #out = subprocess.check_output(call)
            os.system(call)
        except:
            logging.warning('Warnings in LS-Dyna, call failed')

        # In case the Killer thread did not terminate itself (i.E. no convergence was found), terminate it.

        if self.QuasiStaticConvergenceCriterion:
            SimulationSuccessful = False  # default to false, set to true below
            try:
                # try to stop the Killer Thread.
                Killer.stop = True
            except:
                # If there is an error, the Killer thread terminated itself, i.e. it found convergence, so the Simulation was successful
                SimulationSuccessful = True

        logging.debug('LS-Dyna run finished')

        os.chdir(self.projectfolder)
        logging.debug('Changed current dir to projectfolder: "%s"' % self.projectfolder)

        return SimulationSuccessful

    def KillLSDyna(self, killswitch='sw1'):
        killfile = os.path.join(self.workfolder, 'd3kil')
        logging.debug('Writing LS-Dyna kill file "%s" with killswitch "%s"' % (killfile, killswitch))
        with open(killfile, mode='w') as f:
            f.write(killswitch)

    def UpdateLSDynaCard(self, filename, cardname='', cardidentifier=[], cardcontent='', newcardname='', mode='replace', keepold=False):
        """
            Update a Card in a LS-Dyna input deck
            newlines have to be given as \n on all platforms!

            cardidentifier: (string) the beginning of the next line after the card name to identify the correct card

            mode : 'replace' replaces the old card and its content
            mode : 'append' adds the new card to the file, keeping the old card. The new card will be added just before the 'cardname' card.
            mode : 'delete' delete the card
        """
        logging.debug("UpdateLSDynaCard() called")
        oldfilepath = os.path.join(self.workfolder, filename)
        newfilepath = os.path.join(self.workfolder, filename + '_new')

        if not os.path.isfile(oldfilepath):
            logging.critical('File "%s" not found in method UpdateLSDynaCard' % oldfilepath)
            sys.exit()

        if not (cardcontent.endswith('\n') or cardcontent.endswith('\r\n')):
            cardcontent += '\n'

        if newcardname:
            if not (newcardname.endswith('\n') or newcardname.endswith('\r\n')):
                newcardname += '\n'

        if keepold:
            shutil.copyfile(oldfilepath, oldfilepath+'.old')

        if cardidentifier:
            # if a card identifier is given we need to load the entire file to ram for checking the car later on
            with open(oldfilepath, mode='r') as oldfile:
                oldfiletext = oldfile.readlines()

        logging.debug('starting to iterate over the input file "%s"' % oldfilepath)
        with open(oldfilepath, mode='r') as oldfile:
            with open(newfilepath, mode='w') as newfile:

                found = False
                ended = False
                for num, line in enumerate(oldfile):
                    if not found:
                        # card not yet found
                        if cardname in line:
                            skip = False
                            if cardidentifier:
                                # a card identifier was given, so we must make sure that we have the right one
                                if not oldfiletext[num+1].startswith(cardidentifier):
                                    # this card is of the right type, but not with the correct identifier
                                    skip = True

                            if not skip:
                                # We found the correct card!
                                found = True
                                if mode == 'append':
                                    # we want to add the new card to the file, keeping the old card
                                    # write the new card and its content
                                    newfile.write(newcardname)
                                    newfile.write(cardcontent)
                                    logging.debug('Added LS-Dyna card "%s" before card "%s" in file "%s"' % (newcardname, cardname, oldfilepath))

                                    # now add the old card name to continue
                                    newfile.write(line)

                                    # we set "ended" to True so no lines are left out further down.
                                    ended = True
                                elif mode == 'replace':
                                    # we want to replace the old card with the new card
                                    if newcardname:
                                        # if a new card name was defined, write it now...
                                        newfile.write(newcardname)
                                    else:
                                        # ... otherwise keep the old card name
                                        newfile.write(line)
                                    # now we write the new card content
                                    newfile.write(cardcontent)
                                elif mode == 'delete':
                                    # we want to delete this card so we do not write it.
                                    pass

                                # save the line number, where the card was found
                                cardline = num
                        else:
                            # the card was not yet found, so we just write out the old text.
                            newfile.write(line)
                    else:
                        if not ended and not (line.startswith('*') or line.startswith('$')):
                            # this line is content of the previous card, we will not write it to the new file.
                            pass
                        if not ended and (line.startswith('*') or line.startswith('$')):
                            # the next card has been detected, we will write it to the new file
                            ended = True
                        if ended:
                            # all editing is finished, so we just write out the old text to the new file until the end.
                            newfile.write(line)

        if found:
            logging.debug(('Found card "%s" in line: ' + str(cardline)) % cardname)
        else:
            logging.warn('Could not find card "%s" in method UpdateLSDynaCard()' % cardname)

        # delete the old file, move the new file to the old file name
        os.remove(oldfilepath)
        shutil.move(newfilepath, oldfilepath)


class AnsysMatlabJob(AnsysJob):

    def __init__(self, sourcefolder, jobname, inputfile, ansysversion=None, ansysproduct=None, matlabVersion=None, numcpu=2, keepresults=True):
        """
        Defines an Ansys+Matlab Job, derived from Class Ansysjob
        Most defaults defined in EOS Config file!
        sourcefolder:              (string) the folder where the input files are to be found
        jobname:                   (string) the job name
        inputfile                  (string) the Ansys Input file name, e.g. 'ansys.mac'
        matlabVersion='R2016a'     (string) the Matlab version
        numcpu=2                  (integer) Number of CPUs to use for the Ansys simulation
        keepresults=True           (bool) keep the temporary results? This can be used to trigger the method DeleteWorkFolder
                                    This is not done automatically!
        """
        logging.debug("initializing class " + self.__class__.__name__)
        AnsysJob.__init__(self, sourcefolder, jobname, inputfile, ansysversion=ansysversion, ansysproduct=ansysproduct, numcpu=numcpu, keepresults=keepresults)


        # Identify Matlab Executable
        if self.platform == 'windows':
            if matlabVersion: self.config.set('Matlab', 'matlab_version_win', matlabVersion)
            self.matlabExecutable = self.config.get('Matlab', 'matlab_command_win')

        elif self.platform == 'linux':
            # if a LSDyna version was specifically called in the SimJob, overwrite the config
            if matlabVersion: self.config.set('Matlab', 'matlab_version_linux', matlabVersion)
            # otherwise, get the LSDyna Version from the config file
            self.matlabVersion = self.config.get('Matlab', 'matlab_version_linux')

            if self.modulesystem:
                # We are running on the Cluster with a module system. Get the available list of matlab cluster modules
                matlabModules = self.GetClusterModules("matlab")
                if not matlabModules:
                    logging.fatal('no cluster modules found for matlab. If you are running locally on linux, set modulesystem=False in your SimJob call')
                    sys.exit()
                if self.matlabVersion not in matlabModules:
                    highestversion = matlabModules[-1]  # the highest found lsdyna version is the last one
                    logging.warning('Requested matlab cluster module version "%s" not found, switching to "%s"' % (self.matlabVersion, highestversion))
                    self.lsdynaversion = highestversion
                    self.config.set('Matlab', 'matlab_version_linux', self.matlabVersion) # update the config as well so that the path will be correctly built
                else:
                    logging.info('Using matlab cluster module version "%s"' % self.matlabVersion)

                self.matlabExecutable = self.config.get('LSDyna', 'lsdyna_command_linux_module')
            else:
                # We are running locally on linux without a module system
                self.matlabExecutable = self.config.get('LSDyna', 'lsdyna_command_linux')
                if not os.path.isfile(self.matlabExecutable):
                    logging.critical('Matlab Executable not found under the following path: "%s"' % self.matlabExecutable)
                    sys.exit()
        else:
            logging.fatal('Could not determine matlab executable, "self.platform" not set')
            sys.exit()

        return

    def AddMatlabParameterFile(self, params, parfile):

        filepath = os.path.join(self.workfolder, parfile)
        if os.path.isfile(filepath):
            logging.info('parameter file %s already exists, deleting and re-creating' % filepath)
            try:
                os.remove(filepath)
            except:
                logging.warning('failed to delete %s' % filepath)
        else:
            logging.debug('writing parameter file %s' % filepath)
        f = open(filepath, 'w')

        for param, value in sorted(params.items()):
            if isinstance(value, string_types) or isinstance(value, bool):
                f.write(param + '=\'' + str(value) + '\';' + self.newLine)
            else:
                f.write(param + '=' + str(value) + ';' + self.newLine)
        return

    def RunMatlab(self, inputfile, maxTries=60, waitTime=60):
        """
        Execute LS-dyna with the given input file in the job directory
        inputfile:   (string) the Matlab input file, e.g. 'runmatlab.m', also works with just 'runmatlab'

        :return: SimulationSuccessful (bool) - set to false if the QuasiStatic Convergence Criterion was requested, but not found.
        """
        # initialize a flag for failed simulations
        SimulationSuccessful = True

        # remove the .m file ending if the user supplied it, matlab must be called with just the base name
        if inputfile.endswith('.m'):
            inputCommand = inputfile.rstrip('.m')
        else:
            inputCommand = inputfile

        if not os.path.isfile(os.path.join(self.workfolder, inputCommand+'.m')):
            logging.warning('Inputfile "%s" does not exist - Exiting' % os.path.join(self.sourcefolder, inputCommand))
            sys.exit()

        logging.debug('Running Matlab with inputfile "%s"' % (inputCommand))

        markerFileName = 'eos_matlab_started.txt'
        flagFileText = "filename = ['" + markerFileName + "']; fid=fopen(filename,'w'); fprintf(fid, '1'); fclose(fid);"
        # flagFileText = "pause(200);" # for debugging to provoke the process kill
        flagFileCommand = 'eos_matlab_flag_file'
        flagFileName = flagFileCommand + '.m'

        # call = self.matlabExecutable + ' -nosplash -nodesktop -r ' + inputfile + ' -logfile ' + self.jobname + '_matlab.out'
        call = [self.matlabExecutable, '-nodesktop', '-r', flagFileCommand+';'+inputCommand+';quit', '-logfile', self.jobname + '_matlab.out']

        # args = shlex.split(call)
        os.chdir(self.workfolder)
        logging.debug('Changed current dir to workfolder: "%s"' % self.workfolder)

        # write input file for creating the license flag file
        with open(flagFileName, mode='w') as f:
            f.writelines(flagFileText)

        matlabRunning = False
        # delete the marker file if it exists:
        if os.path.isfile(markerFileName):
            os.remove(markerFileName)

        attempts = 0
        while (not matlabRunning) and (attempts < maxTries):
            attempts += 1
            flagFileFound = False
            logging.debug('Starting Matlab with command: "%s"' % call)

            if self.platform == "linux":
                process = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
            else:
                process = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

            timeElapsed = 0
            while (not flagFileFound) and (timeElapsed < waitTime):
                timeElapsed += 1
                if os.path.isfile(markerFileName):
                    logging.debug('Matlab flag file found!')
                    flagFileFound = True
                else:
                    logging.debug('Matlab flag file not yet found, retrying (%i / %i)' % (timeElapsed, waitTime))
                    time.sleep(1)

            if flagFileFound:
                # Matlab is running, all is well
                matlabRunning = True
            else:
                # Matlab is not running, possibly due to a license problem. Kill it and retry
                # time.sleep(10)
                logging.warning("Matlab didn't start, possibly due to license errors. Retrying (%i / %i)..." % (attempts, maxTries))

                if self.platform == "linux":
                    os.killpg(os.getpgid(process.pid), signal.SIGTERM)  # Send the signal to all the process groups
                else:
                    subprocess.Popen("TASKKILL /F /PID {pid} /T".format(pid=process.pid))

                # process.kill()
                #os.killpg(os.getpgid(process.pid), signal.SIGTERM)

        # wait for the process to finish / check if it is already finished
        process.wait()


        if matlabRunning:
            logging.debug('Matlab run finished')
        else:
            logging.warning('Matlab run not finished, possibly due to licensing error.')

        os.chdir(self.projectfolder)
        logging.debug('Changed current dir to projectfolder: "%s"' % self.projectfolder)

        return SimulationSuccessful


class AbaqusJob(SimJob):
    def __init__(self, sourcefolder, inputfile, abaqusversion, projectfolder=None, jobname=None, UserRoutine=None, FortranCompiler=False):
        """
        Defines and runs an abaqus job, derived from Class SimJob
        
        Param: FortranCompiler ... If True initializes the compliler
        """

        if abaqusversion: self.config.set('Abaqus', 'abaqus_version', abaqusversion)

        try:
            self.FileName, self.ext = inputfile.split('.', 1)
        except:
            logging.warn(
                'No file extension given with variable inputfile! -> .inp was set as extension')
            self.FileName = inputfile
            self.ext == 'inp'

        if not jobname:
            jobname = cp.deepcopy(self.FileName)

        SimJob.__init__(self, sourcefolder, jobname, inputfile, projectfolder=projectfolder)

        self.UserRoutine = UserRoutine

        # determine Platform
        if self.platform == 'windows':
            self.executable = self.config.get('Abaqus', 'abaqus_command_win')
            if FortranCompiler:
                # extend executalbe call by fortran compiler routine
                FortranCompExe = self.config.get('Abaqus', 'abaqus_fortran_win')
                self.executable = FortranCompExe + self.executable
                logging.warning("Fortran compiler only works in tandem with 'Abaqus RESEARCH LICENSE' -> Check '.env' file")
            logging.info("Running on Windows, Abaqus Executable: '%s'" % self.executable)
        elif self.platform == 'linux':
            self.executable = self.config.get('Abaqus', 'abaqus_command_linux_module')
            logging.info("Running on Linux, Abaqus Executable: '%s" % self.executable)
            if FortranCompiler:
                logging.fatal("Demanded fortran compiler not linked yet -> Update 'simjob.py' by adding path")
        else:
            logging.error('Operating system currently not supported!')

    def WritePickleFile(self, params, filename):
        '''
        Writes any kind of params to filename.p via pickle
        '''
        pickle.dump(params, open(os.path.join(self.workfolder, filename), 'wb'))

    def Run(self, showGUI = False):
        '''
        Executes a job with .inp or a script with .py
        '''
        if self.ext == 'inp':
            call = self.executable + ' job=' + self.jobname + ' input=' + self.inputfile
            if self.UserRoutine: call += ' user="' + '"'
            call += ' interactive' + ' > ' + self.jobname + '.out 2>&1'
            logging.info(".inp Input detected, Abaqus call: '%s'" % call)
        elif self.ext == 'py' and showGUI:
            call = self.executable + ' cae script=' + self.inputfile + ' > ' + self.jobname + '.out 2>&1'
            logging.info(".py Input detected, Abaqus call with GUI: '%s'" % call)
        elif self.ext == 'py' and not showGUI:
            call = self.executable + ' cae noGUI=' + self.inputfile + ' > ' + self.jobname + '.out 2>&1'
            logging.info(".py Input detected, Abaqus call without GUI: '%s'" % call)
        else:
            logging.error('Specific extension case is not considered yet!')
            sys.exit()

        os.chdir(self.workfolder)
        logging.info("changing to workfolder: '%s'" % self.workfolder)

        try:
            # subprocess.check_output(call)
            logging.info("Calling Abaqus")
            os.system(call)
        except:
            logging.warning('Problem in calling abaqus as a subprocess!')
        os.chdir(self.projectfolder)
        logging.debug('Changed current dir to projectfolder: "%s"' % self.projectfolder)

    def LoadPickleFile(self, filename):
        '''
        Loads any kind of responses from filename.p via pickle
        '''
        return pickle.load(open(os.path.join(self.workfolder, filename), 'rb'))


class NastranJob(SimJob, NastranClass):
    def __init__(self, sourcefolder, inputfile, nastranversion=None, jobname='', ulimit=None):
        """
        Defines and runs an nastran job, derived from Class SimJob
        """
        self.StandardExtensions = ['bdf','fem','blk','nas','dat']
        try:
            self.FileName, self.ext = inputfile.split('.', 1)
            self.inputfile = inputfile
        except:
            logging.warn('No file extension given with variable inputfile! -> .inp was set as extension')
            self.FileName = inputfile
            self.ext == 'bdf'
            self.inputfile = self.FileName+'.'+self.ext

        if len(jobname)==0:
            jobname = cp.deepcopy(self.FileName)

        SimJob.__init__(self, sourcefolder, jobname, self.inputfile, workfolderType='Short')
        NastranClass.__init__(self,self.workfolder)

        # determine Platform
        if self.platform == 'windows':
            if nastranversion: self.config.set('Nastran', 'nastran_version_win', nastranversion)
            self.executable = self.config.get('Nastran', 'nastran_command_win')
            self.NastranFlags = 'scr=yes'
        elif self.platform == 'linux':
            if nastranversion: self.config.set('Nastran', 'nastran_version_linux', nastranversion)
            if ulimit:
                self.config.set('Nastran', 'nastran_ulim', ulimit)
            else:
                self.config.set('Nastran', 'nastran_ulim_str', '')

            self.executable = self.executable = self.config.get('Nastran', 'nastran_command_linux')
            self.NastranFlags = 'bat=no scr=yes sdirectory=%s' % self.tempfolder # continue here!
        else:
            logging.error('Operating system currently not supported!')

    def WriteDVFile(self, params, filename, DesVarIDs=None):
        '''
        Writes any kind of params to filename.p via pickle
        '''
        if DesVarIDs==None:
            DesVarIDs = np.linspace(1,len(params),len(params)).tolist()
        self.write_out_DesignVector(params, DesVarIDs, filename=filename)

    def Run(self):
        '''
        Executes a job with .inp or a script with .py
        '''
        if self.ext in self.StandardExtensions:
            call = self.executable + ' ' + self.inputfile + ' ' + self.NastranFlags

        else:
            logging.warning('Given file extension not in StandardExtensions:')
            for iExt in self.StandardExtensions:
                logging.warning('     - %s'%(iExt))
            logging.error('Specific extension "%s" case is not considered yet!'%(self.ext))
            sys.exit()

        ProjectPath = os.getcwd()
        os.chdir(self.workfolder)
        # Clean up old files
        tmpFileList = []
        tmpFileList.extend(glob.glob("*.op2"))
        tmpFileList.extend(glob.glob("*.op4"))
        tmpFileList.extend(glob.glob("*.pch"))
        tmpFileList.extend(glob.glob("*.f06"))
        tmpFileList.extend(glob.glob("*.f04"))
        tmpFileList.extend(glob.glob("*.DBALL"))
        tmpFileList.extend(glob.glob("*.log"))
        tmpFileList.extend(glob.glob("*.MASTER"))
        tmpFileList.extend(glob.glob("*.pch"))
        tmpFileList.extend(glob.glob("*.plt"))
        tmpFileList.extend(glob.glob("*.rcf"))
        tmpFileList.extend(glob.glob("*.asm"))
        tmpFileList.extend(glob.glob("*.aeso"))
        tmpFileList.extend(glob.glob("*.becho"))
        tmpFileList.extend(glob.glob("*.IFPDAT"))
        for fID in tmpFileList:
            try:
                os.remove(fID)
                logging.warning('File "%s" deleted before computation!'%(fID))
            except:
                continue
        # Run NASTRAN
        tStart = time.time()
        logging.info('MSC.Nastran solving...')
        try:
            # subprocess.check_output(call)
            os.system(call)
        except:
            logging.warning('Problem in calling nastran!')
        os.chdir(ProjectPath)
        NastranRuntime = time.time()-tStart
        Mins = math.floor(NastranRuntime/60.)
        Secs = (NastranRuntime/60.-math.floor(NastranRuntime/60.))*60.
        logging.info('MSC.Nastran finished! | elapsed time: %.0f mins %.0f secs'%(Mins,Secs))
        return NastranRuntime

    def LoadOP4File(self, filename):
        '''
        Loads any kind of responses from filename.p via pickle
        '''
        try:
            self.read_in_OP4_ascii(filename)
        except:
            ErrorStr = 'Two possible failures:\n' + \
                'Fail01: Requested result file "%s" not there!\nFail02: Binary op4 files not supported yet!'%(filename)
            logging.error(ErrorStr)
            sys.exit(ErrorStr)
        finally:
            logging.info('Result file "%s" has been read in'%(filename))


class CEAS():
    #TODO:  this job definition is incorrect, it needs to be derived from SimJob
    def __init__(self, foldername):

        logging.debug("initializing CEAS Class")

        self.localorcluster = 'local'
        if platform.system() == 'Windows':
            # self.newLine = '\r\n'
            self.platform = 'windows'
            self.tempfolder = os.path.join('c:\\temp', getpass.getuser())
            logging.debug('Running on Windows, setting tempfolder to: %s' % self.tempfolder)
        elif platform.system() == 'Linux':
            self.platform = 'linux'
            if self.localorcluster == 'local':
                # we want to run on the local computer, now we determine where to put the tmp-folder
                if os.environ.get('SLURM_JOBID') != '' and os.environ.get('SLURM_JOBID') is not None:
                    # if we are on a cluster node within a slurm job, we use the slurm job folder
                    self.tempfolder = os.path.join('/tmp', getpass.getuser()+'.'+os.environ.get('SLURM_JOBID'), 'tmp')
                    logging.info('running within SLURM, setting tempfolder to %s' % self.tempfolder)
                else:
                    # if we are running outside of slurm, we use /tmp/$username/
                    self.tempfolder = os.path.join('/tmp', getpass.getuser())
                    logging.info('running on linux outside of SLRUM , setting tempfolder to %s' % self.tempfolder)
            elif self.localorcluster == 'cluster':
                # we want to spawn a job using the queuing system - not yet implemented.
                logging.fatal('not implemented yet')
                sys.exit('not implemented yet')
            else:
                logging.fatal('local or cluster not set, could not determine tempfolder')
                sys.exit()
        else:
            logging.fatal('Platform could not be determined')
            sys.exit('Platform could not be determined')

        self.workfolder = os.path.join(self.tempfolder, foldername)
        if os.path.isdir(self.workfolder):
            shutil.rmtree(self.workfolder)
        os.mkdir(self.workfolder)
        os.chdir(self.workfolder)
        os.system('createCase')

    def runCommand(self, command):
        logging.debug('executing command %s' % command)
        os.chdir(self.workfolder)
        os.system(command)


class FluentRun():
    def __init__(self, foldername):

        logging.debug("initializing Fluent Class")
        if platform.system() == 'Windows':
            # self.newLine = '\r\n'
            self.platform = 'windows'
            logging.debug('Running on Windows')
            self.executable = 'C:\\Program Files\\ANSYS Inc\\v160\\fluent\\ntbin\\win64\\fluent '
        elif platform.system() == 'Linux':
            self.platform = 'linux'
            logging.debug('Running on Linux')
            self.executable = '. /etc/profile&&module load ansys/v160&&fluent '
        else:
            logging.fatal('Platform could not be determined')
            sys.exit('Platform could not be determined')

    def execute(self, dimension, nPro, GUI=True, inputfile = ''):
        """
        starts fluent
        arguments:
        -dimension: '2ddp' or '3ddp', specifies if to start 2 dimensinal or 3 dimensional
        -nPro: '-t + number of processors', e.g. '-t4' for 4 processors
        -GUI: start with GUI or not
        -inputfile: execution of an inputfile
        """
        if GUI:
            self.GUI = ' '
        else:
            self.GUI = ' -g '
        if inputfile == '':
            self.inputfile = inputfile
        else:
            self.inputfile = ' -i ' + inputfile
        try:
            Fluentprocess = subprocess.Popen(self.executable + dimension + ' ' + nPro + self.GUI +'-wait' + self.inputfile)
            Fluentprocess.wait()
        except:
            logging.fatal('Check arguments!')
            sys.exit('Check arguments!')


class StructureFluentCoupling(SimJob):
    def __init__(self, AbaqusStarter, CEASFolder, FluentCase, sourcefolder, inputfile, x0, DataTable, FEMFile, profileFile = '', ShowGUI = False, alpha = 0, betha = 0, projectfolder=None, jobname=''):
        """
        Runs a fluent study with different displacements of the mesh based on basevectors of strucutre mesh, 
        derived from Class SimJob
        """
        self.AbaqusStarter = AbaqusStarter
        self.CEASFolder = CEASFolder
        self.FluentCase = FluentCase
        self.FEMFile = FEMFile
        self.ShowGUI = ShowGUI
        self.runNumber = 0
        self.alpha = alpha
        self.betha = betha
        self.x0 = x0
        self.profileFile = profileFile
        try:
            self.FileName, self.ext = inputfile.split('.', 1)
        except:
            logging.warn(
                'No file extension given with variable inputfile! -> .inp was set as extension')
            self.FileName = inputfile
            self.ext == 'jou'

        if len(jobname)==0:
            jobname = cp.deepcopy(self.FileName)
        SimJob.__init__(self, sourcefolder, jobname, inputfile, projectfolder=projectfolder)
        self.__doBasicState(DataTable)
        self.__WriteComplNastranFile()



    def UpdateMesh(self, multipliers):
        '''        
        writes out the updated node coordinates in Nastran file 
        Parameters: \n
        -multipliers for the  model design variables       
        Returns:  \n
        - None
        '''
        import copy as cp
        import shutil as sh
        if os.path.isdir(os.path.join(self.projectfolder,'NastranDataUpd')):
            NastranObj = NastranClass(os.path.join(self.projectfolder,'NastranDataUpd'))
        else:
            os.mkdir(os.path.join(self.projectfolder,'NastranDataUpd'))
            NastranObj = NastranClass(os.path.join(self.projectfolder,'NastranDataUpd'))
        #nodes = self.AbaqusStarter.LoadPickleFile(os.path.join('PickleData','nodes.pkl'))
        #basevectors = self.AbaqusStarter.LoadPickleFile(os.path.join('PickleData','basevectors.pkl'))
        NodesUpd = cp.deepcopy(self.nodes)
        #NodesUpd = NodesUpd.tolist()
        #print NodesUpd
        #for p in range(len(NodesUpd)):
            #del NodesUpd[p][1]
        #NodesUpd = np.array(NodesUpd)
        for (iDV,BV) in zip(multipliers,[self.basevectors['ShapeVar'+str(i+1)] for i in range(len(self.basevectors))]):
            NodesUpd[:,1:] = NodesUpd[:,1:]+iDV*BV[:,1:]
        #NodesUpd = NodesUpd.tolist()
        #print NodesUpd
        #for q in range(len(NodesUpd)):
            #NodesUpd[q].insert(1,0)
        #NodesUpd = np.array(NodesUpd)
        NastranObj.Nodes = NodesUpd
        NastranObj.write_out_Nodes()
        NastranObj.Elements = cp.deepcopy(self.elements3)
        NastranObj.write_out_Elements(Eltype='CTRIA3')
        NastranObj.Elements = cp.deepcopy(self.elements4)
        NastranObj.write_out_Elements(Eltype='CQUAD4')
        sh.copy(os.path.join(self.sourcefolder,self.FEMFile),os.path.join(self.projectfolder,'NastranDataUpd'))

    def __WriteComplNastranFile(self):
        '''        
        writes out the node- and elementinformation in one file(CEAS compatible)
        Parameters: \n
        -None        
        Returns:  \n
        - the path to the file
        '''

        import string
        CurPath = self.projectfolder
        NastranNodeFile = open((CurPath+'/NastranData/Nodes.blk'),'r')
        lines = NastranNodeFile.read().split('\n')
        newlines = list()
        for (p,line) in enumerate(lines):   #changes nodefile in CEAS compatible 8chars file
            line = string.split(line,',')
            if len(line)>1:
                line[0] = string.ljust(line[0],8)
                line[0] = line[0][0:8]
                line[1] = string.rjust(line[1],8)
                line[1] = line[1][0:8]
                line[2] = string.rjust(line[2],8)
                line[2] = line[2][0:8]
                line[3] = string.ljust(line[3],8)
                line[3] = line[3][0:8]
                line[4] = string.ljust(line[4],8)
                line[4] = line[4][0:8]
                line[5] = string.ljust(line[5],8)
                line[5] = line[5][0:8]
            line = string.join(line,'')
            line += '\n'
            newlines.append(line)
        NastranNodeFile.close()
        NastranElementFile = open((CurPath+'/NastranData/Mesh.blk'),'r')
        meshlines = NastranElementFile.read().split('\n')
        newmeshlines = list()
        for (q, meshline) in enumerate(meshlines):  #changes meshfile in CEAS compatible 8chars file
            meshline = string.split(meshline,',')
            if len(meshline)>1:
                meshline[0] = string.ljust(meshline[0],8)
                meshline[0] = meshline[0][0:8]
                meshline[1] = string.rjust(meshline[1],8)
                meshline[1] = meshline[1][0:8]
                meshline[2] = string.rjust(meshline[2],8)
                meshline[2] = meshline[2][0:8]
                meshline[3] = string.ljust(meshline[3],8)
                meshline[3] = meshline[3][0:8]
                meshline[4] = string.ljust(meshline[4],8)
                meshline[4] = meshline[4][0:8]
                meshline[5] = string.ljust(meshline[5],8)
                meshline[5] = meshline[5][0:8]
                if len(meshline)>6:
                    meshline[6] = string.ljust(meshline[6],8)
                    meshline[6] = meshline[6][0:8]
            meshline = string.join(meshline,'')
            meshline += '\n'
            newmeshlines.append(meshline)
        newlines.extend(newmeshlines)
        print(newlines)
        NastranElementFile.close()
        self.CompleteNastranFilePath = CurPath+'/NastranData/completeNastranFile.nas'
        CompleteNastranFile = open(self.CompleteNastranFilePath,'w')
        CompleteNastranFile.writelines(newlines)
        CompleteNastranFile.close()


    def WriteOutDisplacements(self, multipliers):
        '''
        writes out a displacement file for CEAS
        Parameters: \n
        -multipliers for the  model design variables      
        Returns:  \n
        - the path to the displacement file        
        '''
        import copy as cp
        CurPath = self.projectfolder
        import string
        #NodesBasic =  self.AbaqusStarter.LoadPickleFile(os.path.join('PickleData','nodes.pkl'))
        displacements = np.zeros((len(self.basevectors['ShapeVar1']),4))
        displacements[:,0] = cp.deepcopy(self.basevectors['ShapeVar1'][:,0])
        #displacements[:,0] = NodesBasic[:,0]
        for (iDV,BV) in zip(multipliers,[self.basevectors['ShapeVar'+str(i+1)] for i in range(len(self.basevectors))]):
            displacements[:,1:] += iDV*BV[:,1:]
        displacements = displacements.tolist()
        for i in range(len(displacements)):
            for j in range(4):
                displacements[i][j] = str('{0:.7f}'.format(displacements[i][j]))
                displacements[i][j] = string.ljust(displacements[i][j],8)
                displacements[i][j] = displacements[i][j][0:8]
            displacements[i] = string.join(displacements[i],'')
            displacements[i] += '\n'
        DisplacementNastranFilePath = CurPath+'/NastranData/displacementNastranFile' + str(self.runNumber + 1) + '.nas'
        DisplacementNastranFile = open((DisplacementNastranFilePath),'w')
        DisplacementNastranFile.writelines(displacements)
        DisplacementNastranFile.close()
        return DisplacementNastranFilePath

    def __doBasicState(self, DataTable):
        import copy as cp
        import shutil as sh
        if os.path.isdir(os.path.join(self.projectfolder,'NastranData')):
            NastranObj = NastranClass(os.path.join(self.projectfolder,'NastranData'))
        else:
            os.mkdir(os.path.join(self.projectfolder,'NastranData'))
            NastranObj = NastranClass(os.path.join(self.projectfolder,'NastranData'))
        if self.AbaqusStarter: #checks if an Abaqus analyse has to be performed to receive mesh and basevectors
            self.AbaqusStarter.WritePickleFile(self.x0, 'DesVar.p')
            self.AbaqusStarter.Run(showGUI = self.ShowGUI)

            # Write nastran deck
            # --------------------------------------------------------------- #
            self.nodes = cp.copy(self.AbaqusStarter.LoadPickleFile(os.path.join('PickleData','nodes.pkl')))
            NastranObj.Nodes = cp.deepcopy(self.nodes)
            NastranObj.write_out_Nodes()
            self.elements3 =cp.copy(self.AbaqusStarter.LoadPickleFile(os.path.join('PickleData','elements3.pkl')))
            NastranObj.Elements = cp.deepcopy(self.elements3)
            NastranObj.write_out_Elements(Eltype='CTRIA3')
            self.elements4 = cp.copy(self.AbaqusStarter.LoadPickleFile(os.path.join('PickleData','elements4.pkl')))
            NastranObj.Elements = cp.deepcopy(self.elements4)
            NastranObj.write_out_Elements(Eltype='CQUAD4')
            self.basevectors = self.AbaqusStarter.LoadPickleFile(os.path.join('PickleData','basevectors.pkl'))
            NastranObj.Basevectors = cp.deepcopy(self.basevectors)
            NastranObj.write_out_Basevectors()
        else:
            self.DataTable = pickle.load(open(os.path.join(self.sourcefolder,DataTable),'rb'))
            try:
                self.nodes = cp.copy(self.DataTable['Nodes'])
                #nodes = np.insert(nodes, 0, np.arange(0,len(nodes)+1), axis=1)
                #nodes = nodes[1:]
                NastranObj.Nodes = cp.deepcopy(self.nodes)  #format for NastranObj.Nodes 2d array, each row contains 1. node number, 2.-4.: x,y,z coordinates
                NastranObj.write_out_Nodes()
                elements = cp.copy(self.DataTable['Elements'])
                #elements = np.insert(elements, 0, np.arange(0,len(elements)+1), axis=1)
                self.elements3 = list()
                self.elements4 = list()
                for element in elements:    #split in to CTria and CQuad elements
                    if len(element) == 4:
                        self.elements3.append(element.tolist())
                    elif len(element) == 5:
                        self.elements4.append(element.tolist())
                self.elements3 = np.array(self.elements3)
                self.elements4 = np.array(self.elements4)
                #nodes = nodes[1:]
                NastranObj.Elements = cp.deepcopy(self.elements3)  #format for NastranObj.Elements 2d array, each row contains 1. element number, 2.-4.(5.): numbers of nodes building element
                NastranObj.write_out_Elements(Eltype='CTRIA3')
                NastranObj.Elements = cp.deepcopy(self.elements4)
                NastranObj.write_out_Elements(Eltype='CQUAD4')
                self.basevectors = cp.copy(self.DataTable['BaseVectors'])
                self.basevectorsnames = cp.copy(self.DataTable['BaseVectorsNames'])
                for (index,key) in enumerate(self.basevectorsnames):
                    self.basevectors['ShapeVar' + str(index+1)] = self.basevectors[key]
                    del self.basevectors[key]
                NastranObj.Basevectors = cp.deepcopy(self.basevectors)  #format for NastranObj.Basevectors dictionary with shape varibales as keys, each key contains a 2d array, each row of array contains 1. node number, 2.-4.: normed displacement for shape variable in x,y,z directions
                NastranObj.write_out_Basevectors()
            except:
                logging.warning('nodes, elements and basevectors need right input format')
        sh.copy(os.path.join(self.sourcefolder,self.FEMFile),os.path.join(self.projectfolder,'NastranData'))

    def runCEAS(self, multipliers, thread):

        '''        
        execute a CEAS run
        Parameters: \n
        -the number of current run     
        Returns:  \n
        - None
        '''
        import time
        import shutil as sh
        #multipliers from optimization
        DisplacementNastranFilePath = self.WriteOutDisplacements(multipliers)
        if os.path.isdir(os.path.join(self.tempfolder,self.CEASFolder)):
            pass
        else:
            os.mkdir(os.path.join(self.tempfolder, self.CEASFolder))
        os.chdir(os.path.join(self.tempfolder,self.CEASFolder))
        CurPath = os.getcwd()

        if self.runNumber == 0:
            os.system('createCase')
            sh.copy(self.CompleteNastranFilePath,os.path.join(CurPath,'const/nastran.model'))
            sh.copy(os.path.join(self.sourcefolder, 'inverseFeMapperDict'),os.path.join(CurPath,'system'))
            sh.copy(os.path.join(self.sourcefolder,self.FluentCase), os.path.join(CurPath,'const/fluent.model'))
            #os.chdir(os.path.join(CurPath,'const/nastran.model'))
            #os.system('wetelmgConvert -i ' + os.path.basename(self.CompleteNastranFilePath) + ' -d ' + str(self.maxElementId))
            #os.chdir(CurPath)
            os.system('nastranMeshPartToCEAS -i ' + os.path.basename(self.CompleteNastranFilePath) + ' -n structure')
            #os.system('nastranMeshToCEAS -i ' + os.path.basename(self.CompleteNastranFilePath) + ' -n structure')
            os.system('fluentMeshToCEAS -i ' + self.FluentCase + ' -n fluid -t ' + str(thread))
            os.system('nodeDirectionVectorWeighting -n structure')
            os.system('cShapeFunctionMapper -m structure -s fluid')
            os.system('fluentMeshInit -n fluid -t ' + str(thread) + ' -w ascii')
        sh.copy(DisplacementNastranFilePath,os.path.join(CurPath,'const/nastran.model'))
        if os.path.isdir(os.path.join(self.tempfolder,self.CEASFolder,'DATA_FLUENT/Disp.dat')):
            os.remove(os.path.join(self.tempfolder,self.CEASFolder,'DATA_FLUENT/Disp.dat'))
        os.system('dispVecToCEAS -i ' + os.path.basename(DisplacementNastranFilePath) + ' -n structure -d displacements' + str(self.runNumber + 1))
        os.system('mapDisplacement -m structure -d displacements' + str(self.runNumber + 1))
        os.system('ceasDataToFluent -n fluid -d displacements' + str(self.runNumber + 1) + ' -w ascii')
        while not os.path.exists(os.path.join(self.tempfolder,self.CEASFolder,'DATA_FLUENT/Disp.dat')):
            time.sleep(1)

    def AeroAnalyse(self, velocity = None):
        '''if os.path.isdir(os.path.join(self.tempfolder,'FluentAnalyse')):
            pass
        else:
            os.mkdir(os.path.join(self.tempfolder,'FluentAnalyse'))'''
        import time
        import subprocess
        import shutil as sh
        if self.runNumber == 0:
            if os.path.isdir(self.workfolder):
                sh.rmtree(self.workfolder)
            sh.copytree(os.path.join(os.path.dirname(self.sourcefolder),'CEAS_udfLib'), self.workfolder)
            sh.copy(os.path.join(self.tempfolder,self.CEASFolder,'const/fluent.model',self.FluentCase),self.workfolder)
            sh.copy(os.path.join(self.tempfolder,self.CEASFolder,'const/data/FLUENT_INIT.dat'),self.workfolder)
        sh.copy(os.path.join(self.sourcefolder, self.inputfile), self.workfolder)
        if self.profileFile=='':
            pass
        else:
            sh.copy(os.path.join(self.sourcefolder, self.profileFile), self.workfolder)
        if velocity:
            JouFile = open(os.path.join(self.workfolder, self.inputfile), 'r')
            Lines = JouFile.readlines()
            JouFile.close()
            Lines.insert(6,'/define/parameters/input-parameters edit "velocity" "velocity" '+ str(velocity) + '\n')
            JouFile = open(os.path.join(self.workfolder, self.inputfile), 'w')
            JouFile.writelines(Lines)
            JouFile.close()
        #if os.path.isfile(os.path.join(self.tempfolder,'FluentAnalyse','Disp-B.dat')):
            #os.remove(os.path.join(self.tempfolder,'FluentAnalyse','Disp-B.dat'))
        sh.copy(os.path.join(self.tempfolder,self.CEASFolder,'DATA_FLUENT/Disp.dat'),os.path.join(self.workfolder, 'DISP.dat'))
        #if os.path.isfile(os.path.join(self.tempfolder, 'FluentAnalyse', self.journalFile)):
            #os.remove(os.path.join(self.tempfolder,'FluentAnalyse',self.journalFile))
        #Journal = open(os.path.join(self.tempfolder, 'FluentAnalyse', self.journalFile),'r+')
        #journalLines = Journal.readlines()
        #index = journalLines.index('/file/read-case\n')
        #journalLines[index] = '/file/read-case ' + self.FluentCase + '\n'
        #Journal.writelines(journalLines)
        #Journal.close()
        os.chdir(self.workfolder)

        #TODO: put this path in the config file
        Fluentprocess = subprocess.Popen('C:\\Program Files\\ANSYS Inc\\v160\\fluent\\ntbin\\win64\\fluent 3ddp -t4 -g -wait -i ' + self.inputfile)
        Fluentprocess.wait()
        time.sleep(2)
        #os.system('fluent 3ddp -t4 -g -wait -i ' + self.journalFile)
        #os.system('set PATH=%PATH%;C:\Program Files\ANSYS Inc\v160\fluent\ntbin\win64')
        #while not os.path.exists(os.path.join(self.tempfolder, 'FluentAnalyse', 'FORCE-RESPONSE.dat')):
            #time.sleep(1)

    def getResults(self):
        from math import sin, cos
        import shutil as sh
        if os.path.isdir(os.path.join(self.projectfolder, 'FluentSimulationData')):
            pass
        else:
            os.mkdir(os.path.join(self.projectfolder, 'FluentSimulationData'))
        if os.path.isfile(os.path.join(self.workfolder, 'FORCE-RESPONSE.dat')):
            ResultFile = open(os.path.join(self.workfolder, 'FORCE-RESPONSE.dat'), 'r')
            Forces = []
            for line in ResultFile.readlines():
                start = line.index('=') + 2
                end = line.index(';')
                Forces.append(float(line[start:end]))
            ResultFile.close()
            ForcesTransformed = np.array(Forces)
            ForcesTransformed[0] = cos(self.betha)*(cos(self.alpha)*Forces[0] + sin(self.alpha)*Forces[1]) - sin(self.betha)*Forces[2]
            ForcesTransformed[1] = -sin(self.alpha)*Forces[0] + cos(self.alpha)*Forces[1]
            ForcesTransformed[2] = sin(self.betha)*(cos(self.alpha)*Forces[0] + sin(self.alpha)*Forces[1]) + cos(self.betha)*Forces[2]
            pickle.dump(Forces, open(os.path.join(self.projectfolder,'forces'+str(self.runNumber+1))+'.p','wb'))
            pickle.dump(ForcesTransformed, open(os.path.join(self.projectfolder,'forcesTransformed'+str(self.runNumber+1))+'.p','wb'))
            print('force computation completed successfully')
            sh.copy('simulation.out',os.path.join(self.projectfolder, 'FluentSimulationData','simulation' + str(self.runNumber+1) + '.out'))
            sh.copy('FORCE-RESPONSE.dat',os.path.join(self.projectfolder, 'FluentSimulationData','FORCE-RESPONSE' + str(self.runNumber+1) + '.dat'))
            sh.copy('DISP.dat',os.path.join(self.projectfolder, 'FluentSimulationData','Disp' + str(self.runNumber+1) + '.dat'))
            sh.copy(os.path.splitext(os.path.basename(self.FluentCase))[0]+ 'Update.cas',os.path.join(self.projectfolder, 'FluentSimulationData',os.path.splitext(os.path.basename(self.FluentCase))[0]+ 'Update' + str(self.runNumber+1) + '.cas'))
            sh.copy(os.path.splitext(os.path.basename(self.FluentCase))[0]+ 'Update.dat',os.path.join(self.projectfolder, 'FluentSimulationData',os.path.splitext(os.path.basename(self.FluentCase))[0]+ 'Update' + str(self.runNumber+1) + '.dat'))
            #os.remove('simulation.out')
            os.remove('FORCE-RESPONSE.dat')
            self.runNumber += 1
            os.chdir(self.projectfolder)
            print(ForcesTransformed[1])
            print(ForcesTransformed[0])
            return [[Forces[0], Forces[1], Forces[2]],[ForcesTransformed[0], ForcesTransformed[1], ForcesTransformed[2]]]
        else:
            print('fluent run failed')
            self.runNumber += 1
            os.chdir(self.projectfolder)
            return None