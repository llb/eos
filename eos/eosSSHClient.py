# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import paramiko
import os
import sys
import zipfile
import logging
import random
import ConfigParser
import getpass
from datetime import datetime

class eosSSHClient(paramiko.SSHClient):

    def __init__(self, username=None, password=None, hostname=None, jobname='', configfile='eos.cfg'):
        """
        Sets up an SSH client for job submission on the LLB cluster. This can
        :param username:      user name for ssh login, required
        :param password:      password for ssh login, required
        :param hostname:      hostname for ssh login, default = 'master.llb.mw.tum.de'
        :param jobname:       cluster job name, optional. default = 'eos_projectname_YYYY-MM-DD_HH_MM_SS'
        :return:  eosSSHClient Class Object
        """

        paramiko.SSHClient.__init__(self)

        # read configuration file
        config_path = os.path.abspath("..\\..\\" + configfile)

        if os.path.isfile(config_path):
            logging.info('Reading config file "%s"' % config_path)
            self.config = ConfigParser.ConfigParser()
            self.config.read(config_path)
        else:
            msg = 'EOS config file "%s" could not be found. Please copy "eos.cfg.sample" and modify as needed' % config_path
            logging.fatal(msg)
            sys.exit(msg)

        if not username:
        # no username was given in class call, using user from config file
            self.username = self.config.get('Cluster', 'ssh_username')
            if not self.username:
                # no ssh username in config file, using system user name
                self.username = getpass.getuser()
                logging.info('SSH username not defined in config file, using default user.')
        else:
            self.username = username

        logging.info('using SSH username "%s"' % self.username)

        if hostname:
            self.hostname = hostname
        else:
            self.hostname = self.config.get('Cluster', 'hostname')
        logging.info('using SSH hostname "%s"' % self.hostname)

        if password:
            self.password = password
        else:
            win_user = self.config.get('Cluster', 'win_user')
            if not win_user:
                # no win_user is set, get it from environment and update the config so that the pwfile will be correctly generated
                self.config.set('Cluster', 'win_user', getpass.getuser())

            self.pwfile = self.config.get('Cluster', 'pwfile')
            # read the password from pwfile
            if not os.path.isfile(self.pwfile):
                sys.exit('Please save your password in "%s", path can be changed in the config file - Exiting' % self.pwfile)
            else:
                with open(self.pwfile, "r") as f:
                    self.password = f.read().replace('\n', '').replace('\r\n', '')
                logging.info('Read password from "%s"' % self.pwfile)

        self.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.LocalSeparator = '\\'

        self.connect(self.hostname, username=self.username, password=self.password)
        self.sftp = self.open_sftp()

        # set the project name (= folder name)
        self.projectname = os.path.split(os.getcwd())[-1]

        # compose the job name
        timestring = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        if jobname:
            #TODO: test this: self.jobname = self.TextToPath(jobname)
            self.jobname = jobname
        else:
            self.jobname = 'eos_' + self.projectname + '_' + timestring

        # define the jobdir on the cluster
        self.jobdir = "/home/" + self.username + "/jobs/" + self.jobname

        # create the jobdir:
        self.mkdir(self.jobdir)

        # upload the config file

        target_path = self.jobdir + '/' + configfile
        if os.path.isfile(config_path):
            logging.info('uploading config file "%s" to "%s"' % (config_path,target_path))
            self.sftp.put(config_path, target_path)
        else:
            logging.fatal('could not find config file "%s"' % config_path)
            sys.exit()


    def TextToPath(self, text):
        import string
        valid_chars = "-_.()%s%s" % (string.ascii_letters, string.digits)
        path = ''.join(c for c in text if c in valid_chars)
        return path


    @staticmethod
    def normalize_dirpath(dirpath):
        while dirpath.endswith("/"):
            dirpath = dirpath[:-1]
        return dirpath

    #def eos_open_sftp(self):
    #    self.sftp = self.open_sftp()

    def mkdir(self, remotepath, mode=0777, intermediate=False):
        remotepath = self.normalize_dirpath(remotepath)
        if intermediate:
            try:
                self.sftp.mkdir(remotepath, mode=mode)
            except IOError, e:
                self.mkdir(self.sftp, remotepath.rsplit("/", 1)[0], mode=mode,
                           intermediate=True)
                return self.sftp.mkdir(remotepath, mode=mode)
        else:
            self.sftp.mkdir(remotepath, mode=mode)

    def put_dir(self,  localpath, remotepath, preserve_perm=True):
        """upload local directory to remote recursively"""

        assert remotepath.startswith('/'), '%s must be absolute path' % remotepath

        # normalize
        localpath = self.normalize_dirpath(localpath)
        remotepath = self.normalize_dirpath(remotepath)

        sftp = self.sftp


        try:
            sftp.chdir(remotepath)
            localsuffix = localpath.rsplit(self.LocalSeparator, 1)[1]
            print("localsuffix: " + localsuffix)
            remotesuffix = remotepath.rsplit("/", 1)[1]
            print("remotesuffix: " + remotesuffix)

            if localsuffix != remotesuffix:
                remotepath = os.path.join(remotepath, localsuffix)
        except IOError, e:
            pass

        for root, dirs, fls in os.walk(localpath):
            print (root, dirs, fls)
            prefix = os.path.commonprefix([localpath, root])
            suffix = root.split(prefix, 1)[1]
            if suffix.startswith(self.LocalSeparator):
                suffix = suffix[1:]

            remroot = os.path.join(remotepath, suffix).replace(self.LocalSeparator, "/")
            print("remroot: " + remroot)
            print("remotepath: " + remotepath)
            print("suffix: " + suffix)

            try:
                sftp.chdir(remroot)
            except IOError, e:
                if preserve_perm:
                    mode = os.stat(root).st_mode & 0777
                else:
                    mode = 0777
                # print("mkdir " + remroot)
                self.mkdir(remroot, mode=mode, intermediate=True)
                sftp.chdir(remroot)

            for f in fls:
                remfile = os.path.join(remroot, f).replace(self.LocalSeparator, "/")
                localfile = os.path.join(root, f)
                sftp.put(localfile, remfile)
                print ("putting " + localfile + " to " + remfile)
                if preserve_perm:
                    sftp.chmod(remfile, os.stat(localfile).st_mode & 0777)

    def zipdir(self, rootdir, foldername, zipname='', zipfolder='', excludes=[]):
        """
        zip a folder to a .zip file
        excludes: list of strings, folders that will be excluded from the zip
        """


        if not zipfolder:
            zipfolder = "C:\\temp\\"

        # open a zip file as foldername.zip
        if not zipname:
            zipname = foldername + '.zip'

        ziptemp = os.path.join(zipfolder, zipname)

        zf = zipfile.ZipFile(ziptemp, mode='w')
        logging.info('zipping "' + os.path.join(rootdir, foldername) + '" to "' + foldername + '.zip"')

        zippath = os.path.join(rootdir, foldername)

        # save the current work dir so that we can return later. we need to change to the zip dir to get relative paths.
        current_dir = os.getcwd()
        os.chdir(rootdir)

        for root, dirs, files in os.walk(zippath):
            for file in files:
                skip = False
                if '.git' in root:
                    skip = True
                if file.endswith('.pyc'):
                    skip = True
                for exclude in excludes:
                    if exclude in root:
                        skip = True

                if not skip:
                    relative_dir = root.replace(rootdir + self.LocalSeparator, '')  # cut away the root dir to geht the relative dir.
                    zf.write(os.path.join(relative_dir, file))

        zf.close()
        os.chdir(current_dir)

        return zipfolder, zipname

    def put_dir_zipped(self, rootdir, foldername, subfolder='', excludes=[]):
        """
        upload a folder to the ssh server, zipping it for faster transfer
        rootdir: (string)                     the location of the folder that shall be uploaded
        foldername: (string)                  the name of the folder to be uplaoded
        subfolder: (string, optional)         specify, if only a subfolder shall be uploaded. The relative directory structure will be kept.
        excludes: (list of strings, optional) foldernames in this list will not be uploaded. Also works in subfolders.
        """

        if subfolder:
            targetdir = self.jobdir + '/' + subfolder
        else:
            targetdir = self.jobdir

        # zip the folder to a file
        timestring = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        zipfolder, zipname = self.zipdir(rootdir, foldername, zipname='temp_' + timestring + '.zip', excludes=excludes)
        sourcepath = os.path.join(zipfolder, zipname)
        targetpath = targetdir + '/' + zipname

        # create the target folder
        try:
            self.mkdir(targetdir)
        except IOError:
            logging.info('directory "%s" already exists, skipping' % targetdir)

        # upload the zip file
        logging.info('uploading "%s" to "%s"' % (sourcepath,targetdir))
        self.sftp.put(sourcepath, targetpath)
        # delete the local temporary zip file
        os.remove(sourcepath)

        stdin, stdout, stderr = self.exec_command('cd ' + targetdir + '; unzip ' + zipname + '; rm ' + zipname)
        data = stdout.readlines()
        for line in data:
            print line

    def put_slurm_script(self, numcpu=4, nodename='', runfile='run.py', UseXvfb=False):
        """ Compose the script for the SLURM queuing system. This sets all SLURM Variables,
            copies the files from the master to the cluster tmp dir and starts the eos runfile (usually run.py)
            This method also prepares the job directory on the cluster

            :param numcpu: (integer, optional, default=4) the number of CPU cores reserved on the queuing system
            :param nodename: (string, optional) request a specific node for the job
            :param runfile: (string, optional, default="run.py") the file to start on the cluster node, must be inside the project directory
            :param UseXvfb: (bool, optional, default=True) Start virtual X server for plotting?
        """



        # build the slurm script
        s = ''
        s += '#!/bin/bash\n'
        s += '#SBATCH --get-user-env\n'
        s += '#SBATCH --job-name=' + self.jobname + '\n'
        s += '#SBATCH --output=../../results/eos.%j.out\n'
        s += '#SBATCH --error=../../results/eos.%j.err\n'
        s += '#SBATCH --nodes=1\n'
        s += '#SBATCH --cpus-per-task=' + str(numcpu) + '\n'
        s += '#SBATCH --mincpus=' + str(numcpu) + '\n'
        if nodename:
            s += '#SBATCH --nodelist=' + nodename + '\n'
        s += '\n'
        s += 'JOB_ID=$SLURM_JOB_ID\n'
        s += '\n'
        s += 'jobname="' + self.jobname + '"\n'
        s += 'jobdir=' + self.jobdir + '\n'
        s += 'tmpdir=/tmp/$USER.$JOB_ID\n'
        s += 'resdir=/home/$USER/results/$jobname.$JOB_ID\n'
        s += '\n'
        s += 'mkdir $tmpdir\n'
        s += 'mkdir $resdir\n'
        s += 'cp -r $jobdir/* $tmpdir\n'
        s += 'cd $tmpdir/projects/' + self.projectname + '\n'
        s += '\n'
        s += '. /etc/profile\n'

        if UseXvfb:
            # if we use Xvfb (X virtual frame buffer) for screenshots etc., we start it, and save the PID to kill later
            XvfbPort = str(random.randint(10, 60000))  #  we use a random Display Port for the Xvfb
            s += '\nXvfb :' + XvfbPort + ' &\n'
            s += 'sleep 5\n'
            s += 'xvfbpid=$!\n'
            s += 'export DISPLAY=:' + XvfbPort + '\n\n'

        s += 'module load pyopt\n'
        s += 'python ' + runfile + ' > ' + self.projectname + '.out.txt\n'
        s += '\n'

        if UseXvfb:
            # after the run, kill the Xvfb process
            s += '\nkill $xvfbpid\n'
            s += 'export DISPLAY=\n\n'

        s += 'cd $tmpdir\n'
        s += '\n'
        s += 'mv * $resdir\n'
        s += 'cd /tmp\n'
        s += 'rm -r $tmpdir\n'

        shellfilename = 'eos_flettner.sh'

        # write the previously created script to a local file for uploading
        f = file(shellfilename, 'wb')  # using wb instead of w to prevent converstion of unix file endings to windows endings
        f.write(s)
        f.close()

        # if it is missing, create the result directory within the eos dir on the cluster, this is not the result dir of the cluster job!
        resultdir = self.jobdir + '/results'
        try:
            self.mkdir(resultdir)
            logging.info('prepared result directory "%s"' % resultdir)
        except IOError:
            logging.info("result directory %s already exists" % resultdir)

        # compose source (local shell file) and target (shell file in job dir) paths
        sourcepath = os.path.join(os.getcwd(), shellfilename)
        targetpath = self.jobdir + '/' + shellfilename

        # upload the shell file
        logging.info('sftp put "%s" to "%s"' % (sourcepath, targetpath))
        self.sftp.put(sourcepath, targetpath)

        # remove the temporary local shell file
        os.remove(sourcepath)

        return shellfilename

