# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

def comb(a,nQuad):
    if np.size(a)==1:
        c=a**2
    else:
        c=np.zeros([nQuad,])
        nx=np.size(a,0)
        IndexC = 0
        for i in range(nx):
            for j in range(i,(nx)):
                c[IndexC] = a[i]*a[j]
                IndexC+=1
    return c

class PolyReg():
    def __init__(self, PolyDegree=2, PrintInfos=False):
        self.PolyDegree = PolyDegree
        self.PrintInfos = PrintInfos
    def fit(self, xDoE, rDoE):
        nPoly = self.PolyDegree
        if np.size(xDoE)==1:
            nx=1
            nDesign=1
        elif np.shape(xDoE)[0]==np.size(xDoE):
            nx=1
            nDesign=np.shape(xDoE)[0]
        elif np.shape(xDoE)[1]==np.size(xDoE):
            nx=1
            nDesign=np.shape(xDoE)[1]
        else:
            nx=np.size(xDoE,1) #np.shape(x)[1]
            nDesign=np.size(xDoE,0)
        nQuad=(nx**2+nx)/2
        ConstValues=np.ones([nDesign, 1])
        QuadValues=np.zeros([nDesign, nQuad])
        LinValues=np.zeros([nDesign, nx])
        for i in range(nDesign):
            QuadValues[i]=comb(xDoE[i],nQuad)
            LinValues[i]=xDoE[i]
        ValueMatrix=np.concatenate((ConstValues,LinValues,QuadValues),1)
        ValueMatrixTra=np.transpose(ValueMatrix)            # [1 x x^2 ...] evaluated at support points and transposed
        resultmatrix= np.dot(ValueMatrixTra, ValueMatrix)
        resultmatrixInv=np.linalg.inv(resultmatrix)
        cReg= np.dot((np.dot(resultmatrixInv, ValueMatrixTra)), rDoE) # Coefficients of polynomial regression
        self.cReg = cReg
        self.ValueMatrix = ValueMatrix
        self.nx = nx
        if self.PrintInfos:
            print 'Polynomial regression coefficients computed!'
        return self
    def predict(self,x):
        if hasattr(self, 'cReg')==False:
            if self.PrintInfos:
                print 'Idiot, compute polynomial regression coefficients first -> .Fit(x,y)!'
            return []
        if np.size(x)==1:
            nx=1
            nDesign=1
        elif np.shape(x)[0]==np.size(x):
            nx=np.shape(x)[0]
            nDesign=1
        else:
            nx=np.size(x,1) #np.shape(x)[1]
            nDesign=np.size(x,0)
        nQuad=(nx**2+nx)/2
        ConstValues=np.ones([nDesign, 1])
        QuadValues=np.zeros([nDesign, nQuad])
        LinValues=np.zeros([nDesign, nx])
        if nDesign==1:
            QuadValues[0]=comb(x,nQuad)
            LinValues[0]=x
        else:
            for i in range(nDesign):
                QuadValues[i]=comb(x[i],nQuad)
                LinValues[i]=x[i]
        ValueMatrix=np.concatenate((ConstValues,LinValues,QuadValues),1)
        FunctionValue = np.dot(ValueMatrix,self.cReg)
        if np.size(FunctionValue)==1:
            FunctionValue=FunctionValue[0]
        return FunctionValue
    def getRSAcoef(self):
        if hasattr(self, 'cReg'):
            return self.cReg
        else:
            if self.PrintInfos:
                print 'Idiot, compute polynomial regression coefficients first -> .Fit(x,y)!'
            return []
    def getDetails(self):
        Str = 'nPoly: '+str(self.PolyDegree)+', Dimension of regression: '+str(self.nx)
        Str += '\n  -> Coefficients: a0 + a1 x0 + a2 x1 + a3 x0**2 + a4 x0 x1 + a5 x1**2'
        return Str
    def MinimalSamplingSize(self,nDim,PolyNomialOrder=2):
        if PolyNomialOrder==1:
            return nDim+1
        elif PolyNomialOrder==2:
            return nDim+1+(nDim**2+nDim)/2
        else:
            if self.PrintInfos:
                print 'Not implemented yet!'
            return []
    def __str__(self):
        return 'Polynomial regression model of order %i'%(self.PolyDegree)

if __name__ == "__main__":
    print 'This is a Test'
    def Fct(x):
        return x[0**2]+3*x[0]+2*x[0]*x[1]-2*x[1]**2-6*x[1]+5
    # Settings
    RSAlower             = [-5, -5]       # Lower bound of the sample points
    RSAupper             = [5, 5]       # Upper bound of the sample points
    Dimension            = 2

    # Initialize object
    Test = PolyReg(PrintInfos=True)

    # Get minimal sampling size
    Oversampling         = 1
    Samples              = Test.MinimalSamplingSize(Dimension)*(1+Oversampling)

    # Sampling
    import doe_lhs
    import numpy as np
    design = doe_lhs.lhs(Dimension, samples=Samples, criterion=None, iterations=None)
    for k in range(len(RSAlower)):
        design[:,k]= (RSAupper[k]-RSAlower[k])*design[:,k]  + RSAlower[k]
    print design

    # Compute yVals
    yVals = []
    for k in range(Samples):
        yVals.append(Fct(design[k,:]))
    print yVals

    # Fitting
    Test.fit(design,yVals)

    # Test via predict
    Error = Test.predict([0, 0]) - Fct([0,0])
    print Error



