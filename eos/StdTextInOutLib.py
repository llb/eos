# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.
"""
Basic function library for text input/output

Author: Marc-André Koehler
Last Edited: 16.12.14

"""

import numpy as np
        
def read_in(filename):
    """Read in text file as list.

    Args:
      filename (string): The name of the text file to be read
    """
    f = open(filename, 'r')
    List = f.readlines()
    f.close()
    return List
    
def write_out(Str,filename,option):
    """Write out string in text file.

    Args:
      Str (string): The String to be written to text file\n
      filename (string): The name of the text file to be read\n
      option (string):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...
    """
    f = open(filename, option)
    f.write(Str)
    f.close()
          
def extract(List,BeginStr=None,EndStr=None):
    """Extract data from List between occurrences of Beginstr and EndStr.

    Args:
      List (List): List containing the text as linewise data \n
      BeginStr (string, optional): Read in begins in line beginning with BeginStr\n
      EndStr (string, optional):  Read in ends in line beginning with EndStr
    """
    if BeginStr==None and EndStr==None:
        lineBegin=0
        lineEnd=len(List)+1
    else:            
        # Search for Begin
        for iLine in List:
            if iLine.startswith(BeginStr):
                lineBegin = List.index(iLine)+1
        # Search for End
        found=False
        for iLine in List:
            if iLine.startswith(EndStr) and List.index(iLine)>lineBegin:
                lineEnd = List.index(iLine)
                found=True
            if found:
                break
        # End of file reached?    
        if not found:
            lineEnd=len(List)
    # Extract Information
    List = List[lineBegin:lineEnd]    
    return List
    
def split2list(List,Delimiter):
    """Split List featuring linewise data at delimiter and save to numpyArray.

    Args:
      List (List): List featuring linewise data\n
      delimiter (string): Delimiter used to split each line into several elements
    """
    ValueList = []
    # Split line array
    for iLine in List:
        tmpLineSplit = iLine.split(Delimiter)
        #ValueList.append([float(item) for item in tmpLineSplit])
        ValueList.append([item for item in tmpLineSplit])  
    return ValueList
    
def split2array(List,Delimiter):
    """Split List featuring linewise data at delimiter and save to numpyArray.

    Args:
      List (List): List featuring linewise data\n
      delimiter (string): Delimiter used to split each line into several elements
    """
    ValueList = []
    # Split line array
    for iLine in List:
        tmpLineSplit = iLine.split(Delimiter)
        #ValueList.append([float(item) for item in tmpLineSplit])
        ValueList.append([item for item in tmpLineSplit])
    # Assign to numpy array     
    ValueArray = np.array(ValueList)
    ValueArray = ValueArray.astype(float)    
    return ValueArray

def chunks(Str, n):
    """Produce `n`-character chunks from string `Str`."""
    i=0
    List=[]
    for start in range(0, len(Str), n):
          List.extend([Str[start:start+n]])    
          i=i+1 
    return List
    