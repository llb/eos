# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import logging
import math
import numpy as np
from datetime import datetime
from matplotlib2tikz import save as tikz_save
import csv
import os

def CompareCurves(curve1, curve2, numpoints=50, saveplot=True, filename='', saveFolder=''):
    """
    Compares two 2d curves by creating interpolated splines and calculating an approximation to the RMS (see distplot.png)
    :param curve1: 2D-Array of the x and y coordinates of the first curve
    :param curve2: 2D-Array of the x and y coordinates of the second curve
    :param numpoints: number of points used for calculating the difference
    :param filename: (optional) custom filename. Default: %Y-%m-%d_%H-%M-%S.png
    :return:
    """

    import scipy.interpolate
    import matplotlib.pyplot as plt

    # calculate the coefficients of the two splines, that will represent the two curves
    tck1, u1 = scipy.interpolate.splprep([curve1[0], curve1[1]])
    tck2, u2 = scipy.interpolate.splprep([curve2[0], curve2[1]])

    delta = 1./numpoints
    unew = np.arange(0, 1+delta, delta)

    # evaluate the two splines at a defined number of points (numpoints) along the spline
    out1 = scipy.interpolate.splev(unew, tck1)
    out2 = scipy.interpolate.splev(unew, tck2)

    if saveplot:
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.set_aspect('equal', 'datalim')
        ax.plot(out1[0], out1[1], label='out1')
        ax.plot(out2[0], out2[1], label='out2')

    error = 0.

    for i in range(len(out1[0])):
        # iterate along the spline
        # calcualte the deviation, and if requested draw it to the plot as red lines
        x1 = out1[0][i]
        x2 = out2[0][i]
        y1 = out1[1][i]
        y2 = out2[1][i]

        if saveplot:
            line1x = [x1, x2]
            line1y = [y1, y2]
            ax.plot(line1x, line1y, color='red')

        dist = math.sqrt((x2-x1)**2 + (y2-y1)**2)
        error += dist**2

    # normalize error to number of sample points
    rms = math.sqrt(error/numpoints)
    logging.debug("Comparing two Curves, RMS: %s" % str(rms))

    if saveplot:
        timestring = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        # ax.
        # ax.legend(loc='best')
        # ax.title('Error: %s' % str(error))
        # plt.show()

        if filename:
            if filename.endswith('.png'):
                filename = filename.rstrip('.png')
        else:
            filename = 'curves_%s' % timestring

        plt.savefig(os.path.join(saveFolder, '%s.png' % filename))
        # tikz_save('%s.tikz' % filename)

        # change the curve format from a list containing arrays to a single array
        i = 0
        interpolated_curve2 = np.zeros((len(out2[0]), 2))
        for x_value in out2[0]:
            assert (x_value == out2[0][i])
            interpolated_curve2[i][0] = out2[0][i]
            interpolated_curve2[i][1] = out2[1][i]
            i += 1

        # write the interpolated curve to a text file
        with open(os.path.join(saveFolder,filename + '_curve.txt'), mode='w') as fi:
            for line in interpolated_curve2:
                fi.write('%s;%s\n' % (line[0], line[1]))

        plt.close(fig)

    return rms
