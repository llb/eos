# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import logging
import sys
import os
import numpy as np
import copy as cp
import datetime
import time
import traceback

class DesOpt():
    def __init__(self,Model,xLOpt,xUOpt,OptAlg,Normalize,EOSOptOptions,AlgOptions,Sens):
        self.xLOpt = xLOpt
        self.xUOpt = xUOpt
        self.OptAlg = OptAlg
        self.AlgOptions = AlgOptions
        self.EOSOptOptions = EOSOptOptions
        self.Normalize = Normalize
        self.Sens = Sens
        self.Model = Model
        self.FDTol = EOSOptOptions['FDTol']
        self.OptLabel = Model.__class__.__name__+'-'+datetime.datetime.now().strftime("%Y%m%d%H%M")
        self.NonGradBasedAlgs = ('NSGA2','ALPSO','COBYLA','MIDACO','ALHSO')

    def __call__(self,x0N):
        try:
            import pyOpt
            logging.info('PyOpt module has been imported!')
        except:
            logging.fatal('PyOpt module could not be imported!')
            sys.exit('PyOpt module could not be imported!')
        
        # Initialization
        #---------------------------------------------------------------------#  
        STARTTIME       = time.time()
        nDV             = int(np.size(x0N,0))
        xL              = self.xLOpt.astype(float)  # Lower bound
        xU              = self.xUOpt.astype(float)  # Upper bound
        x0              = x0N.astype(float)
        Alg             = self.OptAlg           # Algorithm
        AlgOptions      = self.AlgOptions       # Options for algorithm (piped via **kwargs)
        Options         = self.EOSOptOptions    # Options for PyOpt such as 'ng'
        Normalize       = self.Normalize        # Normalize
        self.SupportedAlgOutFiles = ('NLPQLP')
        OptSettings     = dict()
        self.OptHeadPath = os.getcwd()       

        # Is there a user output function request for each iteration?
        #---------------------------------------------------------------------#        
        self.IterationNum = -1
        self.EvalNum = 0
        self.TotalCalls = -1
        if hasattr(self.Model, 'IterationUserOutput'):
            logging.info('DesOpt detected a user output function request in %s via "IterationUserOutput"'%(self.Model.__class__.__name__))
            self.UserIterOutput = True
        else:
            self.UserIterOutput = False
                   
        # Definition of optimization problem with Obj., Constr. fct., design space etc.
        #---------------------------------------------------------------------#
        if Normalize:
            OptSettings['Normalization'] = True
            ObjConFct = lambda x: self.NormOptResponses(x,xL,xU)
            if self.Sens:
                UserSensitivities = lambda x, f, g: self.NormGradOptResponses(x,f,g,xL,xU,nDV)
            else: 
                UserSensitivities = None
        else:
            OptSettings['Normalization'] = False
            ObjConFct = lambda x: self.NonNormOptResponses(x)
            if self.Sens:
                UserSensitivities = lambda x, f, g: self.Model.ObjAndConstrSensFct(x,f,g)
            else: 
                UserSensitivities = None
        
        # Definition of design space
        #---------------------------------------------------------------------#
        OptSettings['xIni'] = x0
        OptSettings['xLower'] = xL
        OptSettings['xUpper'] = xU
        if Normalize:
            logging.info('Optimization design space is normalized!')
            (Ini, lb, ub)      = (self.Norm(x0,xL,xU), np.array([0.0]*nDV).T, np.array([1.0]*nDV).T)
        else:
            logging.info('Optimization design space is NOT normalized!')
            (Ini, lb, ub)      = (x0,xL,xU)

        # Gradient check?
        #---------------------------------------------------------------------#
        if 'GradCheck' in Options.keys():
            if Options['GradCheck']:
                try:
                    from OptTools import FiniteDiff
                except:
                    logging.fatal('Finnite differences function can not be imported!')    
                    sys.exit('Finite differences function can not be imported!')
                fFD, gFD = FiniteDiff(ObjConFct,Ini,Tol=self.FDTol)
                df, dg, fail = UserSensitivities(Ini,[],[])
                logging.info('Conducted gradient check with %e as FD tolerance\n\t-> Opt returns fFD, gFD, df, dg'%(self.FDTol))
                return fFD, gFD, df, dg         
        
        # checking, if APSIS is necessary or if other non gradient based algorithm which can handle integer or discrete design variables is applied
        #---------------------------------------------------------------------#        
        if  ('i' in Options['DVTypes'] or 'd' in Options['DVTypes']):        
            if not Alg in self.NonGradBasedAlgs:
                import APSIS
                logging.info('\n\n\n########################################################################')                
                logging.info('###  Starting APSIS algorithm for solving MINLP grad.-based.         ###')                
                logging.info('########################################################################\n\n')     
                APSISObj = APSIS.APSIS(x0, xL, xU, self.Model, Alg, Normalize, Options, AlgOptions)   #creation of a substitute model for handling of discrete design variables
                if (not Options['NoPresizing'] or not 'c' in Options['DVTypes']):                
                    Results, PresizingResults = APSISObj.Optimize()  
                    return APSISObj.APSISPostProcessing(Results, PresizingResults)
                else:
                    Results = APSISObj.Optimize()  
                    return APSISObj.APSISPostProcessing(Results)
            # TODO: Check which non-grad based alg can handle either int or disc or both and add more cases here allow for those which work
            elif Alg in self.NonGradBasedAlgs:
                ErrorStr = '\n\n\nDesOpt: Requested combination of discrete/integer variable type and non gradient-based alg not implemented yet!!'
                logging.fatal(ErrorStr)                
                sys.exit(ErrorStr)
        
        # all other cases without integer or discrete design variables go here        
        else:        
        
            # Definition of optimization problem with Obj., Constr. fct. etc.
            #---------------------------------------------------------------------#
            OptProb = pyOpt.Optimization(self.Model.__class__.__name__, ObjConFct)
            OptProb.addObj('f')
            if 'nh' in Options.keys(): 
                nh = Options['nh']
                if nh>0: 
                    OptProb.addConGroup('h', nh, 'e') 
            else:
                nh = 0
            if 'ng' in Options.keys(): 
                ng = Options['ng']
                if ng>0: 
                    OptProb.addConGroup('g', ng, 'i')
            else:
                ng = 0
            if nDV==1:
                if 'DVTypes' in Options.keys():
                    OptProb.addVar('x',Options['DVTypes'], lower=lb, upper=ub, value=Ini)
                else:
                    OptProb.addVar('x','c', lower=lb, upper=ub, value=Ini)
            elif nDV>1:
                for i in range(len(lb)):
                    if 'DVTypes' in Options.keys():
                        OptProb.addVar('x'+str(i+1),Options['DVTypes'][i], lower=lb[i], upper=ub[i], value=Ini[i])
                    else:
                        OptProb.addVar('x'+str(i+1),'c', lower=lb[i], upper=ub[i], value=Ini[i])                     
            OptProblemString = OptProb.__str__()
            self.OptProb = OptProb
            logging.info(OptProblemString)
            print(OptProblemString)
            OptSettings['OptProblemStatement'] = OptProblemString
            
            # Solving the optimization problem
            #---------------------------------------------------------------------#
            OptSettings['OptAlgorithm'] = Alg
            OptAlg = eval('pyOpt.'+Alg+'()')
            if len(AlgOptions.keys())>0:  # Set options passed via **kwargs
                OptSettings['OptAlgorithmNonDefaults'] = AlgOptions
                for (Option, Value) in AlgOptions.iteritems():
                    OptAlg.setOption(Option,Value)
            else: logging.info('No options pipe to pyOpt algorithm via **kwargs') 
            
            if Alg in self.NonGradBasedAlgs:
                [fOpt, xOpt, inform] = OptAlg(OptProb, store_hst=self.OptLabel)
            else:
                [fOpt, xOpt, inform] = self.ConductGradBasedOpt(OptAlg, OptProb, UserSensitivities, FDTol=self.FDTol)
            
            # Post-processing
            #---------------------------------------------------------------------#  
            return self.PostProcessingOptimization(xOpt, fOpt, inform, xL, xU, ng, nh, nDV, Alg, Normalize, STARTTIME, Options, OptSettings)
        
    def PostProcessingOptimization(self, xOpt, fOpt, inform, xL, xU, ng, nh, nDV, AlgUsed, Normalize, STARTTIME, Options, OptSettings):
        # Settings
        #---------------------------------------------------------------------#
        LagrangeTol     = 1.0e-3        
        try:
            import pyOpt
            logging.info('PyOpt module has been imported!')
        except:
            logging.fatal('PyOpt module could not be imported!')
            sys.exit('PyOpt module could not be imported!')
        
        if hasattr(self, 'IterDat'):
            self.IterDat = self.ReadOptHistory(['x','obj','con'])
            if self.UserIterOutput:            
                logging.debug('User requested output function will be called')
                if len(self.IterDat.keys()) != 0:
                    self.Model.IterationUserOutput(self.IterDat)
        HistoryObject = pyOpt.History(os.path.join(self.OptHeadPath, self.OptLabel),'r')  
        xHist         = np.asarray(HistoryObject.read([0,-1],['x'])[0]['x'])
        # Check for two long history in x, especially for NLPQLP and CONMIN
        if np.size(np.shape(xHist))==1:
            xHistList = list()
            for ixHist in xHist:
                TempList = ixHist.tolist()
                xHistList.append(TempList[0:nDV])
            xHist = np.asarray(xHistList)
        fHist         = np.asarray(HistoryObject.read([0,-1],['obj'])[0]['obj'])
        if nh+ng>0:
            ConHist   = np.asarray(HistoryObject.read([0,-1],['con'])[0]['con'])
            if nh>0:
                hHist = ConHist[:,0:nh]   
                hAct  = abs(hHist[-1,:])<LagrangeTol
            else:
                hHist = []
                hAct  = []
            if ng>0:
                gHist = ConHist[:,nh:(nh+ng)]   
                if AlgUsed in ('NLPQLP'): 
                    gHist = [x*(-1.0) for x in gHist]
                    gHist = np.asarray(gHist)
                gAct  = abs(gHist[-1,:])<LagrangeTol
            else:
                gHist = []
                gAct  = [] 
        else:
            (hAct, gAct) = (list(),list())
            (hHist, gHist) = (list(),list())
        if AlgUsed in self.NonGradBasedAlgs:
            (fGradHist,GradConHist,failHist) = (np.array([]),np.array([]),np.array([]))
        else:
            fGradHist = np.asarray(HistoryObject.read([0,-1],['grad_obj'])[0]['grad_obj'])
            failHist  = np.asarray(HistoryObject.read([0,-1],['fail'])[0]['fail'])
            if nh+ng>0:
                GradConHist = np.asarray(HistoryObject.read([0,-1],['grad_con'])[0]['grad_con'])   
            else: 
                GradConHist = list()
        HistoryObject.close()
        
        # Denormalize
        #---------------------------------------------------------------------#
        if Normalize: # Normalization -> Yes
            xOpt            = self.Denorm(xOpt, xL, xU) 
            if AlgUsed not in self.NonGradBasedAlgs:
                for iX in range(np.size(xU)):
                    fGradHist[:,iX] *= 1/(xU[iX]-xL[iX])
                for iCon in range(nh+ng):
                    for iX in range(np.size(xU)):
                        GradConHist[:,(iCon)*nDV+iX] *= 1/(xU[iX]-xL[iX])
        if AlgUsed not in self.NonGradBasedAlgs:
            fGradLast = fGradHist[-1,:]
            if (nh+ng)>0:
                if nh>0:
                    hGradLast = GradConHist[-1,0:nh*nDV]
                if ng>0:
                    gGradLast = GradConHist[-1,nh*nDV:(nh+ng)*nDV]
            xUact               = list()
            for iU, iOpt in zip(xU, xOpt):
                if abs(iU) < LagrangeTol:
                    xUact.append(abs(iOpt-iU)<LagrangeTol)
                else:
                    xUact.append(abs((iOpt-iU)/iU)<LagrangeTol)
            xLact               = list()
            for iL, iOpt in zip(xL, xOpt):
                if abs(iL) < LagrangeTol:
                    xLact.append(abs(iOpt-iL)<LagrangeTol)
                else:
                    xLact.append(abs((iOpt-iL)/iL)<LagrangeTol)        
            
        # Compute lagrangian multipliers and first order optimality
        #---------------------------------------------------------------------#
            xEye = np.eye(nDV)   
            LambdaDescr = list()
            ConArray = np.zeros((nDV,0))
            for (iU, Index) in zip(xUact, range(nDV)):
                if iU == True:
                    ConArray = np.append(ConArray, np.array([xEye[:,Index]]).T, 1) 
                    LambdaDescr.append('xU%i'%(Index))
            for (iL, Index) in zip(xLact, range(nDV)):
                if iL == True:
                    ConArray = np.append(ConArray, np.array([xEye[:,Index]]).T, 1) 
                    LambdaDescr.append('xL%i'%(Index))    
            if np.size(hAct)>0:
                for (iH, Index) in zip(hAct, range(nh)):
                    if iH == True:
                        ConArray = np.append(ConArray, np.array([GradConHist[-1,Index*nDV:(Index+1)*nDV]]).T, 1) 
                        LambdaDescr.append('h%i'%(Index))    
            if np.size(gAct)>0:
                for (iG, Index) in zip(gAct, range(ng)):
                    if iG == True:
                        ConArray = np.append(ConArray, np.array([GradConHist[-1,(nh+Index)*nDV:(nh+Index+1)*nDV]]).T, 1) 
                        LambdaDescr.append('g%i'%(Index))    
            # Compute lambdas
            if len(LambdaDescr) > 0:
                AlgOut = np.linalg.lstsq(ConArray,-fGradLast)
                LambdaVals = AlgOut[0]
                LambdaResidual = AlgOut[1]
                if np.size(LambdaResidual)!=0:
                    logging.info('Lagragian multiplier have been computed with a residual of %e'%(LambdaResidual))
            else:
                LambdaVals = []
                LambdaResidual = []
            # Compute 1st order optimality
            optimality = np.linalg.norm(fGradLast+np.dot(ConArray,LambdaVals))
            # Store in LagrangianData dictionary
            LagrangianData         = dict()
            LagrangianData['Lambda Description'] = LambdaDescr
            LagrangianData['Lambda Definitions'] = 'Optimality info stored in this dictionary:\n\n'+\
                'Key: "Lambda Description": \n\tShows the labels of active constraints \n\tand DVs, where first lower bound \n\ton DV is xL0\n'+\
                'Key: "Lambda Values": \n\tReturns show prices for the corresponding \n\tactive constraint\n'+\
                'Key: "1st Order Opt": \n\tNorm of ||L(x,y)||2 = || Df + Dg*y ||2, \n\twhere y are the shadow prices\n'+\
                'Key: "Lambda Residuals": \n\tRepresents the numerical precision, i.e. \n\tresiduum of computing lambda vals\n'  
            LagrangianData['Lambda Values'] = LambdaVals
            LagrangianData['Lambda Residuals'] = LambdaResidual
            LagrangianData['1st Order Opt'] = optimality
        else:
            LagrangianData         = dict()
            LagrangianData['Info'] = 'No gradients available'
            
        # Condense history to 
        #---------------------------------------------------------------------#    
        if AlgUsed not in self.NonGradBasedAlgs:
            if hasattr(self, 'IterHistIndex'):
                logging.info('Condensing optimization history considering interation and line search information')
                (xHistTemp, fHistTemp, gHistTemp, hHistTemp, failHistTemp) = \
                    (list(), list(), list(), list(), list())
                for iIter in self.IterHistIndex:
                    xHistTemp.append(xHist[iIter,:])
                    fHistTemp.append(fHist[iIter,:])
                    if np.size(gHist) != 0:
                        gHistTemp.append(gHist[iIter,:])
                    if np.size(hHist) != 0:
                        hHistTemp.append(hHist[iIter,:])
                    if np.size(hHist) != 0:
                        failHistTemp.append(failHist[iIter,:])
                (xHist, fHist, gHist, hHist, failHist) = \
                    (np.asarray(xHistTemp), np.asarray(fHistTemp), np.asarray(gHistTemp), \
                    np.asarray(hHistTemp), np.asarray(failHistTemp))   
        elif AlgUsed == 'NSGA2':
            if hasattr(self, 'IterHistIndex'):
                logging.info('Condensing optimization history considering generation and evaluation of population')
                (xHistTemp, fHistTemp, gHistTemp, hHistTemp) = \
                    (list(), list(), list(), list())
                for iIter in self.IterHistIndex:
                    xHistTemp.append(xHist[iIter,:])
                    fHistTemp.append(fHist[iIter,:])
                    if np.size(gHist) != 0:
                        gHistTemp.append(gHist[iIter,:])
                    if np.size(hHist) != 0:
                        hHistTemp.append(hHist[iIter,:])
                (xHist, fHist, gHist, hHist) = \
                    (np.asarray(xHistTemp), np.asarray(fHistTemp), np.asarray(gHistTemp), \
                    np.asarray(hHistTemp)) 
            
        # Automated plotting?
        #---------------------------------------------------------------------#
        if 'ConvergencePlots' in Options.keys():    
            if Options['ConvergencePlots']:
                ConvPlotLabel = ['ConvergencePlot-'+self.OptLabel+'.png',
                                 'DVBarPlot-'+self.OptLabel+'.png']
                self.ConvergencePlots(xHist,fHist,gHist,hHist,ng,nh,nDV,ConvPlotLabel)
            
        # Save results
        #---------------------------------------------------------------------#
        Results                 = dict()
        Results['TimeInSec']    = time.time()-STARTTIME
        Results['fOpt']         = fOpt
        Results['xOpt']         = xOpt
        Results['inform']       = inform
        Results['xHist']        = np.asarray(xHist)
        Results['fHist']        = np.asarray(fHist)
        Results['gHist']        = np.asarray(gHist)
        Results['hHist']        = np.asarray(hHist)
        Results['fGradHist']    = np.asarray(fGradHist)
        Results['GradConHist']  = np.asarray(GradConHist) # dCon1/dx1, dCon1/dx2, ..., dCon1/dxNDV, dCon2/dx1, dCon2/dx2, ..., dCon2/dxNDV etc.
        Results['failHist']     = np.asarray(failHist)
        Results['LagrangianData'] = LagrangianData
        Results['OptSettings']  = OptSettings
        Results['IterDat']      = self.IterDat
        return Results
        
        
    def ConvergencePlots(self,xHist,fHist,gHist,hHist,ng,nh,nDV,Label,showit=True):
        logging.info('convergence plots will be created!')
        try:
            import matplotlib.pyplot as plt
        except:
            logging.fatal('Could not import matplotlib modules!')
            sys.exit('Failed to import matlotlib package!')
                
        # Normalized   
        num_plots = 1 + ng + nh
        Iter = np.linspace(0,np.size(fHist)-1,num=np.size(fHist))
        
        # Convergence plot
        # ------------------------------------------------------------------- #
 
        #colormap = plt.cm.gist_ncar
        fig = plt.figure()
 
        colormap = plt.cm.gist_rainbow
        #colormap = plt.cm.bone
        # plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, num_plots)])
 
 
        
        labels = []
        plt.plot(Iter,fHist/max(abs(fHist)))
        labels.append(r'Objective f')
        for ig in range(ng):
            plt.plot(Iter,gHist[:,ig]/max(abs(gHist[:,ig])))
            labels.append(r'Constraint g%i'%(ig+1) )
        for ih in range(nh):
            plt.plot(Iter,hHist[:,ih]/max(abs(hHist[:,ih])))
            labels.append(r'Constraint h%i'%(ih+1) )
        ax = plt.gca()
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
        ax.legend(labels, loc='center left', bbox_to_anchor=(1, 0.5))
        #plt.legend(labels, ncol=2, loc='upper center', bbox_to_anchor=(0.5, 1.05) )
        if not showit:
            plt.close(fig)   
        fig.savefig(Label[0],dpi=300)
        
        # Bar plot for design variables
        # ------------------------------------------------------------------- #
        Initial = xHist[0,:].tolist()
        Optimal = xHist[-1,:].tolist()
        ind = np.arange(nDV)  # the x locations for the groups
        width = 0.35       # the width of the bars
        fig, ax = plt.subplots()
        rects1 = ax.bar(ind, Initial, width, color='r')
        rects2 = ax.bar(ind+width, Optimal, width, color='b')
        ax.set_ylabel('Normalized design variable values')
        ax.set_title('Design Variables')
        ax.set_xticks(ind+width)
        ax.set_ylim([0.,1.2])
        ax.set_xticklabels( ['DV%02i'%(iDV) for iDV in range(nDV)] )
        ax.legend( (rects1[0], rects2[0]), ('Initial', 'Optimum') )                
        def autolabel(rects):
            for rect in rects:
                height = rect.get_height()
                ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%.1f'%float(height),
                        ha='center', va='bottom')
        autolabel(rects1)
        autolabel(rects2)
        if not showit:
            plt.close(fig)  
        fig.savefig(Label[1],dpi=300)
        
        # Plot several different functions...
        
    def ConductGradBasedOpt(self,OptAlg,OptProb,UserSensitivities,Hist=True,FDTol={}):
        '''
        Returns [fOpt, xOpt, inform] for gradient-based optimization algorithms
        '''
        if self.Sens:
            if Hist:
                return OptAlg(OptProb, sens_type=UserSensitivities,store_hst=self.OptLabel)
            else:
                return OptAlg(OptProb, sens_type=UserSensitivities)
        else:
            if Hist:
                return OptAlg(OptProb, sens_type='FD', store_hst=self.OptLabel, sens_step=FDTol)
            else:
                return OptAlg(OptProb, sens_type='FD', sens_step=FDTol)
        
    def NormOptResponses(self,xNorm,lb,ub):
        '''
        Returns system response (f,g,fail) at normalized design set (xNorm)
        '''        
        
        try:
            x                    = self.Denorm(xNorm, lb, ub)
            logging.debug('Model.ComputeResp has succesfully been called')
            self.IterDat = self.ReadOptHistory(['x','obj','con'])
            if self.UserIterOutput:            
                logging.debug('User requested output function will be called')
                if len(self.IterDat.keys()) != 0:
                    try:
                        self.Model.IterationUserOutput(self.IterDat)
                    except:
                        logging.fatal('Could not call Model.IterationUserOutput!')
                        sys.exit('Could not call Model.IterationUserOutput!')
            f, g, fail           = self.Model.ObjAndConstrFct(x)
        except:
            traceback.print_exc()
            logging.fatal('Could not call Model.ObjAndConstrFct!')
            f                    = []
            g                    = []
            fail                 = 1
        return f, g, fail
        
    def NonNormOptResponses(self,x):
        try:
            logging.debug('Model.ComputeResp has succesfully been called')
            self.IterDat = self.ReadOptHistory(['x','obj','con'])
            if self.UserIterOutput:
                logging.debug('User requested output function will be called')                
                if len(self.IterDat.keys()) != 0:
                    try:
                        self.Model.IterationUserOutput(self.IterDat)
                    except:
                        logging.fatal('Could not call Model.IterationUserOutput!')
                        sys.exit('Could not call Model.IterationUserOutput!')
            f, g, fail           = self.Model.ObjAndConstrFct(x)
        except:
            traceback.print_exc()
            logging.fatal('Could not call Model.ObjAndConstrFct!')
            f                    = []
            g                    = []
            fail                 = 1
        return f, g, fail
    
    def NormGradOptResponses(self,xNorm,f,g,lb,ub,nDV):
        try:
            x                    = self.Denorm(xNorm, lb, ub)
            DF, DG, fail         = self.Model.ObjAndConstrSensFct(x,f,g)
            (df, dg)             = (cp.deepcopy(DF), cp.deepcopy(DG)) 
            for k in range(nDV):
                try:
                    if len(np.shape(df))==2:
                        df[:,k]      = DF[:,k]*(ub[k]-lb[k])
                    else:
                        df[k]        = DF[k]*(ub[k]-lb[k])
                except:
                    df[k]        = DF[k]*(ub[k]-lb[k])
                dg[:,k]          = DG[:,k]*(ub[k]-lb[k])
            logging.debug('Model.ComputeSens has succesfully been called')
        except:
            traceback.print_exc()
            logging.fatal('Could not call Model.ComputeSens!')
            df                   = []
            dg                   = []
            fail                 = 1
        return df, dg, fail
    
    def Denorm(self,xNorm,lb,ub):
        x                        = cp.deepcopy(xNorm)
        for i in range(int(np.size(lb,0))):
            x[i]                 = xNorm[i]*(ub[i]-lb[i])+lb[i]
        logging.debug('Denormalization conducted')
        return x
    
    def Norm(self,x,lb,ub):
        xNorm                    = cp.deepcopy(x)
        for i in range(int(np.size(lb,0))):
            xNorm[i]             = (x[i]-lb[i])/(ub[i]-lb[i])
        logging.debug('Normalization conducted')        
        return xNorm
        
    def GSOPostProcessing(self,OptResultList): # NO USE OF SELF-VARIABLES => COULD GO TO TOOLS!
        '''
        Computes all relevant information for a global search optimization 
        
        Param1: OptResultList: List of optimization output dictionaries
        
        Return: GSinfo: Container of all GS infos
        '''
        (fMin, fMax, fAvg, nOptLoops) = (1.e20, -1.e20, 0., len(OptResultList))
        (IterMin, IterMax, IterAvg) = (1.e20, -1.e20, 0.)
        fList = list()
        fPenaltyList = list()
        gMaxList = list()
        for iResults in OptResultList:
            fOpt = cp.deepcopy(iResults['fOpt'])
            fList.append(fOpt)
            if 'gHist' in iResults.keys():
                if max(iResults['gHist'][-1,])>1e-4:
                    fPenaltyList.append(fOpt+1.e6)
                    gMaxList.append(max(iResults['gHist'][-1,]))
                else:
                    fPenaltyList.append(fOpt)
            else:
                fPenaltyList.append(fOpt)
            Iter = np.shape(iResults['fHist'])[0]
            if fOpt<fMin:
                fMin = cp.deepcopy(fOpt)
            if fOpt>fMax:
                fMax = cp.deepcopy(fOpt)
            fAvg += fOpt
            if Iter<IterMin:
                IterMin = cp.deepcopy(Iter)
            if Iter>IterMax:
                IterMax = cp.deepcopy(Iter)
            IterAvg += Iter
        fAvg /= nOptLoops
        IterAvg /= nOptLoops
        fDeltaPer = (fMax-fMin)/fMin*100
        (fSorted, gSorted, xSorted, ResIDs) = (list(), list(), list(), [fPenaltyList.index(x) for x in sorted(fPenaltyList)])                          
        (xMin, xMax, xRelChange) = (1e20*np.ones(np.size(OptResultList[0]['xOpt'])), -1e20*np.ones(np.size(OptResultList[0]['xOpt'])), list())
        for iIndex in ResIDs:
            xSorted.append(OptResultList[iIndex]['xOpt'])
            fSorted.append(OptResultList[iIndex]['fOpt'])
            if 'gHist' in OptResultList[iIndex].keys():
                gSorted.append( max(OptResultList[iIndex]['gHist'][-1,]) )
            for (xInd, iX) in enumerate(xSorted[-1]):
                if iX < xMin[xInd]: xMin[xInd] = iX
                if iX > xMax[xInd]: xMax[xInd] = iX
        for (ixMin, ixMax) in zip(xMin, xMax):
            if abs(ixMin)<1e-4:
                xRelChange.append('Zero Diff: %e'%(ixMax-ixMin))    
            else:
                xRelChange.append((ixMax-ixMin)/ixMin*100.)    
        # Pass global search details
        GSinfo = dict()    
        (GSinfo['fMin'], GSinfo['fMax'], GSinfo['fAvg'], GSinfo['fDeltaPer']) = (fMin, fMax, fAvg, fDeltaPer)
        (GSinfo['IterMin'], GSinfo['IterMax'], GSinfo['IterAvg'], GSinfo['xOptPercChange']) = (IterMin, IterMax, IterAvg, xRelChange)
        (GSinfo['fSorted'], GSinfo['gSorted'], GSinfo['xOptSorted'], GSinfo['SortedResIDs']) = (fSorted, gSorted, xSorted, ResIDs)
        return GSinfo
        
        
    def ReadOptHistory(self,OutputList):
        import pyOpt
        OptFile = os.path.join(self.OptHeadPath, self.OptLabel)
        if os.path.isfile(OptFile+'.bin'):
            HistoryObject = pyOpt.History( OptFile,'r')  
            IterDat = dict()
            # Read data
            for iOutput in OutputList:
                if iOutput == 'con':
                    if len(self.OptProb._constraints) != 0:
                        IterDat[iOutput+'Iter'] = np.asarray(HistoryObject.read([0,-1],[iOutput])[0][iOutput])
                    else:
                        IterDat['conIter'] = None
                else:        
                    IterDat[iOutput+'Iter'] = np.asarray(HistoryObject.read([0,-1],[iOutput])[0][iOutput])
                if iOutput == 'x':
                    # Check for iteration or evaluation
                    TempIter = len(IterDat['xIter'])
                    if TempIter != 0 and self.OptAlg in self.SupportedAlgOutFiles:
                        if self.OptAlg == 'NLPQLP':
                            with open(os.path.join(self.OptHeadPath, 'NLPQLP.out'),'r') as AlgOutFile:
                                AlgInfos = AlgOutFile.readlines()
                            IterInfoStart = False
                            for iStr in AlgInfos:
                                if 'IT' in iStr and 'F' in iStr and 'SCV' in iStr and 'NA' in iStr and 'KKT' in iStr:
                                    TempIter = -1
                                    IterInfoStart = True
                                elif IterInfoStart:
                                    TempIter += 1
                            if TempIter>self.IterationNum+2: 
                                TempIter = self.IterationNum+1
                    elif TempIter ==0:
                        self.IterHistIndex = list()
                        if self.OptAlg == 'NSGA2':
                            self.TempIter = 0
                    else:
                        if self.OptAlg not in self.NonGradBasedAlgs:
                            IterDat['fGradIter'] = np.asarray(HistoryObject.read([0,-1],['grad_obj'])[0]['grad_obj'])
                            TempIter = np.shape(IterDat['fGradIter'])[0]   
                        elif self.OptAlg == 'NSGA2':
                            if len(self.AlgOptions.keys())>0:
                                if 'PopSize' in self.AlgOptions.keys():
                                    EvalPerGen = self.AlgOptions['PopSize']
                                    if self.EvalNum % EvalPerGen == 0:
                                        self.TempIter += 1
                                    TempIter = self.TempIter

                    if TempIter != self.IterationNum:
                        self.TotalCalls += 1
                        self.IterationNum = TempIter
                        IterDat['EvalNum'] = self.EvalNum
                        self.EvalNum = 1
                        if self.IterationNum != 0: 
                            IterDat['LineSearchDone'] = True                        
                        else:
                            IterDat['LineSearchDone'] = False
                        if self.IterationNum != 0: self.IterHistIndex.append(len(IterDat['xIter'])-1)
                    else:
                        self.EvalNum += 1
                        self.TotalCalls += 1
                        IterDat['EvalNum'] = self.EvalNum
                        IterDat['LineSearchDone'] = False
                    IterDat['IterationNum'] = self.IterationNum
                    IterDat['IterHistIndex'] = self.IterHistIndex
                    IterDat['TotalCalls'] = self.TotalCalls
            HistoryObject.close()
            return IterDat
        else:
            return dict()
        
        
        
