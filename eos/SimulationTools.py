# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import copy as cp

def FiniteDiff(Fct,x0,Tol=1e-3):
    '''
    Computes finite differences for response models as well as for optimization models
    '''
    f0 = Fct(x0)
    if type(f0)==dict:
        DictData=True
    ObjConstr =  isinstance(f0, tuple)
    if ObjConstr:
        F0, G0 = (np.array(f0[0]), np.array(f0[1]))
        GradF = np.zeros((np.size(F0),np.size(x0)))
        GradG = np.zeros((np.size(G0),np.size(x0)))
    else:
        if DictData:
            Grad = dict()
        else:
            f0 = np.array(f0)
            Grad = np.zeros((np.size(f0),np.size(x0)))
    for i, val in enumerate(x0):
        xTest = cp.deepcopy(x0)
        xTest[i] += Tol
        fTest = cp.deepcopy(Fct(xTest))
        if ObjConstr:
            FTest, GTest = (np.array(fTest[0]), np.array(fTest[1]))
            GradF[:,i] = np.transpose((FTest-F0)/(xTest[i]-x0[i]))
            GradG[:,i] = np.transpose((GTest-G0)/(xTest[i]-x0[i]))
        else:
            if DictData:
                for key in f0:
                    if type(f0[key])==list:
                        GradValues = [(ifTest-if0)/(xTest[i]-x0[i]) for (ifTest,if0) in zip(fTest[key],f0[key])]
                        if not key in Grad:
                            Grad[key] = [list() for p in range(len(GradValues))]
                        for k,kVal in enumerate(GradValues):
                            Grad[key][k].append(kVal)
                    else:
                        if not key in Grad:
                            Grad[key] = []
                        Grad[key].append( (fTest[key]-f0[key])/(xTest[i]-x0[i])  )
            else:
                Grad[:,i] = np.transpose((fTest-f0)/(xTest[i]-x0[i]))
    if ObjConstr:
        return GradF, GradG
    else:
        return Grad