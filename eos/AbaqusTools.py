# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

"""
Python class for NASTRAN input/output data

Author: Marc-André Koehler
Last Edited: 16.12.14

"""
import os
import StdTextInOutLib as StdiO     # standard text i/O functions
import numpy as np
import logging

class AbaqusClass:
    # Initialize
    # ----------------------------------------------------------------------- #
    def __init__(self,ModelPath=None):
        # Infostring
        self.InfoStr = '...Object with ABAQUS input/output methods'
        self.ModelPath = ModelPath
        
    def __call__(self):
        pass   
    
    def __str__(self):
        return self.InfoStr
        
    # ReadInFn
    # ----------------------------------------------------------------------- #
        
    def extract_Nodes(self,filename):
        """Extract Nodes from an ABAQUS input file.

        Args:
          filename (string): The name of the input file to be read
        """
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        # read in file
        List = StdiO.read_in(filename)
        #Extract List
        BeginStr="*Node\n"
        EndStr="*"
        List = StdiO.extract(List,BeginStr,EndStr)
        #Transform into numpy array
        Delimiter=","
        self.Nodes = StdiO.split2array(List,Delimiter)
        #Convert to float
        self.Nodes=self.Nodes.astype(float)
        
    def extract_Elements(self,filename):  
        """Extract Elements from an ABAQUS input file.

        Args:
          filename (string): The name of the input file to be read
        """
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        # read in file
        List = StdiO.read_in(filename)
        #Extract List
        BeginStr="*Element,"
        EndStr="*"
        List = StdiO.extract(List,BeginStr,EndStr)
        #Transform into numpy array
        Delimiter=","
        self.Elements = StdiO.split2array(List,Delimiter) 
        #Convert to float
        self.Elements=self.Elements.astype(float)
    
    def extract_Set(self,filename,Settype):  
        """Extract Set from an ABAQUS input file.

        Args:
          filename (string): The name of the input file to be read\n
          Settype (string): Type of the set to be read in: Nset, Elset...
        """
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        # read in file
        List = StdiO.read_in(filename)
        # Check total number of Sets stored
        if hasattr(self, 'Sets'):
            numSets=len(self.Sets)
        else:
            numSets=0
            self.Sets=dict()
        #Extract List
        BeginStr="*Nset,"
        EndStr="*"
        List = StdiO.extract(List,BeginStr,EndStr)
        # Delete last character *
        List = List[:-1]
        #Delete \n and ' '
        for i,iLine in enumerate(List):
            List[i]=iLine.split('\n')[0].strip()+","    
        # join strings    
        List=''.join(List)
        # Split
        List=List.split(',') 
        List = List[:-1]
        self.Sets[Settype+str(numSets+1)]=np.array(List).astype(int)   
        
    def extract_Basevectors(self,filename,separateFile=True):
        """Extract Basevectors from an ABAQUS input file.

        Args:
          filename (string): The name of the input file to be read\n
          separateFile (boolean, optional):  Basevectors are located in separate file: [True]/False
        """
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        # read in file
        List = StdiO.read_in(filename)
        # Check total number of ShapeVariables stored
        if hasattr(self, 'Basevectors'):
            numShapeVar=len(self.Basevectors)
        else:
            numShapeVar=0
            self.Basevectors=dict()
        #Extract List
        if separateFile:
            List = StdiO.extract(List)
        else:
            BeginStr="*PARAMETER SHAPE VARIATION,"
            EndStr="*"
            List = StdiO.extract(List,BeginStr,EndStr)
        #Transform into numpy array
        Delimiter=","
        self.Basevectors['ShapeVar'+str(numShapeVar+1)] = StdiO.split2array(List,Delimiter)

    def extract_Results(self,Nelems,SolverResultsFile):
        if self.ModelPath != None:
            SolverResultsFile = os.path.join(self.ModelPath,SolverResultsFile)
        def is_number(s):
            try:
                float(s)
                return True
            except ValueError:
                return False
        Results=np.zeros((Nelems,2))
        e = 0
        with open(SolverResultsFile,'r',1) as f:
            while True:
                line = f.readline()
                if bool(line):
                    if is_number(line[0]) == True and e<=Nelems-1:
                        cols = line.split(',')
                        for k in xrange(len(cols)-1):
                            Results[e,k]=float(cols[k])
                        e+=1
                    else:
                        InfoStr = 'More element results than element ids in mesh!'
                        print InfoStr
                        #sys.exit(InfoStr)
                if not line:
                    break
        self.Results = Results 
        return self.Results        
        
    # WriteOutFn
    # ----------------------------------------------------------------------- #
    def write_out_Header(self,filename,jobname):
        """Write out Header in ABAQUS input file.

        Args:
          filename (string): The name of the input file to be written\n
          jobname (string):  Name of the ABAQUS job
        """
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        Str=("*Heading\n**Job name: "+jobname+"\n**Generated by: Python\n**\n")
        StdiO.write_out(Str,filename,"w")
        
    def write_out_Nodes(self,Nodes=None,filename='Nodes.inp',option='a'):
        """Write out Nodes in ABAQUS input file.

        Args:
          filename (string): The name of the input file to be written\n
          option (string):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
          Nodes (numpyArray, optional): Optional specify the numpyArray containing the Nodenumber, x, y, z coordinates\n
        """
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        if Nodes==None:
            if hasattr(self, 'Nodes'):
                Nodes = self.Nodes
            else:
                print """**Error**: Object has no nodes specified.
                > Read in nodes first or specify nodes."""
        rows,cols = Nodes.shape
        Str = "*Node\n"
        for i in range(rows):
            Str += '%10.i, %10.3f, %10.3f, %10.3f\n'%(Nodes[i,0],Nodes[i,1],Nodes[i,2],Nodes[i,3])  
        StdiO.write_out(Str,filename,option)                    
                    
    def write_out_Elements(self,Eltype,Elements=None,filename='Elements.inp',option='a'):
        """Write out Elements in ABAQUS input file.

        Args:
          filename (string): The name of the input file to be written\n
          option (string):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
          Elements (numpyArray, optional): Optional specify the numpyArray containing the Elementnumber, Node1, Node2,...\n
          Eltype (string):  Type of the element (=ABAQUS name, example: C4R)\n
        """
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        if Elements==None:
            if hasattr(self, 'Elements'):
                Elements = self.Elements
            else:
                print """**Error**: Object has no elements specified.
                > Read in elements first or specify elements."""
        Str = "*Element, "+Eltype
        # Write out
        for iLine in Elements:
            Str += "\n"
            for i,iVal in enumerate(iLine):
                if i==0:
                    Str +=str(int(iVal))
                else:
                    Str +=","+str(int(iVal))        
        StdiO.write_out(Str,filename,option)
        
    
    def write_out_Basevectors(self,Basevectors=None,filename='Basevectors.inp',option='a'):
        """Write out Basevectors for all ShapeVariables in ABAQUS input file.

        Args:
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
          Basevectors (numpyArray, optional): Optional specify the numpyArray containing the Basevectors for each ShapeVar\n
        """
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        if Basevectors==None:
            if hasattr(self, 'Basevectors'):
                Basevectors = self.Basevectors
            else:
                print """**Error**: Object has no Basevectors specified.
                > Read in Basevectors first or specify Basevectors."""       
        Str = "**\n**SHAPE VARIATIONS\n**"
        # Write out
        for ShapeVar in Basevectors:
            Str += "\n*PARAMETER SHAPE VARIATION, PARAMETER="+ShapeVar
            for j,jLine in enumerate(Basevectors[ShapeVar]):
                Str +="\n"
                for i,iVal in enumerate(jLine):
                    # Node number
                    if i==0:
                        Str +=str(int(iVal))
                    else:
                        Str +=","+str(float(iVal))
        StdiO.write_out(Str,filename,option)
        
    def StoreInShelveFile(self,ResultsList,ResultsNameList,FileName='NastranResults.shelve'):  
        import shelve
        if self.ModelPath != None:
            shelveFile=shelve.open(os.path.join(self.ModelPath,FileName),'n')
        else:
            shelveFile=shelve.open(FileName,'n')
        logging.info('Storing following variables in %s'%(FileName))
        ResultName = ''
        for iName in ResultsNameList:
            ResultName += iName+', '
        logging.info(ResultName)
        if len(ResultsList) != len(ResultsNameList):
            logging.error('ResultsList and ResultsNameList have to be of same length')
        for (iResult, iName) in zip(ResultsList,ResultsNameList):
            shelveFile[iName]=iResult
        shelveFile.close()
        
    def DeleteFiles(self, FileList):
        # Clean up old files
        tmpFileList = []
        for iFile in FileList:
            tmpFileList.extend([os.path.join(self.ModelPath,iFile)])
        for f in tmpFileList:          
            try:
                os.remove(f)
            except:
                continue 
        
    def extract_NodesAndElements(self,FileName):
        """Extract Nodes and QUAD-Elements from an Abaqus input file.

        Args:
          FileName (string): The name of the input file to be read
          
        Return:
          (Nodes, Elements, nNodes, nElms)
        """
        if self.ModelPath != None:
            FileName = os.path.join(self.ModelPath,FileName)
        
        def ReadInp(filename,countmode=True,grid=np.array([]),elems=np.array([])):
            if countmode==True:
                Nnodes = 0
                Nelems = 0
            n = 0
            e = 0
            nodmark = False
            elmark  = False
            with open(filename,'r',1) as fid:
                while True:
                    line = fid.readline()
                    if '*NODE' in line or '*Node' in line:
                        nodmark = True
                        continue
                    if '*' not in line and nodmark==True:
                        if countmode==True:
                            Nnodes +=1
                        else:
                            cols = line.split(',')
                            for k in xrange(0,len(cols)):
                                grid[n,k] = float(cols[k])
                            n+=1
                    if '*' in line and nodmark==True:
                        nodmark = False
                    if '*ELEMENT' in line or '*Element' in line:
                        elmark = True
                        continue
                    if '*' not in line and elmark==True:
                        if countmode == True:
                            Nelems += 1
                        else:
                            cols = line.split(',')
                            for k in xrange(0,len(cols)):
                                elems[e,k] = int(cols[k])
                            e += 1
                    if '*' in line and elmark==True:
                        elmark = False
                    if not line:
                        break
            if countmode == True:
                return Nnodes,Nelems
            else:
                return grid,elems
        self.nNodes,self.nElms = ReadInp(FileName)
        Grid = np.zeros((self.nNodes,4))
        Elements = np.zeros((self.nElms,5),dtype=int)
        self.Nodes,self.Elements = ReadInp(FileName,\
                                countmode=False,grid=Grid,elems=Elements)
        return self.Nodes, self.Elements, self.nNodes, self.nElms
        
# Debugging
# --------------------------------------------------------------------------- #
if __name__=='__main__':
    print 'Debugging'