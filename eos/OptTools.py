# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------------------------------- #
# --------------------------------------------------------------------------- #
# --------             Gathering of optimization tools               -------- #
# --------------------------------------------------------------------------- #
# --------------------------------------------------------------------------- #
#                                                                             #
# Available functions are                                                     #
# o GlobalSearchForDesOpt:                                                    #
#   -> Perform DoE for global search option in DesOpt                         #
# o PerformDoE:                                                               #
#   -> Performs design of experiments                                         #
# o GetRSAmodel:                                                              #
#   -> Computes response surface approximation (surrogate modeling)           #
#                                                                             #
# --------------------------------------------------------------------------- #

# Needed imports
# --------------------------------------------------------------------------- #
import numpy as np
import copy as cp
import logging
import sys
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import itertools
from scipy.spatial import ConvexHull

# Conduct design of experiments
# --------------------------------------------------------------------------- #
def GlobalSearchForDesOpt(xL, xU, ObjConFct, nh=0, nDoE=30, nGS=3, hTol=0.5, gTol=1.e-6, ReplaceDVDict=None):
    '''
    Perform DoE for global search option in DesOpt
    
    :param nDoE: Number of test samples
    :param nGS: Number of global search points
    :param ObjConFct: Objective and constraint function, e.g. "ObjConFct = lambda x: (x**2, 3*x, 0)" -> Thus, returns f, g, fail
    :param ReplaceDVDict: Dictionary: ['xInd'] - Indicies of x to be replaced by values from ['Vals']
 
    :return xGS: Returns an array which can be piped into DesOpt
    :return gInfo: Info whether feasible or not
    '''
    xDoE = PerformDoE(nDoE, xL, xU)
    if ReplaceDVDict:
        for iInd, iVal in zip(ReplaceDVDict['xInd'], ReplaceDVDict['Vals']):
            xDoE[:,iInd] = iVal
    (fList, gInfo, penalty) = (list(), [None]*nDoE, list())
    for iGSO, ix0 in enumerate(xDoE):
        score = 0
        minscore = 6
        fTemp, gTemp, fail = ObjConFct(ix0.T)
        if nh > 0:
            hTemp = gTemp[:nh]
            gTemp = gTemp[nh:]
            minscore = 7
            if np.max(np.abs(hTemp)) <= 1.0e-4*hTol:
                score += 5
            elif np.max(np.abs(hTemp)) <= hTol:
                score += 2
            elif (np.max(np.abs(hTemp)) > hTol) and (np.max(np.abs(hTemp)) < 10*hTol):
                score += 1
            else:
                score += 0
 
        if np.max(gTemp) < gTol:
            score += 6
        else:
            score += 0
            
        if score < minscore:                  # In order to be feasible, g must be within tolerance!
            gInfo[iGSO] = 'Infeasible'
        else:
            gInfo[iGSO] = '-# x0 - f: %e - gMax: %e (at py-Index %i) #-' %(fTemp, max(gTemp), [i for i, j in enumerate(gTemp) if j == max(gTemp)][0])
        penalty.append(score)
        logging.info('%i-th evaluation of %i total nDoE evals done' % (iGSO + 1, nDoE))
    sIndex = sorted(range(len(penalty)), key=lambda k: -penalty[k])

    xGS = np.zeros(( np.size(xL), nGS))
    for iGS in range(nGS):
        xGS[:, iGS] = xDoE[sIndex[iGS], :]
    return xGS, gInfo

# Conduct design of experiments
# --------------------------------------------------------------------------- #
def PerformDoE(NumberSamplePoints, RSAlower, RSAupper, SamplingMethod='LHS', **kwargs):
    '''
    :param NumberSamplePoints: How many sample points for DoE
    :param RSAlower: Lower bound for sample points
    :param RSAupper: Upper bound for sample points
    :param SamplingMethod: Which sampling method? -> ['LHS'], ['FullFactorial']?
    :param kwargs: Can be used to pipe options to DoE method
    :return: (design): Returns an array of sampling points

    Note: for 'FullFactorial' sampling with n>1 dimensions, the desired number of sample points
        is approximated if the n-th root of NumberSamplePoints is not an integer.
    '''
    RSAlower = np.asarray(RSAlower)
    RSAupper = np.asarray(RSAupper)
    dimension = np.size(RSAlower)
    if SamplingMethod == 'LHS':
        import doe_lhs

        design = doe_lhs.lhs(dimension, samples=NumberSamplePoints,
                             **kwargs)  # **kwargs could be criterion=None, iterations=None
        for k in range(dimension):
            design[:, k] = (RSAupper[k] - RSAlower[k]) * design[:, k] + RSAlower[k]
    elif SamplingMethod == 'LHS+Corner':
        '''Performs Latin Hypercube sampling including the points on the boundary of the design space'''        
        NumBounds = 2**dimension
        if NumberSamplePoints < NumBounds:
            logging.fatal('More points on the boundaries than sampling points to be generated')
            sys.exit('More points on the boundaries than sampling points to be generated')
        import doe_lhs
        DoE_vectors = np.zeros((dimension, 2))

        for k in range(dimension):
            # create a linspace for each dimension of the DoE
            DoE_vectors[k] = np.linspace(RSAlower[k], RSAupper[k], 2)

        # create the DoE using the meshgrid function
        fulldesignBounds = np.meshgrid(*DoE_vectors)

        #initiate the final design array
        designBounds = np.zeros((dimension, NumBounds))

        for k in range(dimension):
            # flatten the array for each dimension so that we get a 1-dim array per DoE dimension
            # assemble the DoE (design)
            designBounds[k] = fulldesignBounds[k].flatten()

        designBounds = designBounds.T  # Transpose design for correct output format
        RSAlowerLHS = (RSAupper - RSAlower)/(NumberSamplePoints - 1) + RSAlower      
        RSAupperLHS = RSAupper - (RSAupper - RSAlower)/(NumberSamplePoints - 1)     
        NumberSamplePoints = NumberSamplePoints - NumBounds        
        design = doe_lhs.lhs(dimension, samples=NumberSamplePoints,
                             **kwargs)  # **kwargs could be criterion=None, iterations=None
        for k in range(dimension):
            design[:, k] = (RSAupperLHS[k] - RSAlowerLHS[k]) * design[:, k] + RSAlowerLHS[k]
        design = np.append(design, designBounds, axis = 0)
    elif SamplingMethod == 'FullFactorial' and dimension == 1:
        design = np.linspace(RSAlower, RSAupper, NumberSamplePoints)
        logging.info('Creating full factorial one-dimenstional DoE with %s sample points' % NumberSamplePoints)
    elif SamplingMethod == 'FullFactorial' and dimension != 1:
        # Approximate the number of desired design points by taking the n-root of values per dimension
        PointsPerDim = round(NumberSamplePoints ** (1 / float(dimension)))

        NumberSamplePoints = PointsPerDim ** dimension
        logging.info(
            'Creating full factorial DoE with %s dimensions, %s points per dimension for a total of %s sample points' % (
                str(dimension), str(PointsPerDim), str(NumberSamplePoints)))

        DoE_vectors = np.zeros((dimension, PointsPerDim))

        for k in range(dimension):
            # create a linspace for each dimension of the DoE
            DoE_vectors[k] = np.linspace(RSAlower[k], RSAupper[k], PointsPerDim)

        # create the DoE using the meshgrid function
        fulldesign = np.meshgrid(*DoE_vectors)

        #initiate the final design array
        design = np.zeros((dimension, NumberSamplePoints))

        for k in range(dimension):
            # flatten the array for each dimension so that we get a 1-dim array per DoE dimension
            # assemble the DoE (design)
            design[k] = fulldesign[k].flatten()

    elif SamplingMethod == 'FullFactorialManual' and dimension == 2:

        X, Y = np.mgrid[14.5:17.5:2j, 0.3:2.0:5j]
        DoE = np.vstack((X.flatten(), Y.flatten())).T

        # Approximate the number of desired design points by taking the n-root of values per dimension
        PointsPerDim = round(NumberSamplePoints ** (1 / float(dimension)))

        NumberSamplePoints = PointsPerDim ** dimension
        logging.info(
            'Creating full factorial DoE with %s dimensions, %s points per dimension for a total of %s sample points' % (
                str(dimension), str(PointsPerDim), str(NumberSamplePoints)))

        DoE_vectors = np.zeros((dimension, PointsPerDim))

        for k in range(dimension):
            # create a linspace for each dimension of the DoE
            DoE_vectors[k] = np.linspace(RSAlower[k], RSAupper[k], PointsPerDim)

        # create the DoE using the meshgrid function
        fulldesign = np.meshgrid(*DoE_vectors)

        # initiate the final design array
        design = np.zeros((dimension, NumberSamplePoints))

        for k in range(dimension):
            # flatten the array for each dimension so that we get a 1-dim array per DoE dimension
            # assemble the DoE (design)
            design[k] = fulldesign[k].flatten()

        design = design.T  # Transpose design for correct output format
    elif SamplingMethod == 'Logarithmic' and dimension == 1:
        logging.info('creating logarithmic DoE')
        design = np.logspace(start=RSAlower[0], stop=RSAupper[0], num=NumberSamplePoints, **kwargs)
    else:
        logging.fatal('The demanded DoE method is not available yet!')
        design = []
    return design
    
# Commpute RSA model
# --------------------------------------------------------------------------- #
def GetRSAmodel(xDoE, ModelObj, fDoE=None, RSAMethod='PolyReg', **kwargs):
    '''
    :param xDoE: Array of sampling points -> nDesign=np.size(xDoE,0) & nx=np.size(xDoE,1)
    :param fDoE: Needs to be a TWO-Dimensional numpy array
    :param RSAMethod: Which RSA method? -> ['PolyReg'], 'Kriging'
    :param kwargs: Can be used to pipe options to RSAMethod
    :return: RSAmodel: Returns an RSA object
    '''
    logging.info('Starting computing of fDoEs of %s' % (ModelObj.__class__.__name__))
    if fDoE == None:
        fDoE = list()
        for x in xDoE:
            fDoE.append(ModelObj.ComputeResp(x))
    fDoE = np.asarray(fDoE)
    if len(np.shape(fDoE)) != 2:
        logging.fatal('fDoE needs to be a two dimensional array!')
        sys.exit('fDoE needs to be a 2D array!')
    RegDimension = np.shape(fDoE)[1]
    logging.info('Starting computing RSA model of %s' % (ModelObj.__class__.__name__))
    if RSAMethod == 'PolyReg':
        from PolyReg import PolyReg
        RSAmodel = list()
        for i in range(RegDimension):
            RSAmodelTMP = PolyReg()  # **kwargs could be PolyDegree=2, PrintInfos=False
            logging.info('Created PolyReg model with oversampling of %.1f' % (
                float(np.shape(xDoE)[0]) / RSAmodelTMP.MinimalSamplingSize(np.shape(xDoE)[1])))
            RSAmodelTMP.fit(xDoE, fDoE[:, i])
            RSAmodel.append(RSAmodelTMP)
    elif RSAMethod == 'Kriging':
        from sklearn.gaussian_process import GaussianProcess
        RSAmodel = list()
        for i in range(RegDimension):
            RSAmodelTMP = GaussianProcess(**kwargs)
            # **kwargs could be regr='quadratic',corr='squared_exponential',
            # normalize=True,storage_mode='light', theta0=np.ones([10,])*0.01,
            #       thetaL=np.ones([10,])*1e-4, thetaU=np.ones([10,])*1e+2,optimizer='fmin_cobyla'
            RSAmodelTMP.fit(xDoE, fDoE[:, i])
            RSAmodel.append(RSAmodelTMP)
    else:
        logging.fatal('The demanded RSA method is not available yet!')
        sys.exit('The desired RSA method is not implemented yet!')
    return RSAmodel
        
def GSOPostProcessing(OptResultList):
    '''
    Computes all relevant information for a global search optimization 
    
    Param1: OptResultList: List of optimization output dictionaries
    
    Return: GSinfo: Container of all GS infos
    '''
    (fMin, fMax, fAvg, nOptLoops) = (1.e20, -1.e-20, 0., len(OptResultList))
    (IterMin, IterMax, IterAvg) = (1.e20, -1.e-20, 0.)
    fList = list()
    fPenaltyList = list()
    gMaxList = list()
    for iResults in OptResultList:
        fOpt = cp.deepcopy(iResults['fOpt'])
        fList.append(fOpt)
        if 'gHist' in iResults.keys():
            if max(iResults['gHist'][-1,])>1e-4:
                fPenaltyList.append(fOpt+1.e6)
                gMaxList.append(max(iResults['gHist'][-1,]))
            else:
                fPenaltyList.append(fOpt)
        else:
            fPenaltyList.append(fOpt)
        Iter = np.shape(iResults['fHist'])[0]
        if fOpt<fMin:
            fMin = cp.deepcopy(fOpt)
        if fOpt>fMax:
            fMax = cp.deepcopy(fOpt)
        fAvg += fOpt
        if Iter<IterMin:
            IterMin = cp.deepcopy(Iter)
        if Iter>IterMax:
            IterMax = cp.deepcopy(Iter)
        IterAvg += Iter
    fAvg /= nOptLoops
    IterAvg /= nOptLoops
    fDeltaPer = (fMax-fMin)/fMin*100
    (fSorted, gSorted, xSorted, ResIDs) = (list(), list(), list(), [fPenaltyList.index(x) for x in sorted(fPenaltyList)])                          
    (xMin, xMax, xRelChange) = (1e20*np.ones(np.size(OptResultList[0]['xOpt'])), -1e20*np.ones(np.size(OptResultList[0]['xOpt'])), list())
    for iIndex in ResIDs:
        xSorted.append(OptResultList[iIndex]['xOpt'])
        fSorted.append(OptResultList[iIndex]['fOpt'])
        if 'gHist' in OptResultList[iIndex].keys():
            gSorted.append( max(OptResultList[iIndex]['gHist'][-1,]) )
        for (xInd, iX) in enumerate(xSorted[-1]):
            if iX < xMin[xInd]: xMin[xInd] = iX
            if iX > xMax[xInd]: xMax[xInd] = iX
    for (ixMin, ixMax) in zip(xMin, xMax):
        if abs(ixMin)<1e-4:
            xRelChange.append('Zero Diff: %e'%(ixMax-ixMin))    
        else:
            xRelChange.append((ixMax-ixMin)/ixMin*100.)    
    # Pass global search details
    GSinfo = dict()    
    (GSinfo['fMin'], GSinfo['fMax'], GSinfo['fAvg'], GSinfo['fDeltaPer']) = (fMin, fMax, fAvg, fDeltaPer)
    (GSinfo['IterMin'], GSinfo['IterMax'], GSinfo['IterAvg'], GSinfo['xOptPercChange']) = (IterMin, IterMax, IterAvg, xRelChange)
    (GSinfo['fSorted'], GSinfo['gSorted'], GSinfo['xOptSorted'], GSinfo['SortedResIDs']) = (fSorted, gSorted, xSorted, ResIDs)
    return GSinfo       
    
# Compute finite difference approximation
# --------------------------------------------------------------------------- #
def FiniteDiff(Fct,x0,Tol=1e-3):
    '''
    Computes finite differences for response models as well as for optimization models
    '''
    f0 = Fct(x0)
    ObjConstr =  isinstance(f0, tuple)
    if ObjConstr:
        F0, G0 = (np.array(f0[0]), np.array(f0[1]))
        GradF = np.zeros((np.size(F0),np.size(x0)))
        GradG = np.zeros((np.size(G0),np.size(x0)))
    else:
        f0 = np.array(f0)
        Grad = np.zeros((np.size(f0),np.size(x0)))
    for i, val in enumerate(x0):
        xTest = cp.deepcopy(x0)
        xTest[i] += Tol
        fTest = cp.deepcopy(Fct(xTest))
        if ObjConstr:
            FTest, GTest = (np.array(fTest[0]), np.array(fTest[1]))
            GradF[:,i] = np.transpose((FTest-F0)/(xTest[i]-x0[i]))
            GradG[:,i] = np.transpose((GTest-G0)/(xTest[i]-x0[i]))
        else:
            Grad[:,i] = np.transpose((fTest-f0)/(xTest[i]-x0[i]))
    if ObjConstr:
        return GradF, GradG
    else:
        return Grad
       
# Check optimization options
# --------------------------------------------------------------------------- #
def CheckOptions(Options,nDV):
    '''
    Checks completeness of study options defined by user and completes with 
    default options.
    
    :param Options:     Options set by user in study
    :param nDV:         Number of design variables
    :return:            Corrected options
    '''
    if 'VecOptions' in Options.keys():
        VecOptions = Options['VecOptions']
    else:
        VecOptions = dict()
    
    # set default general options
    defaultOptions = dict()
    defaultOptions['nf']                    = 1
    defaultOptions['ng']                    = 0
    defaultOptions['nh']                    = 0
    defaultOptions['GradCheck']             = False
    defaultOptions['DVTypes']               = ['c']*nDV 
    defaultOptions['ConvergencePlots']      = True
    defaultOptions['Normalization']         = True
    defaultOptions['FDTol']                 = 1E-4
    defaultOptions['Alg']                   = 'SLSQP'
    defaultOptions['VecOptMode']            = 'equaldistance'
    defaultOptions['DiscreteDVChoices']     = dict()    # dictionary, which specifies the possible choices for the discrete design variables. key is entry in x, e.g. '1' for first discrete entry
    # 'Const' is set, if no ramp is desired and specifies a constant weighting factor for the penalty's influence;
    # otherwise the keys for the iteration numbers, where to begin and to end the ramping ('iterStart' and 'iterEnd'), are required as well as the respective weighting factors('NuStart' and 'NuEnd'). e.g. iterStart = 1 to start with the first iteration     
    defaultOptions['Ramp']                  = dict()     
    defaultOptions['NoPresizing']           = False # specifies, whether presizing is required, default option is False    
    # set default vector options        
    defaultVecOptions = dict()       
    defaultVecOptions['NumParetoOptima']   = 10
    defaultVecOptions['ChordFactor']       = 1.25
    defaultVecOptions['GS']                = False
    defaultVecOptions['nDoE']              = 10        # deleted if GS=False
    defaultVecOptions['nGS']               = 3         # deleted if GS=False
    defaultVecOptions['NR']                = False
    defaultVecOptions['nNR']               = 20
    defaultVecOptions['f0NR']              = False
    defaultVecOptions['linesearchNR']      = False
    defaultVecOptions['NormalizeF']        = False
    defaultVecOptions['ConvergencePlots']  = False
    defaultVecOptions['GridpointPlots']    = False
    defaultVecOptions['fPrevInit']         = 2
    defaultVecOptions['RefinedGS']         = False
    defaultVecOptions['AnchorPoints']      = None
    defaultVecOptions['EdgePoints']        = None
    defaultVecOptions['dGamma']            = 0.71
    defaultVecOptions['Spray']             = False
    defaultVecOptions['SprayValue']        = 1.
    defaultVecOptions['GridType']          = 'linebyline'
    defaultVecOptions['GridDir']           = 0
    defaultVecOptions['1stOrderOptPlots']  = False
    
    # check each user defined option and complete with defaults if necessary
    count0 = 0
    for key in defaultOptions.keys():
        if key not in Options.keys():
            Options[key] = defaultOptions[key]
            logging.warning('Options not complete: %s is missing.'
                'Set to default: %s' % (key, str(defaultOptions[key])))
            count0 += 1    
            
    if count0 == 0:
        logging.info('Checking options ended sucessfully.')
    else:
        logging.info('Checking options completed: '
            'Added %d default options.' % count0)
            
    # check each user defined vector option and complete with defaults if necessary
    if VecOptions:
        count1 = 0
        for key in defaultVecOptions.keys():
            if key not in VecOptions.keys():
                VecOptions[key] = defaultVecOptions[key]
                logging.warning('VecOptions not complete: %s is missing.'
                'Set to default: %s' % (key, str(defaultVecOptions[key])))
                count1 += 1
        # delete unused information (if GS is false nDoE&nGS is useless)
        if not VecOptions['GS']: del(VecOptions['nDoE'],VecOptions['nGS'])

        if count1 == 0:
            logging.info('Checking VecOptions ended sucessfully.')
        else:
            logging.info('Checking VecOptions completed: '
                'Added %d default options.' % count0)
        
    return Options, VecOptions
    
# Plotting functions for vector optimization
# --------------------------------------------------------------------------- #    
def ParetoPlotting(FNad,FUto,FAnc):
    '''
    Initialize plotting for vector optimization.
    '''
    fig = plt.figure()

    if len(FAnc)==2:
        ax  = fig.add_subplot(111)
        plt.ion()

        ax.plot(FNad[0],FNad[1],'ok')
        ax.plot(FUto[0],FUto[1],'ok')
        ax.plot(FAnc[:,0],FAnc[:,1],'-xg')
        
    elif len(FAnc)==3:
        from mpl_toolkits.mplot3d import Axes3D
        from mpl_toolkits.mplot3d.art3d import Poly3DCollection
        
#        plt.gca().set_aspect('equal', adjustable='box')
#        ax = Axes3D(fig)
             
        ax = fig.gca(projection='3d')
        ax.set_aspect('equal')
        
        verts = [zip(FAnc[:,0],FAnc[:,1],FAnc[:,2])]
        ax.add_collection3d(Poly3DCollection(verts,facecolor='green',alpha=0.5))
    
        ax.scatter(FNad[0],FNad[1],FNad[2],c='k',marker='s',facecolors='none')
        ax.scatter(FUto[0],FUto[1],FUto[2],c='k',marker='s')
        ax.scatter(FAnc[:,0],FAnc[:,1],FAnc[:,2],c='g')
        
        ax.set_zlabel('Objective $f_3$')
        


    ax.set_xlabel('Objective $f_1$')
    ax.set_ylabel('Objective $f_2$')
    limits = [[FUto[0],FNad[0]],[FUto[1],FNad[1]],[FUto[2],FNad[2]]]
    set_axes_equal(ax,limits)    
    
    return fig,ax
    
def set_axes_equal(ax,limits=False):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''
    if limits:
        x_limits = limits[0]
        y_limits = limits[1]
        z_limits = limits[2]
    else:
        x_limits = ax.get_xlim3d()
        y_limits = ax.get_ylim3d()
        z_limits = ax.get_zlim3d()

    x_range = x_limits[1] - x_limits[0]; x_mean = np.mean(x_limits)
    y_range = y_limits[1] - y_limits[0]; y_mean = np.mean(y_limits)
    z_range = z_limits[1] - z_limits[0]; z_mean = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_mean - plot_radius, x_mean + plot_radius])
    ax.set_ylim3d([y_mean - plot_radius, y_mean + plot_radius])
    ax.set_zlim3d([z_mean - plot_radius, z_mean + plot_radius])
    
def AnalyticalParetoPlotting2D(FNad,FUto,FAnc):
    '''
    Initialize plotting for vector optimization.
    Remarks:
    - Works only for biobjective so far!
    - This script is not efficient and should be renewed a.s.a.p.!
    Returns:
    - list of f_1 values
    - list of f_2 values
    - handle to figure
    '''
    
    # general setup
    fig = plt.figure()
    ax  = fig.add_subplot(111)
    plt.ion()
    ax.set_xlabel('objective $f_1$')
    ax.set_ylabel('objective $f_2$')

    # special points
    ax.plot(FNad[0],FNad[1],'ok')
    ax.plot(FUto[0],FUto[1],'ok')
    ax.plot(FAnc[:,0],FAnc[:,1],'-xg')
 
    return fig,ax
    
def AnalyticalParetoPlotting3D(xL,xU,FNad,FUto,FAnc,Model,Options):
    
    from mpl_toolkits.mplot3d import Axes3D
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection
    
    fig = plt.figure()
    ax = Axes3D(fig)
    verts = [zip(FAnc[:,0],FAnc[:,1],FAnc[:,2])]
    ax.add_collection3d(Poly3DCollection(verts,facecolor='green',alpha=0.5))
    
    ax.scatter(FNad[0],FNad[1],FNad[2],c='r')
    ax.scatter(FUto[0],FUto[1],FUto[2],c='b')
    ax.scatter(FAnc[:,0],FAnc[:,1],FAnc[:,2],c='g')
    
    ax.set_xlabel('Objective $f_1$')
    ax.set_ylabel('Objective $f_2$')
    ax.set_zlabel('Objective $f_3$')
    return fig,ax
    
def addplot(fVect,fPrev,gamma,ax,label=None,color='b'):
    '''
    Add plot of a pareto point to figure.
    '''
    plt.hold(True)
    
    if len(fVect)==2:
        fPrev = fPrev[0]
        ax.arrow(fPrev[0],fPrev[1], fVect[0]-fPrev[0], fVect[1]-fPrev[1], head_width=0.01, head_length=0.01, fc='r', ec='k')
        circ = plt.Circle((fPrev[0],fPrev[1]),gamma, color='b', fill=False)
        ax.add_artist(circ)
        ax.plot(fVect[0],fVect[1],'or')
        
    elif len(fVect)==3:           
        ax.scatter(fVect[0],fVect[1],fVect[2],color=color)
        if label: 
            font = FontProperties()
            font.set_size('x-small')
            ax.text(fVect[0],fVect[1],fVect[2],str(label),fontproperties=font)
        
    plt.draw()
    plt.pause(0.0001)
        
def addplot2D(fVect,fPrev,gamma,ax):
    '''
    Add plot of a pareto point to figure.
    '''
    plt.hold(True)
    ax.arrow(fPrev[0],fPrev[1], fVect[0]-fPrev[0], fVect[1]-fPrev[1], head_width=0.01, head_length=0.01, fc='r', ec='k')
    circ = plt.Circle((fPrev[0],fPrev[1]),gamma, color='b', fill=False)
    ax.add_artist(circ)
    ax.plot(fVect[0],fVect[1],'or')
    #ax.set_xlim(0,1.1)
    #ax.set_ylim(0,10.5)
    ax.axis('equal')
    
    
    plt.draw()
    plt.pause(0.0001)

def addplot3D(fVect,fPrev,gamma,ax):
    '''
    Add plot of a pareto point to figure.
    '''
    plt.hold(True)
    ax.scatter(fVect[0],fVect[1],fVect[2],'or')
    #ax.set_xlim(0,1.1)
    #ax.set_ylim(0,10.5)
    ax.axis('equal')

    plt.draw()
    plt.pause(0.0001)
    
def Plot1stOrderOpt(gridpoints,values):
    
    from mpl_toolkits.mplot3d import Axes3D
    
    gridpoints.append((0, 0))
    gridpoints.append((0, max(gridpoints)[0] + 1))
    gridpoints.append((max(gridpoints)[0] +1 , 0))
    values = np.append(values,[1e-4]*3)
    num = len(values)
    dBar = .25
    fig = plt.figure()
    ax1 = fig.add_subplot(111, projection='3d')
    ax1.set_xlabel('grid position [0]')
    ax1.set_ylabel('grid position [1]')
    ax1.set_zlabel('1st Order Opt')
    
    
    xtick = [pos[0] for pos in gridpoints]
    ytick = [pos[1] for pos in gridpoints]
    xpos = [tick - dBar/2. for tick in xtick]
    ypos = [tick - dBar/2. for tick in ytick]
    plt.xticks(xtick)
    plt.yticks(ytick)
    plt.gca().invert_xaxis()
    zpos = [0] * num
    dx = np.ones(num)*dBar
    dy = np.ones(num)*dBar
    dz = values
    
    ax1.bar3d(xpos, ypos, zpos, dx, dy, dz, color='#00ceaa')
    plt.show()
        
# Check for response function in model definition
# --------------------------------------------------------------------------- #
def CheckForResponseModelInstances(Model):
    '''
    Checks if there is a response function in model definition.
    
    :param Model: model class (in model.py file) that should contain methods named:
                    1.) 'ComputeResp(x)'
                    2.) 'ComputeSens(x)'
    :return Sens: Flag for sensivity computation (if false, FDs are used)
    '''
    if not hasattr(Model, 'ComputeResp'):
        logging.fatal('Simulation model %s has no instance ComputeResp' + \
                      '-> Is needed for all functions of this script\nSolution: Define instance such as [Responses] = ComputeResp(x)' % (
                          Model.__class__.__name__))
        sys.exit('Simulation model has no attribute ComputeResp which is needed to compute responses!')
    if not hasattr(Model, 'ComputeSens'):
        logging.warning(
            'Simulation model %s has no instance ComputeSens -> So finite difference is used for sensitity analysis!' % (
                Model.__class__.__name__))
        Sens = False
    else:
        logging.info('Simulation model %s has instance ComputeSens -> So sensitities will be used!' % (
            Model.__class__.__name__))
        Sens = True
    return Sens
    
def CheckForObjAndConstrFctModelInstances(Model):
    '''
    Checks if there is an objective and constraint functions in model definition.
    
    :param Model: model class (in model.py file)  that should contain methods named:
                    1.) 'ObjAndConstrFct(x)'
                    2.) 'ObjAndConstrSensFct(x)'
    :return Sens: Flag for sensivity computation (if false, FDs are used)
    '''
    if not hasattr(Model, 'ObjAndConstrFct'):
            logging.fatal('Simulation model %s has no instance ObjAndConstrFct' + \
                          '-> Is needed for all functions of this script\nSolution: Define instance such as [f, g] = ObjAndConstrFct(x)' % (
                              Model.__class__.__name__))
            sys.exit(
                'Simulation model has no attribute ObjAndConstrFct which is needed to compute optimization responses!')
    if not hasattr(Model, 'ObjAndConstrSensFct'):
        logging.warning(
            'Simulation model %s has no instance ObjAndConstrSensFct -> So finite difference is used for sensitity analysis!' % (
                Model.__class__.__name__))
        Sens = False
    else:
        logging.info('Simulation model %s has instance ObjAndConstrSensFct -> So sensitities will be used!' % (
            Model.__class__.__name__))
        Sens = True
    return Sens
    
# Plot design space
# --------------------------------------------------------------------------- #
def PlotDesignSpace(xOpt, ObjConstrFct, DVIndicies, xU, xL, NumSup = 100., NumConstr = 20):
    '''
    Plots the design space
    '''
    values = range(int(NumConstr))
    jet = cm = plt.get_cmap('jet') 
    import matplotlib.colors as colors
    cNorm  = colors.Normalize(vmin=0, vmax=values[-1])
    import matplotlib.cm as cmx
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)
    ConstraintColors = [scalarMap.to_rgba(iVal) for iVal in values]
    
    # Initialize and compute f and g values over meshgrid
    # ------------------------------------------------------------------- # 
    x = np.linspace(xL[DVIndicies[0]], xU[DVIndicies[0]], num=NumSup)
    y = np.linspace(xL[DVIndicies[1]], xU[DVIndicies[1]], num=NumSup)
    
    X, Y = np.meshgrid(x, y)
    (fList, gList, fArray, gArray) = (list(), list(), list(), list())
    for (RowX, RowY) in zip(X,Y):
        for (iX, iY) in zip(RowX, RowY):
            xEval = cp.deepcopy(xOpt)
            xEval[DVIndicies] = [iX, iY]
            TMPRes = ObjConstrFct(xEval)
            fList.append(TMPRes[0])
            if len(gList)==0:
                for gIndex, iG in enumerate(TMPRes[1]):
                    gList.append([iG])
            else:
                for gIndex, iG in enumerate(TMPRes[1]):
                    gList[gIndex].append(iG)
        fArray.append(fList)
        if len(gArray)==0:
            for gIndex, iG in enumerate(gList):
                gArray.append([iG])              
        else:
            for gIndex, iG in enumerate(gList):
                gArray[gIndex].append(iG)  
        (fList, gList) = (list(), list())
    fArray = np.array(fArray)
    for gIndex, iG in enumerate(gArray):
        gArray[gIndex] = np.array(iG)
    
    # Plot objective
    # ------------------------------------------------------------------- #  
    levels = np.linspace(fArray.min(), fArray.max(), num=15)
    ObjIsoLines = plt.contour(X, Y, fArray, levels,
                   colors = ('k',),
                   linewidths = (2,),
                   origin = 'lower')
                   
    # Plot constraint fillets
    # ------------------------------------------------------------------- #  
    levels = [0., 1000.]
    for gIndex, iG in enumerate(gArray):
        plt.contourf(X, Y, iG, levels,
                                colors = (ConstraintColors[gIndex],),
                                origin='lower')                                
    plt.title('Design Space')
    plt.clabel(ObjIsoLines, fmt = '%2.1f', colors = 'k', fontsize=14)