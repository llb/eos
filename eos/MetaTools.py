# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import pickle
import numpy as np
import os

class SampleData:
    '''
    Initializes an object, wich can be used to store all function evaluations
    for a subsequent meta modelling. This object can for instance be usefull 
    within a gradient-based optimization.
    '''
    ZeroTol = 1e-4 # Tolerance for design variables to be regarded as zero
    
    def __init__(self, SampleDataFilename=None, FilePath=None):
        if FilePath == None:
            FilePath = os.getcwd()
        if SampleDataFilename != None:
            self.SampleDataFilename = os.path.join(FilePath,SampleDataFilename)
        else:
            self.SampleDataFilename = os.path.join(FilePath,'SampleDataTMP.p')
        if not os.path.isfile(self.SampleDataFilename): 
            SampleData = dict()
            SampleData['xDoE'] = list()
            SampleData['RespDoE'] = list()
            SampleData['x1perc'] = list()
            SampleData['Resp1perc'] = list()
            SampleData['x10perc'] = list()
            SampleData['Resp10perc'] = list()            
            pickle.dump(SampleData, open(self.SampleDataFilename,'wb'))
    
    def StoreEvaluation(self, x, Resp):
        '''
        Stores design and response vector in temporary file for later meta-modelling
        
        param: x: Vector associated with response vector "Resp"
        param: Resp: Vector of responses
        '''
        # Set small numbers to ZeroTol
        x[abs(x)<self.ZeroTol] = self.ZeroTol
        # Load all old sample points
        SampleData = pickle.load( open(self.SampleDataFilename,'rb'))
        # Find percentage difference
        SmallestNorm = 1.e20
        for iX in SampleData['xDoE']:
            if np.size(iX)>1:
                NormTempList = list()
                for (jX, jy) in zip(iX,x):
                    NormTempList.append(abs((jy-jX)/jy))
                NormTemp = max(NormTempList)
            else:
                NormTemp = abs((x-iX)/x)
            if NormTemp < SmallestNorm:
                SmallestNorm = np.linalg.norm(iX-x)
        if SmallestNorm < 0.01:
            SampleData['x1perc'].append(x)
            SampleData['Resp1perc'].append(Resp)
        elif SmallestNorm < 0.1:
            SampleData['x10perc'].append(x)
            SampleData['Resp10perc'].append(Resp) 
        else:
            SampleData['xDoE'].append(x)
            SampleData['RespDoE'].append(Resp)
        # Store updated SampleData dict
        pickle.dump(SampleData, open(self.SampleDataFilename,'wb'))
        
    def CombineSampleData(self,FileA, FileB):
        # Load sample points
        SampleData = pickle.load( open(FileA,'rb'))
        SampleDataB = pickle.load( open(FileB,'rb'))
        self.SampleDataFilename = SampleData
        for (iX, iResp) in zip(SampleDataB['xDoE'], SampleDataB['RespDoE']):
            self.StoreEvaluation(iX, iResp)
        for (iX, iResp) in zip(SampleDataB['x1perc'], SampleDataB['Resp1perc']):
            SampleData['x1perc'].append(iX)
            SampleData['Resp1perc'].append(iResp)
        for (iX, iResp) in zip(SampleDataB['x10perc'], SampleDataB['Resp10perc']):
            SampleData['x10perc'].append(iX)
            SampleData['Resp10perc'].append(iResp)
        
    def ComputeRSA(self):
        pass