# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

from numpy.linalg   import norm
from itertools      import product
from itertools      import izip
from itertools      import chain
import numpy    as np
import copy     as cp

import logging
import sys
import os

class VectorOptimizer:
    def __init__(self, Model, DesOpt, Options, nDV):
        self.Model          = Model
        self.DesOpt         = DesOpt
        self.Options        = Options
        self.VecOptions     = Options['VecOptions']
        self.nDV            = nDV
        self.nNR            = self.VecOptions['nNR']
        self.linesearchNR   = self.VecOptions['linesearchNR']
        self.tolNR          = 1e-4
        self.randNR         = 1e-4
        self.SubstModel     = SubstituteModel(Model, Options, nDV)       
        self.DesOpt.Model   = self.SubstModel
        self.DesOpt.EOSOptOptions['ConvergencePlots'] = False
        self.DVTypes        = cp.deepcopy(self.DesOpt.EOSOptOptions['DVTypes'])
        self.GridType       = self.VecOptions['GridType']
        self.GridDir        = self.VecOptions['GridDir']
            
        # add IterationUserOutput if defined in model.py to SubstituteModel            
        if hasattr(self.Model, 'IterationUserOutput'):
            def IterationUserOutput(self,*args):
                return self.Model.IterationUserOutput(*args)
            IterationUserOutput.__doc__ = "Output function at each iteration is provided by user."
            IterationUserOutput.__name__ = "IterationUserOutput"
            setattr(SubstituteModel,IterationUserOutput.__name__,IterationUserOutput)          

    def GetGrid(self,GridType,GridDir,npo):
        '''
        Get grid for evaluation points
        Arguments:  - npo: Number of points along edge
                    - GridType: Type of grid building order
                    - GridDir: Direction of Grid building order
        Returns:    - list of grid points
        '''
        grid = list()        
        
        for i in range(npo):
            if   GridType == 'edgesfirst': 
                if GridDir   == 0: l = list(izip([i]*(npo-i),range(i,npo-i))) + list(izip(range(i+1,npo-i),[i]*(npo-i)))
                elif GridDir == 1: l = list(izip(range(i,npo-i),[i]*(npo))) + list(izip([i]*(npo-i),range(i+1,npo-i)))
            elif GridType == 'triangle':
                if GridDir   == 0: l = list(izip(range(0,i+1,1),range(i,0-1,-1)))
                elif GridDir == 1: l = list(izip(range(i,0-1,-1),range(0,i+1,1)))
            elif GridType == 'linebyline':
                if GridDir   == 0: l = list(izip([i]*(npo-i),range(npo-i)))
                elif GridDir == 1: l = list(izip(range(npo-i),[i]*(npo-i)))
            else: 
                sys.exit('UNKNOWN GRIDTYPE.')
            grid.append(l)
            
        return list(chain(*grid))
        
    def GetLinePoints(self, p0, p1, npo):
        '''
        Get grid points between two evaluation points
        Arguments:  - p0: first grid point
                    - p0: second grid point
                    - npo: number of points along edge
        Returns:    - 1D-array of grid points
        '''
        return np.array([np.linspace(p0[0],p1[0],npo),np.linspace(p0[1],p1[1],npo),np.linspace(p0[2],p1[2],npo)]).T
    
    def GetEvalPoints(self, p, npo):
        '''
        Get evaluation (starting) points on hyperplane inner triangle
        Arguments:  - p: array of three points that form a hyperplane triangle (anchor points)
                    - npo: number of points along edge
        Returns:    - 3D-array of evaluation points
                    - 1D-array normal of hyperplane
        '''
        points   = np.zeros((npo,npo,3))
        
        points02 = self.GetLinePoints(p[0], p[2], npo)
        points12 = self.GetLinePoints(p[1], p[2], npo)
    
        for ll in range(npo):
            llpoints = self.GetLinePoints(points02[ll], points12[ll], npo-ll)
            for pp, point in enumerate(llpoints):
                points[ll,pp] = point

        normal = np.cross(p[0]-p[1], p[0]-p[2])
        normal = normal / norm(normal)
    
        return points, normal

    def NewtonRaphson(self, x0, xL, xU, f):
        '''
        Get corresponding x for given f (starting point on hyperplane inner triangle)
        Arguments:  - x0: initial start point for x in Newton Raphson search
                    - xL: lower bounds of x
                    - xU: upper bounds of x
                    - f: given f for corresponding (to calculate) x
        Returns:    - X: closest calculated corresponding x for f
                    - F: actual function value of calculated x
        '''
        
        # Initialize
        Xhist, Fhist, nhist   = list(), list(), list()
        x                     = cp.deepcopy(x0)
        xNum                  = len(x)
        
        # Start Newton Raphson Loop
        for i in range(self.nNR):
            
            # Check if x in bounds and andjust
            for j in range(xNum):
                if   x[j] < xL[j]: x[j] = xL[j]
                elif x[j] > xU[j]: x[j] = xU[j]
                else: pass
            
            # Print info
            print('')
            print('NEWTON RAPHSON LOOP %i' % i)
            logging.info('# ------------------------------------------------------------ #')
            logging.info('# ---               Newton Raphson Loop %i                  --- #',i)
            logging.info('# ------------------------------------------------------------ #')
            logging.info('x = %s',x)
            
            # Calculate F, dF
            X            = cp.deepcopy(x)
            F,G,fail     = self.Model.ObjAndConstrFct(X)
            dF,dG,fail   = self.Model.ObjAndConstrSensFct(X, F, G)
            
            # Calculate distance vector r, dr and distance n
            r            = np.array(F)-np.array(f)
            dr           = np.array(dF)
            n            = norm(r)
            Xhist.append(X)
            Fhist.append(F)
            nhist.append(n)
            logging.info('Norm = %s',n)
            
            # Calculate pseudo inverse matrix drI
            drI          = np.linalg.pinv(dr)
                        
            # Calculate search direction vector
            nlrange      = list()
            direction    = np.dot(drI,r)
            directionNorm= norm(direction)
            
            if n < self.tolNR or directionNorm < self.tolNR: break
            else:
                if self.linesearchNR:
                    # Calculate length of dx via linesearch
                    h       = 1e-4
                    lrange  = np.array([directionNorm-h,directionNorm,directionNorm+h])
                    for l,length in enumerate(lrange):
                        dxl           = length * direction/directionNorm
                        Xl            = x - dxl
                        # Print info
                        print('')
                        print('NEWTON RAPHSON LINESEARCH %i ' % l)
                        logging.info('# ------------------------------------------------------------ #')
                        logging.info('# ---            Newton Raphson Linesearch %i               --- #',l)
                        logging.info('# ------------------------------------------------------------ #')
                        logging.info('x = %s',Xl)
                        Fl,Gl,fail    = self.Model.ObjAndConstrFct(cp.deepcopy(Xl))
                        rl            = np.array(Fl)-np.array(f)
                        rlNorm        = norm(rl)
                        nlrange.append(rlNorm)
                        
                    c       = np.dot(np.array(nlrange), np.linalg.inv(np.array([lrange**ni for ni in range(len(lrange))])))
                    Length    = -0.5*c[1]/c[2]
                    dx      = Length * direction/directionNorm
                else:
                    dx  = direction
               
                # Calculate new x
                x -= dx
        
        # Return X, F
        imin = np.argmin(nhist)
        return Xhist[imin], Fhist[imin]
        
    def Optimize(self,x0,xL,xU):
        logging.info('$$ ------------------------- $$')
        logging.info('$$ START VECTOR OPTIMIZATION $$')
        logging.info('$$ ------------------------- $$')
        
        # Assign local variables
        # --------------------------------------------------------- #
        nf      = self.Options['nf']                # Number of objectives
        nDV     = self.nDV                          # Number of design variables
        npo     = self.VecOptions['NumParetoOptima']
        
        TotCalls= 0                                 # Total number of function calls
        
        Pareto, Local = [],[]                   # Result lists of returned dictionaries

        # Generate hyperplane grid
        # --------------------------------------------------------- #
        
        gridpoints = self.GetGrid(self.GridType,self.GridDir,npo)
        print '**************** Gridpoints:', gridpoints
        # Position of edge points
        A = np.array(list(product(range(npo),repeat=(nf-1)))[:npo])        
        EdgFilter = np.array([A,np.fliplr(A)])
        # Position of anchor points
        AncFilter = np.row_stack((np.zeros(nf-1,dtype='intp'),np.eye(nf-1,dtype='intp')*(npo-1))).tolist()

        # Compute anchor points
        # --------------------------------------------------------- #
        self.FAnc = np.zeros((nf,nf))        
        if self.VecOptions['EdgePoints'] != None:
            xEdg = self.VecOptions['EdgePoints']
            xAnc = np.array([xEdg[0][0],xEdg[0][-1],xEdg[1][-1]])
        elif self.VecOptions['AnchorPoints'] != None:
            xEdg = None
            xAnc = self.VecOptions['AnchorPoints']
        else:
            sys.exit('ETHER ANCHOR POINTS OR EDGE POINTS MUST BE PROVIDED (IN DESIGN SPACE).')
            
        for i in range(len(xAnc)):
            print('')
            print('ANCHOR POINT POSITION: %s' % (tuple(AncFilter[i]),))
            logging.info('# ************************************************************ #')
            logging.info('# ***            ANCHOR POINT POSITION: %s             *** #',tuple(AncFilter[i]))
            logging.info('# ************************************************************ #')
            FTemp,GTemp,_ = self.Model.ObjAndConstrFct(xAnc[i])
            self.FAnc[i] = FTemp
            logging.info('Fanc = %s',FTemp)
        Fx0,Gx0,_ = self.Model.ObjAndConstrFct(x0)
            
        # Compute hyperplane start points
        # --------------------------------------------------------- #
        Fstart, Normal = self.GetEvalPoints(p=self.FAnc, npo=npo)
        Fopt = cp.deepcopy(Fstart)
        
        # Compute utopia and nadir points
        # --------------------------------------------------------- #
        FUto = np.array([min(self.FAnc[:,k]) for k in range(nf)])
        FNad = np.asarray([max(self.FAnc[:,k]) for k in range(nf)])
        
        # Plotting
        # --------------------------------------------------------- #
        from OptTools import ParetoPlotting, addplot
        runfig, runax = ParetoPlotting(FNad,FUto,self.FAnc)
        
        # Compute edge points
        # --------------------------------------------------------- #
        if self.VecOptions['EdgePoints'] != None:
            for E, EdgePoints in enumerate(xEdg):
                for e, EdgePoint in enumerate(EdgePoints):
                   pos = EdgFilter[E][e].tolist()
                   if pos not in AncFilter: 
                        print('')
                        print('EDGE POINT POSITION: %s' % (tuple(pos),))
                        logging.info('# ************************************************************ #')
                        logging.info('# ***            EDGE POINT POSITION: %s             *** #',pos)
                        logging.info('# ************************************************************ #')
                        FTemp,GTemp,_ = self.Model.ObjAndConstrFct(EdgePoint)
                        Fopt[tuple(pos)] = FTemp
                        logging.info('Fedge = %s',FTemp)
                        gridpoints.remove(tuple(pos))
                        addplot(FTemp,None,None,runax,str(pos)[1:-1])
        [gridpoints.remove(tuple(AncFilter[i])) for i in range(len(AncFilter))]
        
        # Plot Gridpoints
        # --------------------------------------------------------- #
        if self.VecOptions['GridpointPlots']:
            for pos in gridpoints: 
                addplot(Fstart[pos],None,None,runax,color='g')
        
        # Compute Constraint Line with Spray
        # --------------------------------------------------------- #
        Spray = True
        PVec = np.array([np.mean(self.FAnc.T[0]),np.mean(self.FAnc.T[1]),np.mean(self.FAnc.T[2])])
        if   self.VecOptions['Spray'] == 'Farray': FVec = self.VecOptions['SprayValue']
        elif self.VecOptions['Spray'] == 'Utopia': FVec = FNad + self.VecOptions['SprayValue'] * (FUto-FNad)
        elif self.VecOptions['Spray'] == 'Fstart': FVec = Fx0
        elif self.VecOptions['Spray'] == 'Normal': FVec = PVec + Normal * self.VecOptions['SprayValue']
        else: Spray = False
        if Spray: 
#            addplot(PVec,None,None,runax,'Pnormal',color='g')
            addplot(FVec,None,None,runax,'Pspray',color='m')
#            X,Y,Z = np.array([PVec,FVec]).T.tolist()
#            runax.plot_wireframe(X,Y,Z,color='g')
        addplot(Fx0,None,None,runax,'F(x0)',color='r')
                
        # --------------------------------------------------------- #                
        # LOOP OVER ALL GRID POINTS
        # --------------------------------------------------------- #
        for pos in gridpoints:
            
            print('')
            print('INNER PARETO POSITION: %s' % (str(pos),))
            logging.info('# ************************************************************ #')
            logging.info('# ***            INNER PARETO POSITION: %s             *** #',pos)
            logging.info('# ************************************************************ #')
            
            # Initial solution
            # --------------------------------------------------------------- #
            if self.VecOptions['GS']:  # LOCAL SEARCH MISSING
                from OptTools import GlobalSearchForDesOpt, GSOPostProcessing
                x0GS, gsInfo = GlobalSearchForDesOpt(xL,xU,self.SubstModel.ObjAndConstrFct,
                                                     nh=self.DesOpt.EOSOptOptions['nh'],
                                                     nDoE=self.VecOptions['nDoE'],
                                                     nGS=self.VecOptions['nGS'])
                nLoops = self.VecOptions['nGS']
            elif self.VecOptions['NR']:
                x0, f0 = cp.deepcopy(x0), cp.deepcopy(Fstart[pos])
                x0, F0 = self.NewtonRaphson(x0, xL, xU, f0)
                if self.VecOptions['f0NR']: Fstart[pos] = cp.deepcopy(F0)
                Fstart[pos] -= np.random.rand(len(Fstart[pos]))*self.randNR
                addplot(Fstart[pos],None,None,runax,color='y')
                nLoops = 1
            else:
                x0     = x0
                nLoops = 1
                
            # Assign to DesOpt-model
            # --------------------------------------------------------------- #
            self.DesOpt.xLOpt,self.DesOpt.xUOpt = cp.deepcopy(xL),cp.deepcopy(xU)

            
            # Transfer to substitute model
            # --------------------------------------------------------------- #  
            self.DesOpt.EOSOptOptions['DVTypes'] = self.DVTypes
            self.DesOpt.EOSOptOptions['nh'] = self.Options['nh']
            self.DesOpt.EOSOptOptions['ng'] = self.Options['ng'] + 1
            if Spray: Normal = Fstart[pos] - FVec
            else: pass
            self.SubstModel.Transfer(FUto,FNad,Fstart[pos],Normal)
            
            # Solve SOP
            # --------------------------------------------------------------- #
            Result = []
            for i in range(nLoops):
                if self.VecOptions['GS']:
                    x0 = x0GS[:,i]
                    print('')
                    print('GLOBAL SEARCH ITERATION: %d' % (int(i),))
                    logging.info('# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #')
                    logging.info('# +++            GLOBAL SEARCH ITERATION: %d             +++ #',int(i))
                    logging.info('# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #')
                TempResults                 = self.DesOpt(x0)
                TempResults['x0']           = x0
                TempResults['Pos']          = pos
                TempResults['fOpt-Vect']    = self.SubstModel.fPareto
                TempResults['xOpt']         = self.SubstModel.X
                
                TotCalls                   += len(TempResults['fHist'])
                if self.VecOptions['GS']:
                    TempResults['gsInfo']   = gsInfo
                Result.append(TempResults)
                
            # Process results
            # --------------------------------------------------------------- #
            # Check optimality
            if self.VecOptions['GS']:
                gsinfo = GSOPostProcessing(Result)
                
            # Save best results
            lowU = [n['fOpt'] for n in Result]  # get values of cost-fct from all GSO-result-dictionaries
            indP = lowU.index(min(lowU))        # index of dictionary with lowest u
            xopt = Result[indP]['xOpt']
            fopt = Result[indP]['fOpt-Vect']
            Fopt[pos] = fopt
            logging.info('# ------------------------------------------------------------ #')
            logging.info('# ---               PARETO RESULT AT: %s               --- #',pos)
            logging.info('# ------------------------------------------------------------ #')
            logging.info('x%s = %s',pos,xopt)
            logging.info('f%s = %s',pos,fopt)
            logging.info('1st Order Opt%s = %s',pos,Result[indP]['LagrangianData']['1st Order Opt'])
            logging.info('SCVg%s = %s',pos,sum([v for v in Result[indP]['gHist'][-1][:-1] if v > 0.]))
            logging.info('SCVd%s = %s',pos,sum([v for v in Result[indP]['gHist'][-1][-1:] if v > 0.]))
            
#            if Result[indP]['LagrangianData']['1st Order Opt'] <= 100 and max(Result[indP]['gHist'][-1]) <= .2:
#                addplot(Fopt[pos],None,None,runax,str(pos)[1:-1])
#            else:
#                addplot(Fstart[pos],None,None,runax,str(pos)[1:-1],color='r')
            addplot(Fopt[pos],None,None,runax,str(pos)[1:-1])    
            if self.VecOptions['ConvergencePlots']:
                DirLabel = 'results/ConvergencePlots/'+str(len(Pareto)-1)+'_InnerPoint'
                if not os.path.exists(DirLabel):
                    os.makedirs(DirLabel)
                Label = [DirLabel + '/' + 'ConvPlot.pdf',DirLabel + '/' + 'BarPlot.pdf']
                self.DesOpt.ConvergencePlots(Result[indP]['xHist'],Result[indP]['fHist'],Result[indP]['gHist'],Result[indP]['hHist'],self.Options['ng'],self.Options['nh'],nDV,Label,showit=False)
            runfig.savefig('ParetoFront') 
            
            Pareto.append(Result[indP])
            del Result[indP]
            Local.append(Result)                # save all other dictionaries in "Local"-list

            
        # ------------------------------------------------------------------- #
        # PostProcessing
        # ------------------------------------------------------------------- #
        for npar in Pareto: # Rename fOpt
            npar['uOpt'] = npar.pop('fOpt')
            npar['fOpt'] = npar.pop('fOpt-Vect')
            try:
                npar['uHist'] = npar.pop('fHist')
            except:
                pass
			
        # Infos at the very end
        Summary = dict()
        Summary['f_opt'],Summary['x_opt'],Summary['u_opt'] = [],[],[]
        Summary['Shadow'], Summary['1st Order Opt'], Summary['gridpoints'] = [], [], []
        for sol in Pareto:
            Summary['f_opt'].append(sol['fOpt'])
            Summary['x_opt'].append(sol['xOpt'])
            Summary['u_opt'].append(sol['uOpt'])
            Summary['Shadow'].append(sol['LagrangianData']['Lambda Values'])
            Summary['1st Order Opt'].append(sol['LagrangianData']['1st Order Opt'])
            
        for key in Summary.keys():
            Summary[key] = np.array(Summary[key])
        
        Summary['gridpoints'] = gridpoints
        Summary['Anchor'] = self.FAnc
        Summary['Utopia'] = FUto
        Summary['Nadir']  = FNad
        Summary['fCalls'] = TotCalls
        Summary['Fopt']   = Fopt
#        Pareto.append({'FNad':FNad,'FUto':FUto,'FAnc':FAnc})
        if self.VecOptions['1stOrderOptPlots']:
            from OptTools import Plot1stOrderOpt
            Plot1stOrderOpt(gridpoints,Summary['1st Order Opt'])
        
        print('############### EQUIDISTANT PARETO COMPUTATION DONE. #################')
        return {'Summary':Summary, 'Detailed':Pareto}, Local, runfig, runax
        
class SubstituteModel:
    def __init__(self, Model, Options, nDV):
        self.Options    = Options
        self.VecOptions = Options['VecOptions']
        self.Model      = Model
        self.nDV        = nDV
        self.gdTol       = 1e-1
        
        self.xList,self.dxList      = [],[]
        self.fgList,self.dfdgList   = [],[]
    
    def Transfer(self, FUto, FNad, Fs, Normal):
        self.Fs = Fs
        self.Normal = Normal/norm(Normal)
        if self.VecOptions['NormalizeF']:
            self.Flow,self.Fup = FUto,FNad
        else:
            self.Flow,self.Fup = np.zeros(self.Options['nf']), np.ones(self.Options['nf'])
    
    def NormF(self, F):
        return (F-self.Flow)/(self.Fup-self.Flow)
        
        
    def ObjAndConstrFct(self, x): 
        self.X      = x        
        F,G,fail    = self._getFG(x)
        Fs          = self.Fs
        n           = self.Normal
        
        # u: utility function        
        u = sum(F)
        # g: constraints
        r = np.cross(F-Fs,n)
        gd = norm(r) - self.gdTol
        g = np.hstack((G, gd))
        
#        # g: constraints
#        r = np.dot(Fs-F,n)/np.dot(n,n) 
#        D = Fs-F-r*n
#        gD = abs(D) / self.gdTol - 1.
#        g = np.hstack((G, gD))
        
        return u,g,0
        
    def ObjAndConstrSensFct(self, x, u, g):
        F,G,fail   = self._getFG(x)
        dF,dG,fail = self._getdFdG(x,F,G)
        Fs         = self.Fs
        n          = self.Normal
       
        # du: utility function        
        du = np.sum(dF,axis=0).tolist()
        
        # dg: constraints (iterate over every xi in dF.T)
        dgd = np.zeros(np.shape(dF.T))
        r  = np.cross(F-Fs,n)
        for i, dFi in enumerate(dF.T):
            dri = np.cross(dFi,n)
            dgd[i] = np.dot(dri,r)/norm(r)
        dg = np.vstack((dG, dgd.T))
        
#        # g: constraints
#        r = np.dot(Fs-F,n)/np.dot(n,n) 
#        D = Fs-F-r*n
#        dD = np.zeros(np.shape(dF.T))
#        for i, dFi in enumerate(dF.T):
#            dri = np.dot(-dFi,n) / np.dot(n,n)
#            dD[i] = -dFi - dri*n
#        dD = dD.T
#        dgD = np.sign(D) * dD / self.gdTol
#        dg = np.vstack((dG, dgD))
            
        return du,dg,0
    
        
    def _getFG(self, x):
        '''
        Get objective and constraint values from model.py-file.
        Arguments:  - x: Design variable vector
        Returns:    - F: Objective value vector
                    - G: Constraints value vector
        '''
        F,G,fail = self.Model.ObjAndConstrFct(x)
        self.xList.append(x)
        self.fgList.append([F,G])
        self.fPareto = F
        return F, G, fail

    def _getdFdG(self, x, F, G):
        '''
        Get derivatives of objectives and constraints from model.py-file.
        Arguments:  - x: Design variable vector
        Returns:    - F: Objective value vector
                    - G: Constraints value vector
        '''
        dF,dG, fail = self.Model.ObjAndConstrSensFct(x, F, G)
        self.dxList.append(x)
        self.dfdgList.append([dF,dG])
        return dF, dG, fail