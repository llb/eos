# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

"""
Created on Sun Oct 11 23:28:05 2015

@author: Sebastian
"""
# Needed imports:
import logging
import sys
import os
import numpy as np
import copy as cp
import pickle
import DesOpt
import datetime
from eos import SimulationMaker

def VariableMapping(x, ConIntIndices, ConIntChoices, ConDisIndices, Options):
    
    '''    
    --------------------------------------------        
    remapping of the optimzation continuous design variables into the original discrete ones          
    --------------------------------------------
    Arguments:

    :x: a set of of mapped variables (original -> APSIS), which has to be remapped (APSIS -> original)
    :ConIntIndices, ConIntChoices, ConDisIndices, Options: internal specifications of APSIS for the remapping     
    
    Returns:
    
    :xOriginal: the original set of variables    
    '''    
    TransformedInt = [] # already mapped continuous into integer design variables are stored here      
    TransformedDis = [] # already mapped continuous into discrete design variables are stored here         
    xOriginal   = []         
 
    for (index, DV) in enumerate(x):        
        IntCheck    = False
        DisCheck    = False            
        # test, if current continuous design variable belongs to an integer in the original model            
        for IntItem in ConIntIndices.items():                         
            # remapping of all continuous variables which represent the same integer variable                 
            if (index in IntItem[1] and not index in TransformedInt):
                xInt = x[IntItem[1][0]: (IntItem[1][-1] + 1)]                                        
                # the sum of mapped continuous design variables equals to 1                     
                xInt.append(1-sum(xInt))                        
                xIntSum   = 0                    
                # the sum of each design variable multiplied with the  respective integer variable choice results in the integer design variable                     
                for (IntFactor, IntChoice) in zip(xInt, ConIntChoices[IntItem[0]]):
                    xIntSum += IntFactor * IntChoice    
                xOriginal.append(xIntSum)
                TransformedInt.extend(IntItem[1])        
                IntCheck = True
            elif (index in IntItem[1] and index in TransformedInt):
                IntCheck = True
            else:
                pass
        
        # test, if current continuous design variable belongs to an discrete design variable in the original model               
        for DisItem in ConDisIndices.items():             
            # remapping of all continuous variables which represent the same discrete variable               
            if (index in DisItem[1] and not index in TransformedDis):
                xDis = x[DisItem[1][0]: (DisItem[1][-1] + 1)]                                        
                # the sum of mapped continuous design variables equals to 1                    
                xDis.append(1-sum(xDis))                        
                xDisSum   = 0                    
                # the sum of each design variable multiplied with the  respective discrete variable choice results in the discrete design variable                    
                for (DisFactor, DisChoice) in zip(xDis, Options['DiscreteDVChoices'][DisItem[0]]):
                    xDisSum += DisFactor * DisChoice    
                xOriginal.append(xDisSum)
                TransformedDis.extend(DisItem[1])                    
                DisCheck = True
            elif (index in DisItem[1] and index in TransformedDis):
                DisCheck = True
            else:
                pass
        
        if (IntCheck==False and DisCheck==False):
            #continuous variables do not change             
            xOriginal.append(DV) 
       
    xOriginal    = np.asarray(xOriginal) 
            
    return xOriginal   
        
        
# ------------------------------------------------------------------------------------------------ #
#                                   ALGORITHM CONTROL
# ------------------------------------------------------------------------------------------------ #
class APSIS:
    def __init__(self, x0, xL, xU, Model, Alg, Norm, Options, AlgOptions):
        # Store stuff
        # ------------------------------------------------------------------- #
        self.xL         = xL  
        self.xU         = xU  
        self.Model      = Model
        self.Options    = cp.deepcopy(Options)
        self.Alg        = Alg
        self.Norm       = Norm        
        self.x0         = x0 # Set of n design variables for which objectives and constraints are evaluated (NEVER NORMALIZED => DesOpt normalizes)       
        self.AlgOptions = AlgOptions        
        self._RelaxDS()
        # Important!!!: self.nDV created in self._RelaxDS() determines the number of design variables of the modififed APSIS problem, and not of the original one        
        # check which ramping type is desired        
        if 'Const' in self.Options['Ramp'].keys():
            self.WeightingType  = 'Const'
        elif ('NuStart' in self.Options['Ramp'].keys() and 'NuEnd' in self.Options['Ramp'].keys()\
        and 'iterStart' in self.Options['Ramp'].keys() and 'iterEnd' in self.Options['Ramp'].keys() and not 'Const' in self.Options['Ramp'].keys()):
            self.WeightingType  = 'Ramp'
        else:
            WeightingErrorStr = '\nNo specification for the weighting factor of the penalty term found: \nConstant weighting needs key \'Const\' in Options[\'Ramp\']\n\
            For ramping the weighting factor, the keys \'NuStart\', \'NuEnd\', \'iterStart\' and \'iterEnd\' are required\n\
            A specification of a constant and ramped weighting factor is not allowed'
            logging.fatal(WeightingErrorStr)                
            sys.exit(WeightingErrorStr)        
        
    def Optimize(self):
        
        if (not self.Options['NoPresizing'] and 'c' in self.Options['DVTypesOriginal']):        
            self.Options['DVTypes'] = self.Options['DVTypesPre']
            # Presizing of the continuous variables to avoid local optima
            APSISPresizingSubstituteModel = PresizingSubstituteModel(self.Model, self.ConIntIndices, self.ConIntChoices, self.ConDisIndices, self.nDV, self.Options)    
            APSISPresizingSimulationModel = SimulationMaker(APSISPresizingSubstituteModel)
            APSISPresizingSimulationModel.CheckForObjAndConstrFctModelInstances()
            self.DesOptObj = DesOpt.DesOpt(APSISPresizingSubstituteModel, np.array(self.xLPre), np.array(self.xUPre), self.Alg, self.Norm, self.Options, self.AlgOptions, APSISPresizingSimulationModel.Sens)
            self.DesOptObj.OptLabel = 'PreSizing-' + self.Model.__class__.__name__+'-'+datetime.datetime.now().strftime("%Y%m%d%H%M")
            PresizingResults = self.DesOptObj(np.array(self.x0Pre))

            # -----------------------------------        
            # creating new x0 from  the xOpt of presizing
            # -----------------------------------           
            x0APSIS = [0.]*self.nDV
            TransformedIndices = []        
            
            # integer and discrete design variables equal to a mean value of the respective choices, e.g. 4 choices mean 3 design variables, which all take the value 1/4
            for IntItem in self.ConIntIndices.items():
                x0APSIS[IntItem[1][0] : (IntItem[1][-1]+1)] = [1/float(len(IntItem[1])+1)]*len(IntItem[1])
                TransformedIndices.extend(IntItem[1])
            for DisItem in self.ConDisIndices.items():
                x0APSIS[DisItem[1][0] : (DisItem[1][-1]+1)] = [1/float(len(DisItem[1])+1)]*len(DisItem[1])
                TransformedIndices.extend(DisItem[1])
            
            # the continuous design variables equal to the xOpt from the presizing results            
            if (len(x0APSIS) - len(TransformedIndices)) != len(PresizingResults['xOpt']):
                ErrorString = '\n#########################################\n\
                Numbers of design variables for presizing and APSIS do not match\n\
                #########################################\n'            
                logging.fatal(ErrorString)
                sys.exit(ErrorString)
                
            i = 0
            for (index, item) in enumerate(x0APSIS):
                if not index in TransformedIndices:
                    x0APSIS[index] = PresizingResults['xOpt'][i]
                    i += 1
                else:
                    pass        
        
        # if no presizing is applied, all design variables equal to the desired input x0
        # TODO: either mean value for all integer and discrete design variables as above, or a sampling -> option required         
        else:
            
            # -----------------------------------        
            # integer and discrete design variables equal to a mean value of the respective number of choices 
            # -----------------------------------  
            x0APSIS = [0.]*self.nDV
            TransformedIndices = []        
            
            # integer and discrete design variables equal to a mean value of the respective choices, e.g. 4 choices mean 3 design variables, which all take the value 1/4
            for IntItem in self.ConIntIndices.items():
                x0APSIS[IntItem[1][0] : (IntItem[1][-1]+1)] = [1/float(len(IntItem[1])+1)]*len(IntItem[1])
                TransformedIndices.extend(IntItem[1])
            for DisItem in self.ConDisIndices.items():
                x0APSIS[DisItem[1][0] : (DisItem[1][-1]+1)] = [1/float(len(DisItem[1])+1)]*len(DisItem[1])
                TransformedIndices.extend(DisItem[1])
                
            for (index, item) in enumerate(x0APSIS):
                if not index in TransformedIndices:
                    x0APSIS[index] = self.x0Mod[index]
                else:
                    pass                    
            
            #all design variables equal to the desired input x0             
            #x0APSIS = self.x0Mod    
        
        # -----------------------------------        
        # initialise substitute model for APSIS
        # -----------------------------------        
        APSISSubstituteModel = APSISProblemSubstituteModel(self.Model, self.ConIntIndices, self.ConIntChoices, self.ConDisIndices, self.Options, self.WeightingType, self.nDV)
        
        self.Options['ng'] = APSISSubstituteModel.ng       
        self.Options['DVTypes'] = self.Options['DVTypesMain']
        # check if sensitivities in substitute model, when not using finite differences?
        APSISSimulationModel = SimulationMaker(APSISSubstituteModel)
        APSISSimulationModel.CheckForObjAndConstrFctModelInstances()

        # initialize a DesOpt object, whereby the model is the generated substitute model with the modifified objective and constraint functions      
        self.DesOptObj = DesOpt.DesOpt(APSISSubstituteModel, np.array(self.xLMod), np.array(self.xUMod), self.Alg, self.Norm, self.Options, self.AlgOptions, APSISSimulationModel.Sens)
        self.DesOptObj.OptLabel = 'APSIS-' + self.Model.__class__.__name__+'-'+datetime.datetime.now().strftime("%Y%m%d%H%M")
        # Conduct optimization with x0APSIS:
        if (not self.Options['NoPresizing'] and 'c' in self.Options['DVTypesOriginal']):            
            return (self.DesOptObj(np.array(x0APSIS)), PresizingResults)
                
        else:
            return self.DesOptObj(np.array(x0APSIS))
            
        '''
        # Save results for the modified problem
        #---------------------------------------------------------------------#
        Results                 = dict()
        Results['TimeInSec']    = time.time()-STARTTIME
        Results['fOpt']         = fOpt
        Results['xOpt']         = xOpt
        Results['inform']       = inform
        Results['xHist']        = np.asarray(xHist)
        Results['fHist']        = np.asarray(fHist)
        Results['gHist']        = np.asarray(gHist)
        Results['hHist']        = np.asarray(hHist)
        Results['fGradHist']    = np.asarray(fGradHist)
        Results['GradConHist']  = np.asarray(GradConHist) # dCon1/dx1, dCon1/dx2, ..., dCon1/dxNDV, dCon2/dx1, dCon2/dx2, ..., dCon2/dxNDV etc.
        Results['failHist']     = np.asarray(failHist)
        Results['LagrangianData'] = LagrangianData
        Results['OptSettings']  = OptSettings
        Results['IterDat']      = self.IterDat
        return Results
        '''

    def APSISPostProcessing(self, Results, PresizingResults = None):
        # 1. prepare stuff for standard post processing ("May not be necessary!")
        # 2. conduct standard self.DesOpt.PostProcessingOptimization(xOpt, fOpt, inform, xL, xU, ng, nh, nDV, AlgUsed, Normalize, STARTTIME, Options, OptSettings
        # 3. Conduct APSIS specific post processing, i.e. penalty, seperate objective (pen and f), history of int./disc. variables
        # 4. Add APSIS specific post processing, i.e.
        # Results.update({'PenaltyHist': PenaltyHist, 'OrgDVHist': OrgDVHist})  -> Hence, only update old dict and not OVERWRITE!!!
        # please delete later! will be provided by self.DesOpt.PostProcessingOptimization(..)
        
        self.Results = Results     
        self.Results['Presizing']   = PresizingResults        
        
        APSISOutput     = dict()    # dictionaray where the specific APSIS output is stored
        
        # mapping from transformed variables history into orignal variables history        
        OrgDVHist       = []        
        for xhist in self.Results['xHist']:
            # if Normalization was required, design variable history is also normalized             
            if self.Norm:
                xhist   = self.DesOptObj.Denorm(xhist, self.xLMod, self.xUMod)    
            OrgDVHist.append(VariableMapping(xhist.tolist(), self.ConIntIndices, self.ConIntChoices, self.ConDisIndices, self.Options).tolist())
        OrgDVHist = np.asarray(OrgDVHist)
        APSISOutput['xHist'] = OrgDVHist
        
        # mapping from transformed xOpt into orignal xOpt
                
        
        APSISOutput['xOpt'] = VariableMapping(self.Results['xOpt'].tolist(), self.ConIntIndices, self.ConIntChoices, self.ConDisIndices, self.Options)        
        
        # ----------------------------------------
        # storing the penalty term history
        # ----------------------------------------
        PenHistory  = list() # initialization of a list, whose length equals to the number of iterations
        
        # for each iteration a dictionary is stored, which contains the penalty term for every integer and discrete design variable and the sum of all        
        for DVMod in self.Results['xHist']:
            if self.Norm:
                DVMod = self.DesOptObj.Denorm(DVMod, self.xLMod, self.xUMod)   
            PenHistDict = dict()            
            PenHistTerm = 0            
            for PenHistIntItem in self.ConIntIndices.items():        
                xPenHistInt = DVMod.tolist()[PenHistIntItem[1][0]: (PenHistIntItem[1][-1] + 1)]
                xPenHistInt.append(1-sum(xPenHistInt))            
                # the penalty term for every integer design variable is the sum of every design variable's weighting factor multiplied by its natural logarithm
                xPenHistInt[:]= xPenHistInt[:]*np.log(xPenHistInt[:])
                PenHistDict['I'+ PenHistIntItem[0]] = -sum(xPenHistInt)
                PenHistTerm -= sum(xPenHistInt)
            for PenHistDisItem in self.ConDisIndices.items():        
                xPenHistDis = DVMod.tolist()[PenHistDisItem[1][0]: (PenHistDisItem[1][-1] + 1)] 
                xPenHistDis.append(1-sum(xPenHistDis))            
                # the penalty term for every discrete design variable is the sum of every design variable's weighting factor multiplied by its natural logarithm
                xPenHistDis[:]= xPenHistDis[:]*np.log(xPenHistDis[:])
                PenHistDict['D'+ PenHistDisItem[0]] = -sum(xPenHistDis)
                PenHistTerm -= sum(xPenHistDis)
            PenHistDict['Sum'] = PenHistTerm
            PenHistory.append(PenHistDict)
        APSISOutput['PenHistory'] = PenHistory
        
        # ----------------------------------------
        # storing the original function history
        # ----------------------------------------
        
        OrgFctHist   = [] 
        
        for (iterIndex,fHist) in enumerate(self.Results['fHist']):
            if self.WeightingType == 'Const': # check if a constant weighting factor for the penalty term was specified
                OrgFctHist.append(fHist[0]-self.Options['Ramp']['Const']*PenHistory[iterIndex]['Sum'])
            elif self.WeightingType == 'Ramp': # check if the weighting factor for the penalty term has to be ramped
                if (iterIndex+1) < self.Options['Ramp']['iterStart']:
                    Nu   = 0
                elif ((iterIndex+1) >= self.Options['Ramp']['iterStart'] and (iterIndex+1) < self.Options['Ramp']['iterEnd']):  # linear interpolation for the weighting factor
                    Nu   = (((iterIndex+1)-self.Options['Ramp']['iterStart'])*((self.Options['Ramp']['NuEnd']-self.Options['Ramp']['NuStart'])/(self.Options['Ramp']['iterEnd']-self.Options['Ramp']['iterStart'])) + self.Options['Ramp']['NuStart'])/self.Options['Ramp']['NuEnd']
                elif (iterIndex+1) >= self.Options['Ramp']['iterEnd']: 
                    Nu   = 1
                OrgFctHist.append(fHist[0]-Nu*self.Options['Ramp']['NuEnd']*PenHistory[iterIndex]['Sum'])
        OrgFctHist  = np.asarray(OrgFctHist)
        APSISOutput['fHist'] = OrgFctHist
        
        # ----------------------------------------     
        # updating the basic results with the generated specific APSIS output
        # xhist, xopt and fhist represent the results from the original problrem definition; the results of the transformed problrm (all dv's continuous) are stored with a 'Transformed' key        
        # ----------------------------------------
        self.Results['xTransformedHist']    = self.Results['xHist']         
        self.Results['xTransformedOpt']     = self.Results['xOpt']        
        self.Results['fInclPenHist']        = self.Results['fHist']        
        self.Results['fGradTransformedHist']= self.Results['fGradHist']                
        del(self.Results['fGradHist'])
        self.Results.update(APSISOutput)        
        
        
        # graphical illustration of the convergence course        
        if 'ConvergencePlots' in self.Options.keys():    
            if self.Options['ConvergencePlots']:
                ConvPlotLabel = ['APSISConvergencePlot.png',
                                 'ObjectiveConvergencePlot_InclPresizing.png']
                self.ConvergencePlots(OrgFctHist, PenHistory, ConvPlotLabel)        
        
        return self.Results
    
    def _RelaxDS(self):
        '''
        Transforms all discrete design variables into continuous optimzation variables by relaxation
        Creates x0 as well as upper and lower boundaries for presizing
        
        Arguments:
    
        :None
        
        Returns:
        
        :None
        '''
        #--------------------------------------------
        # changing number of design variables and converting all into continuous variables
        #--------------------------------------------        
                
        # Initialization
        DVTypesMod  = [] # 'c' * number of optimization variables after relaxation        
        if (not self.Options['NoPresizing'] and 'c' in self.Options['DVTypes']):          
            self.x0Pre  = [] # x0 for presizing, here only continuous variables are relevant       
            self.xLPre  = [] # lower bounds for presizing, here only continuous variables are relevant    
            self.xUPre  = [] # upper bounds for presizing, here only continuous variables are relevant     
        self.x0Mod       = [] # modified x0 after relaxation (optimzation variables)
        self.xLMod       = [] # modified lower boundaries after relaxation
        self.xUMod   	   = [] # modified upper boundaries after relaxation
        DDVCounter  = 0 # counts the number of discrete design variables 
        IDVCounter  = 0 # counts the number of integer design variables
        self.ConIntIndices = dict()  # saves all modifified indices which describe integer variables         
        self.ConIntChoices = dict()  # saves all posssible integer choices for the respective modified indices        
        self.ConDisIndices = dict()  # saves all modifified indices which describe discrete variables      
        #-------------------------------------------- 
        for (index,item) in enumerate(self.Options['DVTypes']):
            # continuous design variables don't change           
            if item == 'c':
                DVTypesMod.append('c')
                if (not self.Options['NoPresizing'] and 'c' in self.Options['DVTypes']):                
                    self.x0Pre.append(self.x0[index])               
                    self.xLPre.append(self.xL[index-(DDVCounter)])                
                    self.xUPre.append(self.xU[index-(DDVCounter)])                 
                self.x0Mod.append(self.x0[index])
                self.xLMod.append(self.xL[index-(DDVCounter)])                
                self.xUMod.append(self.xU[index-(DDVCounter)])
            #--------------------------------------------        
            # number and form of integer and discrete design variables change by mapping them into continuous variables  
            elif item == 'i':
                IDVNumber = self.xU[index-(DDVCounter)]- self.xL[index-(DDVCounter)] #number of possible integer values
                DVTypesMod.extend(['c']*IDVNumber)
                IntegerChoices = np.linspace(self.xL[index-(DDVCounter)], self.xU[index-(DDVCounter)], num = (IDVNumber+1)) # all choices of integer values               
                # for the integer value which equals to the respective x0,the new x0 becomes 1, otherwise 0?                 
                for choice in IntegerChoices[0:-1]:              
                    if choice == self.x0[index]:
                        self.x0Mod.append(1-((IDVNumber+1)*1e-5))                
                    else:
                        self.x0Mod.append(1e-5)                
                iStart = len(self.xLMod)
                self.xLMod.extend([1e-5]*IDVNumber) # change to 1e-5 for numerical reasons?
                self.xUMod.extend([(1-((IDVNumber+1)*1e-5))]*IDVNumber) # change to 1-1e-5 for numerical reasons?
                self.ConIntIndices[str(IDVCounter+1)]  = [int(iEl) for iEl in (np.linspace(iStart, (iStart+IDVNumber-1), num = IDVNumber)).tolist()]  # saves all indices of x0Mod for the respective integer variable of x0
                self.ConIntChoices[str(IDVCounter+1)]  = IntegerChoices    # saves all choices of x0Mod for the respective integer variable of x0                
                IDVCounter += 1
            elif item == 'd':    
                DDVNumber = len(self.Options['DiscreteDVChoices'][str(DDVCounter+1)]) -1
                DVTypesMod.extend(['c']*DDVNumber)
                # for the discrete value which equals to the respective x0,the new x0 becomes 1, otherwise 0?                
                for choice in self.Options['DiscreteDVChoices'][str(DDVCounter+1)][0:-1]:              
                    if choice == self.x0[index]:
                        self.x0Mod.append(1-((DDVNumber+1)*1e-5))                
                    else:
                        self.x0Mod.append(1e-5)
                dStart = len(self.xLMod)
                self.xLMod.extend([1e-5]*DDVNumber)
                self.xUMod.extend([(1-((DDVNumber+1)*1e-5))]*DDVNumber) 
                self.ConDisIndices[str(DDVCounter+1)] = [int(dEl) for dEl in (np.linspace(dStart, (dStart+DDVNumber-1), num = DDVNumber)).tolist()]   # saves all indices of x0Mod for the respective discrete variable of x0              
                DDVCounter += 1 
        
        self.Options['DVTypesOriginal']     = self.Options['DVTypes']
        self.Options['DVTypesMain']         = DVTypesMod
        if (not self.Options['NoPresizing'] and 'c' in self.Options['DVTypesOriginal']):          
            self.Options['DVTypesPre']          = ['c']*len(self.x0Pre)
        self.nDV                        = len(self.x0Mod)      
       
    def ConvergencePlots(self, OrgFctHist, PenHistory, Label,showit=True):
        logging.info('convergence plots of the specific APSIS output will be created!')
        try:
            import matplotlib.pyplot as plt
        except:
            logging.fatal('Could not import matplotlib modules!')
            sys.exit('Failed to import matlotlib package!')
                
        # Normalized   
        # has to be determined            
        num_plots = 2 + len(self.ConIntIndices.keys()) + len(self.ConDisIndices.keys())
        Iter = np.linspace(0,np.size(OrgFctHist)-1,num=np.size(OrgFctHist))
        
        # Convergence plot
        # ------------------------------------------------------------------- #
 
        #colormap = plt.cm.gist_ncar
        fig = plt.figure()
 
        colormap = plt.cm.gist_rainbow
        #colormap = plt.cm.bone
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, num_plots)])
 
 
        
        labels = []
        # plotting the course of the original objective function        
        plt.plot(Iter, OrgFctHist/max(abs(OrgFctHist)))
        labels.append(r'Original obj. f')
        # plotting the penalty terms for every single integer and discrete design variable and the sum of all       
        for iNumber in range(len(self.ConIntIndices)):
            iPenPlot   = []            
            for penHistory in PenHistory:
                iPenPlot.append(penHistory['I' + str(iNumber+1)])    
            iPenPlot = np.array(iPenPlot)
            plt.plot(Iter,iPenPlot/max(abs(iPenPlot)))
            labels.append(r'Penalty int DV%i'%(iNumber+1))
        for dNumber in range(len(self.ConDisIndices)):
            dPenPlot   = []            
            for penHistory in PenHistory:
                dPenPlot.append(penHistory['D' + str(dNumber+1)])    
            dPenPlot = np.array(dPenPlot)
            plt.plot(Iter,dPenPlot/max(abs(dPenPlot)))
            labels.append(r'Penalty dis DV%i'%(dNumber+1))
        SumPenPlot   = []            
        for penHistory in PenHistory:
            SumPenPlot.append(penHistory['Sum'])    
        SumPenPlot = np.array(SumPenPlot)
        plt.plot(Iter,SumPenPlot/max(abs(SumPenPlot)))
        labels.append(r'Penalty sum')
        
        
        ax = plt.gca()
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
        ax.legend(labels, loc='center left', bbox_to_anchor=(1, 0.5))
        #plt.legend(labels, ncol=2, loc='upper center', bbox_to_anchor=(0.5, 1.05) )
        if not showit:
            plt.close(fig)   
        fig.savefig(Label[0],dpi=300)
        
        if (not self.Options['NoPresizing'] and 'c' in self.Options['DVTypes']):
            
            FctCourse = np.ravel(self.Results['Presizing']['fHist'])
            FctCourse = FctCourse.tolist()
            FctCourse.extend(OrgFctHist.tolist())
            FctCourse = np.array(FctCourse)
            Iter = np.linspace(0,np.size(FctCourse)-1,num=np.size(FctCourse))
            fig = plt.figure() 
         
            
            label = 'Original obj. course including presizing'
            # plotting the course of the original objective function        
            plt.plot(Iter, FctCourse/max(abs(FctCourse)))
            plt.axvline(x=len(self.Results['Presizing']['fHist']), ymin=0.0, ymax = 1.0, linewidth=2, color='k')
            ax = plt.gca()
            ax.set_ylabel(label, fontsize=12)            
            if not showit:
                plt.close(fig)   
            fig.savefig(Label[1],dpi=300)    
            
        # illustration of discrete design vriables not implemented yet 
        '''
        # Bar plot for design variables
        # ------------------------------------------------------------------- #
        Initial = OrgDVHist[0,:].tolist()
        Optimal = OrgDVHist[-1,:].tolist()
        ind = np.arange(nDV)  # the x locations for the groups
        width = 0.35       # the width of the bars
        fig, ax = plt.subplots()
        rects1 = ax.bar(ind, Initial, width, color='r')
        rects2 = ax.bar(ind+width, Optimal, width, color='b')
        ax.set_ylabel('Normalized design variable values')
        ax.set_title('Design Variables')
        ax.set_xticks(ind+width)
        ax.set_ylim([0.,1.2])
        ax.set_xticklabels( ['DV%02i'%(iDV) for iDV in range(nDV)] )
        ax.legend( (rects1[0], rects2[0]), ('Initial', 'Optimum') )                
        def autolabel(rects):
            for rect in rects:
                height = rect.get_height()
                ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%.1f'%float(height),
                        ha='center', va='bottom')
        autolabel(rects1)
        autolabel(rects2)
        if not showit:
            plt.close(fig)  
        fig.savefig(Label[1],dpi=300)
        
        # Plot several different functions...
        '''

# ------------------------------------------------------------------------------------------------ #
#                                   PRESIZING SUBSTITUTE MODEL
# ------------------------------------------------------------------------------------------------ #

class PresizingSubstituteModel():
    '''
    This child class modifies a ``model.py``-class such that ``DesOpt`` can be 
    used for the presizing of the APSIS algorithm.    
    '''
    def __init__(self, Model, ConIntIndices, ConIntChoices, ConDisIndices, nDV, Options):
        
        self.Model          = Model
        self.ConIntIndices  = ConIntIndices
        self.ConIntChoices  = ConIntChoices
        self.ConDisIndices  = ConDisIndices 
        self.nDV            = nDV                     
        self.Options        = cp.deepcopy(Options)        
        if hasattr(self.Model, 'ObjAndConstrSensFct'):            
            self.__class__ = SensPreClass        
        
    def ObjAndConstrFct(self, x):
        
        '''
        Creates a modified objective function by adding an additional penalty term for each discrete design variable and modifies the constraints.
        
        Arguments: 
        
        :param x: modified design variables, which the responses are computed for
        
        Returns:  
        
        :return f: modified obj. fct. for respective x 
        :return g: Value of constraints for respective x
        :return fail: Flag for fail (default: 0)
        '''
        
        xPre = [0.]*self.nDV
        TransformedIndices = []        
                
        for IntItem in self.ConIntIndices.items():
            xPre[IntItem[1][0] : (IntItem[1][-1]+1)] = [1/float(len(IntItem[1])+1)]*len(IntItem[1])
            TransformedIndices.extend(IntItem[1])
        for DisItem in self.ConDisIndices.items():
            xPre[DisItem[1][0] : (DisItem[1][-1]+1)] = [1/float(len(DisItem[1])+1)]*len(DisItem[1])
            TransformedIndices.extend(DisItem[1])
        
        if (len(xPre) - len(TransformedIndices)) != len(x):
            ErrorString = '\n#########################################\n\
            Numbers of design variables for presizing and APSIS do not match\n\
            #########################################\n'            
            logging.fatal(ErrorString)
            sys.exit(ErrorString)
            
        i = 0
        for (index, item) in enumerate(xPre):
            if not index in TransformedIndices:
                xPre[index] = x[i]
                i += 1
            else:
                pass
        
              
            
        xOrg    = VariableMapping(xPre, self.ConIntIndices, self.ConIntChoices, self.ConDisIndices, self.Options)
        
        #--------------------------------------------        
        # modifying the objective and constraint functions         
        #--------------------------------------------        
        (f, g, fail) = self.Model.ObjAndConstrFct(xOrg)
        
        
        
        return f,g, fail
        

class SensPreClass(PresizingSubstituteModel):

    def ObjAndConstrSensFct(self,x,f,g):
        
        '''
        Analytical gradients, if one doesn't want to use finite differences
        Only available if original model has available the ObjAndConstrSensFct method
        
        Arguments: 
        
        :param x: only continuous design variables, which the responses are computed for; discrete and integer variables are set to a coonstant mean value
        :param f: obejctive function
        :param g: constraint functions 
        
        Returns:  
        
        :return g_obj: gradient for the objective function,
        :return g_con: gradient for the constraints function
        :return fail: Flag for fail (default: 0)
        '''    
        
        
        xPre = [0.]*self.nDV
        TransformedIndices = []        
                
        for IntItem in self.ConIntIndices.items():
            xPre[IntItem[1][0] : (IntItem[1][-1]+1)] = [1/float(len(IntItem[1])+1)]*len(IntItem[1])
            TransformedIndices.extend(IntItem[1])
        for DisItem in self.ConDisIndices.items():
            xPre[DisItem[1][0] : (DisItem[1][-1]+1)] = [1/float(len(DisItem[1])+1)]*len(DisItem[1])
            TransformedIndices.extend(DisItem[1])
        
        if (len(xPre) - len(TransformedIndices)) != len(x):
            ErrorString = '\n#########################################\n\
            Numbers of design variables for presizing and APSIS do not match\n\
            #########################################\n'            
            logging.fatal(ErrorString)
            sys.exit(ErrorString)
            
        i = 0
        for (index, item) in enumerate(xPre):
            if not index in TransformedIndices:
                xPre[index] = x[i]
                i += 1
            else:
                pass
        
              
            
        xOrg    = VariableMapping(xPre, self.ConIntIndices, self.ConIntChoices, self.ConDisIndices, self.Options)        
        
        # computing sensitivities for the original problem and the original design variables 
        
        (g_objInterim,g_conInterim,fail)    = self.Model.ObjAndConstrSensFct(xOrg,f,g)
        
        nCDV  = self.Options['DVTypesOriginal'].count('c')
        #g_obj = [0.0]*nCDV
        #g_con = np.zeros([self.ng, self.nDV])        
        
        g_obj = []
        g_con = np.zeros([(np.shape(g_conInterim))[0], nCDV])
        ConstrDVCounter = 0        
        for (dvIndex,dvType) in enumerate(self.Options['DVTypesOriginal']):
            if dvType   == 'c':            
                g_obj.append(g_objInterim[dvIndex])     
                g_con[:,ConstrDVCounter] = g_conInterim[:,dvIndex]
                ConstrDVCounter +=1
            
        
        return g_obj,g_con,fail


# ------------------------------------------------------------------------------------------------ #
#                                   SUBSTITUTE MODEL
# ------------------------------------------------------------------------------------------------ #

class APSISProblemSubstituteModel():
    '''
    This child class modifies a ``model.py``-class such that ``DesOpt`` can be 
    used for APSIS optimization.    
    '''
    def __init__(self, Model, ConIntIndices, ConIntChoices, ConDisIndices, Options, WeightingType, nDV):
        
        self.Model          = Model
        self.Options        = cp.deepcopy(Options)
        self.ConIntIndices  = ConIntIndices
        self.ConIntChoices  = ConIntChoices
        self.ConDisIndices  = ConDisIndices 
        # number of constraints raises due to additional constraints for each discrete and integer design variable        
        self.ng             = self.Options['ng'] + len(self.ConIntIndices) + len(self.ConDisIndices)                     
        self.WeightingType  = WeightingType
        self.nDV            = nDV        
        if hasattr(self.Model, 'ObjAndConstrSensFct'):            
            self.__class__ = SensAPSISClass        
        
    def ObjAndConstrFct(self, x):
        
        '''
        Creates a modified objective function by adding an additional penalty term for each discrete design variable and modifies the constraints.
        
        Arguments: 
        
        :param x: modified design variables, which the responses are computed for
        
        Returns:  
        
        :return f: modified obj. fct. for respective x 
        :return g: Value of constraints for respective x
        :return fail: Flag for fail (default: 0)
        '''
        
        xList   = x.tolist()
        xOrg    = VariableMapping(xList, self.ConIntIndices, self.ConIntChoices, self.ConDisIndices, self.Options)
        
        #--------------------------------------------        
        # modifying the objective and constraint functions         
        #--------------------------------------------        
        (fInterim, gInterim, fail) = self.Model.ObjAndConstrFct(xOrg)
        
        # setting up the additional penalty term
        PenTerm = 0
        for PenIntItem in self.ConIntIndices.items():        
            xPenInt = xList[PenIntItem[1][0]: (PenIntItem[1][-1] + 1)]
            xPenInt.append(1-sum(xPenInt))            
            # the penalty term for every integer design variable is the sum of every design variable's weighting factor multiplied by its natural logarithm
            xPenInt[:]= xPenInt[:]*np.log(xPenInt[:])
            PenTerm -= sum(xPenInt)
        for PenDisItem in self.ConDisIndices.items():        
            xPenDis = xList[PenDisItem[1][0]: (PenDisItem[1][-1] + 1)] 
            xPenDis.append(1-sum(xPenDis))            
            # the penalty term for every discrete design variable is the sum of every design variable's weighting factor multiplied by its natural logarithm
            xPenDis[:]= xPenDis[:]*np.log(xPenDis[:])
            PenTerm -= sum(xPenDis)
        
        # check, which value the weighting factor has to equal to    
        # if self.Options['Ramp']:
        # weighting depends on iterNumber! information about iterNumber?
        
        #--------------------------------------------        
        # controlling the weighting factor of the penalty function          
        #--------------------------------------------    
                
        
        if self.WeightingType == 'Const':
            f   = fInterim + (self.Options['Ramp']['Const']*PenTerm)   # original objective function plus a penalty term with constant weighting
        
       
        elif self.WeightingType == 'Ramp':     
            if (self.iterCurrent+1) < self.Options['Ramp']['iterStart']:                
                Nu   = 0
            elif ((self.iterCurrent+1) >= self.Options['Ramp']['iterStart'] and (self.iterCurrent+1) < self.Options['Ramp']['iterEnd']):  # linear interpolation for the weighting factor                   
                Nu   = (((self.iterCurrent+1)-self.Options['Ramp']['iterStart'])*((self.Options['Ramp']['NuEnd']-self.Options['Ramp']['NuStart'])/(self.Options['Ramp']['iterEnd']-self.Options['Ramp']['iterStart'])) + self.Options['Ramp']['NuStart'])/self.Options['Ramp']['NuEnd']
            elif (self.iterCurrent+1) >= self.Options['Ramp']['iterEnd']:                    
                Nu   = 1
            f   = fInterim + (Nu*self.Options['Ramp']['NuEnd']*PenTerm)   # original objective function plus a penalty term with ramped weighting
       
        # additional costraints: namely the sum of all continuous optimzation variables for a respective design variable must be lower than 1
        for IntNum in range(len(self.ConIntIndices)):                 
            xConstrInt = xList[self.ConIntIndices[str(IntNum+1)][0]: (self.ConIntIndices[str(IntNum+1)][-1] + 1)]
            gInterim.append(sum(xConstrInt)-1)
            
        for DisNum in range(len(self.ConDisIndices)):                 
            xConstrDis = xList[self.ConDisIndices[str(DisNum+1)][0]: (self.ConDisIndices[str(DisNum+1)][-1] + 1)]
            gInterim.append(sum(xConstrDis)-1)
            
        g = gInterim
        
        return f,g, fail
        
    def IterationUserOutput(self, IterData):
        
        '''
        manages the access to the current iteration number.
        
        Arguments: 
        
        :param IterData: informs about the current iteration progress
        
        Returns:  
        
        None
        '''    
        
        if hasattr(self.Model, 'IterationUserOutput'):
            self.Model.IterationUserOutput(IterData)
        
        if 'IterationNum' in IterData.keys():         
            self.iterCurrent = IterData['IterationNum']
        else:
            ErrorString = '\nNo acces to current iteration number possible'        
            logging.fatal(ErrorString)
            sys.exit(ErrorString)
            


class SensAPSISClass(APSISProblemSubstituteModel):

    def ObjAndConstrSensFct(self,x,f,g):
        
        '''
        Analytical gradients, if one doesn't want to use finite differences
        Only available if original model has available the ObjAndConstrSensFct method
        
        Arguments: 
        
        :param x: modified design variables, which the responses are computed for
        :param f: obejctive function
        :param g: constraint functions 
        
        Returns:  
        
        :return g_obj: gradient for the objective function
        :return g_con: gradient for the constraints function
        :return fail: Flag for fail (default: 0)
        '''    
        
        
        # computing sensitivities for the original problem and the original design variables 
        xList   = x.tolist()            
        xOrg    = VariableMapping(xList, self.ConIntIndices, self.ConIntChoices, self.ConDisIndices, self.Options)
        (g_objInterim,g_conInterim,fail)    = self.Model.ObjAndConstrSensFct(xOrg,f,g)
        
        # checking which value the weighting factor of the penalty term equals to         
        if self.WeightingType == 'Const':
            Nu   = self.Options['Ramp']['Const']   # original objective function plus a penalty term with constant weighting
        
       
        elif self.WeightingType == 'Ramp':     
            if (self.iterCurrent+1) < self.Options['Ramp']['iterStart']:                
                Nu   = 0
            elif ((self.iterCurrent+1) >= self.Options['Ramp']['iterStart'] and (self.iterCurrent+1) < self.Options['Ramp']['iterEnd']):  # linear interpolation for the weighting factor                   
                Nu   = (((self.iterCurrent+1)-self.Options['Ramp']['iterStart'])*((self.Options['Ramp']['NuEnd']-self.Options['Ramp']['NuStart'])/(self.Options['Ramp']['iterEnd']-self.Options['Ramp']['iterStart'])) + self.Options['Ramp']['NuStart'])/self.Options['Ramp']['NuEnd']
            elif (self.iterCurrent+1) >= self.Options['Ramp']['iterEnd']:                    
                Nu   = 1        
            Nu = Nu*self.Options['Ramp']['NuEnd']
        
        
        g_obj = [0.0]*self.nDV
        g_con = np.zeros([self.ng, self.nDV])        
        
        IDVCounter  = 0          
        DDVCounter  = 0        
        APSISDVCounter = 0        
        for (dvIndex,dvType) in enumerate(self.Options['DVTypesOriginal']):
            if dvType   == 'c':            
                g_obj[APSISDVCounter] = g_objInterim[dvIndex]     
                g_con[0:len(g_conInterim),APSISDVCounter] = g_conInterim[:,dvIndex]
                g_con[len(g_conInterim):,APSISDVCounter] = 0                
                APSISDVCounter +=1
            elif dvType == 'i':
                for (intListIndex,intIndex) in enumerate(self.ConIntIndices[str(IDVCounter+1)]):
                    g_obj[APSISDVCounter] = (g_objInterim[dvIndex]*(self.ConIntChoices[str(IDVCounter+1)][intListIndex]-self.ConIntChoices[str(IDVCounter+1)][-1]))-(Nu*(np.log(xList[intIndex])+1))      
                    g_con[0:len(g_conInterim),APSISDVCounter] = g_conInterim[:,dvIndex]*(self.ConIntChoices[str(IDVCounter+1)][intListIndex]-self.ConIntChoices[str(IDVCounter+1)][-1])
                    g_con[len(g_conInterim):,APSISDVCounter] = 0                                    
                    g_con[(len(g_conInterim)+IDVCounter),APSISDVCounter] = 1                     
                    APSISDVCounter +=1
                IDVCounter +=1
            elif dvType == 'd':        
                for (disListIndex,disIndex) in enumerate(self.ConDisIndices[str(DDVCounter+1)]):
                    g_obj[APSISDVCounter] = (g_objInterim[dvIndex]*(self.Options['DiscreteDVChoices'][str(DDVCounter+1)][disListIndex]-self.Options['DiscreteDVChoices'][str(DDVCounter+1)][-1]))-(Nu*(np.log(xList[disIndex])+1))    
                    g_con[0:len(g_conInterim),APSISDVCounter] = g_conInterim[:,dvIndex]*(self.Options['DiscreteDVChoices'][str(DDVCounter+1)][disListIndex]-self.Options['DiscreteDVChoices'][str(DDVCounter+1)][-1])
                    g_con[len(g_conInterim):,APSISDVCounter] = 0                    
                    g_con[(len(g_conInterim)+len(self.ConIntIndices)+DDVCounter),APSISDVCounter] = 1                     
                    APSISDVCounter +=1
                DDVCounter +=1
        
        return g_obj,g_con,fail