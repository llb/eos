# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import logging
import sys
import os
import numpy as np
import copy as cp
from numpy.linalg import norm

# ------------------------------------------------------------------------------------------------ #
#                                   ALGORITHM CONTROL
# ------------------------------------------------------------------------------------------------ #
class VectorOptimizer:
    def __init__(self,Model,DesOpt,Options,VecOptions,nDV):
        self.Options    = Options
        self.VecOptions = VecOptions
        self.Model      = Model
        self.DesOpt     = DesOpt
        self.nDV        = nDV
        
    def Optimize(self,X0,xL,xU):
        '''
        Controls optimization process:
        
        Notes:
            - uses DesOpt for each pareto point
            - calls chosen vector optimization algorithm
            - calls PostProcessing-Method in DesOpt
        
        Arguments:
            - X0:   Initial design set (1D-numpy array)
            - xL:   Lower boundary of design space
            - xU:   Upper boundary of design space
            
        Returns:
            - Results-dictionary
        '''
        # Additional variables:
        nDV = self.nDV
        
        # filter invalid x0        
        if len(X0.shape) > 1:
            logging.fatal('For vector optimization x0 must be 1D!')
            sys.exit('x0 must be 1D!')
        
        # add IterationUserOutput if defined in model.py to SubstituteModel            
        if hasattr(self.Model, 'IterationUserOutput'):
            def IterationUserOutput(self,*args):
                return self.Model.IterationUserOutput(*args)
            IterationUserOutput.__doc__ = "Output function at each iteration is provided by user."
            IterationUserOutput.__name__ = "IterationUserOutput"
            setattr(SubstituteModel,IterationUserOutput.__name__,IterationUserOutput)       
        
        # Import furter tools
        if self.VecOptions['GS']:
            try:
                from OptTools import GlobalSearchForDesOpt          
            except:
                logging.fatal('Tools can not be imported!')    
                sys.exit('Tools can not be imported!')
                
        # CAUTION WHEN USING PLOTTING DURING OPTIMIZATION: These tools call the model objective function!
        if self.Options['nf'] == 2:
            from OptTools import AnalyticalParetoPlotting2D, addplot2D
        elif self.Options['nf'] == 3:
            from OptTools import AnalyticalParetoPlotting3D, addplot3D
        else:
            logging.warning('More than 3 dimensions not plottable!')

        # Create a cost model class
        # ------------------------------------------------------------------- #
        VectorModel = SubstituteModel(self.Options, self.VecOptions, self.Model, nDV)
        
        # Assign substitute model and some options to DesOpt:
        self.DesOpt.Model = VectorModel
        self.DesOpt.EOSOptOptions['ConvergencePlots'] = False

        # =================================================================== #
        # COMPUTE ANCHOR POINTS:
        # =================================================================== #
        # Create result variables
        # ------------------------------------------------------------------- #
        Pareto = []      # List of global results-dictionaries
        Local = []       # List of local results-dictionaries
        
        TotCalls = 0
        # Lambda weights for each objective:
        if self.VecOptions['AnchorPoints'] == None:
            Lambda = np.eye(self.Options['nf'])
        else:
            logging.info('$$ Vector optimizer: Use anchor data provided in options.')
            Lambda = []

        # Loop over each objective
        # ------------------------------------------------------------------- #
        for obj in Lambda:
            logging.info('$$ --------------------------------------------------------- $$')
            logging.info('$$ OPTIMIZATION LOOP FOR ANCHOR POINT NUMBER: %d  HAS BEGUN $$'%(len(Pareto)+1))
            logging.info('$$ ---------------------------------------------------------$$')
            Result = []
            
            # Update variables in SubstituteModel:
            VectorModel.Variables = {'Lambda':np.asarray(obj), 'Gamma':None, 'fscales':dict(), 'fPrev':None, 'fPrev2':None}
            
            # Compute anchor point for each x0
            # --------------------------------------------------------------- #
            if self.VecOptions['GS']:
                logging.info('$$ Vector optimizer: Start global search for anchor point %d'%(len(Pareto)+1))
                feasDesigns = 0
                counter = 0
                while (feasDesigns == 0) and (counter < 5):
                    x0GS, gsInfo = GlobalSearchForDesOpt(xL,xU,VectorModel.ObjAndConstrFct,
                                                         nh=self.Options['nh'], 
                                                         nDoE=self.VecOptions['nDoE'],
                                                         nGS=self.VecOptions['nGS'])
                    nLoops = self.VecOptions['nGS']
                    feasDesigns = self.VecOptions['nDoE']-gsInfo.count('Infeasible')
                    if feasDesigns == 0:
                        logging.warning('Global search produced no feasible design. Retry.')
                    counter += 1
                if feasDesigns < nLoops:
                    logging.warning('Global search produced not enough feasible designs.')
                    nLoops = feasDesigns
                if counter >= 19:
                    logging.fatal('Global search produced no feasible design. No Retry.')
                TotCalls += counter*self.VecOptions['nDoE']
            else:
                x0 = X0
                nLoops = len(X0.shape)
                
            # SOLVE at each x0 via DesOpt:
            # --------------------------------------------------------------- #
            logging.info('$$ Vector optimizer: Start optimization with DesOpt for anchor point %d'%(len(Pareto)+1))                  
            for ni in range(nLoops):
                logging.info('$$ Vector optimizer: Iteration %d out of %d'%(int(ni+1),nLoops))
                if self.VecOptions['GS']:
                    x0 = x0GS[:,ni]
                try:
                    logging.info( '$$ Vector optimizer: x0: '+''.join(str(x0.tolist())) )
                    TempResults = self.DesOpt(x0)
                    # add objective values to results-dictionary 
                    # ('fOpt' is actually uOpt, but GSO PostProcessing only accepts fOpt!)
                    TempResults['x0'] = x0
                    if self.VecOptions['GS']:
                        TempResults['gsInfo'] = gsInfo
                    TempResults['xOpt'] = TempResults['xOpt'][:nDV]
                    TempResults['fOpt-Vect'] = VectorModel.fPareto
                    TempResults['Lambda'] = np.asarray(obj)
                    Result.append(TempResults)
                except:
                    logging.warning('Vector Optimizer: Optimization failed during call of DesOpt for anchor point calculation.')
                        
                
            # Postprocessing? Check Optimality, save a plot, which one is best?
            # --------------------------------------------------------------- #
            if self.VecOptions['GS']:
                from OptTools import GSOPostProcessing
                gsinfo = GSOPostProcessing(Result)
            
            # save "best" results to Variable => IMPROVE: if u-values are equal => take sum of all other objectives!
            # --------------------------------------------------------------- #            
            lowU = [n['fOpt'] for n in Result]      # get values of cost-fct-key from all GSO-result-dictionaries
            indP = lowU.index(min(lowU))            # index of dictionary with lowest u 
            
            if self.VecOptions['ConvergencePlots']:
                DirLabel = 'results/ConvergencePlots/'+str(len(Pareto)+1)+'_Anchor'
                if not os.path.exists(DirLabel):
                    os.makedirs(DirLabel)
                Label = [DirLabel + '/' + 'ConvPlot.pdf',DirLabel + '/' + 'BarPlot.pdf']
                self.DesOpt.ConvergencePlots(Result[indP]['xHist'],Result[indP]['fHist'],Result[indP]['gHist'],Result[indP]['hHist'],self.Options['ng'],self.Options['nh'],nDV,Label,showit=False)
            Pareto.append(Result[indP])             # save best (lowest u) dictionary in "Pareto"-list
            if self.VecOptions['GS']:
                Pareto[-1]['GSinfo'] = gsinfo       # add GS-postprocessing info to dictionary
            del Result[indP]
            Local.append(Result)                    # save all other dictionaries in "Local"-list
        
        
        '''
        Description:
        - Pareto[0]['xOpt'] = Pareto optimal design variables 'xOpt' for objective f1 [0]
        - Local[0][nGS-1]['xOpt'] = (nGS-1)-th local Pareto optimal design variables 'xOpt' for objective f1 [0]   
        '''
        if Lambda == []:
            xOpt = self.VecOptions['AnchorPoints']['xAnc']
            FAnc = list()
            for xIndex, iX in enumerate(xOpt):
                VectorModel.Variables = {'Lambda':np.asarray(xIndex), 'Gamma':None, 'fscales':dict(), 'fPrev':None, 'fPrev2':None}
                uTemp = VectorModel.ObjAndConstrFct(iX)[0]
                FAnc.append(VectorModel.fPareto)
                del uTemp # Do we need it
                TotCalls += 1
            FAnc = np.asarray(FAnc)
            logging.info('$$ Vector optimizer: loading of pickle data for anchor points was successful.')
            Lambda = np.eye(self.Options['nf'])
            for i in range(self.Options['nf']):
                Pareto.append({'xOpt':xOpt[i,:], 'fOpt-Vect':FAnc[i,:], 'Lambda':Lambda[i,:],'fOpt':[FAnc[i,i],]})
        else:
            logging.info('$$ Vector optimizer: Anchor point computation done.')
        # Anchor points (as many as there are objectives)
        # each row is one anchor point => FAnc must be quadratic matrix
        FAnc = np.asarray([n['fOpt-Vect'] for n in Pareto])
        
        # =================================================================== #
        # COMPUTE UTOPIA POINT:
        # Vector out of lowest f1 value, lowest f2 value, ... , lowest fm value
        # =================================================================== #
        #FUto = np.array([n['fOpt'][0] for n in Pareto]).T     # cost function equals best objective!
        FUto = np.array([min(FAnc[:,k]) for k in range(self.Options['nf'])])     # cost function equals best objective!
        
        # =================================================================== #
        # COMPUTE NADIR POINT:
        # Vector of highest f-value of all at each opimum [fhighest(xopt1) fhighest(xopt2) highest(xopt3),..., fhighest(xoptm)]
        # =================================================================== #
        FNad = np.asarray([max(FAnc[:,k]) for k in range(self.Options['nf'])])
        
        logging.info('$$ DONE: ANCHOR points, NADIR and UTOPIA points have been computed.')
        
        # Plot current results
        if self.Options['nf'] <= 2:
            pltfig, pltax = AnalyticalParetoPlotting2D(FNad,FUto,FAnc)
        elif self.Options['nf'] == 3:
            pltfig, pltax = AnalyticalParetoPlotting3D(xL,xU,FNad,FUto,FAnc,self.Model,self.Options)
        else:
            print('Cannot plot more than 3 dimensions.')
        # =================================================================== #
        # EQUIDISTANT PEREYRA ALGORITHM STARTS HERE:
        # In future releases one can switch algorithms here.
        # =================================================================== #
        
        # Initialize variables and algorithm        
        L       = self.VecOptions['NumParetoOptima']               # Number of pareto optima
        alpha   = self.VecOptions['ChordFactor']                   # Chord factor
        nf      = self.Options['nf']                               # Number of objectives
        
        xL = np.append(xL, np.zeros(nf-1))                         # Additional lower boundaries for nf-1 lambdas
        xU = np.append(xU, np.ones(nf-1))                          # Additional upper boundaries for nf-1 lambdas
        
        self.DesOpt.xLOpt = xL                                     # additional lower lambda boundary
        self.DesOpt.xUOpt = xU                                     # additional upper lambda boundary   
        
        [self.DesOpt.EOSOptOptions['DVTypes'].append('c') for i in range(nf-1)]  # lambda is of type 'c'
        logging.info('$$ Vector optimizer: Added %d design variables (objective weights) of type c')
        # Update number of constraints in DesOpt instance (Lambda constaint):
        self.DesOpt.EOSOptOptions['nh'] = self.Options['nh'] + 2   # additional distance constraints
        logging.info('$$ Vector optimizer: Added 2 additional equality constraints.')
        self.DesOpt.EOSOptOptions['ng'] = self.Options['ng'] + 1   # additional weight and backstepping constraints
        logging.info('$$ Vector optimizer: Added 1 additional inequality constraints.')
 
        nDV = np.size(xL)                                     # additional upper lambda boundary

        # Compute Gamma:
        # ------------------------------------------------------------------- #
        F = np.zeros((nf,nf))
        for i in range(nf):
            for j in range(nf):
                F[i,j] = norm(FAnc[i,:]-FAnc[j,:])  # Distance between anchor points
        Gamma = alpha * F.max() / L                 # maximum distance

        fPrev   = FAnc[self.VecOptions['fPrevInit']-1,:]  # Design from which continuation starts 
        fPrev2  = None

        xPrev = np.append(Pareto[self.VecOptions['fPrevInit']-1]['xOpt'],Pareto[self.VecOptions['fPrevInit']-1]['Lambda'])
        xLnew,xUnew = np.zeros(nDV),np.zeros(nDV)
        
        # Compute inner pareto points:
        # ------------------------------------------------------------------- #
        for k in range(L-2):
            logging.info('$$ ---------------------------------------------------------$$')
            logging.info('$$ OPTIMIZATION LOOP FOR PARETO POINT NUMBER: %d  HAS BEGUN $$'%(k+1))
            logging.info('$$ ---------------------------------------------------------$$')
            Result = []
            
            # Normalization of objectives in u
            fscales = {'Utopia': FUto, 'Nadir': FNad}
 
            # Update variables in SubstituteModel:
            VectorModel.Variables = {'Lambda':None, 'Gamma':Gamma, 'fscales':fscales, 'fPrev':fPrev, 'fPrev2':fPrev2}
            
            # Perform global search (now ObjAndConstrFct needs to include Lambda and perform global search also on lambda!!)
            nLoops = 1
            # --------------------------------------------------------------- #
            if self.VecOptions['GS']:
                feasDesigns = 0
                counter = 0
                while (feasDesigns == 0) and (counter < 5):
                    logging.info('$$ Vector optimizer: Start global search for Pareto point %d'%(k+1))
                    if self.VecOptions['RefinedGS']:
                        for i in range(nDV):
                            xLnew[i] = max(xPrev[i]*(1.-2./L),xL[i])
                            xUnew[i] = min(xPrev[i]*(1.+2./L),xU[i])
                    else:
                        xLnew,xUnew = xL,xU
                    x0GS, gsInfo = GlobalSearchForDesOpt(xLnew,xUnew,VectorModel.ObjAndConstrFct,
                                                         nh=self.DesOpt.EOSOptOptions['nh'],
                                                         nDoE=self.VecOptions['nDoE'],
                                                         nGS=self.VecOptions['nGS'])
                    nLoops = self.VecOptions['nGS']
                    feasDesigns = self.VecOptions['nDoE']-gsInfo.count('Infeasible')
                    if feasDesigns == 0:
                        logging.warning('Global search produced no feasible design. Retry.')
                    counter += 1
                if feasDesigns < nLoops:
                    logging.warning('Global search produced not enough feasible designs.')
                    nLoops = feasDesigns
                if counter >= 19:
                    logging.fatal('Global search produced no feasible design.')
                TotCalls += counter * self.VecOptions['nDoE']
            else:
                lam0 = np.ones(self.Options['nf']-1) * 1/(self.Options['nf'])   # default lambda = equally spread!
                if len(X0.shape) > 1:
                    x0 = [np.append(x0i,lam0)  for x0i in X0]
                else:
                    x0 = np.append(X0,lam0)
                nLoops = len(X0.shape)
            
            # SOLVE at each x0 via DesOpt:
            # --------------------------------------------------------------- #
            logging.info('$$ Vector optimizer: Start optimization with DesOpt for Pareto point %d'%(k+1))
            for ni in range(nLoops):
                logging.info('$$ Vector optimizer: Iteration %d out of %d'%(int(ni+1),nLoops))
                if self.VecOptions['GS']:
                    x0 = x0GS[:,ni]
                try:
                    logging.info( '$$ Vector optimizer: x0: '+''.join(str(x0.tolist())) )
                    print('')
                    print('INNER PARETO POINT NUMBER: %d  ITERATION: %d' % ((k+1),int(ni+1)))
                    TempResults = self.DesOpt(x0)
                    # add objective values to results-dictionary 
                    # ('fOpt' is actually uOpt, but GSO PostProcessing only accepts fOpt!)
                    TempResults['x0'] = x0
                    if self.VecOptions['GS']:
                        TempResults['gsInfo'] = gsInfo
                    TempResults['fOpt-Vect'] = VectorModel.fPareto
                    LAMBDA = TempResults['xOpt'][(nDV-(nf-1)):nDV]
                    TempResults['Lambda'] = np.append(LAMBDA,1-sum(LAMBDA))
                    TempResults['xOpt'] = TempResults['xOpt'][:nDV]
                    TotCalls += len(TempResults['fHist'])
                    Result.append(TempResults)
                except:
                    logging.warning('Vector Optimizer: Optimization failed during call of DesOpt for Pareto point calculation.')
            
            logging.info('$$ Vector optimizer: Postprocessing of current Pareto point.')
            # Check optimality?
            # --------------------------------------------------------------- #
            if self.VecOptions['GS']:
                from OptTools import GSOPostProcessing
                gsinfo = GSOPostProcessing(Result)
            
            # save best results to Variable
            # --------------------------------------------------------------- #            
            lowU = [n['fOpt'] for n in Result]  # get values of cost-fct from all GSO-result-dictionaries
            indP = lowU.index(min(lowU))        # index of dictionary with lowest u
 
            if self.VecOptions['ConvergencePlots']:
                DirLabel = 'results/ConvergencePlots/'+str(len(Pareto)-1)+'_InnerPoint'
                if not os.path.exists(DirLabel):
                    os.makedirs(DirLabel)
                Label = [DirLabel + '/' + 'ConvPlot.pdf',DirLabel + '/' + 'BarPlot.pdf']
                self.DesOpt.ConvergencePlots(Result[indP]['xHist'],Result[indP]['fHist'],Result[indP]['gHist'],Result[indP]['hHist'],self.Options['ng'],self.Options['nh'],nDV,Label,showit=False)
            
            Pareto.append(Result[indP])        # save best (lowest u) dictionary in "Pareto"-list
            
            fPrev2 = fPrev
            fPrev = np.asarray(Pareto[-1]['fOpt-Vect'])  
            xPrev = np.append(Pareto[-1]['xOpt'],Pareto[-1]['Lambda'])
            
            if self.VecOptions['GS']:
                Pareto[-1]['GSinfo'] = gsinfo   # add GS-postprocessing info to dictionary
            del Result[indP]
            Local.append(Result)               # save all other dictionaries in "Local"-list
                
            # plotting
            # --------------------------------------------------------------- #
            logging.info('$$ Vector optimizer: Plotting of current Pareto point.')
            if self.Options['nf']<=2:
                addplot2D(Pareto[-1]['fOpt-Vect'],fPrev,Gamma,pltax)
            elif self.Options['nf']==3:
                addplot3D(Pareto[-1]['fOpt-Vect'],fPrev,Gamma,pltax)
            pltfig.savefig('ParetoFront')  

        logging.info('$$ Vector optimizer: Algorithm done.')
        
        # ------------------------------------------------------------------- #
        # PostProcessing
        # ------------------------------------------------------------------- #
        for npar in Pareto: # Rename fOpt
            npar['uOpt'] = npar.pop('fOpt')
            npar['fOpt'] = npar.pop('fOpt-Vect')
            try:
                npar['uHist'] = npar.pop('fHist')
            except:
                pass
                        
        # Second anchor point at the end (only for biobjective):
        if self.Options['nf']==2:
            Pareto.append(Pareto.pop(2-self.VecOptions['fPrevInit']))
			
        # Infos at the very end
        Summary = dict()
        Summary['f_opt'],Summary['x_opt'],Summary['lambda_opt'],Summary['u_opt'] = [],[],[],[]
        Summary['Shadow'] = []
        for sol in Pareto:
            Summary['f_opt'].append(sol['fOpt'])
            Summary['x_opt'].append(sol['xOpt'])
            Summary['lambda_opt'].append(sol['Lambda'])
            Summary['u_opt'].append(sol['uOpt'])
            #Summary['Shadow'].append(sol['LagrangianData']['Lambda Values'])
        for key in Summary.keys():
            Summary[key] = np.array(Summary[key])
            
        Summary['Anchor'] = FAnc
        Summary['Utopia'] = FUto
        Summary['Nadir']  = FNad
        Summary['fCalls'] = TotCalls
#        Pareto.append({'FNad':FNad,'FUto':FUto,'FAnc':FAnc})
        
        print('############### EQUIDISTANT PARETO COMPUTATION DONE. #################')
        return {'Summary':Summary, 'Detailed':Pareto}, Local, pltfig


# ------------------------------------------------------------------------------------------------ #
#                                   SUBSTITUTE MODEL
# ------------------------------------------------------------------------------------------------ #

class SubstituteModel():
    '''
    This child class mimics a ``model.py``-class such that ``DesOpt`` can be 
    used for vector optimization.    
    '''
    def __init__(self, Options, VecOptions, Model, nDV):
        
        self.Options    = Options
        self.VecOptions = VecOptions
        self.Model      = Model
        self.nDV        = nDV
        self.xList      = []
        self.dxList     = []
        self.fgList     = []
        self.dfdgList   = []
        
    def ObjAndConstrFct(self, x):
        '''
        Computes weighted sum of objectives (cost function) and constraints for
        a design x and weighting factors Lambda.
        
        Remarks: SubstituteModel.Variables-dict must be assigned to instance before call !!
            - 'Lambda': [None] if lambda is a design variable in pyOpt. [m-1 values] provided by user. 
            - 'Gamma':  [None] if anchor points are computed. [float value] that represents a fractional distance between anchor points.
            - 'fscales':[None] if anchor points are computed. [dict with 'Utopia' and 'Nadir' keys] to scale f.
            - 'fPrev':  [None] if anchor points are computed. [m values] of last converged pareto point.
        
        Arguments:
    
        :param x: Set of n design variables for which objectives and constraints are evaluated (NEVER NORMALIZED => DesOpt normalizes)
        
        Returns:  
        
        :return u: Value of cost function for respective x
        :return g: Value of constraints for respective x
        :return fail: Flag for fail (default: 0)
        '''
        # Additional local variables
        Lambda = self.Variables['Lambda']
        Gamma = self.Variables['Gamma']
        fscales = self.Variables['fscales']
        fPrev = self.Variables['fPrev']
        fPrev2 = self.Variables['fPrev2']
        
        
        nf = self.Options['nf']         # Number of objectives
        nDV = self.nDV                  # Number of design variables
        
        # x conversion to numpy array
        x = np.asarray(x)        
        
        # Normalize F
        # if one wants to normalize but no scales are given (fscales is empty dict)
        if not (fscales and self.VecOptions['NormalizeF']):
            fscales['Utopia'] = np.zeros(nf)
            fscales['Nadir'] = np.ones(nf)
            
        # scaling
        FUto = fscales['Utopia']
        FNad = fscales['Nadir']
        
        # split x vector from pyOpt               
        if Lambda == None:                  # Algorithm case / lambda may vary
            X       = cp.deepcopy(x[:nDV])
            LAMBDA  = np.append(x[nDV:], 1-sum(x[nDV:]))
        else:                               # User defines weights (e.g. anchor case) / lambda explicity given
            X       = cp.deepcopy(x)
            LAMBDA  = cp.deepcopy(Lambda)
                
        # get objective F and constraints G from model
        if X.tolist() in self.xList:
            match = [self.xList.index(xi) for xi in self.xList if (xi==X).all()]
            (F, G) = self.fgList[match[0]]
        else: 
            F, G, fail = self.Model.ObjAndConstrFct(X)
            self.xList.append(X.tolist())
            self.fgList.append((F,G))
        
        self.fPareto = F
        F = np.asarray(F)
        
        # assemble cost function
        u = np.dot(LAMBDA, (F-FUto)/(FNad-FUto))
        
        # constraints
        if Gamma == None:
            g = G
        else:
            hgam = np.array([norm(F-fPrev)**2/Gamma**2 - 1])            # equidist constriant
            hLam = sum(LAMBDA[:-1])-1                                   # weight constraint
#            gBS = np.array([ 1 - F[1]/fPrev[1] ])                       # Enzo's backstepping constraint
            
            if fPrev2 == None:
                gBS = -1
            else:
                StandardBS = False
                if 'dPhi' in self.VecOptions.keys():    #cone for backstepping constraint (no sensitivities available!)
                    if self.VecOptions['dPhi']:
                        gBS = np.dot(F-fPrev,fPrev2-fPrev)/(norm(fPrev2-fPrev)*norm(F-fPrev))+np.deg2rad(self.VecOptions['dPhi'])
                    else:
                        StandardBS = True
                elif 'dGamma' in self.VecOptions.keys():    #circle for backstepping constraint
                    if self.VecOptions['dGamma']:   
                        gBS = self.VecOptions['dGamma']*Gamma-norm(F-fPrev2)
                    else:
                        StandardBS = True
                else:
                    StandardBS = True
                if StandardBS: gBS = np.dot(F-fPrev,fPrev2-fPrev)#/norm(fPrev2-fPrev)   # Markus' backstepping constraint
            g = np.hstack([hgam,hLam,G,gBS]).tolist() #,gBS
        return u, g, 0
        
    def ObjAndConstrSensFct(self, x, u, g):
        '''
        GRADIENTS OF COST FUNCTION:
        Computes gradients of the weighted sum of objectives (cost function)  
        for a design x and weighting factors Lambda.
        
        Remarks: SubstituteModel.Variables-dict must be assigned to instance before call !!
            - 'Lambda': [None] if lambda is a design variable in pyOpt. [m-1 values] provided by user. 
            - 'Gamma':  [None] if anchor points are computed. [float value] that represents a fractional distance between anchor points.
            - 'fscales':[None] if anchor points are computed. [dict with 'Utopia' and 'Nadir' keys] to scale f's.
            - 'fPrev':  [None] if anchor points are computed. [m values] of last converged pareto point.
        
        Arguments:
        
        :param x:           Set of n design variables for which objectives and constraints are evaluated (NEVER NORMALIZED!!! DesOpt normalizes)
        :param u:           Value of cost function at x
        :param g:           Value of constraint functions at x
        
        Returns:        
        
        :return du:         Vector of cost function gradients at respective x
        :return dg:         Vector of constraints at respective x
        :return fail:       Flag for fail (default: 0)
        '''
        Lambda = self.Variables['Lambda']
        Gamma = self.Variables['Gamma']
        fscales = self.Variables['fscales']
        fPrev = self.Variables['fPrev']
        fPrev2 = self.Variables['fPrev2']
        
        # Additional local variables
        nf = self.Options['nf']         # Number of objectives
        nh = self.Options['nh']         # Number of equality constraints
        ng = self.Options['ng']         # Number of inequality constraints
        nDV = self.nDV                  # Number of design variables
        
        # x conversion to numpy array
        x = np.asarray(x)  

        # Normalize F
        # if one wants to normalize but no scales are given (fscales is empty dict)
        if not (fscales and self.VecOptions['NormalizeF']):
            fscales['Utopia'] = np.zeros(nf)
            fscales['Nadir'] = np.ones(nf)
            
        # scaling
        FUto = fscales['Utopia']
        FNad = fscales['Nadir']
        
        # split x vector from pyOpt               
        if Lambda == None:
            X      = cp.deepcopy(x[:nDV])
            LAMBDA = np.append(x[nDV:], 1-sum(x[nDV:]))
        else:
            X      = cp.deepcopy(x)
            LAMBDA = cp.deepcopy(Lambda)
        
       # get objective F and constraints G from model
        if X.tolist() in self.xList:
            match = [self.xList.index(xi) for xi in self.xList if (xi==X).all()]
            (F, g) = self.fgList[match[0]]
        else: # F and G should be known already
            F, g, fail = self.Model.ObjAndConstrFct(X)
            self.xList.append(X.tolist())
            self.fgList.append((F,g)) 
        
        # get gradients of objective f and constraints g from model           
        if X.tolist() in self.dxList:
            match = [self.dxList.index(xi) for xi in self.dxList if (xi==X).all()]
            (dF, dG) = self.dfdgList[match[0]]
            #self.SensLast = (dF, dG)               # last sensivity
        else:
            dF, dG, fail = self.Model.ObjAndConstrSensFct(X,F,g)
            self.dxList.append(X.tolist())
            self.dfdgList.append((dF,dG))
            
        dF = np.asarray(dF)
        dG = np.asarray(dG)
        
        # Gradient of cost function wrt x (and lambda)
        du = np.dot(dF.T,LAMBDA/(FNad-FUto))        # derivative wrt x
        if Lambda == None:      # lambda is part of pyOpt-x
            dudLambda = (F[:-1]-FUto[:-1])/(FNad[:-1]-FUto[:-1]) - \
                        (F[-1]-FUto[-1])/(FNad[-1]-FUto[-1])
            du = np.append(du,dudLambda)
        du = du.tolist()
        
        # Gradient of constraints wrt x (and lambda)
        # if Lambda == None: pyOpt varies lambda else: User determines lambda => pyOpt doesnt need d/dLambda
        if Lambda == None:
            dhgam = np.append(2/Gamma**2*np.dot((np.asarray(F)-fPrev),dF),np.zeros(nf-1))
            dhLam = np.append(np.zeros(nDV),np.ones(nf-1))                      # weight constraint wrt (x,lambda)
            dG    = np.column_stack(( dG, np.zeros((np.size(dG,0),nf-1)) ))     # dg from model wrt (x,lambda)
#            dgBS  = np.append(-dF[1,:],np.zeros(nf-1))                          # Enzo's backstepping constraint
            if fPrev2 == None:
                dgBS = np.zeros(nDV+nf-1)
            else:
                StandardBS = False
                if 'dPhi' in self.VecOptions.keys():    #cone for backstepping constraint (no sensitivities available!)
                    if self.VecOptions['dPhi']:
                        logging.error('Vector Optimizer: No sensitivities available for cone backstepping constraint!')
                    else:
                        StandardBS = True
                elif 'dGamma' in self.VecOptions.keys():    #circle for backstepping constraint
                    if self.VecOptions['dGamma']:   
                        V = np.asarray(F)-fPrev2
                        dgBS   = np.append(-1.*np.dot(V,dF)/norm(V),np.zeros(nf-1))
                    else:
                        StandardBS = True
                else:
                    StandardBS = True
                if StandardBS: dgBS = np.append(np.dot(dF.T,(fPrev2-fPrev))/(norm(fPrev2-fPrev)*norm(F-fPrev)),np.zeros(nf-1))    # Markus' backstepping constraint
                
                
            dg    = np.vstack((dhgam,dhLam,dG,dgBS))
        else:
            dg = dG

        return du, dg, 0