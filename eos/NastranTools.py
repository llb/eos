# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.
"""
Python class for NASTRAN input/output data

Author: Marc-André Koehler
Last Edited: 03.06.15

"""
import os
import eos.StdTextInOutLib as StdiO     # standard text i/O functions
import numpy as np
import logging
import glob


class NastranClass:
    # ----------------------------------------------------------------------- #
    # ----------------------------------------------------------------------- #  
    # Initialize
    # ----------------------------------------------------------------------- #
    # ----------------------------------------------------------------------- #
    def __init__(self,ModelPath=None):
        # Infostring
        self.InfoStr = '...Object with NASTRAN input/output methods'
        # Results
        self.Results = {}
        self.ModelPath = ModelPath
        
    def __call__(self):
        pass   
    def __str__(self):
        return self.InfoStr
    # ----------------------------------------------------------------------- #    
    # ----------------------------------------------------------------------- #    
    # Input File iO
    # ----------------------------------------------------------------------- #
    # ----------------------------------------------------------------------- #    
    @staticmethod
    def SplitLine(line, length=1):
        """
        works similarily to str.split() but splits string
        into packages of int(length) characters
        and stores the packages in a list
        """
        resultlist=list([line[:8]])
        line=line[8:]
        resultlist.extend([line[i:i+length] for i in range(0, len(line), length)])
        return resultlist

    def extract_Nodes(self,filename,ShortFormat=False):
        """Extract Nodes from an NASTRAN input file.

        Args:
          filename (string): The name of the input file to be read
        """
        # read in file
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        if ShortFormat:
            NodeList = list()
            with open(filename,'r',1) as f1:
                while 1:
                    line = f1.readline()
                    if 'GRID' in line and '$' not in line:
                        SplitLine = self.SplitLine(line,  length=8)
                        
                        SplitLine[1] = int(SplitLine[1])
                        for i in range(3,6):
                            try:
                                SplitLine[i] = float(SplitLine[i])
                            except:
                                FloatStr = SplitLine[i].replace('-',';-').replace('+',';+').split(';')
                                SplitLine[i] = float('%se%s'%(FloatStr[-2],FloatStr[-1]))
                        iNode = SplitLine[0:6]
                        NodeList.append([iNode[1], iNode[3], iNode[4], iNode[5]])
                    elif not line:
                        break
            self.Nodes = np.asarray(NodeList)
        else:
            ReadIn=np.loadtxt(filename,dtype='|S8',comments='$',delimiter=',')
            #Delete unneccessary lines and convert to float
            self.Nodes=np.delete(ReadIn,[0,2],1).astype(float)
        
    def extract_Elements(self,filename):  
        """Extract Elements from an NASTRAN input file.
           File must only contain element definitions or comments!

        Args:
          filename (string): The name of the input file to be read
        """
        # read in file
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        ReadIn=np.loadtxt(filename,dtype='|S8',comments='$',delimiter=',') 
        # store element type
        self.Eltype=ReadIn[0,0]
        # Store as float numpy array
        self.Elements=np.delete(ReadIn,0,1).astype(float)      
        
    def extract_Set(self,filename,Settype):  
        """Extract Sets from an NASTRAN input file.

        Args:
          filename (string): The name of the input file to be read\n
          Settype (string): Type of the set to be read in: Nset, Elset...
        """
        # read in file
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        List = StdiO.read_in(filename)
        # Check total number of Sets stored
        if hasattr(self, 'Sets'):
            numSets=0
            for keys in self.Sets:
                if Settype in keys:
                    numSets += 1
        else:
            numSets=0
            self.Sets=dict()
        #Extract List
        BeginStr="$ SET"
        EndStr="$"
        List = StdiO.extract(List,BeginStr,EndStr)
        #Delete SET statement and \n and ' '
        List[0]= List[0].split('=')[1]
        for i,iLine in enumerate(List):
            List[i]=iLine.split('\n')[0].strip()    
        # join strings    
        List=''.join(List)
        # Split
        List=List.split(',') 
        self.Sets[Settype+str(numSets+1)]=np.array(List).astype(int)
        
    def extract_Basevectors(self,filename,separateFile=True):
        """Extract Basevectors from an NASTRAN input file.

        Args:
          filename (string): The name of the input file to be read\n
          separateFile (boolean, optional):  Basevectors are located in separate file: [True]/False
        """
        # read in file
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        List = StdiO.read_in(filename)
        # Check total number of ShapeVariables stored
        if hasattr(self, 'Basevectors'):
            numShapeVar=len(self.Basevectors)
        else:
            numShapeVar=0
            self.Basevectors=dict()
        #Extract List
        if separateFile:
            List = StdiO.extract(List)
            logging.error('Nastran basevector extraction from seperate files not corrected yet!')
            # Old code        
            Delimiter=","
            tmpArray = StdiO.split2array(List,Delimiter)         
            #Delete unneccessary lines and convert to float
            self.Basevectors['ShapeVar'+str(numShapeVar+1)] = np.delete(tmpArray,[0,1,3,4],1)
            self.Basevectors['ShapeVar'+str(numShapeVar+1)]=self.Basevectors['ShapeVar'+str(numShapeVar+1)].astype(float)
        else:
            BeginStr="$$ Basevectors"
            EndStr="$"
            List = StdiO.extract(List,BeginStr,EndStr)
            Delimiter=","
            tmpArray = np.asarray(StdiO.split2list(List,Delimiter))
            tmpArray = np.delete(tmpArray,0,1).astype(float) 
            BasevectorIDs = np.unique(tmpArray[:,0])
            
            for iBaseID in BasevectorIDs:
                (DVGRIDids, ) = np.where(tmpArray[:,0]==iBaseID)
                DVGRIDids = DVGRIDids.tolist()
                self.Basevectors['DVGRID%i'%(iBaseID)] = np.zeros((len(DVGRIDids), 4))
                for iLoop, iGrid in enumerate(DVGRIDids): 
                    self.Basevectors['DVGRID%i'%(iBaseID)][iLoop,0] = tmpArray[iGrid,1]
                    self.Basevectors['DVGRID%i'%(iBaseID)][iLoop,1:] = tmpArray[iGrid,4:]
        
    def write_out_Nodes(self,Nodes='',CosyID='',filename="Nodes.blk",option='w'):
        """Write out Nodes in NASTRAN input file.

        Args:
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
          Nodes (numpyArray, optional): Optional specify the numpyArray containing the Nodenumber, x, y, z coordinates\n
          CosyID  (int, optional):  Coordinate System ID for the nodal coordinates (default=blank)\n
        """
        if Nodes=='':
            if hasattr(self, 'Nodes'):
                Nodes = self.Nodes
            else:
                print("""**Error**: Object has no nodes specified.
                > Read in nodes first or specify nodes.""")
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        with open(filename,option,1) as f1:   
            self.write_string(f1,"$ **Node definition",newline='\n')
            for iNode in Nodes:
                self.write_string(f1,'GRID',length=8,delimiter=',',wtype='max')
                self.write_string(f1,iNode[0],length=8,delimiter=',',wtype='max') # Node ID
                self.write_string(f1,CosyID,length=8,delimiter=',',wtype='max') # Coordinate System ID
                self.write_string(f1,iNode[1:],length=8,delimiter=',',newline='\n',wtype='max') # Coordinates

    def write_out_Elements(self,filename='Mesh.blk',option='a',Eltype=None):
        """Write out Elements in NASTRAN input file.

        Args:
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
        """
        if hasattr(self, 'Elements'):
            Elements = self.Elements
        else:
            print("""**Error**: Object has no elements specified.
                > Read in elements first or specify elements.""")
        Str = "\n$ Elements\n"      
        if Eltype == None:
            Eltype = self.Eltype
            # Write out
            for iEl in Elements:
                Str += Eltype+',%i,%i,%i,%i,%i,%i,%.3f\n'%(iEl[0],iEl[1],iEl[2],iEl[3],iEl[4],iEl[5],iEl[6])    
        else:    
            # Write out
            for iLine in Elements:
                Str += "\n"+Eltype+","
                for i,iVal in enumerate(iLine):
                    if i==0:
                        Str +=str(int(iVal))+",1"
                    else:
                        Str +=","+str(int(iVal))    
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        StdiO.write_out(Str,filename,option)
        
    def write_out_DesignVector(self,x,IDs,filename='DesVar.bdf',option='w'):
        """Write out Design vector in NASTRAN input file.

        Args:
          x (numpy array): Design vector\n
          IDs (List): List containing the indices of the DV in the design vector [Shape,SecThi,SecPhi,PatchThi,PatchPhi]\n
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
        """
        Str='$ DESVAR,ID,LABEL,XINIT,XLB,XUB,DELXV(OPTIONAL)'
        Str=Str+'\n$ ShapeVars:'
        for i in IDs[0]:
            Str=Str+'\nDESVAR*'+','+str(i)+','+'DV'+str(i)+','+str(x[i-1])+','+'-1.0e+06'+','+'1.0e+06'
        Str=Str+'\n$ SizingVars:'
        Str=Str+'\n$    Section thickness:'
        for i in IDs[1]:
            Str=Str+'\nDESVAR*'+','+str(i)+','+'DV'+str(i)+','+str(x[i-1])+','+'-1.0e+06'+','+'1.0e+06'
        Str=Str+'\n$    Section ply angles:'
        for i in IDs[2]:
            Str=Str+'\nDESVAR*'+','+str(i)+','+'DV'+str(i)+','+str(x[i-1])+','+'-1.0e+06'+','+'1.0e+06'
        if not IDs[3]==[]:
            Str=Str+'\n$    Patch thickness:'
            for i in IDs[3]:
                Str=Str+'\nDESVAR*'+','+str(i)+','+'DV'+str(i)+','+str(x[i-1])+','+'-1.0e+06'+','+'1.0e+06'
        if not IDs[4]==[]:
            Str=Str+'\n$    Patch ply angles:'
            for i in IDs[4]:
                Str=Str+'\nDESVAR*'+','+str(i)+','+'DV'+str(i)+','+str(x[i-1])+','+'-1.0e+06'+','+'1.0e+06'   
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        StdiO.write_out(Str,filename,option)      
        
    def write_out_Basevectors(self,Basevectors=None,filename='Basevectors.blk',option='a'):
        """Write out Basevectors for all ShapeVariables in NASTRAN input file.

        Args:
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
          Basevectors (numpyArray, optional): Optional specify the numpyArray containing the Basevectors for each ShapeVar\n
        """
        if Basevectors==None:
            if hasattr(self, 'Basevectors'):
                Basevectors = self.Basevectors
            else:
                print("""**Error**: Object has no Basevectors specified.
                > Read in Basevectors first or specify Basevectors.""")
        # Delete all basevectors with 0,0,0 (necessary for Nastran)
        for ShapeVar in Basevectors:
            iArray=Basevectors[ShapeVar]
            newArray=np.empty((1,4))
            k=0          
            for j,jLine in enumerate(iArray):
               if not (jLine[1]==0 and jLine[2]==0 and jLine[3]==0):
                   k+=1
                   newArray=np.vstack([newArray,jLine])
            Basevectors[ShapeVar]=newArray[1:]  
        # Write out
        Str = "$$ Basevectors $$"
        for ShapeVar in Basevectors:
            for j,jLine in enumerate(Basevectors[ShapeVar]):
                Str +="\nDVGRID,"+ShapeVar.split('ShapeVar')[1]+","
                for i,iVal in enumerate(jLine):
                    # Node number
                    if i==0:
                        Str +=str(int(iVal))+",0,1.0"
                    else:
                        Str +=","+str(float(iVal))
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        StdiO.write_out(Str,filename,option)
        
    def write_out_Sets(self,Sets=None,filename='Sets.blk',option='a'):
        """Write out Sets in NASTRAN input file.

        Args:
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
          Sets (dict, optional): Optional specify the dict containing the Sets for each ShapeVar\n
        """
        if Sets==None:
            if hasattr(self, 'Sets'):
                numSets=len(self.Sets)
                Sets = self.Sets
            else:
                print("""**Error**: Object has no Sets specified.
                > Read in Sets first or specify Sets.""")
        Str = "$$ Sets $$"
        # Write out
        for iSet in Sets:
            Str +="\n$ "+iSet+"\nSET "+str(numSets)+" = "
            for j,jLine in enumerate(Sets[iSet]):
                if j%7==0 and not j==0:
                    Str += str(jLine)+",\n        "
                else:
                    Str += str(jLine)+","
                if j==len(Sets[iSet])-1:
                    Str += str(jLine)
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        StdiO.write_out(Str,filename,option)   

    # APillar specific methods----------------------------------------------- #     
    
    def write_out_Properties_APillar(self,Sections,Patches,filename='Properties.blk',option='a'):
        """Write out Properties for APillar model in NASTRAN input file.

        Args:
          Sections (Dict): Dictionary containing all the information about the sections\n
          Patches (Dict): Dictionary containing all the information about the patches\n
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
        """
        Str='$ Property Definition'
        Str=Str+'\n$ Sections:'
        for iSec in Sections['Elems']:
            Str=Str+'\nPCOMP'+','+str(iSec)+',,,1e9,'+Sections['FailureTheory']+',,,SYM\n'+ \
                   ',1,'+str(Sections['PlyThick'])+',45.0,YES\n'+ \
                   ',1,'+str(Sections['PlyThick'])+',-45.0,YES\n'+ \
                   ',1,'+str(Sections['PlyThick'])+',0.0,YES'
        if Patches['TotalNum']!=0:
            Str=Str+'\n$ Patches:'
            for iPatch in Patches['Elems']:
                PatchID=int(iPatch)+Patches['PropStep']
                if Patches['PlyAngles'][iPatch-1]=='var': #variable PlyAngle = 2plies
                    Str=Str+'\nPCOMP'+','+str(PatchID)+',,,1e9,'+Patches['FailureTheory']+',,,SYM\n'+ \
                           ',1,'+str(Patches['UnitThick'])+',30.0,YES\n'+ \
                           ',1,'+str(Patches['UnitThick'])+',-30.0,YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',45.0,YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',-45.0,YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',0.0,YES'  
                elif Patches['PlyAngles'][iPatch-1]==0 or Patches['PlyAngles'][iPatch-1]==90: #fixed ply angle 0°/90° = 1 ply
                    Str=Str+'\nPCOMP'+','+str(PatchID)+',,,1e9,'+Patches['FailureTheory']+',,,SYM\n'+ \
                           ',1,'+str(Patches['UnitThick'])+','+str(Patches['PlyAngles'][iPatch-1])+',YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',45.0,YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',-45.0,YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',0.0,YES' 
                else:  #fixed +/-Theta = 2 plies
                    Str=Str+'\nPCOMP'+','+str(PatchID)+',,,1e9,'+Patches['FailureTheory']+',,,SYM\n'+ \
                           ',1,'+str(Patches['UnitThick'])+','+str(Patches['PlyAngles'][iPatch-1])+',YES\n'+ \
                           ',1,'+str(Patches['UnitThick'])+',-'+str(Patches['PlyAngles'][iPatch-1])+',YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',45.0,YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',-45.0,YES\n'+ \
                           ',1,'+str(Sections['PlyThick'])+',0.0,YES'  
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        StdiO.write_out(Str,filename,option)        
    
    def write_out_Constr_APillar(self,Sections,Patches,filename='constr.blk',option='a'):
        """Write out optimization constraints for APillar model in NASTRAN input file.

        Args:
          Sections (Dict): Dictionary containing all the information about the sections\n
          Patches (Dict): Dictionary containing all the information about the patches\n
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
        """
        iDispResp=100
        iFailureResp=500
        LCnameList=['CrushX','CrushY','CrushZ','StiffnX','StiffnY','StiffnZ']
        DispRespDir=['1','2','3','1','2','3']
        DispRespNodeID=['47711','47711','47711','47710','47710','47710']
        DesSubID=['200','201','202','203']
        ActConstrLC=[['101','102','103'],['104'],['105'],['106']]
        ConstrID=[]
        Str='$ Optimization constraints'
        # Stiffness + Compliance
        Str=Str+'\n$ Stiffness + Compliance:'
        for i in range(6):
            Str=Str+'\nDRESP1,'+str(iDispResp+i+1)+','+str(LCnameList[i])+',DISP,,,'+str(DispRespDir[i])+',,'+str(DispRespNodeID[i])+ \
                    '\nDCONSTR,'+str(iDispResp+i+1)+','+str(iDispResp+i+1)+',-.00001,.00001'
        # Failure indices
        Str=Str+'\n$ Failure indices:'
        #Sections
        for iSec in Sections['Elems']:
            Str=Str+'\n$ Section'+str(iSec)+':'
            Str=Str+'\nDRESP1,'+str(iFailureResp+1)+',FI1,CFAILURE,PCOMP,1,5,1,'+str(iSec)+ \
                    '\nDRESP1,'+str(iFailureResp+2)+',FI2,CFAILURE,PCOMP,1,5,2,'+str(iSec)+ \
                    '\nDRESP1,'+str(iFailureResp+3)+',FI3,CFAILURE,PCOMP,1,5,3,'+str(iSec)+ \
                    '\nDRESP1,'+str(iFailureResp+4)+',FI4,CFAILURE,PCOMP,1,5,4,'+str(iSec)+ \
                    '\nDRESP1,'+str(iFailureResp+5)+',FI5,CFAILURE,PCOMP,1,5,5,'+str(iSec)+ \
                    '\nDRESP1,'+str(iFailureResp+6)+',FI6,CFAILURE,PCOMP,1,5,6,'+str(iSec)
            Str=Str+'\nDRESP2,'+str(iFailureResp+7)+',FIMAX,5,,,,,\n,DRESP1,'+str(iFailureResp+1)+','+str(iFailureResp+2)+','+ \
                    str(iFailureResp+3)+','+str(iFailureResp+4)+','+str(iFailureResp+5)+','+str(iFailureResp+6)       
            Str=Str+'\nDCONSTR,'+str(iFailureResp+7)+','+str(iFailureResp+7)+',,-0.1'
            iFailureResp=iFailureResp+7
            ConstrID.append(iFailureResp)
        # Patches
            if Patches['TotalNum']!=0:
                for iPatch,CoveredProps in Patches['CoveredProps'].iteritems(): 
                    PatchID=int(iPatch)+Patches['PropStep']
                    if iSec in Patches['CoveredProps'][iPatch]: #Patch is defined on top of section iSec?
                        if Patches['PlyAngles'][int(iPatch)-1]==90 or Patches['PlyAngles'][int(iPatch)-1]==0: #fixed ply angle 0°/90° = 1 ply
                            Str=Str+'\nDRESP1,'+str(iFailureResp+1)+',FIP,CFAILURE,PCOMP,1,5,1,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+2)+',FI1,CFAILURE,PCOMP,1,5,2,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+3)+',FI2,CFAILURE,PCOMP,1,5,3,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+4)+',FI3,CFAILURE,PCOMP,1,5,4,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+5)+',FI4,CFAILURE,PCOMP,1,5,5,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+6)+',FI5,CFAILURE,PCOMP,1,5,6,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+7)+',FI6,CFAILURE,PCOMP,1,5,7,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+8)+',FIP,CFAILURE,PCOMP,1,5,8,'+str(PatchID)
                            Str=Str+'\nDRESP2,'+str(iFailureResp+9)+',FIMAX,6,,,,,\n,DRESP1,'+str(iFailureResp+1)+','+str(iFailureResp+2)+','+ \
                                    str(iFailureResp+3)+','+str(iFailureResp+4)+','+str(iFailureResp+5)+','+str(iFailureResp+6)+'\n,'+str(iFailureResp+7)+','+str(iFailureResp+8)        
                            Str=Str+'\nDCONSTR,'+str(iFailureResp+9)+','+str(iFailureResp+9)+',,-0.1'
                            iFailureResp=iFailureResp+9
                            ConstrID.append(iFailureResp)
                        else: #variable PlyAngle or fixed +/-Theta = 2 plies
                            Str=Str+'\nDRESP1,'+str(iFailureResp+1)+',FIP,CFAILURE,PCOMP,1,5,1,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+2)+',FIP,CFAILURE,PCOMP,1,5,2,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+3)+',FI1,CFAILURE,PCOMP,1,5,3,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+4)+',FI2,CFAILURE,PCOMP,1,5,4,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+5)+',FI3,CFAILURE,PCOMP,1,5,5,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+6)+',FI4,CFAILURE,PCOMP,1,5,6,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+7)+',FI5,CFAILURE,PCOMP,1,5,7,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+8)+',FI6,CFAILURE,PCOMP,1,5,8,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+9)+',FIP,CFAILURE,PCOMP,1,5,9,'+str(PatchID)+ \
                                    '\nDRESP1,'+str(iFailureResp+10)+',FIP,CFAILURE,PCOMP,1,5,10,'+str(PatchID)
                            Str=Str+'\nDRESP2,'+str(iFailureResp+11)+',FIMAX,7,,,,,\n,DRESP1,'+str(iFailureResp+1)+','+str(iFailureResp+2)+','+ \
                                    str(iFailureResp+3)+','+str(iFailureResp+4)+','+str(iFailureResp+5)+','+str(iFailureResp+6)+'\n,'+str(iFailureResp+7)+','+ \
                                    str(iFailureResp+8)+','+str(iFailureResp+9)+','+str(iFailureResp+10)        
                            Str=Str+'\nDCONSTR,'+str(iFailureResp+11)+','+str(iFailureResp+11)+',,-0.1'
                            iFailureResp=iFailureResp+11
                            ConstrID.append(iFailureResp)
        # Define active constraints for each LC
        Str=Str+'\n$ Active constraints for each LC:'
        for i,v in enumerate(ActConstrLC):
            ActConstrLC[i].extend(ConstrID)
        for k in range(len(DesSubID)): 
            ConstrStr=''
            for i,j in enumerate(ActConstrLC[k]):
                if i==0:
                    ConstrStr=ConstrStr+str(j) 
                elif i%6==0:
                    ConstrStr=ConstrStr+'\n,'+str(j) 
                else:
                    ConstrStr=ConstrStr+','+str(j)  
            Str=Str+'\nDCONADD,'+str(DesSubID[k])+','+ConstrStr                
        #Write out to file        
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)                                              
        StdiO.write_out(Str,filename,option)     
    
    def write_out_DVPMrelations_APillar(self,Sections,Patches,DesVarIDs,filename='dvPMrel.blk',option='a'):
        """Write out design variable relations for APillar model in NASTRAN input file.

        Args:
          Sections (Dict): Dictionary containing all the information about the sections\n
          Patches (Dict): Dictionary containing all the information about the patches\n
          DesVarIDs(List): List containing the indices of the DV in the design vector [Shape,SecThi,SecPhi,PatchThi,PatchPhi]\n
          filename (string, optional): The name of the input file to be written\n
          option (string, optional):  Text option: 'a'=append to existing file, 'w'=overwrite existing file,...\n
        """
        iDVrel=10
        Str='$ Design variable relations'
        # Sections
        for iSec in Sections['Elems']:
            Str=Str+'\n$ Section'+str(iSec)+':'
            # Section thickness
            iDV0=DesVarIDs[1][2*(iSec)-2]
            iDVTheta=DesVarIDs[1][2*(iSec)-1]
            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(iSec)+',23,,,'+str(Sections['LayerThickTol'])+',,'+str(iDV0)+','+str(Sections['PlyThick'])+ \
                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(iSec)+',13,,,'+str(Sections['LayerThickTol'])+',,'+str(iDVTheta)+','+str(Sections['PlyThick'])+ \
                    '\nDVPREL1'+','+str(iDVrel+2)+',PCOMP,'+str(iSec)+',17,,,'+str(Sections['LayerThickTol'])+',,'+str(iDVTheta)+','+str(Sections['PlyThick'])
            iDVrel=iDVrel+3       
            # Section ply angles
            iDV=DesVarIDs[2][iSec-1]
            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(iSec)+',14,,,0.0,,'+str(iDV)+',1.0'+ \
                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(iSec)+',18,,,0.0,,'+str(iDV)+',-1.0'
            iDVrel=iDVrel+2 
            # Patches
            if Patches['TotalNum']!=0:
                for iPatch,CoveredProps in Patches['CoveredProps'].iteritems():
                    PatchID=int(iPatch)+Patches['PropStep']
                    # Check if section is covered by iPatch
                    if iSec in CoveredProps:
                        Str=Str+'\n$ Patch:'+str(PatchID)+':'
                        if Patches['PlyAngles'][int(iPatch)-1]==90 or Patches['PlyAngles'][int(iPatch)-1]==0: #fixed ply angle 0°/90° = 1 ply
                            # Patch thickness
                            iDV=DesVarIDs[3][int(iPatch)-1]
                            #if iPatch==1:
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',13,,,'+str(Patches['PatchThickTol'])+',,'+str(iDV)+','+str(Patches['UnitThick'])
                            iDVrel=iDVrel+1 
                            #elif iPatch==2:
                            #    Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',13,,,'+str(Patches['PatchThickTol'])+',,'+str(iDV)+','+str(Patches['UnitThick'])+','+str(iDV-1)+','+str(Patches['UnitThick'])
                            #    iDVrel=iDVrel+1
                            #elif iPatch==3:
                            #    Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',13,,,'+str(Patches['PatchThickTol'])+',,'+str(iDV)+','+str(Patches['UnitThick'])+','+str(iDV-1)+','+str(Patches['UnitThick'])+','+str(iDV-2)+','+str(Patches['UnitThick'])
                            #    iDVrel=iDVrel+1                                   
                            # Section thickness
                            iDV0=DesVarIDs[1][2*(iSec)-2]
                            iDVTheta=DesVarIDs[1][2*(iSec)-1]
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',27,,,'+str(Sections['LayerThickTol'])+',,'+str(iDV0)+','+str(Sections['PlyThick'])+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',17,,,'+str(Sections['LayerThickTol'])+',,'+str(iDVTheta)+','+str(Sections['PlyThick'])+ \
                                    '\nDVPREL1'+','+str(iDVrel+2)+',PCOMP,'+str(PatchID)+',23,,,'+str(Sections['LayerThickTol'])+',,'+str(iDVTheta)+','+str(Sections['PlyThick'])
                            iDVrel=iDVrel+3 
                            # Section ply angles
                            iDV=DesVarIDs[2][iSec-1]
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',18,,,0.0,,'+str(iDV)+',1.0'+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',24,,,0.0,,'+str(iDV)+',-1.0'
                            iDVrel=iDVrel+2                             
                        elif Patches['PlyAngles'][int(iPatch)-1]=='var':  #variable PlyAngle                         
                            # Patch thickness
                            iDV=DesVarIDs[3][int(iPatch)-1]
                            #if iPatch==6:
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',13,,,'+str(Patches['PatchThickTol'])+',,'+str(iDV)+','+str(Patches['UnitThick'])+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',17,,,'+str(Patches['PatchThickTol'])+',,'+str(iDV)+','+str(Patches['UnitThick'])
                            iDVrel=iDVrel+2 
                            # Patch ply angles
                            iDV=DesVarIDs[4][int(iPatch)-1]  
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',14,,,0.0,,'+str(iDV)+',1.0'+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',18,,,0.0,,'+str(iDV)+',-1.0'
                            iDVrel=iDVrel+2                                
                            # Section thickness
                            iDV0=DesVarIDs[1][2*(iSec)-2]
                            iDVTheta=DesVarIDs[1][2*(iSec)-1]
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',33,,,'+str(Sections['LayerThickTol'])+',,'+str(iDV0)+','+str(Sections['PlyThick'])+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',23,,,'+str(Sections['LayerThickTol'])+',,'+str(iDVTheta)+','+str(Sections['PlyThick'])+ \
                                    '\nDVPREL1'+','+str(iDVrel+2)+',PCOMP,'+str(PatchID)+',27,,,'+str(Sections['LayerThickTol'])+',,'+str(iDVTheta)+','+str(Sections['PlyThick'])
                            iDVrel=iDVrel+3 
                            # Section ply angles
                            iDV=DesVarIDs[2][iSec-1]
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',24,,,0.0,,'+str(iDV)+',1.0'+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',28,,,0.0,,'+str(iDV)+',-1.0'
                            iDVrel=iDVrel+2
                        else: #fixed ply angle +/- Theta
                            # Patch thickness
                            iDV=DesVarIDs[3][int(iPatch)-1]
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',13,,,'+str(Patches['PatchThickTol'])+',,'+str(iDV)+','+str(Patches['UnitThick'])+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',17,,,'+str(Patches['PatchThickTol'])+',,'+str(iDV)+','+str(Patches['UnitThick'])
                            iDVrel=iDVrel+2                                
                            # Section thickness
                            iDV0=DesVarIDs[1][2*(iSec)-2]
                            iDVTheta=DesVarIDs[1][2*(iSec)-1]
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',33,,,'+str(Sections['LayerThickTol'])+',,'+str(iDV0)+','+str(Sections['PlyThick'])+'\n'+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',23,,,'+str(Sections['LayerThickTol'])+',,'+str(iDVTheta)+','+str(Sections['PlyThick'])+ \
                                    '\nDVPREL1'+','+str(iDVrel+2)+',PCOMP,'+str(PatchID)+',27,,,'+str(Sections['LayerThickTol'])+',,'+str(iDVTheta)+','+str(Sections['PlyThick'])
                            iDVrel=iDVrel+3 
                            # Section ply angles
                            iDV=DesVarIDs[2][iSec-1]
                            Str=Str+'\nDVPREL1'+','+str(iDVrel)+',PCOMP,'+str(PatchID)+',24,,,0.0,,'+str(iDV)+',1.0'+ \
                                    '\nDVPREL1'+','+str(iDVrel+1)+',PCOMP,'+str(PatchID)+',28,,,0.0,,'+str(iDV)+',-1.0'
                            iDVrel=iDVrel+2                                 
                        
        #Write out to file  
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)                                                    
        StdiO.write_out(Str,filename,option)        
        
    
    # ----------------------------------------------------------------------- #    
    # ----------------------------------------------------------------------- #    
    # NastranCall
    # ----------------------------------------------------------------------- #
    # ----------------------------------------------------------------------- #
    def nastrancall(self,filename):
        """Call NASTRAN solver

        Args:
          filename (string): The name of the NASTRAN input file (without .bdf)
        """
        import time
        from sys import platform as _platform
        if self.ModelPath != None:
            filename = os.path.join(self.ModelPath,filename)
        # Change to temporary path
        OrgPath = os.getcwd()
        TempPath = os.path.dirname(os.path.realpath(__file__))
        os.chdir(TempPath)
        # Clean up old files
        tmpFileList = []
        tmpFileList.extend(glob.glob("*.op2"))
        tmpFileList.extend(glob.glob("*.op4"))
        tmpFileList.extend(glob.glob("*.pch"))
        tmpFileList.extend(glob.glob("*.f06"))
        tmpFileList.extend(glob.glob("*.f04"))
        tmpFileList.extend(glob.glob("*.DBALL"))
        tmpFileList.extend(glob.glob("*.log"))
        tmpFileList.extend(glob.glob("*.MASTER"))
        tmpFileList.extend(glob.glob("*.pch"))
        tmpFileList.extend(glob.glob("*.plt"))
        tmpFileList.extend(glob.glob("*.rcf"))
        tmpFileList.extend(glob.glob("*.asm"))
        tmpFileList.extend(glob.glob("*.aeso"))
        tmpFileList.extend(glob.glob("*.becho"))
        tmpFileList.extend(glob.glob("*.IFPDAT"))
        for f in tmpFileList:
            try:
                os.remove(f)
            except:
                continue
        # Check OS
        if _platform=='win32':
            NastranPath='\"C:/MSC.Software/MSC_Nastran/20121/bin/nastran.exe\"'
            CmdLine=NastranPath+' '+filename+'.bdf scr=yes'
        elif _platform=='linux' or _platform =='linux2':
            CmdLine='. /etc/profile&&module load nastran/2012&&ulimit -s 8192&&nastran '+filename+'.bdf bat=no scr=yes'
        else:
            logging.error('Operating system currently not supported!')
        # Call Nastran
        t = time.time()
        os.system(CmdLine)
        NastranRuntime=time.time()-t
        os.chdir(OrgPath)
        return NastranRuntime
        
    # ----------------------------------------------------------------------- #
    # ----------------------------------------------------------------------- #    
    # Output file ReadIn
    # ----------------------------------------------------------------------- #
    # ----------------------------------------------------------------------- #
    def read_in_OP4_ascii(self,filename):
        """Read in results from ascii encoded .op4 file

        Args:
          filename (string): The name of the NASTRAN output file (without .op4)
        """
        # check if file exists
        if self.ModelPath != None:
            op4file=os.path.join(self.ModelPath,filename+'.op4')
        else:
            op4file=os.path.join(filename+'.op4')
        if not os.path.exists(op4file):
            print('ERROR: '+op4file+' does not exist!')
        # open file
        key=filename.lower()
        f=open(op4file, mode = 'r')
        # read in
        BeginData=False
        for line in f:
            # Read in result format information
            if line[0:3]=='   ' and not BeginData:
                ncols, nrows, form, Type = line[0:32].split()
                ncols=int(ncols)
                nrows=int(nrows)
                form=int(form)
                Type=int(Type)
                Array=np.zeros((nrows,ncols))
                BeginData=True
            # Reached end of file
            elif line==' 1.000000000E+00\n':
                break  
            # Column information
            elif line[0:3]=='   ' and len(line[0:32].split())==3:
                icol,irowS,irowE = [int(iEl) for iEl in line[0:32].split()]
                # differentiation between r1valr,r2valr and dscm matrix
                if irowS==0:
                    irowS=1
                irow=irowS
            # read in data in current column
            elif line[0:3]!='   ' and BeginData:
                LineData=StdiO.chunks(line.rstrip('\n'), 16)
                NumEntries=len(LineData)
                for iVal in range(NumEntries):
                    Array[irow-1+iVal,icol-1]=float(LineData[iVal])
                irow=irow+NumEntries
  
        # close file
        f.close()
        self.Results[key]=Array     
        
    def ReadTextFilesViaLoadTXT(self,Dir):
        tmpPathName             = os.path.join(self.ModelPath,Dir,'*.txt')
        tmpFileList             = glob.glob(tmpPathName)
        tmpFileList.sort()
        TXTdict                 = dict()
        # Load from file
        for iSec,f in enumerate(tmpFileList):
            TXTdict[(iSec+1)] = np.loadtxt(f,dtype=float,comments='$') 
        return TXTdict
            
    def StoreInShelveFile(self,ResultsList,ResultsNameList,FileName='NastranResults.shelve'):  
        import shelve
        if self.ModelPath != None:
            shelveFile=shelve.open(os.path.join(self.ModelPath,FileName),'n')
        else:
            shelveFile=shelve.open(FileName,'n')
        logging.info('Storing following variables in %s'%(FileName))
        ResultName = ''
        for iName in ResultsNameList:
            ResultName += iName+', '
        logging.info(ResultName)
        if len(ResultsList) != len(ResultsNameList):
            logging.error('ResultsList and ResultsNameList have to be of same length')
        for (iResult, iName) in zip(ResultsList,ResultsNameList):
            shelveFile[iName]=iResult
        shelveFile.close()
        
    def DeleteFiles(self, FileList):
        # Clean up old files
        tmpFileList = []
        for iFile in FileList:
            tmpFileList.extend([os.path.join(self.ModelPath,iFile)])
        for f in tmpFileList:          
            try:
                os.remove(f)
            except:
                continue 
            
    def _FindIndexForID(self,ID,Array,IDcolum=0, TempArray=None):
        '''
        Find associated Index, where Array[Index,IDcolum] == ID
        '''
        if Array[ID,IDcolum]==ID: # right at that position
            return ID
        if ID > 0: # one back
            if Array[ID-1,IDcolum]==ID:
                return ID-1
        if ID > 1: # two back
            if Array[ID-2,IDcolum]==ID:
                return ID-2
        if ID < np.shape(Array)[1]: # one forward
            if Array[ID+1,IDcolum]==ID:
                return ID+1
        if ID < np.shape(Array)[1]-1: # two forward
            if Array[ID+2,IDcolum]==ID:
                return ID+2
        if ID-Array[0,IDcolum] > -1: # right at zero plus offset
            if ID-Array[0,IDcolum] < np.shape(Array)[1]+1: # right at zero
                if Array[ID-Array[0,IDcolum],IDcolum]==ID:
                    return ID-Array[0,IDcolum]
        if ID-Array[0,IDcolum] > 0: # one back plus offset
            if ID-Array[0,IDcolum] < np.shape(Array)[1]+2: # right at zero
                if Array[ID-Array[0,IDcolum]-1,IDcolum]==ID:
                    return ID-Array[0,IDcolum]-1
        if ID-Array[0,IDcolum] > -2: # one forward plus offset
            if ID-Array[0,IDcolum] < np.shape(Array)[1]+0: # right at zero
                if Array[ID-Array[0,IDcolum]+1,IDcolum]==ID:
                    return ID-Array[0,IDcolum]+1
        if TempArray == None:
            return np.where(Array[:,IDcolum]==ID)[0]
        else:
            # TempArray = np.delete(TempArray, Index, axis=0)
            return np.where(TempArray[:,IDcolum]==ID)[0]
            
    def write_string(self, handler, element, length=0, delimiter='',newline='',
                     alignment=None, wtype='exact'):
        '''
        Class for writing numbers or strings with an exact or an maximal user 
        specified length.
        
        Args:
            handler:    file to write in \n
            element:    either number, string, list() or np.ndarray() \n
            length:     length of written string, can be list() if element is list() or np.ndarray() \n
            delimiter:  delimmiter bewteen strings \n
            newline:    type of newline \n
            alignment:  left, right, center and padding characters can be list() if element is list() or np.ndarray() \n
            wtype       defines if the length of the written string is exact or max the user specified length \n
        '''
        # Definition of settings/parameters...
        # ------------------------------------------------------------------- #        
        self.wtype = wtype
        
        # Check if element is list() or np.ndarray()
        # ------------------------------------------------------------------- #
        if isinstance(element,list) or isinstance(element,np.ndarray):
            # Loop over elements in list or np.ndarray
            # --------------------------------------------------------------- #
            index=0
            for instance in element:
                if isinstance(length,list):
                    if len(length)!=len(element):
                        print ('len(length) != len(element)!')
                        raise SystemError
                    self.length = length[index]
                else:
                    self.length = length
                if isinstance(alignment,list):
                    if len(alignment)!=len(element):
                        print ('len(alignment) != len(element)!')
                        raise SystemError
                    self.alignment = alignment[index]
                else:
                    self.alignment = alignment
                handler.write(self.call_write_string(instance))
                if index+1 != len(element):
                    handler.write(delimiter)
                index +=1
            handler.write(newline)
        else:
            # Writing of instance
            # --------------------------------------------------------------- #
            if isinstance(length,list):
                print ('length is list(), but there is only one element!')
                raise SystemError
            else:
                self.length = length
            if isinstance(alignment,list):
                print ('alignment is list(), but there is only one element!')
                raise SystemError
            else:
                self.alignment = alignment
            handler.write(self.call_write_string(element)+delimiter+newline)
            
    def call_write_string(self,element):
        '''
        Converting string and numbers
        '''  
        # Check alignment type
        # ------------------------------------------------------------------- #
        if self.alignment=='left':
            fmt = '{:<'
        elif self.alignment=='right':
            fmt = '{:>'
        elif self.alignment=='center':
            fmt = '{:^'
        elif self.alignment=='padding characters':
            fmt = '{:='
        elif self.alignment==None:
            fmt = '{:'
        else:
            print ('Alignment not supported!')
            raise SystemError
        # Check if length==0, so no limit is given
        # ------------------------------------------------------------------- #
        if self.length==0:
            fmt+='}'
            resultstr = fmt.format(element)
        else:
            # Try to convert in number
            # --------------------------------------------------------------- #
            try:
                try:
                    number=float(element)
                except:
                    # element format is exponential but without 'e' e.g.: 1.2-5
                    # ------------------------------------------------------- #
                    FloatStr = element.replace('-',';-').replace('+',';+').split(';')
                    if len (FloatStr)==1:
                        number=float(FloatStr[0])
                    else:
                        number = float('%se%s'%(FloatStr[-2],FloatStr[-1]))
                if number.is_integer():
                    number=int(number)
                    fmt+=str(self.length)+'}'
                elif number<0:
                    if abs(number) > 10**(self.length-1) or abs(number) < 1e-3:
                        fmt+=str(self.length)+'.'+str(self.length-7)+'e}'
                    else:
                        fmt+=str(self.length)+'f}'
                elif number>0:
                    if abs(number) > 10**(self.length-1) or abs(number) < 1e-3:
                        fmt+=str(self.length)+'.'+str(self.length-6)+'e}'
                    else:
                        fmt+=str(self.length)+'f}'
                else:
                    print ('Unknown error occcured!')
                    raise SystemError
                resultstr = fmt.format(number)[:self.length]
            except ValueError:
                # element is no number
                # ----------------------------------------------------------- #
                fmt+=str(self.length)+'}'
                resultstr = fmt.format(element)[:self.length]
        # Check wtype type
        # ------------------------------------------------------------------- #
        if self.wtype == 'max':
            return resultstr.strip()
        elif self.wtype == 'exact':
            return resultstr
        else:
            print ('wtype not supported!')
            raise SystemError
    
    def ConvtToComSep(self,inputfile=None, outputfile=None, key=None):
        '''
        Function for converting .fem or .bulk files without comma seperation into
        bulk files with comma seperation
        
        Supported keys:     'Nodes'
                            'Basevectors'
        '''
        with open(inputfile,'r',1) as f1:
            with open(outputfile,'w',1) as f2:
                f2.write('$$ '+str(key)+' $$\n')
                newdvgrid=False
                newgrid=False
                while 1:
                    line = f1.readline()
                    if 'DVGRID*' in line.split() and '$' not in line and key=='Basevectors':
                        self.write_string(f2,line.split(),length=8,delimiter=',',newline=',',wtype='max')
                        newdvgrid=True
                    elif 'GRID' in line.split() and '$' not in line and key=='Nodes':
                        line=self.SplitLine(line, length=8)
                        self.write_string(f2,line[:6],length=8,delimiter=',',newline='\n',wtype='max')
                    elif 'GRID*' in line.split() and '$' not in line and key=='Nodes':
                        line=self.SplitLine(line, length=16)
                        line[0]='GRID'
                        self.write_string(f2,line[:5],length=8,delimiter=',',newline=',',wtype='max')
                        newgrid=True
                    elif not line:
                                break
                    elif newdvgrid and line[0]=='*':
                        line=self.SplitLine(line, length=16)
                        self.write_string(f2,line[1:4],length=8,delimiter=',',newline='\n',wtype='max')
                        newdvgrid=False
                    elif newgrid and line[0]=='*':
                        line=self.SplitLine(line, length=16)
                        self.write_string(f2,line[1],length=8,newline='\n',wtype='max')
                        newgrid=False
                    else:
                        newgrid=False
                        newdvgrid=False
        
# Debugging
# --------------------------------------------------------------------------- #
if __name__=='__main__':
    print('Debugging')
    NastranObj = NastranClass()
    NastranObj.extract_Nodes('mesh_fl_NonPertubated.bdf',ShortFormat=True)
    Nodes = NastranObj.Nodes
    NastranObj.extract_Basevectors('basevectors_st_ShortFormat.blk',separateFile=False)
    BaseVecs = NastranObj.Basevectors
    
    