# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import pyOpt
import numpy as np

File = 'APillarPrePreg-201501231901'


HistoryObject = pyOpt.History(File,'r')  
xHist         = np.asarray(HistoryObject.read([0,-1],['x'])[0]['x'])
fHist         = np.asarray(HistoryObject.read([0,-1],['obj'])[0]['obj'])
try:
    ConHist   = np.asarray(HistoryObject.read([0,-1],['con'])[0]['con'])
except:
    print 'No constraints'

