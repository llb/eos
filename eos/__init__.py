# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import logging
import sys
import numpy as np
import copy as cp
import os
import glob
import shutil
import configparser
import platform
import pickle
from datetime import datetime
from numpy.linalg import norm
import traceback
import collections
import pprint
import time
from math import floor, log10

# check if there is already a logger. If not: create a new one on DEBUG logging level
#if len(logging.Logger.root.handlers) == 0:
#    # create new logger
#    try:
#        os.remove('eos.log')
#    except:
#        pass
#
#    logging.basicConfig(filename='eos.log', format='%(asctime)s - %(levelname)s:  %(message)s',
#                        datefmt='%Y-%m-%d %H:%M:%S', level=logging.DEBUG)
#    logging.info("-------------------------------------------------")
#    logging.info("Initializing EOS, Starting new Log on DEBUG Level")
#    logging.info("-------------------------------------------------")
#else:
#    logging.info("Initializing EOS, using previously created logger")


def StartLogger(LoggingLevel='DEBUG'):
    # create new logger
    try:
        os.remove('eos.log')
    except:
        pass

    logging.basicConfig(filename='eos.log', format='%(asctime)s - %(levelname)s:  %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S', level=logging.DEBUG)
    if LoggingLevel == 'DEBUG':
        logging.Logger.root.level = logging.DEBUG
        logging.Logger.root.handlers[0].level = logging.DEBUG
    elif LoggingLevel == 'INFO':
        logging.Logger.root.level = logging.INFO
        logging.Logger.root.handlers[0].level = logging.INFO
    elif LoggingLevel == 'WARNING':
        logging.Logger.root.level = logging.WARNING
        logging.Logger.root.handlers[0].level = logging.WARNING
    elif LoggingLevel == 'ERROR':
        logging.Logger.root.level = logging.ERROR
        logging.Logger.root.handlers[0].level = logging.ERROR
    elif LoggingLevel == 'CRITICAL':
        logging.Logger.root.level = logging.CRITICAL
        logging.Logger.root.handlers[0].level = logging.CRITICAL
    logging.info("-------------------------------------------------")
    logging.info("Initializing EOS, Starting new Log on %s Level" % LoggingLevel)
    logging.info("-------------------------------------------------")


def SetupEosWorkDir(jobname='', rootdir=''):
    if platform.system() != 'Windows':
        # using print here instead of logging, not to trigger a logger before the log file is created on linux
        print('SetupWorkDir() method only implemented for Windows, ignoring')
        return
    else:
        # get the current folder (i.E. project folder)
        ProjectDir = os.getcwd()

        if not rootdir:
            EosPath = os.path.dirname(os.path.dirname(ProjectDir))
            rootdir = os.path.join(EosPath, 'results')

        if not jobname:
            jobname = 'eos'

        timestring = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        DirName = jobname + '_' + timestring

        EosWorkDir = os.path.join(rootdir, DirName)

        shutil.copytree(ProjectDir, EosWorkDir, ignore=shutil.ignore_patterns('.git*', 'results'))

        # remove the first entry from sys.path, this is the "old" project directory.
        if "eos" in sys.path[0]:
            sys.path.pop(0)

        #instead, append the new work dir to the path
        sys.path.append(EosWorkDir)

        os.chdir(EosWorkDir)

        return EosWorkDir


class SimulationMaker():
    def __init__(self, Model, LoggingLevel=None):
        """
        Defines object for performing multiple simulations such as simple evaluation, optimization, sensitivity study etc.\n\n

        Model           ... Simulation model. At least ComputeResp instance. If ComputeSens is given its sensitivities will be used\n
                    Model.ComputeResp  ... Returns f,g   -> f  = [f], g = [g1, g2, g3]\n
                    Model.ComputeSens  ... Returns df,dg -> df = [df/dx1, df/dx2, ..., df/dxN],
                                            g = [[dg1/dx1, dg1/dx2, ..., dg1/dxN],
                                                 [dg2/dx1, dg2/dx2, ..., dg2/dxN],
                                                 [dg3/dx1, dg3/dx2, ..., dg3/dxN],]
        LoggingLevel    ... Change the logging level from DEBUG to: 'INFO', 'WARNING', 'ERROR', 'CRITICAL'
        """
        logging.info("Initializing Simulation Maker Class")

        if LoggingLevel is not None:
            logging.info('Setting log level to "%s"' % LoggingLevel)
            if LoggingLevel == 'DEBUG':
                logging.Logger.root.level = logging.DEBUG
                logging.Logger.root.handlers[0].level = logging.DEBUG
            elif LoggingLevel == 'INFO':
                logging.Logger.root.level = logging.INFO
                logging.Logger.root.handlers[0].level = logging.INFO
            elif LoggingLevel == 'WARNING':
                logging.Logger.root.level = logging.WARNING
                logging.Logger.root.handlers[0].level = logging.WARNING
            elif LoggingLevel == 'ERROR':
                logging.Logger.root.level = logging.ERROR
                logging.Logger.root.handlers[0].level = logging.ERROR
            elif LoggingLevel == 'CRITICAL':
                logging.Logger.root.level = logging.CRITICAL
                logging.Logger.root.handlers[0].level = logging.CRITICAL

        logging.info("Initializing Simulation Maker Class")
        logging.Logger.root.level = 10

        self.Model = Model

    def __str__(self):
        return 'Test in SimulationMaker'

    def gitCommit(self, branch='studies', commitMessage="eos commit", addFiles = ['.']):
        # create an automated git commit message, if requested
        try:
            from git import Repo
            self.gitRepo = Repo(os.getcwd())
            git = self.gitRepo.git
            if self.gitRepo.active_branch.name == branch:
                logging.info('current git branch is "%s", creating automated commit with message "%s"' % (branch, commitMessage))
                for file in addFiles:
                    logging.info('adding file "%s" to git' % file)
                    # self.gitRepo.index.add(file)
                    git.add(file)
                # self.gitRepo.index.commit(commitMessage)
                git.commit('-m', commitMessage)
                checksum = self.gitRepo.head.commit.hexsha
                logText = 'created git commit "%s" with checksum "%s"' % (commitMessage, checksum)
                logging.info(logText)
                print(logText)
            else:
                logging.warning('current git branch is not "studies" - not creating automatic commit. Please change the branch manually.')
        except:
            print("git commit failed")
            logging.warning("git commit failed")

    def Eval(self, x=[]):
        # check for compute response method, both current (compute_response) and legacy (ComputeResp)) and run it.
        try:
            compute_resp_legacy = getattr(self.Model, 'ComputeResp')
        except AttributeError:
            compute_resp_legacy = ''

        try:
            compute_resp = getattr(self.Model, 'compute_response')
        except AttributeError:
            compute_resp = ''

        if callable(compute_resp):
            return compute_resp(x)
        elif callable(compute_resp_legacy):
            return compute_resp_legacy(x)
        else:
            sys.exit("Model has no method compute_response")

    def SensitivityStudy(self, x):
        if not hasattr(self,'Sens'):
            self.CheckForResponseModelInstances()
        if self.Sens:
            logging.info('Available gradients used')
            return self.Model.ComputeSens(x)
        else:
            try:
                from eos.SimulationTools import FiniteDiff
            except:
                logging.fatal('Finite differences function can not be imported!')
                sys.exit('Finite differences function can not be imported!')
            logging.info('Finite differences used')
            return FiniteDiff(self.Model.ComputeResp, x, Tol=1e-3)

    def CheckForResponseModelInstances(self):
        '''
        Checks if there is a response function in model definition.
        
        :param Model: model class (in model.py file) that should contain methods named:
                        1.) 'ComputeResp(x)'
                        2.) 'ComputeSens(x)'
        :return Sens: Flag for sensivity computation (if false, FDs are used)
        '''
        from eos.OptTools import CheckForResponseModelInstances
        self.Sens = CheckForResponseModelInstances(self.Model)

    def CheckForObjAndConstrFctModelInstances(self):
        '''
        Checks if there is an objective and constraint functions in model definition.
        
        :param Model: model class (in model.py file)  that should contain methods named:
                        1.) 'ObjAndConstrFct(x)'
                        2.) 'ObjAndConstrSensFct(x)'
        :return Sens: Flag for sensivity computation (if false, FDs are used)
        '''
        from eos.OptTools import CheckForObjAndConstrFctModelInstances
        self.Sens = CheckForObjAndConstrFctModelInstances(self.Model)

    def Optimize(self, x0, xL, xU, Alg='SLSQP', Norm=True, Options=dict(), **kwargs):
        """
        Performs an optimization based on PyOpt - [...] is default!
            x0        ... Start design vector
            xL        ... Lower bound
            xU        ... Upper bound
            Options   ... Dictionary with options
                    Options['ng']       ... Number of constraints g [0]
            Alg       ... Algorithm choice: ['SLSQP'], 'NSGA2', 'ALPSO'
            Norm      ... Normalization: [True], False
            **kwargs  ... Can be used to directly pass options to PyOpt algorithms
        Note:
            - All inputs need to be numpy arrays, e.g. x0=np.array([[0],[1]]) or x0=np.array([[0, 1]]).T or x0=np.array([0.,0.])
            - Responses provided by Model.ComputeResp need to look like [f], [g1, g2] 
            - Sensitivities provided by Model.ComputeSens need to look like 
                   df = [df/dx1, df/dx2, ..., df/dxN]
                    g = [[dg1/dx1, dg1/dx2, ..., dg1/dxN],
                         [dg2/dx1, dg2/dx2, ..., dg2/dxN],
                         [dg3/dx1, dg3/dx2, ..., dg3/dxN],]
        """
        # PERFORM VARIOUS CHECKS:
        # ------------------------------------------------------------------- #
        self.CheckForObjAndConstrFctModelInstances()
        self.nDV = int(np.size(xL))         
        
        from eos.OptTools import CheckOptions
        (self.Options, self.VecOptions) = CheckOptions(Options, self.nDV)
        
        logging.info('$$ ---------------------------------------------------------$$')
        logging.info('$$ OPTIONS:                                                 $$')
        logging.info('$$ ---------------------------------------------------------$$')
        for key in self.Options:
            logging.info(' o '+str(key)+': '+str(self.Options[key]))
        if self.VecOptions:
            logging.info('VecOptions detected: perform vector optimization.')
            for key in self.VecOptions:
                logging.info(' o '+str(key)+': '+str(self.VecOptions[key]))
        # Fetch algorithm options
        if kwargs:
            AlgOptions = kwargs
            logging.info('Alg options are fetched')
        else:
            AlgOptions = dict()
        
        # Check types -> need to be two dimensional numpy arrays
        try:
            if (len(x0.shape) == 2, len(x0.shape) == 1) and (len(xL.shape) == 1) and (len(xU.shape) == 1):
                logging.info('All inputs are one dimensional numpy arrays. x0 may be two dimensional')
            else:
                logging.fatal('All inputs need to be 2D numpy arrays, i.e. x0, xL and xU')
                sys.exit('Inputs x0, xl and xu are no 2D numpy arrays')
        except:
            traceback.print_exc()
            logging.fatal('All inputs need to be of type numpy array, i.e. x0, xL and xU')
            sys.exit('Inputs x0, xl and xu need to be of type numpy array')
        
        # Number of optimization loops
        if len(x0.shape) == 1:
            nOptLoops = 1
        else:
            nOptLoops = int(np.shape(x0)[1])
        
        # Create design optimization instance
        from eos.DesOpt import DesOpt
        Options = cp.deepcopy(self.Options)
        self.DesOpt = DesOpt(self.Model, xL, xU, Alg, Norm, Options, AlgOptions, self.Sens)
        
        # PERFORM OPTIMIZATION:
        # ------------------------------------------------------------------- #        
        # Multiobjective optimization 2D
        if self.Options['nf'] == 2:
            from eos.VecOpt import VectorOptimizer
            MultObjProb = VectorOptimizer(self.Model, self.DesOpt, self.Options, self.VecOptions, self.nDV)
            return MultObjProb.Optimize(x0, xL, xU)
            
        # Multiobjective optimization
        if self.Options['nf'] == 3:
            if self.Options['VecOptMode'] == 'hyperplane':
                from eos.VecOpt3DHyper import VectorOptimizer
            elif self.Options['VecOptMode'] == 'equaldistance':
                from eos.VecOpt3DEqual import VectorOptimizer
            else:
                sys.exit('Failed to import VectorOptimizer!')
            MultObjProb = VectorOptimizer(self.Model, self.DesOpt, self.Options, self.nDV)
            return MultObjProb.Optimize(x0, xL, xU)
            
        # Singleobjective optimization
        logging.info('%i optimization run(s) will be conducted' % (nOptLoops))
        
        if nOptLoops > 1:
            # Multiple optimizations
            Results = []
            for ix0Ind in range(nOptLoops):
                self.DesOpt.OptLabel = '%s-%iof%i' % (self.Model.__class__.__name__, ix0Ind + 1, nOptLoops)
                Results.append(self.DesOpt(x0[:, ix0Ind]))
                logging.info('Optimization %i of %i done' % (ix0Ind + 1, nOptLoops))
                GSResultFolder = 'GlobalOpt-%i' % (ix0Ind + 1)
                if os.path.exists(GSResultFolder):
                    shutil.rmtree(GSResultFolder)
                    logging.warning('Old GSO result folder "%s" deleted' % (GSResultFolder))
                os.mkdir(GSResultFolder)
                for iEnding in ["*.bin", "*.cue", "*.out", "*.png"]:
                    files = glob.iglob(os.path.join(os.getcwd(), iEnding))
                    for ifile in files:
                        if os.path.isfile(ifile):
                            shutil.move(ifile, os.path.join(os.getcwd(), GSResultFolder))
            return Results, self.DesOpt.GSOPostProcessing(Results)
        else:
            if len(x0.shape) == 2:
                return self.DesOpt(x0[:, 0])
            else:
                return self.DesOpt(x0)

    def GlobalSearchForDesOpt(self, xL, xU, nh=0, nDoE=30, nGS=3, ReplaceDVDict=None):
        '''
        Perform DoE for global search option in DesOpt
        
        :param nDoE: Number of test samples
        :param nGS: Number of global search points
        :param ReplaceDVDict: Dictionary: ['xInd'] - Indicies of x to be replaced by values from eos.['Vals']

        :return xGS: Returns an array which can be piped into DesOpt
        :return gInfo: Info whether feasible or not
        '''
        from eos.OptTools import GlobalSearchForDesOpt as GSOFct # Import from opt tools
        return GSOFct(xL, xU, self.Model.ObjAndConstrFct, nh=nh, nDoE=nDoE, nGS=nGS, hTol=0.1, gTol=1.e-6, ReplaceDVDict=ReplaceDVDict)

    def PerformDoE(self, NumberSamplePoints, RSAlower, RSAupper, SamplingMethod='LHS', **kwargs):
        '''
        :param NumberSamplePoints: How many sample points for DoE
        :param RSAlower: Lower bound for sample points
        :param RSAupper: Upper bound for sample points
        :param SamplingMethod: Which sampling method? -> ['LHS'], [LHS+Corner], ['FullFactorial']?
        :param kwargs: Can be used to pipe options to DoE method
        :return: (design): Returns an array of sampling points

        Note: for 'FullFactorial' sampling with n>1 dimensions, the desired number of sample points
            is approximated if the n-th root of NumberSamplePoints is not an integer.
        '''
        self.CheckForResponseModelInstances()
        from eos.OptTools import PerformDoE # Import from opt tools
        return PerformDoE(NumberSamplePoints, RSAlower, RSAupper, SamplingMethod=SamplingMethod, **kwargs)

    def GetfDoE(self, xDoE):
        '''
        Computes responses f for given sampling points
        :param xDoE: Array of sampling points -> nDesign=np.size(xDoE,0) & nx=np.size(xDoE,1)
        '''
        fDoE = list()
        try:
            for x in xDoE:
                fDoE.append(self.Model.ComputeResp(x))
        except:
            return 'fDoE computation failed'

        fDoE = np.asarray(fDoE)
        if len(np.shape(fDoE)) == 1:
            fDoE = np.array([fDoE]).T
        return fDoE

    def GetRSAmodel(self, xDoE, fDoE=None, RSAMethod='PolyReg',CheckForResponseModelInstances=True, **kwargs):
        '''
        :param xDoE: Array of sampling points -> nDesign=np.size(xDoE,0) & nx=np.size(xDoE,1)
        :param fDoE: Needs to be a TWO-Dimensional numpy array
        :param RSAMethod: Which RSA method? -> ['PolyReg'], 'Kriging'
        :param kwargs: Can be used to pipe options to RSAMethod
        :return: RSAmodel: Returns an RSA object
        '''
        if CheckForResponseModelInstances==True:
            self.CheckForResponseModelInstances()
        from eos.OptTools import GetRSAmodel as RSAFct # Import from opt tools
        return RSAFct(xDoE, self.Model, fDoE=fDoE, RSAMethod=RSAMethod, **kwargs)
        
    def PlotDesignSpace(self,xOpt, DVIndicies, xU, xL, NumSup = 100., NumConstr = 20):
        from eos.OptTools import PlotDesignSpace as PlotDS # Import from opt tools
        PlotDS(xOpt, self.Model.ObjAndConstrFct, DVIndicies, xU, xL, NumSup = NumSup, NumConstr = NumConstr)
        
    def LinePlot(self, x, y, xLabel='x', yLabel='y', fLegend=None, FigTitle=None, ShowFig=False, \
                 FigFileName='Figure.png', Plotstyle='b-', MatPlotLibObj=None, LegenLocation='upper left'):
        '''
        Plots lines
        
        :param MatPlotLibObj: User can pass objects needed by MatPlotLib, e.g. MatPlotLibObj = [fig, axes]
        '''
        try:
            import matplotlib.pyplot as plt
        except:
            logging.fatal('Could not import matplotlib modules!')
            sys.exit('Failed to import matlotlib package!')
        if MatPlotLibObj:
            [fig, axes] = MatPlotLibObj
        else:
            fig, axes = plt.subplots()

        if fLegend == None:
            axes.plot(x, y, Plotstyle)
        else:
            axes.plot(x, y, Plotstyle, label=fLegend)
        axes.set_xlabel(xLabel)
        axes.set_ylabel(yLabel)
        if FigTitle:
            axes.set_title(FigTitle)
        if FigFileName:
            fig.savefig(FigFileName, dpi=300)
        if fLegend:
            axes.legend(loc=LegenLocation)
        if ShowFig:
            fig.show()
        return fig, axes

    def SurfacePlot(self, X, Y, z, xLabel='x', yLabel='y', zLabel='z', FigTitle=None, ShowFig=False, \
                    FigFileName='Figure.png', SubFigure=None):
        '''
        Plots surfaces
        
        :param X: x coord -> X, Y = np.meshgrid(np.linspace(0.,1.,num=5), np.linspace(0.,1.,num=5))
        :param Y: y coord -> X, Y = np.meshgrid(np.linspace(0.,1.,num=5), np.linspace(0.,1.,num=5))
        '''
        try:
            import matplotlib.pyplot as plt
            from matplotlib import cm
            from mpl_toolkits.mplot3d import proj3d
            from mpl_toolkits.mplot3d import Axes3D
        except:
            logging.fatal('Could not import matplotlib modules!')
            sys.exit('Failed to import matlotlib package!')
        # Orthogonal projection for perpendicular axis
        def orthogonal_proj(zfront, zback):
            a = (zfront + zback) / (zfront - zback)
            b = -2 * (zfront * zback) / (zfront - zback)
            return np.array([[1, 0, 0, 0],
                             [0, 1, 0, 0],
                             [0, 0, a, b],
                             [0, 0, -0.0001, zback]])

        # Standard settings
        LabelSize = [16, 18]  # Minor and major label size
        FontSize = [18, 22]  # Minor and major font size
        # Create suface plot
        if SubFigure:
            if SubFigure[2] == 1:
                fig = plt.figure()
                ax = fig.add_subplot(SubFigure[0], SubFigure[1], SubFigure[2], projection='3d')
            else:
                fig = SubFigure[3]
                ax = SubFigure[4]
                ax = fig.add_subplot(SubFigure[0], SubFigure[1], SubFigure[2], projection='3d')
        else:
            fig = plt.figure()
            ax = fig.gca(projection='3d')
        if FigTitle:
            ax.set_title(FigTitle, fontsize=FontSize[1], fontweight='bold')
        ax.tick_params(axis='both', which='major', labelsize=LabelSize[1])
        ax.tick_params(axis='both', which='minor', labelsize=LabelSize[0])
        surf = ax.plot_surface(X, Y, z, alpha=1.0, rstride=1, cstride=1, cmap=cm.coolwarm,
                               linewidth=0, antialiased=False)
        proj3d.persp_transformation = orthogonal_proj
        cbar = fig.colorbar(surf, shrink=0.5, aspect=5)
        cbar.ax.tick_params(labelsize=LabelSize[1])
        if xLabel:
            ax.set_xlabel(xLabel, fontsize=FontSize[0])
        if yLabel:
            ax.set_ylabel(yLabel, fontsize=FontSize[0])
        if zLabel:
            ax.set_zlabel(zLabel, fontsize=FontSize[0])
        if FigFileName:
            fig.savefig(FigFileName, dpi=300)
        if ShowFig:
            fig.show()
        if SubFigure:
            return fig, ax, plt

    def PlotShellMeshResult(self, NodeArray, ElementArray, Results=None, Camera=dict(), FigName='ShellMeshPlot.png'):
        '''
        Plots shell mesh via "MeshPlotter.py" class, which can be imported from eos.everywhere as well!
        
        :param NodeArray: Array of nodes with NodeID | x | y | z
        :param ElementArray: Array of elements with ElID | N1ID | N2ID etc.
        :param Results: Array of element results with ElID | Result
        :param Camera: Dict of camera positions, e.g. 'Position', 'FocalPoint', 'ViewAngle', 'ViewUP', 'ClipRange'
        :return: None
        '''
        from eos.MeshPlotter import MeshPlotter

        PlotterObj = MeshPlotter(white=True)
        if Results == None:
            PlotterObj.PassElementsNodesResults(ElementArray, NodeArray)
        else:
            PlotterObj.PassElementsNodesResults(ElementArray, NodeArray, Results=Results)
        PlotterObj.Plot(Camera=Camera, FigName=FigName, ShowFig=False)

    def RSAplotting(self, RSAobj, xRSA, xRSAIndicies, RSALower, RSAUpper, xLabels=None, RespLabel=None,
                    xDoE=None, fDoE=None, PointsTol=0.1, NumSupportPoints=50, FigFileName='Figure.png',):
        '''
        Plots the surface of the RSA        
        
        :param RSAobj: RSA object of ONE response, i.e. RSAobj.predict(x)!
        :param xRSA: xRSA is a list of design point, where the RSA shall be created
        :param xRSAIndicies: Indicies of the xRSA list over which the RSA shall be plotted
        :param RSALower: Lower indicies of the RSA plot
        :param RSAUpper: Upper indicies of the RSA plot
        :param xLabels: List of design labels for 2d and string for 1d plot
        :param RespLabel: Label of the response, e.g. "Mass"
        :param xDoE: Support points for which the RSA has been computed, thus DoE
        :param fDoE: Responses of the DoE at xDoE
        :param PointsTol: Tolerance for the DoE sample points plot!
        :param NumSupportPoints: Number of support points
        :param FigFileName: Name of file as which the figure will be stored
        :param tolerance_factor: for 1D plot: factor to take into account additional points for plotting
        :return: None: No return
        '''
        import matplotlib.pyplot as plt

        if len(xRSAIndicies) == 1:  # 1D Plot - All Variables fixed except one

            import pylab

            numsteps = 100.
            xL = RSALower[xRSAIndicies[0]]
            xU = RSAUpper[xRSAIndicies[0]]
            stepsize = (xU-xL)/numsteps
            x_var = pylab.arange(xL, xU, stepsize)  # Variable from RSALower to RSAUpper with stepsize
            y1 = []
            for i in x_var:  # Calculates y-Points (=fDoE-Points) with RSA for given x
                xRSA[xRSAIndicies[0]] = i
                y1.append(RSAobj.predict(xRSA))

                # Show fDoE of original Modell if they within a certain tolerance
            collect = []
            for i in xrange(0, np.shape(xDoE)[1]):
                if i != xRSAIndicies[0]:
                    tolerance = abs(RSAUpper[i] - RSALower[i]) * PointsTol
                    collect.append(set((np.where((xDoE.T[i] >= xRSA[i] - tolerance) & (xDoE.T[i] <= xRSA[i] + tolerance))[0]).flat))  # find the fDoEs that are close enough

            result = set.intersection(*collect)  # Make sure all the now fixed Variables are in the tolerance range
            result_array = np.array(list(result))

            # Start plotting
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.set_axis_bgcolor('white')
            ax.xaxis.set_ticks_position('none')
            ax.yaxis.set_ticks_position('none')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.set_xlabel(xLabels[0])
            ax.set_ylabel(xLabels[1])
            plt.title(RespLabel)

            ax.plot(x_var, y1)  # PLot line
            for i in result_array:
                ax.plot(xDoE.T[xRSAIndicies[0]][i], fDoE[i], 'ro')  # Plot original Model fDoEs if available

            fig.show()
            fig.savefig(FigFileName, dpi=300)

        # RSA surface plots
        if len(xRSAIndicies) == 2:
            # 1. Important imports
            # ---------------------------------------------------------------------#
            import matplotlib.pyplot as plt
            from mpl_toolkits.mplot3d import Axes3D
            from mpl_toolkits.mplot3d import proj3d

            def orthogonal_proj(zfront, zback):
                a = (zfront + zback) / (zfront - zback)
                b = -2 * (zfront * zback) / (zfront - zback)
                return np.array([[1, 0, 0, 0],
                                 [0, 1, 0, 0],
                                 [0, 0, a, b],
                                 [0, 0, -0.0001, zback]])

            # 2. Compute support points of the RSA surface
            #---------------------------------------------------------------------#
            xi = np.linspace(RSALower[xRSAIndicies[0]], RSAUpper[xRSAIndicies[0]], round(np.sqrt(NumSupportPoints)))
            yi = np.linspace(RSALower[xRSAIndicies[1]], RSAUpper[xRSAIndicies[1]], round(np.sqrt(NumSupportPoints)))
            XI, YI = np.meshgrid(xi, yi)
            RSAEval = np.zeros((np.size(xi), np.size(yi)))
            DesignPoint = xRSA
            for j in range(np.size(yi)):
                for i in range(np.size(xi)):
                    DesignPoint[xRSAIndicies[0]] = xi[i]
                    DesignPoint[xRSAIndicies[1]] = yi[j]
                    RSAEval[j, i] = RSAobj.predict(DesignPoint)

            # 3. Calculate RSAmodel Error compared to original Model          
            #---------------------------------------------------------------------#        
            fDoE_RSA = RSAobj.predict(xDoE)
            f_error = fDoE_RSA - fDoE

            # 4. Plot results
            #---------------------------------------------------------------------#
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')

            if(PointsTol != 0.0):
                if xDoE != None:
                    if fDoE != None:
                        DesignPoint = xRSA
                        NearxDoE = list()
                        FarxDoE = list()
                        NearfDoE = list()
                        FarfDoE = list()
                        for (ifDoE, ixDoE) in zip(fDoE, xDoE):
                            DesignPoint[xRSAIndicies[0]] = ixDoE[xRSAIndicies[0]]
                            DesignPoint[xRSAIndicies[1]] = ixDoE[xRSAIndicies[1]]
                            if np.linalg.norm(DesignPoint - ixDoE) < PointsTol:
                                NearxDoE.append(ixDoE)
                                NearfDoE.append(ifDoE)
                            else:
                                FarxDoE.append(ixDoE)
                                FarfDoE.append(ifDoE)
                        if len(NearxDoE) > 0:
                            NearxDoE = np.asarray(NearxDoE)
                            NearfDoE = np.asarray(NearfDoE)
                            ax.scatter(NearxDoE[:, xRSAIndicies[0]], NearxDoE[:, xRSAIndicies[1]], NearfDoE, color='b')
                        if len(FarxDoE) > 0:
                            FarxDoE = np.asarray(FarxDoE)
                            FarfDoE = np.asarray(FarfDoE)
                            ax.scatter(FarxDoE[:, xRSAIndicies[0]], FarxDoE[:, xRSAIndicies[1]], FarfDoE, color='c')
                    else:
                        logging.info('No DoE sample points available -> not given with "fDoE"')
                else:
                    logging.info('No DoE sample points available -> not given with "xDoE"')


            ax.plot_wireframe(XI, YI, RSAEval, color='r')
            proj3d.persp_transformation = orthogonal_proj

            if RespLabel:
                plt.title('RSA of response ' + RespLabel)
            else:
                plt.title('RSA')
            if xLabels:
                ax.set_xlabel(xLabels[0])
                ax.set_ylabel(xLabels[1])
            if RespLabel:
                ax.set_zlabel(RespLabel)

            ax.set_autoscale_on(False)
            ax.autoscale_view(False, False, False)
            ax.grid(False)
            ax.patch.set_facecolor('white')

            if(PointsTol != 0.0):
                for i in np.arange(0, len(xDoE)):  ## Add error bar
                    ax.plot([xDoE[i][xRSAIndicies[0]], xDoE[i][xRSAIndicies[0]]],
                            [xDoE[i][xRSAIndicies[1]], xDoE[i][xRSAIndicies[1]]], [fDoE[i] + f_error[i], fDoE[i]],
                            color='black', marker="_")

            ax.set_axis_bgcolor('white')
            fig.show()
            fig.savefig(FigFileName, dpi=300)
            
        if len(xRSAIndicies) == 3:
                
            try:
                from mpl_toolkits.mplot3d import Axes3D
                import matplotlib.pyplot as plt
                from mpl_toolkits.mplot3d import proj3d
            except:
                logging.fatal('Could not import matplotlib modules!')
                sys.exit('Failed to import matlotlib package!')
 
            def orthogonal_proj(zfront, zback):
                a = (zfront + zback) / (zfront - zback)
                b = -2 * (zfront * zback) / (zfront - zback)
                return np.array([[1, 0, 0, 0],
                                 [0, 1, 0, 0],
                                 [0, 0, a, b],
                                 [0, 0, -0.0001, zback]])
 
            xi = np.linspace(RSALower[xRSAIndicies[0]], RSAUpper[xRSAIndicies[0]], round((NumSupportPoints)**(1./3.)))
            yi = np.linspace(RSALower[xRSAIndicies[1]], RSAUpper[xRSAIndicies[1]], round((NumSupportPoints)**(1./3.))+4)            
            zi = np.linspace(RSALower[xRSAIndicies[2]], RSAUpper[xRSAIndicies[2]], round((NumSupportPoints)**(1./3.))+4)
            x = list()
            y = list()
            z = list()            
            RSAEval = list()
            DesignPoint = xRSA
            for i in range(np.size(xi)):
                for j in range(np.size(yi)):
                    for k in range(np.size(zi)):                        
                        DesignPoint[xRSAIndicies[0]] = xi[i]
                        DesignPoint[xRSAIndicies[1]] = yi[j]
                        DesignPoint[xRSAIndicies[2]] = zi[k]
                        RSAEval.append(RSAobj.predict(DesignPoint))            
                        x.append(xi[i])
                        y.append(yi[j])
                        z.append(zi[k])
                    
            RSAEval = np.asarray(RSAEval)
            x = np.asarray(x)
            y = np.asarray(y)    
            z = np.asarray(z)
            maxRSAEval = max(RSAEval)
            minRSAEval = min(RSAEval)
            s = np.zeros((len(RSAEval)))
            for (index, RSAVal) in enumerate(RSAEval.tolist()):
                s[index] = 20*4**((RSAVal-minRSAEval)/(maxRSAEval-minRSAEval))
            FontSize = 16
            fig = plt.figure(facecolor='w')
            ax = fig.add_subplot(111, projection='3d', axisbg = 'white')
            ax.w_xaxis.gridlines.set_alpha(0.3)
            ax.w_yaxis.gridlines.set_alpha(0.3)
            ax.w_zaxis.gridlines.set_alpha(0.3)
            
            cmYB = plt.cm.get_cmap("YlOrBr")
            
            l = ax.scatter(x, y, z, s = s, c=RSAEval, cmap=cmYB)
            ax.w_xaxis.set_pane_color((1.0,1.0,1.0,1.0))
            ax.w_yaxis.set_pane_color((1.0,1.0,1.0,1.0))
            ax.w_zaxis.set_pane_color((1.0,1.0,1.0,1.0))
            proj3d.persp_transformation = orthogonal_proj
            cb = fig.colorbar(l) 
    
            if RespLabel:
                plt.title(RespLabel)
            else:
                plt.title('RSA')
            if xLabels[0]:
                ax.set_xlabel(xLabels[0], fontsize = FontSize)
            if xLabels[1]:            
                ax.set_ylabel(xLabels[1], fontsize = FontSize)
            if xLabels[2]:            
                ax.set_zlabel(xLabels[2], fontsize = FontSize)
            plt.show()
            fig.savefig(FigFileName, dpi=300)            


class EosStudy:

    def __init__(self, Model=False):
        """
        This class is used to define an EOS Study.

        To use it, import the Class:
        from eos import EosStudy

        Then, define your Study classes as a derived Class:
        class SimpleEval(EosStudy):

        """
        self.start_time = datetime.now()
        self.studyResults = list()
        self.studyParamsList = list()
        self.studyParams = list()
        self.studyName = self.__class__.__name__
        self.projectName = os.path.split(os.getcwd())[-1]
        if Model:
            self.Simulation = SimulationMaker(Model)
            self.modelName = Model.__class__.__name__
        else:
            self.modelName = 'none'

        # read eos configuration file
        self.config = configparser.ConfigParser()
        EosPath = os.path.dirname(os.path.dirname(__file__))
        config_path = os.path.join(EosPath, 'eos.cfg')
        if os.path.isfile(config_path):
            logging.info('Reading config file "%s"' % config_path)
            self.config.read(config_path)
        else:
            msg = 'EOS config file "%s" could not be found. Please copy "eos.cfg.sample" and modify as needed' % config_path
            logging.fatal(msg)
            sys.exit(msg)
        self.orgName = self.config.get('General', 'org_name')
        logging.info('Initializing EOS study "%s" with model "%s"' % (self.studyName, self.modelName))

        return

    def NotifyEmail(self, email, sender='services@llb.mw.tum.de', runname=''):
        """
        Send a notification email at the end of a job
        :param email:  (string) The email address of the recipient
        :param sender: (string, optional, default = 'services@llb.mw.tum.de') The sender address
        :param runname:(string, optional, default='') A description of this specific run that helps identifying it.
        """
        import smtplib
        from email.mime.text import MIMEText
        from email.parser import Parser
        import socket

        try:
            model_name = self.Simulation.Model.ModelName
        except:
            model_name = '-unknown-'

        study_name = self.__class__.__name__
        hostname = socket.gethostname()
        self.finish_time = datetime.now()
        self.delta_time = self.finish_time - self.start_time

        try:
            jobid = os.environ['SLURM_JOB_ID']
            jobid_text = 'Job ID:          ' + jobid + '\n'
        except:
            jobid_text = ''

        if runname:
            runname_text = 'Run Name:        ' + runname + '\n'
        else:
            runname_text = ''

        text =  'Start Time:      ' + self.start_time.strftime("%Y-%m-%d_%H-%M-%S") + '\n' \
                'End Time:        ' + self.finish_time.strftime("%Y-%m-%d_%H-%M-%S") + '\n' \
                'Duration:        ' + str(self.delta_time).split('.', 2)[0] + '\n' \
                'Execution Host:  ' + hostname + '\n' \
                + jobid_text + \
                'Study:           ' + study_name + '\n' \
                'Modelname:       ' + model_name + '\n' \
                + runname_text


        sender = 'services@llb.mw.tum.de'

        msg = MIMEText(text)
        msg['Subject'] = 'EOS simulation ' + study_name + ' ( ' + model_name + ' ) complete\n'
        # msg['From'] = sender
        # msg['To'] = email

        s = smtplib.SMTP('mailout.lrz.de')
        try:
            s.sendmail(sender, [email], msg.as_string())
        except:
            logging.warning('Notification email to %s could not be sent' % email)
        s.quit()

    def AddStudyStep(self, results={}):
        studyStepParams = dict()

        # make sure that self.studyParamsList also contains all entries from designVariables
        if len(self.Simulation.Model.designVariables) > 0:
            # combine designVariables and studyParamsList and take only unique values via set() command
            self.studyParamsList = list(set(self.Simulation.Model.designVariables + self.studyParamsList))

        for param in self.studyParamsList:
            studyStepParams[param] = self.Simulation.Model.params[param]
        self.studyParams.append(cp.deepcopy(studyStepParams))

        if not isinstance(results, dict):
            logging.warning('study step results for AddStudyStep() must be given as a dict()!')
            print('study step results for AddStudyStep() must be given as a dict()!')
        self.studyResults.append(cp.deepcopy(results))

    def WriteIterationOutput(self, filename='', filePath=False):
        """
        Write out the last iteration of a study to a text file
        By default, a new file is created for each iteration. If a fixed filename is given, the results are appended to the existing file 
        """
        if not filename:
            if isinstance(self.Simulation.Model.params['iteration'], Parameter):
                filename = 'eos_iteration_results_%i.txt' % self.Simulation.Model.params['iteration'].value
            else:
                filename = 'eos_iteration_results_%i.txt' % self.Simulation.Model.params['iteration']

        # if a path was requested, use it, otherwise use current working dir
        if not filePath:
            filePath = os.getcwd()

        resultFilePath = os.path.join(filePath, filename)

        # check if the file already exists:
        if os.path.isfile(resultFilePath):
            newFile = False
        else:
            newFile = True

        with open(resultFilePath, 'a') as file:
            logging.info('writing study result file "%s"' % resultFilePath)
            if newFile:
                file.write('---------------------------------------------------------------------------------------\n')
                file.write('EOS Study "%s" in Project "%s" using Model "%s"\n' % (self.studyName, self.projectName, self.modelName))
                file.write(self.orgName + '\n')
                file.write('---------------------------------------------------------------------------------------\n')

            paramsHeadingText = 'Study params:\n'
            file.write(paramsHeadingText)
            pprint.pprint(self.studyParams[-1], file)
            file.write('Study results:\n')
            pprint.pprint(self.studyResults[-1], file)

    def WriteStudyOutput(self, filename=False, filePath=False, writeTxt=True, writeDocx=True, significantDigits=False, writeXlsx=True):

        logging.debug('Study result output requested')

        if not filename:
            timeString = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            filename = 'eos_report_%s_%s' % (self.Simulation.Model.ModelName, timeString)

        # if a path was requested, use it, otherwise use current working dir
        if not filePath:
            filePath = os.getcwd()

        # write parameters and results to the console and the log file
        sorted_params = list()
        for paramSet in self.studyParams:
            if len(paramSet) > 1:
                sorted_params.append(collections.OrderedDict(paramSet))
            else:
                sorted_params.append(cp.deepcopy(paramSet))

        sorted_results = list()
        for resultSet in self.studyResults:
            if len(resultSet) > 1:
                sorted_results.append(collections.OrderedDict(resultSet))
            else:
                sorted_results.append(cp.deepcopy(resultSet))

        # if an additional parameter Text was given, also add an additional space
        counter = 0
        for resultSet in sorted_results:
            printText = 'Params: ' + str(sorted_params[counter]) + ' - Results: ' + str(sorted_results[counter])
            # write to console
            print(printText)
            # write to log (in single line)
            logging.info('Study step complete:' + printText)
            counter += 1

        if writeTxt:
            counter = 0
            txtFileName = '%s.txt' % filename
            txtFilePath = os.path.join(filePath, txtFileName)
            with open(txtFilePath, 'w') as f:
                f.write('EOS Study %s in Project %s using Model %s\n' % (self.studyName, self.projectName, self.modelName))
                f.write(self.orgName + '\n\n')
                f.write('----- Study Results -----\n')
                for resultSet in sorted_results:
                    printText = 'Params: ' + str(sorted_params[counter]) + ' - Results: ' + str(sorted_results[counter]) + '\n'
                    f.write(str(printText))
                    counter += 1

                # Write Result and Parameter Descriptions
                f.write('\n\n----- List of Parameters and Results -----\n')

                parameterList = list()
                for item in self.studyParams[0].items():
                    parameterList.append(item)
                resultList = list()
                for item in self.studyResults[0].items():
                    resultList.append(item)
                parameterSet = sorted(parameterList) + sorted(resultList)

                for key, item in parameterSet:  # we re-use the list built above
                    if isinstance(item, Parameter):
                        if item.title:
                            paramText = item.title
                        else:
                            paramText = key
                        if item.unit:
                            paramText += ' [%s]' % item.unit
                    else:
                        paramText = key

                    if isinstance(item, Parameter):
                        paramText += ':   %s' % item.description

                    f.write(paramText + '\n')

                # Write last Full Parameter Set
                f.write('\n\n----- Last full parameter set -----\n')
                for key, item in sorted(self.Simulation.Model.params.items()):
                    if isinstance(item, Parameter):
                        if item.title:
                            paramText = item.title
                        else:
                            paramText = key

                        paramText += ': ' + str(item.value)

                        if item.unit:
                            paramText += ' ' + str(item.unit)

                        if item.description:
                            paramText += '   (%s)' % item.description
                    else:
                        paramText = str(key) + ': ' + str(item)
                    f.write(paramText + '\n')

                # Write some git information
                gitInfos = self.getGitInfos()
                if gitInfos:
                    f.write('\n\n----- Git Information -----\n')
                    for key, value in gitInfos.items():
                        f.write('%s:   %s\n' % (str(key), str(value)))

        if writeDocx:
            try:
                from docx import Document
            except:
                logging.warning('Python module "python-docx" not found, skipping word file report')
                return

            document = Document()
            document.add_heading('EOS Study Results', 0)
            document.add_paragraph(self.orgName)
            document.add_paragraph('EOS Study %s in Project %s using Model %s\n' % (self.studyName, self.projectName, self.modelName))

            # count the required number of columns: params + results
            numColumns = len(self.studyParamsList) + len(self.studyResults[0])

            document.add_heading('Parameter Study Results', 1)
            table = document.add_table(rows=1, cols=numColumns)


            # write Word Title Line
            parameterList = list()
            for item in self.studyParams[0].items():
                parameterList.append(item)

            resultList = list()
            for item in self.studyResults[0].items():
                resultList.append(item)

            col = 0
            parameterSet = sorted(parameterList) + sorted(resultList)
            for key, parameter in parameterSet:
                # use the parameter Title if available, otherwise use its name directly
                if isinstance(parameter, Parameter):
                    if parameter.title:
                        printText = parameter.title
                    else:
                        printText = key
                    if parameter.unit:
                        printText += ' [%s]' % parameter.unit
                else:
                    printText = key

                hdr_cells = table.rows[0].cells
                hdr_cells[col].text = printText
                col += 1

            # write Word Result Lines
            resultCounter = 0
            for resultSet in self.studyResults:

                parameterList = list()
                for item in self.studyParams[resultCounter].items():
                    parameterList.append(item)
                resultList = list()
                for item in self.studyResults[resultCounter].items():
                    resultList.append(item)
                parameterSet = sorted(parameterList) + sorted(resultList)

                row_cells = table.add_row().cells
                col = 0
                for key, parameter in parameterSet:
                    # use the parameter Title if available, otherwise use its name directly
                    if isinstance(parameter, Parameter):
                        if isinstance(parameter.value, float) and significantDigits:
                            printingResult = round_significant(parameter.value, significantDigits)
                        else:
                            printingResult = parameter.value
                    else:
                        if isinstance(parameter, float) and significantDigits:
                            printingResult = round_significant(parameter, significantDigits)
                        else:
                            printingResult = parameter

                    row_cells[col].text = str(printingResult)
                    col += 1
                resultCounter += 1

            # Write last Full Parameter Set
            document.add_heading('Last full parameter set', 1)
            table = document.add_table(rows=1, cols=5)
            hdr_cells = table.rows[0].cells
            hdr_cells[0].text = 'Name'
            hdr_cells[1].text = 'Title'
            hdr_cells[2].text = 'Unit'
            hdr_cells[3].text = 'Value'
            hdr_cells[4].text = 'Description'
            for key, item in sorted(self.Simulation.Model.params.items()):
                row_cells = table.add_row().cells
                row_cells[0].text = str(key)
                if isinstance(item, Parameter):
                    row_cells[1].text = str(item.title)
                    row_cells[2].text = str(item.unit)
                    row_cells[3].text = str(item.value)
                    row_cells[4].text = str(item.description)
                else:
                    row_cells[3].text = str(item)

            docxFileName = '%s.docx' % filename
            docxFilePath = os.path.join(filePath, docxFileName)
            logging.info('saving docx report "%s"' % docxFilePath)
            document.save(docxFilePath)

        if writeXlsx:
            try:
                import xlsxwriter
            except:
                logging.warning('Python module "xlsxwriter" not found, skipping Excel file report')
                return

            xlsxFileName = '%s.xlsx' % filename
            xlsxFilePath = os.path.join(filePath, xlsxFileName)
            logging.info('Creating Excel report "%s"' % xlsxFilePath)

            workbook = xlsxwriter.Workbook(xlsxFilePath)
            worksheet = workbook.add_worksheet(name='Study Results')
            bold = workbook.add_format({'bold': True})

            row = 0
            col = 0
            worksheet.write(row, col, 'EOS Study %s in Project %s using Model %s - %s' % (self.studyName, self.projectName, self.modelName, self.orgName), bold)
            row += 2

            # Write Excel Heading Line
            parameterList = list()
            for item in self.studyParams[0].items():
                parameterList.append(item)
            resultList = list()
            for item in self.studyResults[0].items():
                resultList.append(item)
            parameterSet = sorted(parameterList) + sorted(resultList)

            for key, parameter in parameterSet:
                # use the parameter Title if available, otherwise use its name directly
                if isinstance(parameter, Parameter):
                    if parameter.title:
                        printText = parameter.title
                    else:
                        printText = key
                    if parameter.unit:
                        printText += ' [%s]' % parameter.unit
                else:
                    printText = key
                worksheet.write(row, col, printText, bold)
                worksheet.set_column(col, col, len(printText))  # adjust cell width
                col += 1

            # Freeze Panes after the Heading and after the parameters for easier scrolling
            worksheet.freeze_panes(row+1, len(self.studyParamsList))

            # Write Excel Result Lines
            resultCounter = 0
            for resultSet in self.studyResults:
                row += 1
                col = 0

                parameterList = list()
                for item in self.studyParams[resultCounter].items():
                    parameterList.append(item)
                resultList = list()
                for item in self.studyResults[resultCounter].items():
                    resultList.append(item)
                parameterSet = sorted(parameterList) + sorted(resultList)

                for key, value in parameterSet:
                    # use the parameter Title if available, otherwise use its name directly
                    if isinstance(value, Parameter):
                        if isinstance(value.value, float) and significantDigits:
                            printingResult = round_significant(value.value, significantDigits)
                        else:
                            printingResult = value.value
                    else:
                        if isinstance(value, float) and significantDigits:
                            printingResult = round_significant(value, significantDigits)
                        else:
                            printingResult = value

                    worksheet.write(row, col, printingResult)
                    col += 1

                resultCounter += 1

            # Write Explanation of Result Parameters
            row += 2

            # List of result parameters with explanation
            worksheet.write(row, 0, 'List of Parameters and Results', bold)
            row += 1
            worksheet.write(row, 0, 'Parameter', bold)
            worksheet.write(row, 2, 'Description', bold)

            for key, item in parameterSet:  # we re-use the list built above
                row += 1
                if isinstance(item, Parameter):
                    if item.title:
                        paramText = item.title
                    else:
                        paramText = key
                    if item.unit:
                        paramText += ' [%s]' % item.unit
                else:
                    paramText = key
                worksheet.write(row, 0, paramText)

                if isinstance(item, Parameter):
                    worksheet.write(row, 2, item.description)

            # Write some git information
            gitInfos = self.getGitInfos()
            if gitInfos:
                row += 2
                worksheet.write(row, 0, 'Git Information', bold)
                row += 1
                for key, value in gitInfos.items():
                    worksheet.write(row, 0, key)
                    worksheet.write(row, 2, value)
                    row += 1

            # Write second work sheet with all Parameters of the last set
            worksheet2 = workbook.add_worksheet(name='Last Full Parameters')
            row = 0
            col = 0
            worksheet2.write(row, col, 'Last full parameter set', bold)
            worksheet2.set_column(col, col, 30)
            row += 2
            col = 0
            worksheet2.write(row, col, 'Name', bold)
            worksheet2.set_column(col, col, 30)
            col += 1
            worksheet2.write(row, col, 'Title', bold)
            worksheet2.set_column(col, col, 15)
            col += 1
            worksheet2.write(row, col, 'Unit', bold)
            worksheet2.set_column(col, col, 8)
            col += 1
            worksheet2.write(row, col, 'Value', bold)
            worksheet2.set_column(col, col, 17)
            col += 1
            worksheet2.write(row, col, 'Description', bold)
            worksheet2.set_column(col, col, 120)

            for key, item in sorted(self.Simulation.Model.params.items()):
                row += 1
                worksheet2.write(row, 0, str(key))
                if isinstance(item, Parameter):
                    worksheet2.write(row, 1, item.title)
                    worksheet2.write(row, 2, item.unit)
                    worksheet2.write(row, 3, item.value)
                    worksheet2.write(row, 4, item.description)
                else:
                    worksheet2.write(row, 3, item)

            workbook.close()

    def getGitInfos(self):
        try:
            from git import Repo
            # trying to import "gitpython"
            gitRepo = Repo(os.getcwd())
            gitInfos = dict()
            gitInfos['branch'] = gitRepo.active_branch.name
            gitInfos['lastCommit'] = gitRepo.head.commit.hexsha
            gitInfos['lastCommitMessage'] = gitRepo.head.commit.message.replace('\n', '').replace('\r', '')  # remove linebreaks
            gitInfos['lastCommitAuthor'] = gitRepo.head.commit.committer.name
            gitInfos['lastCommitEmail'] = gitRepo.head.commit.committer.email
            gitInfos['lastCommitDate'] = str(gitRepo.head.commit.committed_datetime)
            gitInfos['repoPathLocal'] = gitRepo.working_dir
            gitInfos['repoPathOrigin'] = gitRepo.remotes.origin.url
            return gitInfos

        except:
            msg = 'method getGitInfos() failed'
            print(msg)
            logging.warning(msg)
            return False


class EosModel:
    def __init__(self):
        from eos import Parameter as Par
        # derive model name from folder name
        # ModelName = os.path.split(os.path.dirname(os.path.realpath(__file__)))[-1]
        # ModelName = self.__class__.__name__
        self.ModelName = self.__class__.__name__

        # define all model parameters with default values. This does not have to be equal to the design vector x
        self.params = dict()
        self.params['iteration'] = Par(0, '', 'iteration counter', '')
        self.params['modelName'] = Par(self.ModelName, '', 'Model name for identification and file naming purposes')

        # use a list to assign model parameters and design variables.
        self.designVariables = list()

        # initialize f and g for optimization
        self.f = 0.
        self.g = [0.]

        # prepare a dict for the results of the evaluation
        self.evalResults = dict()

        # prepare some timing variables
        self.startTime = time.time()
        self.stopTime = time.time()
        self.duration = 0

        logging.info('Initialized EOS model "%s"' % self.ModelName)

    def startTimer(self):
        self.startTime = time.time()

    def stopTimer(self):
        self.stopTime = time.time()
        self.duration = self.stopTime - self.startTime
        return self.duration

    def WriteEvaluationOutput(self, filename=False, filePath=False,):

        if not filename:
            timeString = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            filename = 'eos_eval_%s_%s.txt' % (self.ModelName, timeString)

        # if a path was requested, use it, otherwise use the current working dir
        if not filePath:
            filePath = os.getcwd()

        sorted_params = collections.OrderedDict(sorted(self.params.items()))
        sorted_results = collections.OrderedDict(sorted(self.evalResults.items()))

        logging.info("-------------- full parameter set --------------\n%s" % sorted_params)
        logging.info("-------------- full evaluation result set--------------\n%s" % sorted_results)

        resultFilePath = os.path.join(filePath, filename)
        logging.debug('Writing evaluation result output to file "%s"' % resultFilePath)

        with open(resultFilePath, mode='a') as resultFile:
            resultFile.write('-------------------- Full Result Set-----------------------\n')
            pprint.pprint(self.evalResults, resultFile)

            resultFile.write('\n\n-------------------- Full Parameter Set-----------------------\n')
            pprint.pprint(self.params, resultFile)

    def assignDesignVariables(self, x):

        # make sure that x is always a numpy-array, also if only an int or float are given
        if isinstance(x, int) or isinstance(x, float):
            x = np.array([x])

        # check if the length of the designVariables list matches the length of the x vector:
        # using "designVariables" is optional, mostly required for optimization
        if not len(self.designVariables) == len(x):
            message = 'the length of designVariables "%i" does not match the length of the design vector x "%i"' % (len(self.designVariables), len(x))
            logging.fatal(message)
            sys.exit(message)

        # assign values from the design vector x to the model parameters:
        for number, param in enumerate(self.designVariables):
            self.params[param].value = x[number]


class Parameter:
    def __init__(self, value=0., unit='', description='', title=''):
        self.value = value
        self.unit = unit
        self.description = description
        self.title = title

    def __str__(self):
        if self.unit:
            unitstring = ' %s' % self.unit
        else:
            unitstring = ''
        string = '%s%s' % (str(self.value), unitstring)
        return string

    def __repr__(self):
        if self.unit:
            unitstring = ' %s' % self.unit
        else:
            unitstring = ''
        string = '%s%s' % (str(self.value), unitstring)
        return string


def round_significant(num, significantDigits):
    import math
    if num != 0:
        return round(num, -int(math.floor(math.log10(abs(num))) - (significantDigits - 1)))
    else:
        return 0  # Can't take the log of 0