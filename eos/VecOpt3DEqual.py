# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

from numpy.linalg   import norm
from itertools      import product
from itertools      import izip
from itertools      import chain
import numpy    as np
import copy     as cp

import logging
import sys
import os

class VectorOptimizer:
    def __init__(self, Model, DesOpt, Options, nDV):
        self.Model      = Model
        self.DesOpt     = DesOpt
        self.Options    = Options
        self.VecOptions = Options['VecOptions']
        self.nDV        = nDV
        
        self.SubstModel     = SubstituteModel(Model, Options, nDV)       
        self.DesOpt.Model   = self.SubstModel
        self.DesOpt.EOSOptOptions['ConvergencePlots'] = False
        self.DVTypes        = cp.deepcopy(self.DesOpt.EOSOptOptions['DVTypes'])
        
        if 'GridType' in Options['VecOptions'].keys():
            self.GridType = Options['VecOptions']['GridType']
        else:
            self.GridType = 'linebyline'
        if 'GridDir' in Options['VecOptions'].keys():
            self.GridDir = Options['VecOptions']['GridDir']
        else:
            self.GridDir = 0
            
        # add IterationUserOutput if defined in model.py to SubstituteModel            
        if hasattr(self.Model, 'IterationUserOutput'):
            def IterationUserOutput(self,*args):
                return self.Model.IterationUserOutput(*args)
            IterationUserOutput.__doc__ = "Output function at each iteration is provided by user."
            IterationUserOutput.__name__ = "IterationUserOutput"
            setattr(SubstituteModel,IterationUserOutput.__name__,IterationUserOutput)          

    def GetGrid(self,GridType,GridDir,npo):
        '''
        Get grid for evaluation points
        Arguments:  - npo: Number of points along edge
                    - GridType: Type of grid building order
                    - GridDir: Direction of Grid building order
        Returns:    - list of grid points
        '''
        grid = list()        
        
        for i in range(npo):
            if   GridType == 'edgesfirst': 
                if GridDir   == 0: l = list(izip([i]*(npo-i),range(i,npo-i))) + list(izip(range(i+1,npo-i),[i]*(npo-i)))
                elif GridDir == 1: l = list(izip(range(i,npo-i),[i]*(npo))) + list(izip([i]*(npo-i),range(i+1,npo-i)))
            elif GridType == 'triangle':
                if GridDir   == 0: l = list(izip(range(0,i+1,1),range(i,0-1,-1)))
                elif GridDir == 1: l = list(izip(range(i,0-1,-1),range(0,i+1,1)))
            elif GridType == 'linebyline':
                if GridDir   == 0: l = list(izip([i]*(npo-i),range(npo-i)))
                elif GridDir == 1: l = list(izip(range(npo-i),[i]*(npo-i)))
            else: 
                sys.exit('UNKNOWN GRIDTYPE.')
            grid.append(l)
            
        return list(chain(*grid))

        
    def Optimize(self,x0,xL,xU):
        logging.info('$$ ------------------------- $$')
        logging.info('$$ START VECTOR OPTIMIZATION $$')
        logging.info('$$ ------------------------- $$')
        
        # Assign local variables
        # --------------------------------------------------------- #
        nf      = self.Options['nf']                # Number of objectives
        nDV     = self.nDV                          # Number of design variables
        npo     = self.VecOptions['NumParetoOptima']# Number of desired Pareto points
        alpha   = self.VecOptions['ChordFactor']    # Chord Factor
        TotCalls= 0                                 # Total number of function calls
        self.x0 = cp.deepcopy(x0)
        
        Pareto, Local = [],[]                   # Result lists of returned dictionaries
        Fopt = np.zeros((npo,)*(nf-1) + (nf,))  # Summary of F-opt at each pos
        
        # Generate hyperplane grid
        # --------------------------------------------------------- #
        
        gridpoints = self.GetGrid(self.GridType,self.GridDir,npo)
        print '**************** Gridpoints:', gridpoints
        gridpointsCP = cp.deepcopy(gridpoints)
        # Position of edge points
        A = np.array(list(product(range(npo),repeat=(nf-1)))[:npo])        
        EdgFilter = np.array([A,np.fliplr(A)])
        # Position of anchor points
        AncFilter = np.row_stack((np.zeros(nf-1,dtype='intp'),np.eye(nf-1,dtype='intp')*(npo-1))).tolist()

        # Compute anchor points
        # --------------------------------------------------------- #
        self.FAnc = np.zeros((nf,nf))        
        if 'EdgePoints' in self.VecOptions.keys():
            xEdg = self.VecOptions['EdgePoints']
            xAnc = np.array([xEdg[0][0],xEdg[0][-1],xEdg[1][-1]])
        elif 'AnchorPoints' in self.VecOptions.keys():
            xEdg = None
            xAnc = np.array(self.VecOptions['AnchorPoints'])
        else:
            sys.exit('ETHER ANCHOR POINTS OR EDGE POINTS MUST BE PROVIDED (IN DESIGN SPACE).')
            
        for i in range(xAnc.shape[0]):
            print('')
            print('ANCHOR POINT POSITION: %s' % (tuple(AncFilter[i]),))
            FTemp,GTemp,_ = self.Model.ObjAndConstrFct(xAnc[i,:])
            self.FAnc[i,:] = FTemp
            for i,pos in enumerate(AncFilter):
                Fopt[tuple(pos)] = self.FAnc[i] 

        
        # Compute utopia and nadir points
        # --------------------------------------------------------- #
        FUto = np.array([min(self.FAnc[:,k]) for k in range(self.Options['nf'])])
        FNad = np.asarray([max(self.FAnc[:,k]) for k in range(self.Options['nf'])])
        
        # Plotting
        # --------------------------------------------------------- #
        from OptTools import ParetoPlotting, addplot
        runfig, runax = ParetoPlotting(FNad,FUto,self.FAnc)
        for pos in AncFilter: addplot(Fopt[tuple(pos)],None,None,runax,tuple(pos))
            
        # Compute edge points
        # --------------------------------------------------------- #
        if 'EdgePoints' in self.VecOptions.keys():
            for E, EdgePoints in enumerate(xEdg):
                for e, EdgePoint in enumerate(EdgePoints):
                   pos = EdgFilter[E][e].tolist()
                   if pos not in AncFilter: 
                        print('')
                        print('EDGE POINT POSITION: %s' % (tuple(pos),))
                        FTemp,GTemp,_ = self.Model.ObjAndConstrFct(EdgePoint)
                        Fopt[tuple(pos)] = FTemp
                        gridpoints.remove(tuple(pos))
                        addplot(FTemp,None,None,runax,tuple(pos))
        [gridpoints.remove(tuple(AncFilter[i])) for i in range(len(AncFilter))]

        # --------------------------------------------------------- #                
        # LOOP OVER ALL GRID POINTS
        # --------------------------------------------------------- #
        for pos in gridpoints:

            Lambda  = np.ones(nf)

            if 0 in pos:
                Lambda[pos.index(0)+1] = 0          # Position of active weights
            nLam = np.count_nonzero(Lambda)         # Number of active weights
            
            nhgam = nLam-1                          # Number of equidistance constraints
            nggam = nhgam*2
            if nLam>1: ngLam = True                 # Flag for weight constraint
            
            # Update base variables
            # --------------------------------------------------------------- #   
            xLlam,xUlam = np.append(xL, np.zeros(nLam-1)),np.append(xU, np.ones(nLam-1))
            
            # Assign to DesOpt-model
            # --------------------------------------------------------------- #
            self.DesOpt.xLOpt,self.DesOpt.xUOpt = cp.deepcopy(xLlam),cp.deepcopy(xUlam)

            # Compute equidistance constraint variables
            # --------------------------------------------------------------- #          
#            from OptTools import positioningTool
#            prevpos,prevpos2 = positioningTool(pos,direction=0)
            # --------------------------------------------------------------- #
            prevpos = list()
            eye = np.eye(2,dtype='intp')*1
            for prev in eye.tolist():
                prevpos.append(np.array(pos)-prev)
            for i, item in enumerate(prevpos):
                if np.min(item) < 0: prevpos[i] = tuple(prevpos[i-1])
                else:             prevpos[i] = tuple(prevpos[i])
            #print prevpos
            Fhgam   = np.array([Fopt[ppos] for ppos in prevpos])

            # --------------------------------------------------------------- #
#            prevprevpos = list()
#            eye = np.eye(2,dtype='intp')*1
#            for p, prev in enumerate(eye.tolist()):
#                P = np.array(prevpos[p])-prev
#                P = P.clip(min=0)
#                prevprevpos.append(tuple(P))
#            #print prevprevpos
#            FgBS   = np.array([Fopt[ppos] for ppos in prevprevpos])
            # --------------------------------------------------------------- #
            BSpos = gridpointsCP[:gridpointsCP.index(pos)]
            #BSpos = gridpoints[:gridpoints.index(pos)]
            BSpos = [ppos for ppos in BSpos if abs(ppos[0]-pos[0]) <= 2]
            BSpos = [ppos for ppos in BSpos if abs(ppos[1]-pos[1]) <= 2]
            FgBS = np.array([Fopt[ppos] for ppos in BSpos])
            ngBS = len(FgBS)
            # --------------------------------------------------------------- #
            gamma = []
            for i,L in enumerate(Lambda[1:]):
                if bool(L):
                    gamma.append(alpha[i] * norm(self.FAnc[0,:]-self.FAnc[i+1,:]) / (npo-1))
                else:
                    gamma.append(0.)
            
            # Transfer to substitute model
            # --------------------------------------------------------------- # 
            self.DesOpt.EOSOptOptions['DVTypes'] = self.DVTypes + ['c',]*(nLam-1)
            self.DesOpt.EOSOptOptions['nh'] = self.Options['nh']# + nhgam
            self.DesOpt.EOSOptOptions['ng'] = self.Options['ng'] + int(ngBS) + int(ngLam) + nggam
            
            self.SubstModel.Transfer(gamma,Fhgam,FgBS,nhgam,ngBS,ngLam,Lambda,FUto,FNad)
            
            # Compute x0
            # --------------------------------------------------------- #
            
#            if ngBS <= 1:
#                self.x0 = cp.deepcopy(x0)
#            else:
#                if norm(FgBS[-1]-self.FAnc[1,:]) < norm(FgBS[-1]-self.FAnc[2,:]):
#                    self.x0 = cp.deepcopy(xAnc[1,:])
#                else:
#                    self.x0 = cp.deepcopy(xAnc[2,:]) 
            # Initial solution
            # --------------------------------------------------------------- #
            if self.VecOptions['GS']:  # LOCAL SEARCH MISSING
                from OptTools import GlobalSearchForDesOpt, GSOPostProcessing
                x0GS, gsInfo = GlobalSearchForDesOpt(xLlam,xUlam,self.SubstModel.ObjAndConstrFct,
                                                     nh=self.DesOpt.EOSOptOptions['nh'],
                                                     nDoE=self.VecOptions['nDoE'],
                                                     nGS=self.VecOptions['nGS'])
                nLoops = self.VecOptions['nGS']
            else:
                #x0     = np.append(self.x0, (nLam-1) * [1/(nLam-1)])
                x0     = np.append(self.x0, (nLam-1) * [.5])
                nLoops = 1
            
            # Solve SOP
            # --------------------------------------------------------------- #
            Result = []
            for i in range(nLoops):
                if self.VecOptions['GS']:
                    x0 = x0GS[:,i]
                
                print('')
                print('INNER PARETO POSITION: %s  ITERATION: %d' % (str(pos),int(i)))
                TempResults = self.DesOpt(x0)
                TempResults['x0']           = x0
                TempResults['Pos']          = pos
                TempResults['PosBS']        = BSpos
                TempResults['PosH']         = prevpos
                TempResults['fOpt-Vect']    = self.SubstModel.fPareto
                TempResults['Lambda']       = self.SubstModel.lAMBDA
                TempResults['xOpt']         = self.SubstModel.X
                
                TotCalls                   += len(TempResults['fHist'])
                if self.VecOptions['GS']:
                    TempResults['gsInfo']   = gsInfo
                Result.append(TempResults)
                
            # Process results
            # --------------------------------------------------------------- #
            # Check optimality
            if self.VecOptions['GS']:
                gsinfo = GSOPostProcessing(Result)
                
            # Save best results
            lowU = [n['fOpt'] for n in Result]  # get values of cost-fct from all GSO-result-dictionaries
            indP = lowU.index(min(lowU))        # index of dictionary with lowest u
            
            Fopt[pos] = Result[indP]['fOpt-Vect']
            addplot(Fopt[pos],Fhgam,gamma,runax,tuple(pos))
            if self.VecOptions['ConvergencePlots']:
                DirLabel = 'results/ConvergencePlots/'+str(len(Pareto)-1)+'_InnerPoint'
                if not os.path.exists(DirLabel):
                    os.makedirs(DirLabel)
                Label = [DirLabel + '/' + 'ConvPlot.pdf',DirLabel + '/' + 'BarPlot.pdf']
                self.DesOpt.ConvergencePlots(Result[indP]['xHist'],Result[indP]['fHist'],Result[indP]['gHist'],Result[indP]['hHist'],self.Options['ng'],self.Options['nh'],nDV,Label,showit=False)
            runfig.savefig('ParetoFront') 
            
            Pareto.append(Result[indP])
            del Result[indP]
            Local.append(Result)                # save all other dictionaries in "Local"-list
            
        # ------------------------------------------------------------------- #
        # PostProcessing
        # ------------------------------------------------------------------- #
        for npar in Pareto: # Rename fOpt
            npar['uOpt'] = npar.pop('fOpt')
            npar['fOpt'] = npar.pop('fOpt-Vect')
            try:
                npar['uHist'] = npar.pop('fHist')
            except:
                pass
                       
        # Second anchor point at the end (only for biobjective):
        if self.Options['nf']==2:
            Pareto.append(Pareto.pop(2-self.VecOptions['fPrevInit']))
			
        # Infos at the very end
        Summary = dict()
        Summary['f_opt'],Summary['x_opt'],Summary['lambda_opt'],Summary['u_opt'] = [],[],[],[]
        Summary['Shadow'] = []
        for sol in Pareto:
            Summary['f_opt'].append(sol['fOpt'])
            Summary['x_opt'].append(sol['xOpt'])
            Summary['lambda_opt'].append(sol['Lambda'])
            Summary['u_opt'].append(sol['uOpt'])
            Summary['Shadow'].append(sol['LagrangianData']['Lambda Values'])
        for key in Summary.keys():
            Summary[key] = np.array(Summary[key])
            
        Summary['Anchor'] = self.FAnc
        Summary['Utopia'] = FUto
        Summary['Nadir']  = FNad
        Summary['fCalls'] = TotCalls
        Summary['Fopt']   = Fopt
        Summary['Gridpoints'] = gridpoints
#        Pareto.append({'FNad':FNad,'FUto':FUto,'FAnc':FAnc})
        
        print('############### EQUIDISTANT PARETO COMPUTATION DONE. #################')
        return {'Summary':Summary, 'Detailed':Pareto}, Local, runfig, runax
        
class SubstituteModel:
    def __init__(self, Model, Options, nDV):
        self.Options    = Options
        self.VecOptions = Options['VecOptions']
        self.Model      = Model
        self.nDV        = nDV
        
        self.xList,self.dxList      = [],[]
        self.fgList,self.dfdgList   = [],[]
    
    def Transfer(self, gamma, Fhgam, FgBS, nhgam, ngBS, ngLam, Lambda, FUto, FNad):
        self.gamma, self.Fhgam, self.FgBS = gamma,Fhgam,FgBS
        self.nhgam, self.ngBS, self.ngLam = nhgam, ngBS, ngLam
        self.Lambda = Lambda
        
        if self.VecOptions['NormalizeF']:
            self.Flow,self.Fup = FUto,FNad
        else:
            self.Flow,self.Fup = np.zeros(self.Options['nf']), np.ones(self.Options['nf'])
        
    def ObjAndConstrFct(self, x):
        Lambda      = self.Lambda
        X, lAMBDA   = self._seperateX(x)        
        F,G,fail    = self._getFG(X)
        Fn          = (F-self.Flow)/(self.Fup-self.Flow)
        
        u = np.dot(lAMBDA,Fn)
        
        # constraints
        g = G
        if self.nhgam > 0:
            for i,L in enumerate(self.Lambda[1:]):
                if L != 0:
                    f = self.Fhgam[i]
                    V = np.asarray(F)-np.asarray(f)
                    tol = 1e-4
                    ggamP = norm(V) / self.gamma[i] - (1.+tol)
                    ggamN = -ggamP
                    g    = np.hstack((g,ggamP))
                    g    = np.hstack((g,ggamN))
                    
        if self.ngLam:
            gLam = np.sum(lAMBDA[:]) - 1.
            g    = np.hstack((g,gLam))
            
        if self.ngBS:
            for f in self.FgBS:
                dgamma = min([i for i in self.gamma if i > 0])
                V = np.asarray(F)-np.asarray(f)
                gBS = 1. - norm(V) / (self.VecOptions['dGamma']*dgamma)
                g   = np.hstack((g,gBS))

        return u,g,0
        
    def ObjAndConstrSensFct(self, x, u, g):
        Lambda     = self.Lambda        
        X, lAMBDA  = self._seperateX(x)
        F,G,fail   = self._getFG(x)
        dF,dG,fail = self._getdFdG(X,F,G)
        Fn          = (F-self.Flow)/(self.Fup-self.Flow)
        
        du = np.dot(dF,lAMBDA/(self.Fup-self.Flow))
        dudLambda = []

        if self.Lambda[2]:
            dudLambda.append(Fn[0] - Fn[2]) 
            if self.Lambda[1]:
                dudLambda.append(Fn[1] - Fn[2])
        elif self.Lambda[1]:
            dudLambda.append(Fn[0] - Fn[1]) 
        else: pass

#        for i,L in enumerate(self.Lambda[1:]):
#            if L != 0:
#                dudLambda.append(F[i]-F[-1]/(self.Fup-self.Flow))
                
                
                
        du = np.append(du,dudLambda)
        du = du.tolist()
        # constraints
        LamDV = np.count_nonzero(self.Lambda)-1
        dG = np.hstack((dG,np.zeros((dG.shape[0],LamDV))))
        dg = dG
        if self.nhgam > 0:
            for i,L in enumerate(self.Lambda[1:]):
                if L != 0:  
                    f = self.Fhgam[i]
                    V = np.asarray(F)-np.asarray(f)
                    dggamdxP = np.dot(V,dF) / norm(V) / self.gamma[i]
                    dggamP   = np.append(dggamdxP, np.zeros(LamDV))
                    dggamdxN = -dggamdxP
                    dggamN   = np.append(dggamdxN, np.zeros(LamDV))
                    dg      = np.vstack((dg,dggamP))
                    dg      = np.vstack((dg,dggamN))
                    
        if self.ngLam:
            dgLam = np.append(np.zeros(self.nDV), np.ones(LamDV))
            dg    = np.vstack((dg,dgLam))
            
        if self.ngBS:
            for f in self.FgBS:
                dgamma = min([i for i in self.gamma if i > 0])
                V = np.asarray(F)-np.asarray(f)
                dgBSdx = -1. * np.dot(V,dF) / norm(V) / (self.VecOptions['dGamma']*dgamma)
                dgBS   = np.append(dgBSdx, np.zeros(LamDV))
                dg     = np.vstack((dg,dgBS))
           
        return du,dg,0
    
    def _seperateX(self, x):
        '''
        Separate design variables and weight variables.
        Arguments:  - x: design variables vector from pyOpt
        Returns:    - X: true design variables
                    - lAMBDA: weights
        '''
        X       = x[:self.nDV]
        lAMBDA  = np.zeros(self.Options['nf'])
        iLam    = [i for i, L in enumerate(self.Lambda) if L == 1]
        for i,ind in enumerate(iLam[:-1]):
            lAMBDA[ind] = x[self.nDV+i]
        lAMBDA[iLam[-1]] = 1. - sum(lAMBDA)      
        self.X, self.lAMBDA = X, lAMBDA
        return X,lAMBDA
        
    def _getFG(self, x):
        '''
        Get objective and constraint values from model.py-file.
        Arguments:  - x: Design variable vector
        Returns:    - F: Objective value vector
                    - G: Constraints value vector
        '''
        F,G,fail = self.Model.ObjAndConstrFct(x)
        self.xList.append(x)
        self.fgList.append([F,G])
        self.fPareto = F
        return F, G, fail

    def _getdFdG(self, x, F, G):
        '''
        Get derivatives of objectives and constraints from model.py-file.
        Arguments:  - x: Design variable vector
        Returns:    - F: Objective value vector
                    - G: Constraints value vector
        '''
        dF,dG, fail = self.Model.ObjAndConstrSensFct(x, F, G)
        self.dxList.append(x)
        self.dfdgList.append([dF,dG])
        return dF, dG, fail

# --------------------------------------------------------------------------- #
# --------------------------------------------------------------------------- #
#                       INPUTS FROM EXTERNALS
# --------------------------------------------------------------------------- #
# --------------------------------------------------------------------------- #                   
#class MyModel:
#    def __init__(self,nf,ng,nDV):
#        self.nf = nf
#        self.ng = ng
#        self.nDV = nDV
#        
#    def ObjAndConstrFct(self,x):
#        f = np.random.rand(self.nf)
#        g = np.random.rand(self.ng)
#        return f,g,0
#        
#    def ObjAndConstrSensFct(self,x,f,g):
#        df = np.random.rand(self.nf,self.nDV)
#        dg = np.random.rand(self.ng,self.nDV)
#        return df,dg,0
#
#class DesignOpti:
#    def __init__(self,nDV):
#        self.Model = 123.
#        self.xLOpt = [1.,]*nDV
#        self.xUOpt = [0.,]*nDV
#        self.EOSOptOptions = dict()
#        self.EOSOptOptions['ConvergencePlots'] = False
#        self.EOSOptOptions['DVTypes'] = ['c',]*nDV
#        
#    def __call__(self,x0):
#        TempResults = {'fHist': np.zeros((np.random.randint(0,100),1)), 'fOpt': np.random.rand()}
#        return TempResults
#                
#nf  = 3
#ng  = 2
#nDV = 3
#ANCS = 2.*np.ones((nf,nf))
#for i in range(nf):
#    ANCS[i,i] = 2.-np.sqrt(1.5)
#
#VecOptions = {'NumParetoOptima':10, 'ChordFactor':[1.2,]*(nf-1),'NormalizeF':True,'GS':False,'nDoE':10,'nGS':2}
#Options = {'nf':nf, 'VecOptions':VecOptions, 'AnchorPoints':ANCS, 'nh':0, 'ng':ng}
#Model = MyModel(nf,ng,nDV)
#DesOpt = DesignOpti(nDV)
#
#MOopt = VectorOptimizer(Model,DesOpt,Options,nDV)
#Pareto, Local,_ = MOopt.Optimize(np.random.rand(nDV),np.random.rand(nDV),np.random.rand(nDV))#[1,2,3],[3,4,5],[5,6,7]
