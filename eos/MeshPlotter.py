# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.
'''
Used to plot any mesh defined via elements and nodes with the help of mayavi

So far, only quad elements supported!
'''

import numpy as np
import os
import sys
from mayavi import mlab
import logging

class MeshPlotter(object):
    def __init__(self,white=True):
        '''
        Plots Abaqus and NASTRAN meshes and element results
        '''
        self.white = white
        self.Grid = None
        self.Elements = None
        self.Results = None 
        
    def __call__(self, Camera=dict()):
        self.__CheckForElmsNodes()
        if self.PrintOutputs:
            print 'Mesh info detected!'
        if self.Results!=None: 
            if self.ResultType == 'Element' and self.Contour == 'Smooth':
                self.__Averaging()
            if self.PrintOutputs:
                print 'Results fetched'
                print '-> Node results generated via element back averaging'
        else:
            self.nodval = np.zeros((self.Grid.shape[0]))
            if self.PrintOutputs:
                print 'No results fetched'
        self.__MeshPlot(Camera=Camera)
    
    def __CheckForElmsNodes(self):
        if self.Elements == None:
            if self.PrintOutputs:
                print 'No elements given -> use method "PassElementsNodesResults"'
        if self.Grid == None:
            if self.PrintOutputs:
                print 'No nodes given -> use method "PassElementsNodesResults"'
        
    def __Averaging(self):
        Nnod = self.Grid.shape[0]
        nodval = np.zeros((Nnod))
        for n in xrange(0,Nnod):
            avdomain = np.where(self.Grid[n,0]==self.Elements[:,1:])[0]
            if avdomain.size == 0:
                continue
            nodval[n] = np.mean(self.Results[avdomain,1])
            #print 'Node '+str(n)+' done'
        self.nodval = nodval
        
    def __MeshPlot(self,Camera = dict()):
        Nel = self.Elements.shape[0]
        tritemp = np.zeros((Nel,4))
        for k in xrange(0,Nel):
            for j in xrange(0,4):
                tritemp[k,j]=np.where(self.Grid[:,0]==self.Elements[k,j+1])[0]
        tris1 = tritemp[:,[0,1,3]]
        tris2 = tritemp[:,[2,1,3]]
        tris = np.append(tris1,tris2,axis=0)
        if self.ResultType == 'Element' and self.Contour == 'NonSmooth':
            ResultsTmp = np.append(self.Results[:,1],self.Results[:,1],axis=0)
        
        if self.white==False:
            mlab.figure(figure=None,bgcolor=(0,0,0),fgcolor=(1,1,1),size=(1366,768))              
        else:
            if self.PrintOutputs:
                print 'White figure'
            mlab.figure(figure=None,bgcolor=(1,1,1),fgcolor=(0,0,0),size=(1366,768))
            
        if self.Results!=None:
            if self.Contour == 'Smooth':  
                TriSurf = mlab.triangular_mesh(self.Grid[:,1],self.Grid[:,2],self.Grid[:,3],tris,\
                                     scalars = self.nodval)
                cb = mlab.scalarbar(object=None,title=self.ResultTitle, orientation = 'vertical',nb_labels = 10, label_fmt = '%.3f', nb_colors = 9)   
                if len(self.CBarBounds) > 0:
                    cb.data_range     = self.CBarBounds    
                          
            elif self.ResultType == 'Element' and self.Contour == 'NonSmooth':
                TriSurf1 = mlab.triangular_mesh(self.Grid[:,1],self.Grid[:,2],self.Grid[:,3],tris,\
                                     representation='wireframe',opacity=0)
                cell_data = TriSurf1.mlab_source.dataset.cell_data
                cell_data.scalars = ResultsTmp
                cell_data.scalars.name = self.ResultTitle
                cell_data.update()                
                TriSurf = mlab.pipeline.set_active_attribute(TriSurf1, cell_scalars=self.ResultTitle) 
                TriSurf = mlab.pipeline.surface(TriSurf) 
                if len(self.CBarBounds) > 0:
                    TriSurf.module_manager.scalar_lut_manager.data_range = self.CBarBounds                
                TriSurf.module_manager.scalar_lut_manager.show_scalar_bar = True
                TriSurf.module_manager.scalar_lut_manager.title_text_property.font_size = 24
                TriSurf.module_manager.scalar_lut_manager.lut.nan_color = [.9,.9,.9,1.0]
                
            elif self.ResultType == 'Nodes':
                TriSurf1 = mlab.triangular_mesh(self.Grid[:,1],self.Grid[:,2],self.Grid[:,3],tris,\
                                     representation='wireframe',opacity=0)
                
                point_data = TriSurf1.mlab_source.dataset.point_data
                point_data.scalars = self.Results[:,1]
                point_data.scalars.name = 'Point data'
                point_data.update()
                TriSurf = mlab.pipeline.set_active_attribute(TriSurf1,
                        cell_scalars='Cell data') 
                TriSurf = mlab.pipeline.set_active_attribute(TriSurf1,
                        point_scalars='Point data')
                mlab.pipeline.surface(TriSurf) 
                mlab.colorbar(title=self.ResultTitle,orientation = 'vertical',nb_labels = 10, label_fmt = '%.3f', nb_colors = 9) 
                ErMSG = 'MeshPlotter Class: Wrong order in Node results yet! Code needs to be finished!'                   
                logging.error(ErMSG)
            else:            
                ErMSG = 'MeshPlotter Class: Tri-Plot case not considered yet!'                   
                logging.error(ErMSG)
        else:
            TriSurf = mlab.triangular_mesh(self.Grid[:,1],self.Grid[:,2],self.Grid[:,3],tris,\
                                     scalars = self.nodval)
                             
                             
        mlab.draw()
        CamChange = False
        if 'Position' in Camera.keys():
            CamChange = True
            TriSurf.scene.camera.position = Camera['Position']
        if 'FocalPoint' in Camera.keys():
            CamChange = True
            TriSurf.scene.camera.focal_point = Camera['FocalPoint']
        if 'ViewAngle' in Camera.keys():
            CamChange = True
            TriSurf.scene.camera.view_angle = Camera['ViewAngle']
        if 'ViewUP' in Camera.keys():
            CamChange = True
            TriSurf.scene.camera.view_up = Camera['ViewUP'] 
        if 'ClipRange' in Camera.keys():
            CamChange = True
            TriSurf.scene.camera.clipping_range = Camera['ClipRange']      
        if CamChange:
            TriSurf.scene.camera.compute_view_plane_normal()
            TriSurf.scene.render()
        mlab.savefig(filename=self.FigName)
        if self.ShowFig:
            mlab.show() 
        else:
            mlab.close()
        
    def PassElementsNodesResults(self,Elements,Nodes,Results=None, ResultTitle = 'FI [-]', \
            ResultType = 'Element', Contour = 'Smooth', CBarBounds = []):
        '''
        param: ResultTitle: Title over results bar
        param: ResultType: Can be "Node" or "Element"
        param: Contour: Can be "Smooth" or "NonSmooth" (Only active for "Element" choice)
        '''
        self.Grid = Nodes
        self.Elements = Elements
        self.Results = Results 
        self.ResultTitle = ResultTitle 
        self.ResultType = ResultType
        self.Contour = Contour
        self.CBarBounds = CBarBounds
        
    def Plot(self, Camera=dict(), FigName='ShellMeshPlot.png', ShowFig=False, PrintOutputs=False):
        self.FigName = FigName
        self.ShowFig = ShowFig
        self.PrintOutputs = PrintOutputs
        self(Camera=Camera)
    
        
        
if __name__ == '__main__':
    
    # See test case: 02-MeshPlotting
    print 'See test case: 02-MeshPlotting'  
    
    