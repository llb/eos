# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ # 
# Temporarily added -> Work in progress!!
import platform
if platform.system()=='Windows':
    import os
    FilePath = os.path.dirname(os.path.realpath(__file__))
    print FilePath
    os.chdir(FilePath)
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ # 
    
# Import of the eos package
# --------------------------------------------------------------------------- #
import os, sys
HeadPath = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(HeadPath)
from eos.AbaqusTools import AbaqusClass
from eos.MeshPlotter import MeshPlotter

Plotting = 'TubeMesh' 
# TubeMesh, TubeFI, TubeMeshVideo


if Plotting == 'TubeMesh':
    # Inititialize Abaqus input and output reader
    AbaqusInOutObj = AbaqusClass(ModelPath=os.path.join(os.getcwd(),'models','Tube'))
    # Initialize plotter obj
    PlotterObj = MeshPlotter(white=True)
    # Initialize model
    from models.Tube.TubeDef import Tube
    TubeObj = Tube(AbaqusInOutObj)
    #Initialize study
    from studies.Tube import TubePlotting as Plot
    PlotObj = Plot(TubeObj,PlotterObj)
    InfoStr =  PlotObj.run(ResultChoice = 'Mesh') # Mesh, ResultsFile
elif Plotting == 'TubeFI':
    # Inititialize Abaqus input and output reader
    AbaqusInOutObj = AbaqusClass(ModelPath=os.path.join(os.getcwd(),'models','Tube'))
    # Initialize plotter obj
    PlotterObj = MeshPlotter(white=True)
    # Initialize model
    from models.Tube.TubeDef import Tube
    TubeObj = Tube(AbaqusInOutObj)
    #Initialize study
    from studies.Tube import TubePlotting as Plot
    PlotObj = Plot(TubeObj,PlotterObj)
    InfoStr =  PlotObj.run(ResultChoice = 'TubeFI') # Mesh, ResultsFile
elif Plotting == 'TubeMeshVideo':
    # Inititialize Abaqus input and output reader
    AbaqusInOutObj = AbaqusClass(ModelPath=os.path.join(os.getcwd(),'models','Tube'))
    # Initialize plotter obj
    PlotterObj = MeshPlotter(white=True)
    # Initialize model
    from models.Tube.TubeDef import Tube
    TubeObj = Tube(AbaqusInOutObj)
    #Initialize study
    from studies.Tube import TubeVideo as Plot
    PlotObj = Plot(TubeObj,PlotterObj)
    InfoStr =  PlotObj.run(ResultChoice = 'Mesh') # Mesh, ResultsFile


# Plot infos
# --------------------------------------------------------------------------- #
print InfoStr
