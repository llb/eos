# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# Import needed modules
# --------------------------------------------------------------------------- #
import numpy as np
import os

# Definition of simulation model class
# --------------------------------------------------------------------------- #
class Tube:
    MeshFileName = 'Tube.inp'
    ResFileName = 'TubeFI.res'
    
    def __init__(self,AbaqusTools):
        self.AbaqusTools = AbaqusTools
    
    def ReadMesh(self,x):
        self.Nodes, self.Elements, self.nNodes, self.nElms = \
            self.AbaqusTools.extract_NodesAndElements(self.MeshFileName)
        self.Nodes[:,1:2] *= x
        return  self.Nodes, self.Elements, self.nNodes, self.nElms
        
    def ReadElementResults(self,nElms):
        self.Results = self.AbaqusTools.extract_Results(nElms,self.ResFileName)
        return self.Results