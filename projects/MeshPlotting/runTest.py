# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

from PIL import Image, ImageSequence
import sys, os
filename = sys.argv[1]
im = Image.open(filename)
original_duration = im.info['duration']
frames = [frame.copy() for frame in ImageSequence.Iterator(im)]    
frames.reverse()

from images2gif import writeGif
writeGif("reverse_" + os.path.basename(filename), frames, duration=original_duration/1000.0, dither=0)


import opencv
from opencv.cv import *
from opencv.highgui import *

isColor = 1
fps     = 25  # or 30, frames per second
frameW  = 256 # images width
frameH  = 256 # images height
writer = cvCreateVideoWriter("video.avi",-1, 
fps,cvSize(frameW,frameH),isColor)

#-----------------------------
#Writing the video file:
#-----------------------------

nFrames = 70; #number of frames
for i in range(nFrames):
    img = cvLoadImage("image_number_%d.png"%i) #specify filename and the extension
     # add the frame to the video
    cvWriteFrame(writer,img)

cvReleaseVideoWriter(writer) #


__author__ = 'Robert'
from images2gif import writeGif
from PIL import Image
import os

file_names = sorted((fn for fn in os.listdir('.') if fn.endswith('.png')))
#['animationframa.png', 'animationframb.png', 'animationframc.png', ...] "

images = [Image.open(fn) for fn in file_names]

print writeGif.__doc__
# writeGif(filename, images, duration=0.1, loops=0, dither=1)
#    Write an animated gif from the specified images.
#    images should be a list of numpy arrays of PIL images.
#    Numpy images of type float should have pixels between 0 and 1.
#    Numpy images of other types are expected to have values between 0 and 255.


#images.extend(reversed(images)) #infinit loop will go backwards and forwards.

filename = "my_gif.GIF"
writeGif(filename, images, duration=0.2)
#54 frames written
#
#Process finished with exit code 0