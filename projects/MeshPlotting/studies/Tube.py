# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import time

class TubePlotting():
    
    def __init__(self,TubeObj,PlotterObj):
        self.TubeObj = TubeObj
        self.PlotterObj = PlotterObj

    def run(self,ResultChoice = 'Mesh'): 
        tStart = time.clock()
            
        # Definition of plot case
        # ------------------------------------------------------------------- #
        Nodes, Elements, nNodes, nElms = self.TubeObj.ReadMesh(1.)
        if ResultChoice == 'Mesh':
            self.PlotterObj.PassElementsNodesResults(Elements,Nodes)
        elif ResultChoice == 'TubeFI':
            Results = self.TubeObj.ReadElementResults(nElms)
            self.PlotterObj.PassElementsNodesResults(Elements,Nodes,Results=Results)
        
        Camera = dict()
        Camera['Position'] = [-78.809399771969822, 165.4524112085162, 775.83485483341053]
        Camera['FocalPoint'] = [68.846315411589558, -47.507645843801079, 338.39762892491348]
        Camera['ViewAngle'] = 30.
        Camera['ViewUP'] = [-0.95649115882992519, -0.15339430240733626, -0.24818309988626033]
        Camera['ClipRange'] =  [1.2049554155391839, 1204.9554155391838] 
        self.PlotterObj.Plot(Camera=Camera)
        
        print 'Nedded %.2f seconds'%(time.clock() - tStart)
        
        # Return no fail in study!
        # ------------------------------------------------------------------- #        
        return 'No Fail'  
        
class TubeVideo():
    
    def __init__(self,TubeObj,PlotterObj):
        self.TubeObj = TubeObj
        self.PlotterObj = PlotterObj

    def run(self,ResultChoice = 'Mesh'):             
        Camera = dict()
        Camera['Position'] = [-78.809399771969822, 165.4524112085162, 775.83485483341053]
        Camera['FocalPoint'] = [68.846315411589558, -47.507645843801079, 338.39762892491348]
        Camera['ViewAngle'] = 30.
        Camera['ViewUP'] = [-0.95649115882992519, -0.15339430240733626, -0.24818309988626033]
        Camera['ClipRange'] =  [1.2049554155391839, 1204.9554155391838] 
        # Definition of plot case
        # ------------------------------------------------------------------- #
        xList = [.5, .6, .7, .8, .9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5]        
        FigNum = 0
        for ix in xList:
            Nodes, Elements, nNodes, nElms = self.TubeObj.ReadMesh(ix)
            if ResultChoice == 'Mesh':
                self.PlotterObj.PassElementsNodesResults(Elements,Nodes)
            elif ResultChoice == 'TubeFI':
                Results = self.TubeObj.ReadElementResults(nElms)
                self.PlotterObj.PassElementsNodesResults(Elements,Nodes,Results=Results)
            self.PlotterObj.Plot(Camera=Camera,FigName='Beam%04i.png'%(FigNum),Video=True)
            FigNum += 1
        
        # Return no fail in study!
        # ------------------------------------------------------------------- #        
        return 'No Fail'  