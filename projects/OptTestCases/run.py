# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ # 
# Initialize logging and working dir
import logging, os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.getcwd()))) # Add eos folder to path
import eos
from eos import SimulationMaker
eos.SetupEosWorkDir(jobname='OptimizationTests') # create Working dir in eos\results
eos.StartLogger() # start logging
logging.info('Current work dir:"%s"' % os.getcwd())
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ # 

AnalysisList = ['RosenSuzuki', 'RosenSuzukiGSO', 'SingleOptTest', 'VectorOpt01', 'VectorOpt03']
AnalysisList = [ 'RosenSuzukiGSO' ]
AnalysisList = [ 'APSISPresizing', 'APSISNoPresizing', 'APSISRamp' ]
(ResultList, InfoList) = (list(), list())

# RosenSuzuki, RosenSuzukiGSO, SingleOptTest, VectorOpt01, VectorOpt02, VectorOpt03

for Analysis in AnalysisList:
    if Analysis == 'RosenSuzuki':
        from models.RosenSuzuki import RosenSuzuki
        TestModel = RosenSuzuki()
        TestSimulation = SimulationMaker(TestModel, LoggingLevel='INFO')
        from studies.RosenSuzuki import RosenSuzuki as OptTestStudy
        studyTest = OptTestStudy(TestSimulation)
        Results, InfoStr =  studyTest.run(AlgChoice = 'SLSQP') # SLSQP, NLPQLP, ALHSO, NSGA2, CONMIN
    elif Analysis == 'APSISPresizing':   # APSIS simulation is performed with Presizing
        from models.APSIS_model import APSISModel
        TestModel = APSISModel()
        TestSimulation = SimulationMaker(TestModel, LoggingLevel='INFO')
        from studies.APSIS_study import APSISStudy as OptTestStudy
        studyTest = OptTestStudy(TestSimulation)
        Results, InfoStr =  studyTest.run(AlgChoice = 'SLSQP', NoRamp = True, Const=0.4) # SLSQP, NLPQLP, ALHSO, NSGA2, CONMIN
    elif Analysis == 'APSISNoPresizing':   # APSIS simulation is performed without Presizing
        from models.APSIS_model import APSISModel
        TestModel = APSISModel()
        TestSimulation = SimulationMaker(TestModel, LoggingLevel='INFO')
        from studies.APSIS_study import APSISStudy as OptTestStudy
        studyTest = OptTestStudy(TestSimulation)
        Results, InfoStr =  studyTest.run(AlgChoice = 'SLSQP', NoPresizing = True, NoRamp = True, Const=0.4) # SLSQP, NLPQLP, ALHSO, NSGA2, CONMIN
    elif Analysis == 'APSISRamp':   # APSIS simulation is performed with Presizing
        from models.APSIS_model import APSISModel
        TestModel = APSISModel()
        TestSimulation = SimulationMaker(TestModel, LoggingLevel='INFO')
        from studies.APSIS_study import APSISStudy as OptTestStudy
        studyTest = OptTestStudy(TestSimulation)
        Results, InfoStr =  studyTest.run(AlgChoice = 'SLSQP', NoRamp = False, iterStart = 1, iterEnd = 7, NuStart = 0.05, NuEnd = 0.4) # SLSQP, NLPQLP, ALHSO, NSGA2, CONMIN    
    elif Analysis == 'RosenSuzukiGSO':
        from models.RosenSuzuki import RosenSuzuki
        TestModel = RosenSuzuki()
        TestSimulation = SimulationMaker(TestModel, LoggingLevel='INFO')
        from studies.RosenSuzuki import RosenSuzukiGSO as OptTestStudy
        studyTest = OptTestStudy(TestSimulation)
        Results, GSinfo, gInfo, InfoStr =  studyTest.run(AlgChoice = 'NLPQLP') # SLSQP, NLPQLP, ALHSO, NSGA2
    elif Analysis == 'SingleOptTest':
        from models.Test import Test
        OptModelObj = Test()
        TestSimulation = SimulationMaker(OptModelObj, LoggingLevel='INFO')
        from studies.Teststudy import OptTestStudy
        studyTest = OptTestStudy(TestSimulation)
        Results, InfoStr =  studyTest.run()
    elif Analysis == 'VectorOpt01':
        from models.VectorOpt import PereyraExp1
        OptModelObj = PereyraExp1()
        TestSimulation = SimulationMaker(OptModelObj, LoggingLevel='INFO')
        from studies.VectorStudy import VectorOpt as OptTestStudy
        studyTest = OptTestStudy(TestSimulation)
        Results, LocalResults, InfoStr =  studyTest.run()
    elif Analysis == 'RosenDesignSpacePlot':
        from models.RosenSuzuki import RosenSuzuki
        TestModel = RosenSuzuki()
        TestSimulation = SimulationMaker(TestModel, LoggingLevel='INFO')
        from studies.RosenSuzuki import RosenSuzukiContourPlot as PlotTestStudy
        studyTest = PlotTestStudy(TestSimulation)
        Results, InfoStr =  studyTest.run()
    elif Analysis == 'VectorOpt03':
        from models.VectorOpt import ThesisExpl
        OptModelObj = ThesisExpl()
        TestSimulation = SimulationMaker(OptModelObj, LoggingLevel='INFO')
        from studies.VectorStudy import ThesisOpt as OptTestStudy
        studyTest = OptTestStudy(TestSimulation)
        Results, LocalResults, InfoStr =  studyTest.run()
        
    ResultList.append(Results)
    InfoList.append(InfoStr)

print '----------------------'
print 'Verification runs done'
print '----------------------'
print 'Stats are:'
for iInfo in InfoList:
    print iInfo