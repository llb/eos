# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.
"""
Created on Wed Sep 02 11:08:34 2015

@author: m.koehler
"""

#!/usr/bin/env python
'''
    Solves Schittkowski's TP37 Constrained Problem.
    min 	-x1*x2*x3
    s.t.:	x1 + 2.*x2 + 2.*x3 - 72 <= 0
            - x1 - 2.*x2 - 2.*x3 <= 0
            0 <= xi <= 42,  i = 1,2,3
    
    f* = -3456 , x* = [24, 12, 12]

    Solves Pereyra VectorOpt Example Problem.
    Taken from:\n
    Deb, K.; Pratap, A.; Agarwal, S., Meyarivan T. [2002]:\n
    "A Fast and Elitist Multiobjective Genetic Algorithm: NSGA-II",\n
    IEEE TRANSACTIONS ON EVOLUTIONARY COMPUTATION, VOL. 6, NO. 2
    p. 193, Table V., Problem "CONSTR"
    
    Properties:
        - 2 inequality constraints
        - 2 design variables
        - 2 objectives
 '''

# =============================================================================
# Standard Python modules
# =============================================================================
import os, sys, time
import pdb
import matplotlib.pyplot as plt
import numpy as np
# =============================================================================
# Extension modules
# =============================================================================
#from pyOpt import *
from pyOpt import Optimization
from pyOpt import NSGA2


# =============================================================================
# 
# =============================================================================
def objfunc(x):
    
    f = -x[0]*x[1]*x[2]
    g = [0.0]*2
    g[0] = x[0] + 2.*x[1] + 2.*x[2] - 72.0
    g[1] = -x[0] - 2.*x[1] - 2.*x[2]
    
    fail = 0
    return f,g, fail
    
    
def Vecobjfunc(x):
    
    f = [0.0] * 2
    f[0] = x[0]                         # f1
    f[1] = (1+x[1])/x[0]                # f2
		
    g = [0.0] * 2
    g[0] = 1-9.0/6.0*x[0]-1.0/6.0*x[1]  # g1
    g[1] = 1-9.0*x[0]+x[1]              # g2
    
    return f, g, 0    
    
def ThesisExample(x):
    f = [x[0], x[1]]
    #f = [x[0]**2.0-x[1], x[1]**2.0-x[0]]
    g = [ +(x[0]-5.0)**2.0 +(x[1]-5.0)**2.0 -16.0, x[0]-4.0, x[1]-4.0]
    return f, g, 0 # f - List, g - List
    
def ThesisAnalytical(f1):
    return 5.-np.sqrt(16.0-(f1-5.0)**2.0)

# =============================================================================
# 
# =============================================================================

ObjType='ThesisExpl'

# Instantiate Optimization Problem 
if ObjType=='Single':
    opt_prob = Optimization('TP37 Constrained Problem',objfunc)
    opt_prob.addVar('x1','c',lower=0.0,upper=42.0,value=10.0)
    opt_prob.addVar('x2','c',lower=0.0,upper=42.0,value=10.0)
    opt_prob.addVar('x3','c',lower=0.0,upper=42.0,value=10.0)
    opt_prob.addObj('f')
    opt_prob.addCon('g1','i')
    opt_prob.addCon('g2','i')
elif ObjType=='Vector':
    opt_prob = Optimization('Pereyra VectorOpt Problem',Vecobjfunc)
    opt_prob.addVar('x1','c',lower=0.0,upper=42.0,value=10.0)
    opt_prob.addVar('x2','c',lower=0.0,upper=42.0,value=10.0)
    opt_prob.addObj('f1')
    opt_prob.addObj('f2')
    opt_prob.addCon('g1','i')
    opt_prob.addCon('g2','i')
elif ObjType=='ThesisExpl':
    opt_prob = Optimization('Pereyra VectorOpt Problem',ThesisExample)
    opt_prob.addVar('x1','c',lower=0.0,upper=5.0,value=5.0)
    opt_prob.addVar('x2','c',lower=0.0,upper=5.0,value=5.0)
    opt_prob.addObj('f1')
    opt_prob.addObj('f2')
    opt_prob.addCon('g1','i')
    opt_prob.addCon('g2','i')
    opt_prob.addCon('g3','i')
    
# print Opt problem information    
print opt_prob

# Instantiate Optimizer (NSGA2) & Solve Problem
nsga2 = NSGA2(options={'PrintOut':1,'PopSize':100,'maxGen':20})
#nsga2.setOption('PrintOut',1,'PopSize',100,'maxGen',10)
[fOpt , xOpt , OptInfo] = nsga2(opt_prob,store_sol=True,store_hst='OptHist',hot_start=False)
print opt_prob.solution(0)



# read in best results
f1=list()
f2=list()
f=open('nsga2_best_pop.out', mode = 'r')
lines = f.readlines()
for iLine in lines:
    if not iLine[0]=="#":
        SplitLine=iLine.split()
        f1.append(SplitLine[0])
        f2.append(SplitLine[1])
# close file
f.close()
F1Array = np.linspace(5.0-np.sqrt(15.), 4.0, num=50)
plt.scatter(f1,f2)
plt.plot(F1Array,[ThesisAnalytical(iF1) for iF1 in F1Array], 'r')
plt.ylabel('f2')
plt.xlabel('f1')
plt.axis([1.0, 4.0, 1.0, 4.0])
plt.savefig('ParetoFront.png')

AnalyticalFront = ''
for iF1 in F1Array:
    AnalyticalFront += '                           (%.4f, %.4f)\n'%(iF1,ThesisAnalytical(iF1))
    
ComputedFront = ''
for if1, if2 in zip(f1, f2):
    ComputedFront += '                           (%.4f, %.4f)\n'%(float(if1), float(if2))