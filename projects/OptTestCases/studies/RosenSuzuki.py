# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import pickle

class RosenSuzuki():
    def __init__(self,Simulation):
        self.Simulation = Simulation

    def run(self,AlgChoice = 'NLPQLP'):             
        # Definition of the test optimization problem
        # ------------------------------------------------------------------- #
        GradAlgs = ('SLSQP', 'NLPQLP')
        Options = dict()
        Options['ng'] = 3
        Options['GradCheck'] = True
        Options['ConvergencePlots'] = True # default is inactive, i.e. False!
        Options['Normalization'] = True
        x0=np.array([1.5, 1.5, 1.5, 1.5]).T
        xL=np.array([-3.0, -3.0, -3.0, -3.0]).T
        xU=np.array([ 3.0,  3.0,  3.0,  3.0]).T
        
        # Perform gradient check
        # ------------------------------------------------------------------- #   
        if AlgChoice in GradAlgs:
            try:
                fFD, gFD, df, dg = self.Simulation.Optimize(x0,xL,xU,Norm=True,Options=Options)
            except:
                return [], 'Study failed checking gradients of optimization model'
    
            # Output results of gradient check        
            # --------------------------------------------------------------- #
            print 'Error in objective sensitivities:'
            print fFD-df
            print 'Error in objective sensitivities:'
            print gFD-dg
        
        # Define test optimization
        # ------------------------------------------------------------------- #
        Options['GradCheck'] = False # default option!
        try:
            if AlgChoice == 'SLSQP':
                Results = self.Simulation.Optimize(x0,xL,xU,Norm=True,Options=Options)
            elif AlgChoice == 'NLPQLP':
                Results = self.Simulation.Optimize(x0,xL,xU,Alg='NLPQLP',Norm=True \
                    ,Options=Options,MAXFUN=30,ACC=1e-5,STPMIN=1e-6,ACCQP=1e-8)
            elif AlgChoice == 'CONMIN':
                Results = self.Simulation.Optimize(x0,xL,xU,Alg='CONMIN',Norm=True \
                    ,Options=Options)
            elif AlgChoice == 'ALHSO':
                Results = self.Simulation.Optimize(x0,xL,xU,Alg='ALHSO',Norm=True \
                    ,Options=Options)
            elif AlgChoice == 'NSGA2':
                Results = self.Simulation.Optimize(x0,xL,xU,Alg='NSGA2',Norm=True \
                    ,Options=Options,PopSize=40,maxGen=80)
        except:
            return [], 'Study failed in test optimization'
        
        # Output results of plain test optimization
        # ------------------------------------------------------------------- #
        for (ResultType, Value) in Results.iteritems():
            print ResultType+' is:'
            print Value
        pickle.dump(Results,open('OptResults.pkl','wb'))
        
        # Print optimality infos
        # ------------------------------------------------------------------- #
        if AlgChoice in GradAlgs:
            print '1st order optimality'
            print Results['LagrangianData']['1st Order Opt']
            print 'Lambdas have been computed to be:'
            for (Lam, Des) in zip(Results['LagrangianData']['Lambda Values'], Results['LagrangianData']['Lambda Description']):
                print Des, ': ', Lam
            print 'Lambdas have been computed with following residual error:'
            print Results['LagrangianData']['Lambda Residuals']
        
        # Return no fail in study!
        # ------------------------------------------------------------------- #        
        return Results, 'No Fail: Shadow prices with convergence plotting works'  
        
class RosenSuzukiContourPlot():
    def __init__(self,Simulation):
        self.Simulation = Simulation

    def run(self):             
        # Definition of the test optimization problem
        # ------------------------------------------------------------------- #
        xOpt=np.array([0., 1.0, 2.0, -1.0]).T
        xL=np.array([-3.0, -3.0, -3.0, -3.0]).T
        xU=np.array([ 3.0,  3.0,  3.0,  3.0]).T
        
        # Perform gradient check
        # ------------------------------------------------------------------- #   
        DVIndicies = [1,3]
        self.Simulation.PlotDesignSpace(xOpt, DVIndicies, xU, xL, NumSup = 100., NumConstr = 6)
        
        # Return no fail in study!
        # ------------------------------------------------------------------- #        
        return {'Info': 'No Results'}, 'No Fail: Shadow prices with convergence plotting works'  
        
class RosenSuzukiGSO():
    def __init__(self,Simulation):
        self.Simulation = Simulation

    def run(self,AlgChoice = 'NLPQLP'):             
        # Definition of the test optimization problem
        # ------------------------------------------------------------------- #
        Options = dict()
        Options['ng'] = 3
        Options['ConvergencePlots'] = False # default is inactive, i.e. False!
        Options['Normalization'] = True
        xL=np.array([-3.0, -3.0, -3.0, -3.0]).T
        xU=np.array([ 3.0,  3.0,  3.0,  3.0]).T
        nDoE = 50
        (xGS, gInfo) = self.Simulation.GlobalSearchForDesOpt( xL, xU, nDoE = nDoE, nGS = 5)
        print('\n-- Found following x0 vectors where %i out of %i where infeasible:'%(len([g for g in gInfo if "Infeasible" in g]),nDoE))
        '''        
        xGS=np.array([[1.5, 1.5, 1.5, 1.5], [-3.0, -3.0, -3.0, -3.0],
                     [-3., -3., 3.0, 3.0], [ 3.0,  3.0,  3.0,  3.0],
                     [0.5, 2.5, 0.5, 2.5], [-1.0, -2.0, -1.0, -2.0]]).T
        '''
            
        # Define test optimization
        # ------------------------------------------------------------------- #
        try:
            if AlgChoice == 'SLSQP':
                Results, GSinfo = self.Simulation.Optimize(xGS,xL,xU,Norm=True,Options=Options)
            elif AlgChoice == 'NLPQLP':
                Results, GSinfo = self.Simulation.Optimize(xGS,xL,xU,Alg='NLPQLP',Norm=True \
                    ,Options=Options,MAXFUN=30,ACC=1e-5,STPMIN=1e-6,ACCQP=1e-8)
            elif AlgChoice == 'ALHSO':
                Results, GSinfo = self.Simulation.Optimize(xGS,xL,xU,Alg='ALHSO',Norm=True \
                    ,Options=Options)
            elif AlgChoice == 'NSGA2':
                Results, GSinfo = self.Simulation.Optimize(xGS,xL,xU,Alg='NSGA2',Norm=True \
                    ,Options=Options,PopSize=40,maxGen=80)
        except:
            return [], [], 'Study failed in test optimization'
        
        # Output results of plain test optimization
        # ------------------------------------------------------------------- #
        print GSinfo
        
        # Return no fail in study!
        # ------------------------------------------------------------------- #        
        return Results, GSinfo, gInfo, 'No fail: Global search works'  