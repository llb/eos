# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# Needed imports:
import numpy as np
import pickle

# CLASS DEFINITION:
class VectorOpt():
    def __init__(self,Simulation):
        self.Simulation = Simulation

    def run(self):
             
        # Optimization test
        # ------------------------------------------------------------------- #
        xL = np.array([.1, 0.0])                    # Lower boundary of design space
        xU = np.array([1.0, 5.0])                   # Upper boundary of design space
        x0 = np.array([0.5, 0.5])                   # Initial design (is overwritten in case of global search!)
        
        AlgChoice = 'NLPQLP'
        
        # Definition of model parameters
        Options = dict()
        Options['nf'] = 2                           # Number of objectives (default: 1)
        Options['nh'] = 0                           # Number of equality constraints (default: 0)
        Options['ng'] = 2                           # Number of inequality constraints (default: 0)

        # General options
        Options['DVTypes']          = ['c','c']     # List of types of design variables (default: 'c' = all continous)
        Options['GradCheck']        = False         # Perform gradient counter check (default: False)
        Options['FDTol']            = 1E-4          # Tolerance in finite difference counter check (default: 1E-4)
        Options['Alg']              = AlgChoice     # Algorithm choice (default: 'SLSQP')
        Options['Normalization']    = True          # Normalization of x design space(default: True)
        
        # Vector optimization options
        VecOptions = dict()
        VecOptions['NumParetoOptima']  = 8          # Number of Pareto points (default: 10)
        VecOptions['ChordFactor']      = 1.15       # Chord factor (default: 1.25)
        VecOptions['GS']               = True       # Performing global optimum search (default: False)
        VecOptions['nDoE']             = 80         # Number of experimental designs (default: 10)
        VecOptions['nGS']              = 10         # Number of chosen designs to optimize on (default: 2)
        VecOptions['RefinedGS']        = True       # Refined global search (area close to previous optimum)
        VecOptions['NormalizeF']       = True       # Normalization of cost function through normalized objectives (default: True)
        VecOptions['fPrevInit']        = 2          # Index of solution from which continuation starts (1: f1,opt 2: f2,opt 3:...)
        VecOptions['ConvergencePlots'] = False      # Flag to generate convergence plots
        VecOptions['AnchorPoints']     = {'xAnc': np.array([[1.,0.],[0.3889, 2.5]])}
        
        Options['VecOptions'] = VecOptions          # Reduce transfers
        
        # Piped algorithm options (**kwargs)
        # examplarily: SLSQP
        if AlgChoice == 'SLSQP':
            AlgOptions = dict()
            AlgOptions['MAXIT'] = 60                # Maximum number of outer iterations
            AlgOptions['ACC']   = 1e-4              # Convergence accuracy

        # examplarily: NLPQLP
        if AlgChoice == 'NLPQLP':
            AlgOptions = dict()
            AlgOptions['MAXIT'] = 150               # Maximum number of outer iterations
            AlgOptions['MAXFUN'] = 20               # Maximum number of function calls during line search
            AlgOptions['ACC'] = 1e-7                # Convergence accuracy
        
        # Optimize
        # ------------------------------------------------------------------- #
        try:
            Results,LocalResults,plotfig = self.Simulation.Optimize(x0, xL, xU, 
                                               Alg        = AlgChoice, 
                                               Norm       = Options['Normalization'],
                                               Options    = Options,
                                               **AlgOptions)
        except:
            return [], [], 'Study failed in vector optimization'
        
        # Return no fail in study!
        # ------------------------------------------------------------------- #        
        return Results, LocalResults, 'No Fail: Shadow prices with convergence plotting works' 
        
class ThesisOpt():
    def __init__(self,Simulation):
        self.Simulation = Simulation

    def run(self):
             
        # Optimization test
        # ------------------------------------------------------------------- #
        xL = np.array([0.,  0.])                    # Lower boundary of design space
        xU = np.array([4.,  4.])                   # Upper boundary of design space
        x0 = np.array([4.,  4.])                   # Initial design (is overwritten in case of global search!)
        
        AlgChoice = 'SLSQP'
        
        # Definition of model parameters
        Options = dict()
        Options['nf'] = 2                           # Number of objectives (default: 1)
        Options['nh'] = 0                           # Number of equality constraints (default: 0)
        Options['ng'] = 3                           # Number of inequality constraints (default: 0)

        # General options
        Options['DVTypes']          = ['c','c']     # List of types of design variables (default: 'c' = all continous)
        Options['GradCheck']        = False         # Perform gradient counter check (default: False)
        Options['FDTol']            = 1E-4          # Tolerance in finite difference counter check (default: 1E-4)
        Options['Alg']              = AlgChoice     # Algorithm choice (default: 'SLSQP')
        Options['Normalization']    = True          # Normalization of x design space(default: True)
        
        # Vector optimization options
        VecOptions = dict()
        VecOptions['NumParetoOptima']  = 8          # Number of Pareto points (default: 10)
        VecOptions['ChordFactor']      = 1.22       # Chord factor (default: 1.25)
        VecOptions['GS']               = False      # Performing global optimum search (default: False)
        VecOptions['nDoE']             = 40         # Number of experimental designs (default: 10)
        VecOptions['nGS']              = 4          # Number of chosen designs to optimize on (default: 2)
        VecOptions['NormalizeF']       = False       # Normalization of cost functions through objectives (default: True)
        VecOptions['fPrevInit']        = 2          # Normalization of cost functions through objectives (default: True)
        
        Options['VecOptions'] = VecOptions          # Reduce transfers
        
        # Piped algorithm options (**kwargs)
        # examplarily: SLSQP
#        AlgOptions = dict()
#        AlgOptions['MAXIT'] = 60                    # Maximum number of outer iterations
#        AlgOptions['ACC']   = 1e-4                  # Convergence accuracy

        # examplarily: NLPQLP
#        AlgOptions = dict()
#        AlgOptions['MAXIT'] = 150                  # Maximum number of outer iterations
#        AlgOptions['MAXFUN'] = 20                  # Maximum number of function calls during line search
#        AlgOptions['ACC'] = 1e-7                   # Convergence accuracy
        
        # Optimize
        # ------------------------------------------------------------------- #
        Results,LocalResults,plotfig = self.Simulation.Optimize(x0, xL, xU, 
                                               Alg        = AlgChoice, 
                                               Norm       = Options['Normalization'],
                                               Options    = Options)
                                               ##,
                                               ##**AlgOptions)
                                               
        # Post process
        # ------------------------------------------------------------------- #
        def AnaSol(f1):
            return 5.0-np.sqrt(16.0-(f1-5.0)**2.0)
        def AnaDer(f1):
            return (f1-5.0)/np.sqrt(16.0-(f1-5.0)**2.0)
        F1List = Results['Summary']['f_opt'][:,0]
        F2Ana = [AnaSol(if1) for if1 in F1List]
        KappaAna =  np.asarray([AnaSol(if1)/(if1+AnaSol(if1)) for if1 in F1List])
        FPareto = Results['Summary']['f_opt']
        Utility = Results['Summary']['u_opt']
        KappaPer = np.asarray([(iU-iPar[1])/(iPar[0]-iPar[1]) for iU, iPar in zip(Utility, FPareto)])
        dF2dF1Kappa = [ iKap/(1.0-iKap) for iKap in KappaAna ]
        dF2dF1Ana =  np.asarray([AnaDer(if1) for if1 in F1List])
        (dF2dF1KappaStr, dF2dF1AnaStr, KappaAnaStr, ErrordF2dF1) = ('', '', '', '')
        for idF2dF1Kappa, idF2dF1Ana, iKappaAna, iF1 in zip(dF2dF1Kappa, dF2dF1Ana, KappaAna, F1List):
            dF2dF1KappaStr += '                           (%.4f, %.4f)\n'%(iF1,idF2dF1Kappa)
            dF2dF1AnaStr += '                           (%.4f, %.4f)\n'%(iF1,idF2dF1Ana)
            KappaAnaStr += '                           (%.4f, %.4f)\n'%(iF1,iKappaAna)
            ErrordF2dF1 += '                           (%.4f, %.4f)\n'%(iF1,abs(idF2dF1Kappa-idF2dF1Ana)/abs(idF2dF1Ana)*100.)
            
        Results.update({'KappaAna': KappaAna, 'KappaPer': KappaPer, 'dF2dF1Kappa': dF2dF1Kappa, \
            'dF2dF1Ana': dF2dF1Ana, 'F2Ana': F2Ana, 'dF2dF1KappaStr': dF2dF1KappaStr, 'dF2dF1AnaStr': dF2dF1AnaStr, \
            'KappaAnaStr': KappaAnaStr, 'ErrordF2dF1': ErrordF2dF1})
        
        
        # Plot results
        # ------------------------------------------------------------------- #
        import matplotlib.pyplot as plt
        import pylab
        
        ax = plotfig.get_axes()[-1]
        ax.plot(F1List, F2Ana,'-b')
        
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_axis_bgcolor('white')
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        plt.title('Kappas')
        ax.plot(F1List, dF2dF1Kappa, '-r')  # PLot line
        ax.plot(F1List, dF2dF1Ana, '-b')  # PLot line
        ax.plot(F1List, [abs(idF2dF1Kappa-idF2dF1Ana)/abs(idF2dF1Ana)*100. for (idF2dF1Kappa, idF2dF1Ana) in zip(dF2dF1Kappa, dF2dF1Ana)], '-k')  # PLot line
                                               
        
            
        # Output results of plain test optimization
        # ------------------------------------------------------------------- #
        pickle.dump(Results, open('OptResults.pkl','wb'))
        
        # Return no fail in study!
        # ------------------------------------------------------------------- #        
        return Results, LocalResults, 'No Fail: Vector test problem "ThesisExpl" works smoothly' 
        
