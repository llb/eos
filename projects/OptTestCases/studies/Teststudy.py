# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

class OptTestStudy():
    def __init__(self,Simulation):
        self.Simulation = Simulation

    def run(self):             
        # Definition of the test optimization problem
        # ------------------------------------------------------------------- #
        Options = dict()
        Options['ng'] = 2
        Options['GradCheck'] = True
        Options['ConvergencePlots'] = False
        x0=np.array([[1.5, 1.5]]).T
        xL=np.array([-3.0, -3.0]).T
        xU=np.array([ 3.0,  3.0]).T
        
        # Perform gradient check
        # ------------------------------------------------------------------- #        
        try:
            fFD, gFD, df, dg = self.Simulation.Optimize(x0,xL,xU,Norm=True,Options=Options)
        except:
            return 'Study failed checking gradients of optimization model'

        # Output results of gradient check        
        # ------------------------------------------------------------------- #
        print 'Error in objective sensitivities:'
        print fFD-df
        print 'Error in objective sensitivities:'
        print gFD-dg
        
        # Define test optimization
        # ------------------------------------------------------------------- #
        Options['GradCheck'] = False # default option!
        try:
            OptResults = self.Simulation.Optimize(x0,xL,xU,Norm=True,Options=Options)
        except:
            return 'Study failed in constraint test optimization'
        
        # Define test optimization
        # ------------------------------------------------------------------- #
        Options['ng'] = 0
        try:
            OptResults = self.Simulation.Optimize(x0,xL,xU,Norm=True,Options=Options)
        except:
            return 'Study failed in unconstraint test optimization'
        
        # Output results of plain test optimization
        # ------------------------------------------------------------------- #
        for (ResultType, Value) in OptResults.iteritems():
            print ResultType+' is:'
            print Value
        xOpt = OptResults['xOpt']
        
        # Modifying test model object by changing a parameter
        # ------------------------------------------------------------------- #
        print 'Modify simulation model -> vector optimization!'
        self.Simulation.Model.P[0] += 10.0
        
        # Simple evaluation test
        # ------------------------------------------------------------------- #        
        try:
            print self.Simulation.Eval(xOpt)
        except:
            return 'Study failed in evaluating modified test simulation model'
        
        # Simple sensitivity study
        # ------------------------------------------------------------------- #        
        try:
            print self.Simulation.SensitivityStudy(xOpt)
        except:
            return 'Study failed in performing sensitivity study on modified test simulation model'        
        
        # RSA options
        # ------------------------------------------------------------------- #
        NumberSamplePoints = 15
        RSAlower = xL
        RSAupper = xU
        
        # Perform DoE
        # ------------------------------------------------------------------- #
        try:
            xDoE = self.Simulation.PerformDoE(NumberSamplePoints,RSAlower,RSAupper,SamplingMethod='LHS')
        except:
            return 'Study failed in generating DoE - Design of Experiments' 
            
        # Perform RSA
        # ------------------------------------------------------------------- #
        try:
            RSAmodel = self.Simulation.GetRSAmodel(xDoE,fDoE=None,RSAMethod='PolyReg')
        except:
            return 'Study failed in performing RSA computation' 
        
        # Print RSA info of response [0]
        # ------------------------------------------------------------------- #
        print RSAmodel[0]
        
        # Evaluate RSA of response [0]
        # ------------------------------------------------------------------- #
        print RSAmodel[0].predict(xOpt)
        
        # Return no fail in study!
        # ------------------------------------------------------------------- #        
        return 'All resutls in python shell', 'No fail: Standard optimization tool do work'  