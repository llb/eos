# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.
"""
Created on Wed Nov 25 10:15:09 2015

@author: s.flach
"""

import numpy as np
import pickle

class APSISStudy():
    def __init__(self,Simulation):
        self.Simulation = Simulation
        
    def run(self, AlgChoice = 'SLSQP', NoPresizing = False, NoRamp = True, Const = 0.8, iterStart = 1, iterEnd = 7, NuStart = 0.05, NuEnd = 0.4):             
        # Definition of the test optimization problem
        # ------------------------------------------------------------------- #
        Options = dict()
        Options['ng'] = 3
        Options['GradCheck'] = False
        Options['ConvergencePlots'] = True # default is inactive, i.e. False!
        Options['Normalization'] = True
        Options['DVTypes']  = ['c','i', 'c', 'd']    # specifying the types of design variables
        
        # specifying the value choices for the discrete design variables
        Options['DiscreteDVChoices'] = dict()
        Options['DiscreteDVChoices']['1'] = [-2.4,-1.8,-0.2, 0.4, 1.9]        
        
        # specifying a constant weighting of the penalty term
        Options['Ramp'] = dict()
        if NoRamp: # no ramping required, so constant weighting factor       
            Options['Ramp']['Const'] = Const  # specifying a constant weighting of the penalty term       
        else: # ramping required with iterStart, iterEnd, NuStart and NuEnd
            Options['Ramp']['iterStart']= iterStart
            Options['Ramp']['iterEnd']  = iterEnd
            Options['Ramp']['NuStart']  = NuStart
            Options['Ramp']['NuEnd']    = NuEnd
        # initial values for all design vriables, upper and lower bounds for continuous and integer design variables        
        x0=np.array([1.5, -2., 1.5, 0.4]).T
        xL=np.array([-3.0, -3.0, -3.0]).T
        xU=np.array([ 3.0,  3.0,  3.0]).T

        Options['NoPresizing'] = NoPresizing  # presizing is required with False, this is the default option
        
        # Define test optimization
        # ------------------------------------------------------------------- #
        Options['GradCheck'] = False # default option!
        try:
            if AlgChoice == 'SLSQP':
                Results = self.Simulation.Optimize(x0,xL,xU,Norm=True,Options=Options)
        except:
            return [], 'Study failed in test optimization'
        
        # Output results of plain test optimization
        # ------------------------------------------------------------------- #
        for (ResultType, Value) in Results.iteritems():
            print ResultType+' is:'
            print Value
        pickle.dump(Results,open('OptResults.pkl','wb'))
        return Results, 'No Fail: APSIS optimization including convergence plotting works'  
        
