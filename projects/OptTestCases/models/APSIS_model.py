# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

"""
Created on Tue Nov 24 14:00:21 2015

@author: s.flach
"""

import numpy as np

class APSISModel():
    
    def __init__(self):
        pass
    
    def ComputeResp(self,x):
        pass        
        
    def ObjAndConstrFct(self,x):

        f = x[0]**2 - 5.*x[0] + x[1]**2 - 5.*x[1] + 2.*x[2]**2 - 21.*x[2] + x[3]**2 + 7.0*x[3] + 50.
        g = [0.0]*3
        g[0] = x[0]**2 + x[0] + x[1]**2 - x[1] + x[2]**2 + x[2] + x[3]**2 - x[3] - 8.0
        g[1] = x[0]**2 - x[0] + 2. * x[1]**2 + x[2]**2 + 2.*x[3]**2 - x[3] - 10.0
        g[2] = 2.*x[0]**2 + 2.*x[0] + x[1]**2 - x[1] + x[2]**2 - x[3] -5.0
        
        fail = 0
        
        return f,g,fail
        
    def ObjAndConstrSensFct(self,x,f,g):
        
        g_obj = [0.0]*4
        g_obj[0] = 2.*x[0] - 5
        g_obj[1] = 2.*x[1] - 5
        g_obj[2] = 4.*x[2] - 21
        g_obj[3] = 2.*x[3] + 7
        
        g_con = np.zeros([3,4])
        g_con[0][0] = 2.*x[0] + 1
        g_con[0][1] = 2.*x[1] - 1
        g_con[0][2] = 2.*x[2] + 1
        g_con[0][3] = 2.*x[3] - 1
        g_con[1][0] = 2.*x[0] - 1
        g_con[1][1] = 4.*x[1]
        g_con[1][2] = 2.*x[2]
        g_con[1][3] = 4.*x[3] - 1
        g_con[2][0] = 4.*x[0] + 2
        g_con[2][1] = 2.*x[1] - 1
        g_con[2][2] = 2.*x[2]
        g_con[2][3] = -1.
        
        fail = 0
        
        return g_obj,g_con,fail