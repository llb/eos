# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

"""
Created on Fri Nov 06 12:49:14 2015

@author: s.roedl
"""

# Newton Raphson
import numpy as np

def f(x): return np.sin(x), np.cos(x)

tolNR, imaxNR, x = 1e-4, 20, (2./4.)*np.pi
for i in range(imaxNR):
    F, dF = f(x)
    if abs(F) < tolNR: break
    else: x -= np.divide(F, dF)
print 'i =', i
print 'x =', x
print 'F =', F
print 'dF =', dF