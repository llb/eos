# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# Important imports
# --------------------------------------------------------------------------- #
import numpy as np

# Definition of simulation model classes
# --------------------------------------------------------------------------- #
class PereyraExp1:
    '''
    Taken from:\n
    Deb, K.; Pratap, A.; Agarwal, S., Meyarivan T. [2002]:\n
    "A Fast and Elitist Multiobjective Genetic Algorithm: NSGA-II",\n
    IEEE TRANSACTIONS ON EVOLUTIONARY COMPUTATION, VOL. 6, NO. 2
    p. 193, Table V., Problem "CONSTR"
    
    Properties:
        - 2 inequality constraints
        - 2 design variables
        - 2 objectives
    '''
    def ComputeResp(self,x):
        pass
        
    def ComputeSens(self,x):
        pass
        
    def ObjAndConstrFct(self,x):
        f = [0.0] * 2
        f[0] = x[0]                         # f1
        f[1] = (1+x[1])/x[0]                # f2
		
        g = [0.0] * 2
        g[0] = 1-9.0/6.0*x[0]-1.0/6.0*x[1]  # g1
        g[1] = 1-9.0*x[0]+x[1]              # g2
        
        return f, g, 0
        
    def ObjAndConstrSensFct(self,x,f,g):
        df = np.zeros((2,2))
        df[0,0] = 1.0                      # df1/dx1
        df[0,1] = 0.0                      # df1/dx2
        df[1,0] = -(1.0+x[1])*x[0]**(-2.0) # df2/dx1
        df[1,1] = 1.0/x[0]                 # df2/dx2
        
        dg = np.zeros((2,2))
        dg[0,0] = -9.0/6.0			   # dg1/dx1
        dg[0,1] = -1.0/6.0			   # dg1/dx2
        dg[1,0] = -9.0				   # dg2/dx1
        dg[1,1] = 1.0				   # dg2/dx2
        
        return df, dg, 0
        
    def IterationUserOutput(self,IterData):
        if IterData['LineSearchDone']:
            # ODB
            print '    -> line search done (%i evals in total)\n Iteration: %i Evaluation: %i'%(IterData['TotalCalls'], IterData['IterationNum'],IterData['EvalNum'])
        else:
            print ' Iteration: %i Evaluation: %i'%(IterData['IterationNum'],IterData['EvalNum'])
        
        
        
class ThesisExpl:
    def ComputeResp(self,x):
        pass
        
    def ComputeSens(self,x):
        pass
        
    def ObjAndConstrFct(self,x):
        f = [x[0], x[1]]
        #f = [x[0]**2.0-x[1], x[1]**2.0-x[0]]
        g = [ +(x[0]-5.0)**2.0 +(x[1]-5.0)**2.0 -16.0, x[0]-4.0, x[1]-4.0]
        return f, g, 0 # f - List, g - List
        
#    def ObjAndConstrSensFct(self,x,f,g):
#        df = np.array([[1.0, 0.], [0., 1.0]])
#        dg = np.array([[-2.0*x[0], -2.0*x[1]], [1.0, 0.], [0., 1.0]])
#        return (df, dg, 0) # df - List (Array if vector opt!), g - Array
        
        
class SimpleExp:
    def ComputeResp(self,x):
        pass
        
    def ComputeSens(self,x):
        pass
        
    def ObjAndConstrFct(self,x):
        f = [x[0]+x[1]*5.0, x[0]**2+x[1]**3]
        g = [1-(9.0*x[0]+x[1])/6.0, 1-(9.0*x[0]-x[1])]
        return f, g, 0 # f - List, g - List
        
    def ObjAndConstrSensFct(self,x,f,g):
        df = np.array([[1.0, 5.0],[2.0*x[0], 3.0*x[1]**2]])
        dg = np.array([[-9.0/6.0, -1.0/6.0],[-9.0, 1.0]])
        return (df, dg, 0) # df - List (Array if vector opt!), g - List