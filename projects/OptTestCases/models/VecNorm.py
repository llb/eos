# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

"""
Created on Tue Aug 25 08:22:19 2015

@author: schatz
"""

import numpy as np

# Thesis solution

def f2Sol(f1):
    return 5.0 - np.sqrt(16.0-(f1-5.0)**2.0)
    
FParetoList = list()

for iRes in Results[:-1]:
    FParetoList.append(iRes['fOpt'])
    
Error = list()

for iPareto in FParetoList:
    Error.append( (f2Sol(iPareto[0])-iPareto[1])/f2Sol(iPareto[0]) )