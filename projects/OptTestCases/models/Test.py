# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# Import needed modules
# --------------------------------------------------------------------------- #
import numpy as np

# Definition of simulation model class
# --------------------------------------------------------------------------- #
class Test:
    P = [0.0, 2.0]
    
    def __init__(self):
        from eos.MetaTools import SampleData
        self.SampleData = SampleData(SampleDataFilename=self.__class__.__name__+'SampleData.p')
    
    def ComputeResp(self,x):
        Resp = np.array([x[0]+x[1]+self.P[0], self.P[0]+x[0]*2.0+x[1]*2.0, 2.0*x[0]+x[1]**2.0-3.0])
        self.SampleData.StoreEvaluation( x, Resp)
        return Resp
        
    def ComputeSens(self,x):
        return np.array([[1.0, 1.0],[2.0, 2.0],[2.0, 2.0*x[1]]])
        
    def ObjAndConstrFct(self,x):
        Resp = np.asarray(self.ComputeResp(x))
        f = Resp[0]
        g = Resp[1:]
        return f, g, 0 # f - List, g - List
        
    def ObjAndConstrSensFct(self,x,f,g):
        Sens = self.ComputeSens(x)
        df = Sens[0,:]
        dg = Sens[1:,:]
        return (df.tolist(), dg, 0) # df - List (Array if vector opt!), g - Array