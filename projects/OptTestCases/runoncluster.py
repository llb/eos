# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

"""
This Script is intended to start a SLURM Cluster Job of an EOS project from a Windows Workstation.
Just copy it to your project folder (next to run.py), and run the file.

You have to save your Password to a Text file in C:\Users\[username]\eos_pass.txt
"""


import os
import logging
import sys
import getpass

# set the user name
username = getpass.getuser()

# read the password from C:\Users\[username]\eos_pass.txt
pwfile = "C:\\Users\\" + username + "\\eos_pass.txt"
if not os.path.isfile(pwfile):
    sys.exit('Please save your password in "%s" - Exiting' % pwfile)
else:
    with open(pwfile, "r") as f:
        password = f.read().replace('\n', '').replace('\r\n', '')


if os.path.isfile('cluster.log'):
    os.remove('cluster.log')
logging.basicConfig(filename='cluster.log', format='%(asctime)s - %(levelname)s:  %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)


cwd = os.getcwd()
logging.info('----------------------------------------')
logging.info('------  Starting Cluster Job  ----------')
logging.info('----------------------------------------')
logging.debug('Current work dir: "%s"' % cwd)


# Add eos folder to Path
EosRootPath = os.path.dirname(os.path.dirname(os.getcwd()))
sys.path.append(EosRootPath)
logging.info('Appended Path "%s"' % EosRootPath)

from eos.eosSSHClient import eosSSHClient


projectspath = os.path.join(EosRootPath, 'projects')
projectname = os.path.split(os.getcwd())[-1]

ssh = eosSSHClient(username=username, password=password)

# Upload EOS
ssh.put_dir_zipped(rootdir=EosRootPath, foldername='eos')

# Upload the Project folder
ssh.put_dir_zipped(rootdir=projectspath, foldername=projectname, subfolder='projects')

# Write the Slurm script to the job folder
slurm_script = ssh.put_slurm_script(UseXvfb=True)

# Submit the Job
command = 'cd ' + ssh.jobdir + '; sbatch ' + slurm_script + '; smap -c | grep ' + username + ';'

logging.info('running command "%s"' % command)
stdin, stdout, stderr = ssh.exec_command(command)

# write the ssh output to the console
data = stdout.readlines()
for line in data:
    print line

ssh.close()