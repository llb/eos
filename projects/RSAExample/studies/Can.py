# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

"""
Created on Wed Mar 11 16:05:43 2015


"""

import numpy as np
import pickle
import random



x0=np.array([2000., 2000.]).T # Start Point
xL=np.array([500., 500.]).T # Lower Boundary 
xU=np.array([2000., 2000.]).T # Upper Boundary 

x03d=np.array([2000., 2000., 2000.]) # Start Point
xL3d=np.array([500., 500., 500.]) # Lower Boundary 
xU3d=np.array([2000., 2000., 2000.]) # Upper Boundary 

NumberSamplePoints = 40 # Sampling points for RSA-Computation


class CanComputeRSA():
    
    def __init__(self, Simulation):    
      
        self.Simulation = Simulation
                
    def run(self):
        
           
        #     Perform DoE
        #    ------------------------------------------------------------------- #
        try:
            xDoE = self.Simulation.PerformDoE(NumberSamplePoints,xL,xU,SamplingMethod='LHS')
            pickle.dump(xDoE, open('xDoE.pkl', 'wb'))
            
        except:
            return 'Study failed in generating DoE - Design of Experiments' 
       
        # Perform RSA
        # ------------------------------------------------------------------- #
       
     
        try:
            fDoE = self.Simulation.GetfDoE (xDoE)
            RSAmodel = self.Simulation.GetRSAmodel(xDoE,fDoE=fDoE,RSAMethod='PolyReg')
            pickle.dump(RSAmodel, open('RSAmodel.pkl', 'wb'))
            pickle.dump(fDoE, open('fDoE.pkl', 'wb'))
        
        except:
            return 'Study failed in performing RSA computation' 
        
    
    
        
        return 'RSA Computation Succesful'

class CanPlotRSA():
    
    def __init__(self, Simulation):    
      
        self.Simulation = Simulation
        
                
    def run(self):
        ## Load xDoe, fDoE and RSAmodel from previous calculation
        try:
            xDoE = pickle.load(open('xDoE.pkl', 'rb')) 
        except:
            return 'Failed to load xDoE --> Run ComputeRSA first'
        ## load fDoE from original Model            
        try:
            fDoE = pickle.load(open('fDoE.pkl', 'rb'))
        except:
           return 'Failed to load fDoE --> Run ComputeRSA first'
       
        try:
            RSAmodel = pickle.load(open('RSAmodel.pkl', 'rb'))
        except:
           return 'Failed to load RSAmodel --> Run ComputeRSA first'
      
     
       
        #     Plot RSA
        #    ------------------------------------------------------------------- #
        try:
            self.Simulation.RSAplotting(RSAmodel[1],[800,1460],[0],xL,xU, xLabels=['radius','height'],RespLabel='RSA \nx0 variable    x1 = 1460', xDoE=xDoE, fDoE=fDoE.T[1],FigFileName='RSA_1D_x0.png')
            self.Simulation.RSAplotting(RSAmodel[1],[800,1460],[1],xL,xU, xLabels=['radius','height'],RespLabel='RSA \nx0 = 800    x1 variable', xDoE=xDoE, fDoE=fDoE.T[1],FigFileName='RSA_1D_x1.png')
            self.Simulation.RSAplotting(RSAmodel[1],[800,1460],[0,1],xL,xU, xLabels=['radius','height'],RespLabel='surface area', xDoE=xDoE, fDoE=fDoE.T[1],FigFileName='RSA_F.png')
            self.Simulation.RSAplotting(RSAmodel[1],[800,1460],[0,1],xL,xU, xLabels=['radius','height'],RespLabel='surface area', xDoE=xDoE, fDoE=fDoE.T[1],FigFileName='RSA_G.png')
        except:
            return 'Failed plotting RSA' 
       
        
        return 'RSA Plotting Succesful'

class CanPlotScatterRSA():
    
    def __init__(self, Simulation):    
      
        self.Simulation = Simulation
        
                
    def run(self):
        ## Load xDoe, fDoE and RSAmodel from previous calculation
        try:
            xDoE = self.Simulation.PerformDoE(NumberSamplePoints,xL3d,xU3d,SamplingMethod='LHS')
        except:
            return 'Failed to generate xDoE'
        ## load fDoE from original Model            
        try:
            fDoE = self.Simulation.GetfDoE(xDoE)
        except:
           return 'Failed to compute fDoE'
       
        try:
            RSAmodel = self.Simulation.GetRSAmodel(xDoE, fDoE = fDoE)
        except:
           return 'Failed to load RSAmodel'
      
     
       
        #     Plot RSA
        #    ------------------------------------------------------------------- #
        try:
            self.Simulation.RSAplotting(RSAmodel[0],[800,1460,900],[0,1,2],xL3d,xU3d, xLabels=['radius','height', 'strength'], RespLabel='RSA \nx0, x1 and x2 variable', xDoE=xDoE, fDoE=fDoE.T[0],FigFileName='RSA_3D.png')
        except:    
            return 'Failed plotting RSA' 
       
        
        return 'RSA Plotting Succesful'

        
class CanOptimize():
    
        def __init__(self, Simulation):    
      
            self.Simulation = Simulation
            
        def run(self, AlgChoice = 'NLPQLP'):
       
           # Definition of the test optimization problem
            # ------------------------------------------------------------------- #
            GradAlgs = ('SLSQP', 'NLPQLP')
            Options = dict()
            Options['ng'] = 1   
            Options['ConvergencePlots'] = True # default is inactive, i.e. False!
            Options['Normalization'] = True
           
            # Define test optimization
            # ------------------------------------------------------------------- #
            try:
                if AlgChoice == 'SLSQP':
                    Results = self.Simulation.Optimize(x0,xL,xU,Norm=True,Options=Options)
                elif AlgChoice == 'NLPQLP':
                    Results = self.Simulation.Optimize(x0,xL,xU,Alg='NLPQLP',Norm=True \
                        ,Options=Options,MAXFUN=30,ACC=1e-5,STPMIN=1e-6,ACCQP=1e-8)
                elif AlgChoice == 'ALHSO':
                    Results = self.Simulation.Optimize(x0,xL,xU,Alg='ALHSO',Norm=True \
                        ,Options=Options)
                elif AlgChoice == 'NSGA2':
                    Results = self.Simulation.Optimize(x0,xL,xU,Alg='NSGA2',Norm=True \
                        ,Options=Options,PopSize=80,maxGen=20)
            except:
                return [], 'Study failed in test optimization'
            
            # Output results of plain test optimization
            # ------------------------------------------------------------------- #
            for (ResultType, Value) in Results.iteritems():
                print ResultType+' is:'
                print Value
            pickle.dump(Results,open('OptResults.pkl','wb'))
    #        
            # Print optimality infos
            # ------------------------------------------------------------------- #
            if AlgChoice in GradAlgs:
                print '1st order optimality'
                print Results['LagrangianData']['1st Order Opt']
                print 'Lambdas have been computed to be:'
                for (Lam, Des) in zip(Results['LagrangianData']['Lambda Values'], Results['LagrangianData']['Lambda Description']):
                    print Des, ': ', Lam
                print 'Lambdas have been computed with following residual error:'
                print Results['LagrangianData']['Lambda Residuals']
        
            return Results, 'No Fail: Shadow prices with convergence plotting works' 


class CanVerificationRSA():
    
    def __init__(self, Modelsimulation, RSAsimulation):    
      
        self.Modelsimulation = Modelsimulation
        self.RSAsimulation = RSAsimulation
                
    def run(self):
                
        import matplotlib.pyplot as plt
        
        NumberSamplePointsVer = 100 * NumberSamplePoints # More Sample Points for Verification

        
        try:
            xDoE = self.Modelsimulation.PerformDoE(NumberSamplePointsVer,xL,xU,SamplingMethod='LHS')
        except:
            return 'Study failed in generating DoE - Design of Experiments' 
            
        AmountOfxDoE = 40 # Give certain amount of xDoE for which fDoE should be calculated - xDoEs are chosen randomly
        xDoESelected = list()
        for x in range (0, AmountOfxDoE):
            xDoESelected.append(xDoE[random.randrange(0, NumberSamplePointsVer-1)]) 
        xDoESelected = np.asarray(xDoESelected)
        
        try:
           pickle.dump(xDoESelected, open('xDoEVerif.pkl', 'wb'))
        except:
           return 'Failed to save selected xDoE for Verification'        
        
       
        ###calculate fDoEs
       
        #fDoE for Model Response
        try:            
            fDoEModel = self.Modelsimulation.GetfDoE(xDoESelected)
        except:
           return 'Failed to calculate Model fDoE for Verification'          
        
        try:
           pickle.dump(fDoEModel, open('fDoEModelVerif.pkl', 'wb'))
        except:
           return 'Failed to save Model fDoE for Verification'  
           
        #fDoE for RSA Response
        try:
               fDoERSA = self.RSAsimulation.GetfDoE(xDoESelected)
        except:
           return 'Failed to calculate RSA fDoE for Verification'          
        
        try:
           pickle.dump(fDoEModel, open('fDoERSAVerif.pkl', 'wb'))
        except:
           return 'Failed to save RSA fDoE for Verification'
                    
        for i in xrange(0,len(fDoERSA.T)):  
               
            Max_f_falue = max(max(fDoERSA.T[i]),max(fDoEModel.T[i])) 
            Min_f_value = min(min(fDoERSA.T[i]),min(fDoEModel.T[i]))
            
            f_plot = plt.figure(i)
          
            axesCalibration = (max(abs(Max_f_falue),abs(Min_f_value))*0.2)
            plt.xlim([Min_f_value-axesCalibration,Max_f_falue+axesCalibration])
            plt.ylim([Min_f_value-axesCalibration,Max_f_falue+axesCalibration])
            
            plt.title('RSA Verification')
            
            
            angleBisectorMax = (max(abs(Max_f_falue),abs(Min_f_value))*1.5)
            plt.plot([-angleBisectorMax,angleBisectorMax],[-angleBisectorMax,angleBisectorMax], color='#3C4884')
            plt.plot([-angleBisectorMax,angleBisectorMax],[-angleBisectorMax*1.05,angleBisectorMax*1.05],color='#F29F05', label='5%')
            plt.plot([-angleBisectorMax,angleBisectorMax],[-angleBisectorMax*1.10,angleBisectorMax*1.10],color='#D92525', label='10%')
            plt.plot([-angleBisectorMax,angleBisectorMax],[-angleBisectorMax*0.95,angleBisectorMax*0.95],color='#F25C05')
            plt.plot([-angleBisectorMax,angleBisectorMax],[-angleBisectorMax*0.90,angleBisectorMax*0.90],color='#D92525')
         
            plt.plot(fDoERSA.T[i],fDoEModel.T[i],'o',color='#0E3D59')      
            plt.legend(loc=0)
            
            f_plot.savefig('RSA_Verification_plot_%s' %i)



        return 'RSA Verification Succesful' 
