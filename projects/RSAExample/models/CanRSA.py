# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# Import needed modules
# --------------------------------------------------------------------------- #
import numpy as np
import sys

import pickle

# Definition of simulation model class
# --------------------------------------------------------------------------- #
class CanRSA:
    
    def __init__(self):
       
       try:
            self.RSAmodel = pickle.load(open('RSAmodel.pkl','rb'))
       except:
           sys.exit('Failed to load RSAmodel --> Run ComputeRSA first') 
    
    def ComputeResp(self,x):
        
        Resp = np.array([self.RSAmodel[0].predict(x), self.RSAmodel[1].predict(x)]) #x[0] radius x[1] height
        return Resp 

    def ObjAndConstrFct(self,x):
       
        Resp = np.asarray(self.ComputeResp(x))
        f = Resp[0] 
        g = [0.0]*1
        g[0] = Resp[1]
      
        fail = 0
        
        return f,g,fail
        
    def IterationUserOutput(self,IterData):
        if IterData['LineSearchDone']:
            # ODB
            print '    -> line search done (%i evals in total)\n Iteration: %i Evaluation: %i'%(IterData['TotalCalls'], IterData['IterationNum'],IterData['EvalNum'])
        else:
            print ' Iteration: %i Evaluation: %i'%(IterData['IterationNum'],IterData['EvalNum'])
            
class CanScatterRSA:
    
    def __init__(self):
       pass
    
    def ComputeResp(self,x):
        
        Resp = 3*x[0] + 3*x[1] + 3*x[2] #defines a linear function with 3 input variables
        return Resp 


       

        
    