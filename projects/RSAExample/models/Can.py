# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# Import needed modules
# --------------------------------------------------------------------------- #
import numpy as np
import math


# Definition of simulation model class
# --------------------------------------------------------------------------- #
class Can:
    
    def __init__(self):
       pass
    
    def ComputeResp(self,x):
       Resp = np.array([(math.pi*2*x[0]*x[1] + x[0]**2* math.pi *2.)*1e-6, \
           1 - ((math.pi * x[0]**2 *x[1])/ 3e9)]) #x[0] radius x[1] height
       return Resp 
    
       
    def ObjAndConstrFct(self,x):
                          
        Resp = np.asarray(self.ComputeResp(x))
        f = Resp[0] 
        g = [0.0]*1
        g[0] = Resp[1]
             
        fail = 0
        
        return f,g,fail
        
    def IterationUserOutput(self,IterData):
        if IterData['LineSearchDone']:
            # ODB
            print '    -> line search done (%i evals in total)\n Iteration: %i Evaluation: %i'%(IterData['TotalCalls'], IterData['IterationNum'],IterData['EvalNum'])
        else:
            print ' Iteration: %i Evaluation: %i'%(IterData['IterationNum'],IterData['EvalNum'])
       

        
    