# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ # 
# Initialize logging and working dir
import logging, os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.getcwd()))) # Add eos folder to path
import eos
from eos import SimulationMaker
eos.SetupEosWorkDir(jobname='SurrogateTests') # create Working dir in eos\results
eos.StartLogger() # start logging
logging.info('Current work dir:"%s"' % os.getcwd())
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ # 

# ['ComputeRSA', 'PlotRSA','OptimizeOnRSA', 'OptimizeOnModel', 'VerificationRSA']
AnalysisList = ['PlotScatterRSA']

(ResultList, InfoList) = (list(), list())

for Analysis in AnalysisList:  
 
    if Analysis == 'ComputeRSA':
        from models.Can import Can
        Testmodel = Can()
        Testsimulation = SimulationMaker(Testmodel, LoggingLevel="INFO")
        from studies.Can import CanComputeRSA as ComputeRSA
        studyTest = ComputeRSA(Testsimulation)
        InfoStr =  studyTest.run() 
        
    elif Analysis == 'PlotRSA':
        from models.CanRSA import CanRSA 
        RSAModel = CanRSA()
        Testsimulation = SimulationMaker(RSAModel, LoggingLevel="INFO")
        from studies.Can import CanPlotRSA as PlotRSA
        studyTest = PlotRSA(Testsimulation)
        InfoStr =  studyTest.run()

    elif Analysis == 'PlotScatterRSA':
        from models.CanRSA import CanScatterRSA 
        RSAModel = CanScatterRSA()
        Testsimulation = SimulationMaker(RSAModel, LoggingLevel="INFO")
        from studies.Can import CanPlotScatterRSA as PlotScatterRSA
        studyTest = PlotScatterRSA(Testsimulation)
        InfoStr =  studyTest.run()
        
    elif Analysis == 'OptimizeOnRSA':
        from models.CanRSA import CanRSA 
        Testmodel = CanRSA()
        Testsimulation = SimulationMaker(Testmodel, LoggingLevel="INFO")        
        from studies.Can import CanOptimize as Optimize
        studyTest = Optimize(Testsimulation)
        Results, InfoStr =  studyTest.run(AlgChoice = 'NLPQLP') # SLSQP, NLPQLP, ALHSO, NSGA2
        
    elif Analysis == 'OptimizeOnModel':
        from models.Can import Can
        Testmodel = Can()
        Testsimulation = SimulationMaker(Testmodel, LoggingLevel="INFO")        
        from studies.Can import CanOptimize as Optimize
        studyTest = Optimize(Testsimulation)
        Results, InfoStr =  studyTest.run(AlgChoice = 'NLPQLP') # SLSQP, NLPQLP, ALHSO, NSGA2
        
    elif Analysis == 'VerificationRSA':
        from models.Can import Can
        from models.CanRSA import CanRSA 
        Model = Can()
        RSAModel = CanRSA()

        Modelsimulation = SimulationMaker(Model, LoggingLevel="INFO")       
        RSAsimulation = SimulationMaker(RSAModel, LoggingLevel="INFO")  
          
        from studies.Can import CanVerificationRSA as VerificationRSA
        studyTest = VerificationRSA(Modelsimulation, RSAsimulation)
        InfoStr =  studyTest.run()
  
print InfoStr
print '----done----'
