# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from eos import EosStudy


class SimpleEval(EosStudy):
    """
    Evaluation class:

    Simply evaluate model.
    """
    def __init__(self, model):
        EosStudy.__init__(self, model)

    def run(self):

        self.Simulation.Eval()
        self.AddStudyStep(self.Simulation.Model.evalResults)
        self.WriteIterationOutput(filePath=self.Simulation.Model.AnsysObj.workfolder)
        self.WriteStudyOutput(filePath=self.Simulation.Model.AnsysObj.workfolder)

        # move the results from the temp-folder to the result folder in the eos directory
        # self.Simulation.Model.AnsysObj.FetchResults()

        return


class ParameterStudy1Par(EosStudy):
    """
    ParameterStudy class:

    Run small parameter Study
    """
    def __init__(self, model):
        EosStudy.__init__(self, model)

    def run(self):

        # define, which model parameters from the "params" dict are design Variables, in this case only 1 parameter
        self.Simulation.Model.designVariables = ['height']

        # self.Simulation.Model.keepResults = False  # we don't need to keep all ansys result files of all iterations

        numstep = 5
        lower_bound = [1]
        upper_bound = [5]

        # create the DoE, i.E. the design points for the parameter study
        design_of_experiments = self.Simulation.PerformDoE(numstep, lower_bound, upper_bound, 'FullFactorial')

        for x in design_of_experiments:
            self.Simulation.Eval(x)
            self.AddStudyStep(self.Simulation.Model.evalResults)
            self.WriteIterationOutput(filePath=self.Simulation.Model.AnsysObj.workfolder)

            # this command removes all ANSYS result files, not advisable for debugging
            self.Simulation.Model.AnsysObj.CleanupAnsysFiles()

        self.WriteStudyOutput(filePath=self.Simulation.Model.AnsysObj.workfolder)

        # move the results from the temp-folder to the result folder in the eos directory
        # self.Simulation.Model.AnsysObj.FetchResults()


class ParameterStudy2Par(EosStudy):
    """
    ParameterStudy class:

    Run small parameter Study
    """
    def __init__(self, model):
        EosStudy.__init__(self, model)

    def run(self):

        # define, which model parameters from the "params" dict are design Variables, in this case only 1 parameter
        self.Simulation.Model.designVariables = ['height', 'width']

        # self.Simulation.Model.keepResults = False  # we don't need to keep all ansys result files of all iterations

        num_steps = 10
        lower_bound = [1, 1]
        upper_bound = [5, 5]

        # create the DoE, i.E. the design points for the parameter study
        design_of_experiments = self.Simulation.PerformDoE(num_steps, lower_bound, upper_bound, 'FullFactorialManual')

        for x in design_of_experiments:
            self.Simulation.Eval(x)
            self.AddStudyStep(self.Simulation.Model.evalResults)
            self.WriteIterationOutput(filePath=self.Simulation.Model.AnsysObj.workfolder)

            # this command removes all ANSYS result files, not advisable for debugging
            self.Simulation.Model.AnsysObj.CleanupAnsysFiles()

        self.WriteStudyOutput(filePath=self.Simulation.Model.AnsysObj.workfolder)

        # move the results from the temp-folder to the result folder in the eos directory
        # self.Simulation.Model.AnsysObj.FetchResults()


class Optimize(EosStudy):
    """
    Optimization class:

    Run small parameter Study
    """
    def __init__(self, model):
        EosStudy.__init__(self, model)

    def run(self):

        # assign the design vector to params of the model, in this case 2 parameters
        self.Simulation.Model.designVariables = ['height', 'width']
        self.Simulation.Model.keepResults = False  # we don't need to keep all ansys result files of all iterations

        options = dict()
        options['ng'] = 1  # number of constraints
        options['ConvergencePlots'] = True  # default is inactive, i.e. False!

        x_l = np.array([1., 1.]).T
        x_u = np.array([10., 10.]).T
        x_0 = np.array([8., 8.]).T

        # start Optimization
        alg = 'SLSQP'
        # alg = 'CONMIN'
        resp = self.Simulation.Optimize(x_0, x_l, x_u, Alg=alg, Norm=False, Options=options)

        return resp
