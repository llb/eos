# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import sys
import shutil

# Add parent folder to Path so that the "import eos" statement works.
sys.path.append('../..')
import eos
from models.Beam import Beam

# start logging
eos.StartLogger()
logging.info('current work dir:"%s"' % os.getcwd())

# Initialize Model

ModelObj = Beam()

# select study / studies in a list
# studies = ['SimpleEval', 'ParameterStudy1Par', 'ParameterStudy2Par', 'Optimize']
# studies = ['Optimize']
studies = ['SimpleEval']
# studies = ['ParameterStudy1Par']
# studies = ['ParameterStudy2Par']

# run all selected studies
for study in studies:
    module_ = __import__('studies')
    StudyClass = getattr(module_, study)
    StudyObj = StudyClass(ModelObj)
    Responses = StudyObj.run()

# copy logfile to work folder
logfileName = logging.Logger.root.handlers[0].baseFilename
try:
    workFolder = StudyObj.Simulation.Model.AnsysObj.workfolder
    shutil.copy(logfileName, workFolder)
except:
    logging.debug('no workfolder found, not copying eos logfile.')