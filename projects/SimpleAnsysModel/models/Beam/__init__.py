# -*- coding: utf-8 -*-
# This file is part of EOS, the (E)nvironment for (O)ptimization and (S)imulation.
# Copyright 2014-2016 Luiz da Rocha-Schmidt, Markus Schatz
# Further code contributors listed in README.md
#
# EOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EOS.  If not, see <http://www.gnu.org/licenses/>.

import os
from eos.simjob import AnsysJob
from eos import EosModel
from eos import Parameter as Par


class Beam(EosModel):
    """
        Model Class for a simple Ansys beam Model
    """

    def __init__(self):
        """
            Model initialization: define parameters
        """
        # Run the init method of the parent class for Model name and other initializations
        EosModel.__init__(self)

        # define FEM simulation Object with the current folder as source folder
        self.AnsysObj = AnsysJob(os.path.dirname(os.path.realpath(__file__)), self.ModelName, 'beam.mac')

        # define all model parameters with default values. This does not have to be equal to the design vector x
        self.params = dict()
        self.params['height'] = Par(5., 'mm', 'Height of the beam cross section')
        self.params['width'] = Par(3., 'mm', 'Width of the beam cross section')
        self.params['length'] = Par(100., 'mm', 'Length of the beam')
        self.params['elementSize'] = Par(5, 'mm', 'target element size for meshing')
        self.params['density'] = Par(2.71E-9, 't/mm^3', 'Material density')
        self.params['youngsModulus'] = Par(70E3, 'MPa', 'Youngs Modulus')
        self.params['poissonsRatio'] = Par(0.3, '', 'The materials Poissons Ratio')
        self.params['tipForce'] = Par(-10, 'N', 'Force applied to the tip of the beam')

        # model setup parameters
        self.params['iteration'] = Par(0, '', ('A continuously growing value to identify'''
                                               'and seperate different runs of the same model'))
        self.keepResults = True

        # set, whether the "iteration" parameter is automatically incremented when evaluating the model
        self.autoIterationIncrement = True

    def __call__(self, x):
        """
        :param x: design vector as int, float or np.array
        """
        # increase the iteration value if not deactivated
        if self.autoIterationIncrement:
            self.params['iteration'].value += 1

        # assign design variables from the x vector
        self.assignDesignVariables(x)

        # update the job name to take into account the iteration number. Useful for parametric studies
        self.AnsysObj.jobname = self.ModelName + '_' + str(self.params['iteration'].value)

        # write the parameter dictionary to an Ansys input file in the work folder
        self.AnsysObj.AddParameterFile(self.params, 'params.mac')  # for ANSYS
        self.AnsysObj.AddParameterFile(self.params, 'params_%i.mac' % self.params['iteration'].value)  # for archiving

        # execute Ansys
        self.AnsysObj.Run()

        # read results
        self.get_all_results()

        # if the detailed results are not needed, delete the ansys files
        if not self.keepResults:
            self.AnsysObj.CleanupAnsysFiles(AdditionalFilesToDelete=['*.db'])

    def get_all_results(self):
        """ 
        call all result reading methods and write some evaluation output
        In each method, results are stored in the self.evalResults dict. 
        """

        self.get_tip_deformation()
        self.get_first_eigenfrequency()
        # add more as required

        # write the evaluation parameters and results to the log file and to a report file
        self.WriteEvaluationOutput(filePath=self.AnsysObj.workfolder)

    def get_tip_deformation(self):
        """
        read the tip deformation from a text file that was created explicitly in the ANSYS input file during
        postprocessing
        """

        # build the result file name, include the iteration number to
        # distinguish multiple runs e.g. of a parameter study
        filename = 'tip_deformation_%i.txt' % self.params['iteration'].value
        tip_deformation = self.AnsysObj.ReadTextResults(filename)
        self.evalResults['tipDeformation'] = Par(tip_deformation, 'mm', ('the z deformation of unconstrained'
                                                                         'the tip of the beam'))

    def get_first_eigenfrequency(self):
        """ read the modal results directly from the ANSYS output file """
        eigenfrequencies = self.AnsysObj.GetModalResults()

        if eigenfrequencies:
            self.evalResults['firstEigenfrequency'] = Par(eigenfrequencies[0],
                                                          'Hz',
                                                          'First Eigenfrequency of the Modal analysis',
                                                          'First Eigenfreq.')
        else:
            self.evalResults['firstEigenfrequency'] = Par(0.,
                                                          'failed',
                                                          'First Eigenfrequency of the Modal analysis',
                                                          'First Eigenfreq.')

    def ComputeResp(self, x):
        """
        calculate the response function, used for optimization.
        """
        self(x)

        mass = self.params['width'].value * self.params['height'].value * \
            self.params['length'].value * self.params['density'].value
        f = mass * 1E6  # convert from tons to grams

        return f

    def ObjAndConstrFct(self, x):
        """
        Return the function evaluation  value (__call__ from above) as well as constraint values.
        used for optimization.
        """
        self(x)

        fail = 0
        tip_deformation = self.evalResults['tipDeformation'].value
        acceptable_deformation = -3.

        mass = self.params['width'].value * self.params['height'].value * \
            self.params['length'].value * self.params['density'].value
        f = mass * 1E6  # convert from tons to grams
        g = [(tip_deformation / acceptable_deformation) - 1.]

        return f, g, fail

    def IterationUserOutput(self, IterData):
        """ Print progress during optimizations """
        if IterData['LineSearchDone']:
            # ODB
            print('    -> line search done (%i evals in total)\n Iteration: %i Evaluation: %i' % (
            IterData['TotalCalls'], IterData['IterationNum'], IterData['EvalNum']))
        else:
            print(' Iteration: %i Evaluation: %i' % (IterData['IterationNum'], IterData['EvalNum']))
